package client;

import constants.GameConstants;
import handling.Buffstat;
import server.Randomizer;
import tools.Pair;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public enum MapleBuffStat implements Serializable, Buffstat {
    IndiePad(0),
    IndieMad(1),
    IndiePdd(
            2),
    IndieHp(3),
    IndieHpR(4),
    IndieMp(5),
    IndieMpR(6),
    IndieAcc(7),
    IndieEva(8),
    IndieJump(9),
    IndieSpeed(10),
    IndieAllStat(11),
    IndieAllStatR(12),
    IndieDodgeCriticalTime(13),
    IndieExp(14),
    IndieBooster(15),
    IndieFixedDamageR(16),
    PyramidStunBuff(17),
    PyramidFrozenBuff(18),
    PyramidFireBuff(19),
    PyramidBonusDamageBuff(20),
    IndieRelaxEXP(21),
    IndieStr(22),
    IndieDex(23),
    IndieInt(24),
    IndieLuk(25),
    IndieDamR(26),
    IndieScriptBuff(27),
    IndieMaxDamageR(28),// NOT
    IndieAsrR(29),
    IndieTerR(30),
    IndieCr(31),
    IndiePddR(32),
    IndieCD(33),
    IndieBDR(34),
    IndieStatR(35),
    IndieStance(36),
    IndieIgnoreMobPdpR(37),
    IndieEmpty(38),
    IndiePadR(39),
    IndieMadR(40),
    IndieEvaR(41),
    IndieDrainHP(42),
    IndiePmdR(43),
    IndieForceJump(44),
    IndieForceSpeed(45),
    IndieDamageReduce(46),
    IndieSummon(47),
    IndieReduceCooltime(48),// NOT
    IndieNotDamaged(49),
    IndieJointAttack(50),
    IndieKeyDownMoving(51),
    IndieUnkIllium(52),// NOT
    IndieEvasion(53),
    IndieShotDamage(54),
    IndieSuperStance(55),
    IndieGrandCross(56),
    IndieBarrierDischarge(57),// NOT
    Indie_STAT_COUNT(72),
    Indie_DamageReflection(59),// NOT
    Unk60(60),// NOT
    Unk61(61),// NOT
    IndieFloating(62),// NOT
    IndieWickening(63),// NOT
    IndieWickening1(64),// NOT
    IndieWickening2(65),// NOT
    IndieBlockSkill(67),
    IndieDarknessAura(67),// NOT
    Pad(73),
    Pdd(74),
    Mad(75),
    Acc(76),
    Eva(77),
    Craft(78),
    Speed(79),
    Jump(80),
    MagicGaurd(81),// NOT
    DarkSight(82),
    Booster(83),
    PowerGaurd(84),// NOT
    MaxHP(85),
    MaxMP(86),
    Invincible(87),
    SoulArrow(88),
    Stun(89),
    Poison(90),
    Seal(91),
    Darkness(92),
    ComboCounter(93),
    WeaponCharge(97),
    BlessedHammer(95),
    BlessedHammer2(96),
    SnowCharge(94),
    HolySymbol(98),
    MesoUp(99),
    ShadowPartner(100),
    PickPocket(101),
    MesoGuard(102),
    THAW(103),// NOT
    Weakness(104),
    Curse(105),
    Slow(106),
    Morph(107),
    Recovery(108),
    BasicStatUp(109),
    Stance(110),
    SharpEyes(111),
    ManaReflection(112),
    Attract(113),
    NoBulletConsume(114),
    Infinity(115),
    AdvancedBless(116),
    ILLUSION(117),// NOT
    Blind(118),
    Concentration(119),
    BanMap(120),
    MaxLevelBuff(121),
    MesoUpByItem(122),
    WealthOfUnion(123),
    RuneOfGreed(124),
    Ghost(125),
    Barrier(126),
    ReverseInput(127),
    ItemUpByItem(128),
    RespectPImmune(129),
    RespectMImmune(130),
    DefenseAtt(131),
    DefenseState(132),
    DojangBerserk(133),
    DojangInvincible(134),
    DojangShield(135),
    SoulMasterFinal(136),
    WindBreakerFinal(137),
    ElementalReset(138),
    HideAttack(139),
    EVENT_RATE(140),// NOT
    AranCombo(141),
    AuraRecovery(142),
    Unk136(143),// NOT
    BodyPressure(144),
    RepeatEffect(145),
    ExpBuffRate(146),
    StopPortion(147),
    StopMotion(148),
    Fear(149),
    HiddenPieceOn(150),
    MagicShield(151),
    ICE_SKILL(152),// NOT
    SoulStone(153),
    Flying(154),
    Frozen(155),
    AssistCharge(156),
    Enrage(157),
    DrawBack(158),
    NotDamaged(159),
    FinalCut(160),
    HowlingParty(161),
    BeastFormDamage(162),
    Dance(163),
    EnhancedMaxHp(164),
    EnhancedMaxMp(165),
    EnhancedPad(166),
    EnhancedMad(167),
    EnhancedPdd(168),
    PerfectArmor(169),
    UnkBuffStat2(170),
    IncreaseJabelinDam(167),// NOT
    HowlingCritical(168),// NOT
    HowlingMaxMp(169),// NOT
    HowlingDefence(170),// NOT
    HowlingEvasion(171),// NOT
    PinkbeanMinibeenMove(172),// NOT
    Sneak(173),
    Mechanic(174),
    BeastFormMaxHP(175),
    DiceRoll(176),
    BlessingArmor(177),
    DamR(178),
    TeleportMastery(179),
    CombatOrders(180),
    Beholder(181),
    DispelItemOption(182),
    Inflation(183),
    OnixDivineProtection(184),
    Web(185),
    Bless(186),
    TimeBomb(187),
    DisOrder(188),
    Thread(189),
    Team(190),
    Explosion(191),
    BUFFLIMIT(192),// NOT
    STR(193),
    INT(194),
    DEX(195),
    LUK(196),
    DISPEL_BY_FIELD(197),// NOT
    DarkTornado(198),
    PVPDamage(199),
    PVP_SCORE_BONUS(200),// NOT
    PVP_INVINCIBLE(201),// NOT
    PvPRaceEffect(202),
    WeaknessMdamage(203),
    Frozen2(204),
    PVP_DAMAGE_SKILL(205),// NOT
    AmplifyDamage(206),
    Shock(207),
    InfinityForce(208),
    IncMaxHP(209),
    IncMaxMP(210),
    HolyMagicShell(211),
    KeyDownTimeIgnore(212),
    ArcaneAim(213),
    MasterMagicOn(214),
    Asr(215),
    Ter(216),
    DamAbsorbShield(217),
    DevilishPower(218),
    Roulette(219),
    SpiritLink(220),
    AsrRByItem(221),// NOT
    Event(222),
    CriticalIncrease(223),
    DropItemRate(224),
    DropRate(225),
    ItemInvincible(226),
    Awake(227),
    ItemCritical(228),
    ItemEvade(229),
    Event2(230),
    DrainHp(231),
    IncDefenseR(232),
    IncTerR(233),
    IncAsrR(234),
    DeathMark(235),
    Infiltrate(236),
    Lapidification(237),
    VenomSnake(238),
    CarnivalAttack(239),
    CarnivalDefence(240),
    CarnivalExp(241),
    SlowAttack(242),
    PyramidEffect(243),
    KillingPoint(244),
    Unk248(245),// NOT
    KeyDownMoving(246),
    IgnoreTargetDEF(247),
    ReviveOnce(248),
    Invisible(249),
    EnrageCr(250),
    EnrageCrDamMin(251),
    Judgement(252),
    DojangLuckyBonus(253),
    PainMark(254),
    Magnet(255),
    MagnetArea(256),
    GuidedArrow(257),
    UnkBuffStat4(258),// NOT
    BlessMark(259),
    BonusAttack(260),
    UnkBuffStat5(261),
    FlowOfFight(262),
    GrandCrossSize(263),
    LuckOfUnion(266),
    VampDeath(267),
    BlessingArmorIncPad(268),
    KeyDownAreaMoving(269),
    Larkness(270),
    StackBuff(271),
    BlessOfDarkness(272),
    AntiMagicShell(273),
    LifeTidal(274),
    HitCriDamR(275),
    SmashStack(276),
    RoburstArmor(277),
    ReshuffleSwitch(278),
    SpecialAction(279),
    VampDeathSummon(280),
    StopForceAtominfo(281),
    SoulGazeCriDamR(282),
    Affinity(283),// NOT
    PowerTransferGauge(284),
    AffinitySlug(285),
    Trinity(286),
    INCMAXDAMAGE(287),// NOT
    BossShield(288),
    MobZoneState(289),
    GiveMeHeal(290),
    TouchMe(291),
    Contagion(292),
    ComboUnlimited(293),
    SoulExalt(294),
    IgnorePCounter(295),
    IgnoreAllCounter(296),
    IgnorePImmune(297),
    IgnoreAllImmune(298),
    UnkBuffStat6(299),
    FireAura(300),
    VengeanceOfAngel(301),
    HeavensDoor(302),
    Preparation(303),
    BullsEye(304),
    IncEffectHPPotion(305),
    IncEffectMPPotion(306),
    BleedingToxin(307),
    IgnoreMobDamR(308),
    Asura(309),
    MegaSmasher(310),
    FlipTheCoin(311),
    UnityOfPower(312),
    Stimulate(313),
    ReturnTeleport(314),
    DropRIncrease(315),
    IgnoreMobPdpR(316),
    BdR(317),
    CapDebuff(318),
    Exceed(319),
    DiabloicRecovery(320),
    FinalAttackProp(321),
    Unk319(322),// NOT
    OverloadCount(323),
    Buckshot(324),
    FireBomb(325),
    HalfstatByDebuff(326),
    SurplusSupply(327),
    SetBaseDamage(328),
    EvaR(329),
    NewFlying(330),
    AmaranthGenerator(331),
    OnCapsule(332),
    CygnusElementSkill(333),
    StrikerHyperElectric(334),
    EventPointAbsorb(335),
    EventAssemble(336),
    StormBringer(337),
    AddAvoid(338),// NOT
    AddAcc(339),// NOT
    Albatross(340),
    Translucence(341),
    PoseType(342),
    LightOfSpirit(343),
    ElementSoul(344),
    GlimmeringTime(345),
    SolunaTime(346),
    WindWalk(347),
    SoulMP(348),
    FullSoulMP(349),
    SoulSkillDamageUp(350),
    ElementalCharge(351),
    Listonation(352),
    CrossOverChain(353),
    ChargeBuff(354),
    Reincarnation(355),
    ChillingStep(357),
    DotBasedBuff(358),
    BlessingAnsanble(359),
    ComboCostInc(360),
    ExtremeArchery(361),
    NaviFlying(362),
    QuiverCatridge(363),
    AdvancedQuiver(364),
    UserControlMob(365),
    ImmuneBarrier(366),
    ArmorPiercing(367),
    CriticalGrowing(368),
    CardinalMark(369),
    QuickDraw(370),
    BowMasterConcentration(371),
    TimeFastABuff(372),
    TimeFastBBuff(373),
    GatherDropR(374),
    AimBox2D(375),
    TrueSniping(376),
    DebuffTolerance(377),
    Unk376(378),// NOT
    DotHealHPPerSecond(379),
    DotHealMPPerSecond(380),
    SpiritGuard(381),
    PreReviveOnce(382),
    SetBaseDamageByBuff(383),
    LimitMP(384),
    ReflectDamR(385),
    ComboTempest(386),
    MHPCutR(387),
    MMPCutR(388),
    SelfWeakness(389),
    ElementDarkness(390),
    FlareTrick(391),
    Ember(392),
    Dominion(393),
    SiphonVitality(394),
    DarknessAscension(395),
    BossWaitingLinesBuff(396),
    DamageReduce(397),
    ShadowServant(398),
    ShadowIllusion(399),
    KnockBack(400),
    IgnisRore(401),
    ComplusionSlant(402),
    JaguarSummoned(403),
    JaguarCount(404),
    SSFShootingAttack(405),
    DEVIL_CRY(406),// NOT
    ShieldAttack(407),
    DarkLighting(408),
    AttackCountX(409),
    BMageDeath(410),
    BombTime(411),
    NoDebuff(412),
    BattlePvP_Mike_Shield(413),
    BattlePvP_Mike_Bugle(414),
    AegisSystem(415),
    SoulSeekerExpert(416),
    HiddenPossession(417),
    ShadowBatt(418),
    MarkofNightLord(419),
    WizardIgnite(420),
    FireBarrier(421),
    CHANGE_FOXMAN(422),// NOT
    HolyUnity(423),
    DemonFrenzy(424),
    ShadowSpear(425),
    UnkBuffStat9(426),// NOT
    Ellision(427),
    QuiverFullBurst(428),
    LuminousPerfusion(429),
    WildGrenadier(430),
    GrandCross(431),
    Unk432(432),// NOT
    BattlePvP_Helena_Mark(433),
    BattlePvP_Helena_WindSpirit(434),
    BattlePvP_LangE_Protection(435),
    BattlePvP_LeeMalNyun_ScaleUp(436),
    BattlePvP_Revive(437),
    PinkbeanAttackBuff(438),
    PinkbeanRelax(439),
    PinkbeanRollingGrade(440),
    PinkbeanYoYoStack(441),
    UnkBuffStat10(442),
    RandAreaAttack(443),
    NextAttackEnhance(444),
    BeyondNextAttackProb(445),
    NautilusFinalAttack(446),
    ViperTimeLeap(447),
    RoyalGuardState(448),
    RoyalGuardPrepare(449),
    MichaelSoulLink(450),
    Unk451(451),// NOT
    TryflingWarm(452),
    AddRange(453),
    KinesisPsychicPoint(454),
    KinesisPsychicOver(455),
    KinesisPsychicShield(456),
    KinesisIncMastery(457),
    KinesisPsychicEnergeShield(458),// NOT
    BladeStance(459),
    DebuffActiveHp(460),
    DebuffIncHp(461),
    MortalBlow(462),
    SoulResonance(463),
    Fever(464),
    SikSin(465),
    TeleportMasteryRange(466),
    FixCooltime(467),
    IncMobRateDummy(468),
    AdrenalinBoost(469),
    AranSmashSwing(470),
    AranDrain(471),
    AranBoostEndHunt(472),
    HiddenHyperLinkMaximization(473),
    RWCylinder(474),
    RWCombination(475),
    UnkBuffStat12(476),// NOT
    RwMagnumBlow(477),
    RwBarrier(478),
    RWBarrierHeal(479),
    RWMaximizeCannon(480),
    RWOverHeat(481),
    UsingScouter(483),
    RWMovingEvar(482),
    Stigma(484),
    InstallMaha(485),
    CooldownHeavensDoor(486),
    CooldownRune(488),
    PinPointRocket(489),
    Transform(490),
    EnergyBurst(491),
    Striker1st(492),
    BulletParty(493),
    LoadedDice(494),// NOT
    Pray(495),
    ChainArtsFury(496),
    DamageDecreaseWithHP(497),
    Unk498(498),// NOT
    AuraWeapon(499),
    OverloadMana(500),
    RhoAias(501),
    PsychicTornado(502),
    SpreadThrow(503),
    HowlingGale(504),
    VMatrixStackBuff(505),
    ShadowAssult(506),
    MultipleOption(507),
    Unk508(508),// NOT
    BlitzShield(509),
    SplitArrow(510),
    FreudsProtection(511),
    Overload(512),
    Spotlight(513),
    UnkBuffStat16(514),
    WeaponVariety(515),
    GloryWing(516),
    ShadowerDebuff(517),
    OverDrive(518),
    Etherealform(519),
    ReadyToDie(520),
    CriticalReinForce(521),
    CurseOfCreation(522),
    CurseOfDestruction(523),
    BlackMageDebuff(524),// NOT
    BodyOfSteal(525),
    UnkBuffStat18(526),
    UnkBuffStat19(527),
    HarmonyLink(528),
    FastCharge(529),
    UnkBuffStat20(530),
    CrystalBattery(531),
    Deus(532),
    CrystalChargeMax(533),// NOT
    UnkBuffStat22(534),// NOT
    Unk536(535),// NOT
    Unk537(536),// NOT
    Unk538(537),// NOT
    SpectorGauge(538),
    SpectorTransForm(539),
    PlainBuff(540),
    ScarletBuff(541),
    GustBuff(542),
    AbyssBuff(543),
    ComingDeath(544),
    FightJazz(545),
    ChargeSpellAmplification(546),
    InfinitySpell(547),
    MagicCircuitFullDrive(548),
    LinkOfArk(549),
    MemoryOfSource(550),
    UnkBuffStat26(551),// NOT
    WillPoison(552),
    Unk556(553),// NOT
    UnkBuffStat28(554),
    CooltimeHolyMagicShell(555),
    Striker3rd(556),
    ComboInstict(557),// NOT
    WindWall(558),
    UnkBuffStat29(559),// NOT
    SwordOfSoulLight(560),
    MarkOfPhantomStack(561),
    MarkOfPhantomDebuff(562),
    Unk565(563),// NOT
    Unk566(564),// NOT
    Unk567(565),// NOT
    Unk568(566),// NOT
    Unk569(567),// NOT
    EventSpecialSkill(568),
    PmdReduce(569),
    ForbidOpPotion(570),
    ForbidEquipChange(571),
    YalBuff(572),
    IonBuff(573),
    Unk576(572),// NOT
    UnkBuffStat36(573),// NOT
    Unk578(574),// NOT
    Protective(577),
    UnkBuffStat38(578),// NOT
    AncientGuidance(579),
    UnkBuffStat39(580),// NOT
    UnkBuffStat40(581),// NOT
    UnkBuffStat41(582),// NOT
    UnkBuffStat42(583),// NOT
    UnkBuffStat43(584),
    UnkBuffStat44(585),
    Bless5th(586),
    UnkBuffStat45(587),// NOT
    UnkBuffStat46(588),
    UnkBuffStat47(589),
    UnkBuffStat48(590),
    UnkBuffStat49(591),
    UnkBuffStat50(592),
    PapyrusOfLuck(593),// NOT
    HoyoungThirdProperty(596),
    TidalForce(597),
    Alterego(598),
    AltergoReinforce(599),
    ButterflyDream(600),
    Sungi(601),
    WrathOfGods(602),// NOT
    EmpiricalKnowledge(603),
    UnkBuffStat52(604),
    UnkBuffStat53(605),// NOT
    Graffiti(606),
    DreamDowon(607),
    AdelGauge(609),// NOT
    Creation(610),
    Dike(611),
    Wonder(612),
    Restore(613),
    Novility(614),// NOT
    AdelResonance(615),// NOT
    RuneOfPure(616),
    UnkBuffStat56(617),// NOT
    DuskDarkness(618),
    YellowAura(619),
    DrainAura(620),
    BlueAura(621),
    DarkAura(622),
    DebuffAura(623),
    UnionAura(624),
    IceAura(625),
    KnightsAura(626),
    ZeroAuraStr(627),
    ZeroAuraSpd(628),
    AdventOfGods(630),
    Revenant(631),
    RevenantDamage(632),
    PhotonRay(635),
    AbyssalLightning(636),
    RoyalKnights(638),
    RepeatinCartrige(641),// NOT
    ThrowBlasting(643),
    SageElementalClone(644),
    DarknessAura(645),
    WeaponVarietyFinale(646),
    LiberationOrb(647),
    LiberationOrbActive(648),
    EgoWeapon(649),
    RelicUnbound(650),// NOT
    UnkBuffStat58(651),// NOT
    unk647(652),// NOT
    Malice(653),
    Possession(654),
    DeathBlessing(655),
    ThanatosDescent(656),
    RemainInsence(657),// NOT
    GripOfAgony(658),
    DragonPang(659),
    SerenDebuff(661),
    SerenDebuffUnk(662),
    KainLink(663),// NOT
    ReadVeinOfInfinity(677),
    GuardOfMountain(679),
    AbsorptionRiver(680),
    AbsorptionWind(681),
    AbsorptionSun(682),
    
    EnergyCharged(688),
    DashJump(689),
    DashSpeed(690),
    RideVehicle(691),
    PartyBooster(692),
    GuidedBullet(693),
    Undead(694),
    RideVehicleExpire(695),
    RelikGauge(696),// NOT
    Grave(697),
    CountPlus1(992),;

    private static final long serialVersionUID = 0L;
    private int buffstat;
    private int first;
    private boolean stacked = false;
    private int disease;
    private int flag;
    // [8] [7] [6] [5] [4] [3] [2] [1]
    // [0] [1] [2] [3] [4] [5] [6] [7]

    private MapleBuffStat(int flag) {
        this.buffstat = (1 << (31 - (flag % 32)));
        this.setFirst(GameConstants.MAX_BUFFSTAT - (byte) Math.floor(flag / 32));
        this.setStacked(name().startsWith("Indie") || name().startsWith("Pyramid"));
        this.setFlag(flag);
    }

    private MapleBuffStat(int flag, int disease) {
        this.buffstat = (1 << (31 - (flag % 32)));
        this.setFirst(GameConstants.MAX_BUFFSTAT - (byte) Math.floor(flag / 32));
        this.setStacked(name().startsWith("Indie") || name().startsWith("Pyramid"));
        this.setFlag(flag);
        this.disease = disease;
    }

    private MapleBuffStat(int buffstat, int first, boolean stacked) {
        this.buffstat = buffstat;
        this.setFirst(first);
        this.setStacked(stacked);
    }

    private MapleBuffStat(int buffstat, int first, int disease) {
        this.buffstat = buffstat;
        this.setFirst(first);
        this.disease = disease;
    }

    public final int getPosition() {
        return getFirst();//getPosition(stacked);
    }

    public final int getPosition(boolean stacked) {
        if (!stacked) {
            return getFirst();
        }
        switch (getFirst()) {
            case 16:
                return 0;
            case 15:
                return 1;
            case 14:
                return 2;
            case 13:
                return 3;
            case 12:
                return 4;
            case 11:
                return 5;
            case 10:
                return 6;
            case 9:
                return 7;
            case 8:
                return 8;
            case 7:
                return 9;
            case 6:
                return 10;
            case 5:
                return 11;
            case 4:
                return 12;
            case 3:
                return 13;
            case 2:
                return 14;
            case 1:
                return 15;
            case 0:
                return 16;
        }
        return 0; // none
    }

    public final int getValue() {
        return getBuffstat();
    }

    public final boolean canStack() {
        return isStacked();
    }

    public int getDisease() {
        return disease;
    }

    public static final MapleBuffStat getByFlag(final int flag) {
        for (MapleBuffStat d : MapleBuffStat.values()) {
            if (d.getFlag() == flag) {
                return d;
            }
        }
        return null;
    }

    public static final MapleBuffStat getBySkill(final int skill) {
        for (MapleBuffStat d : MapleBuffStat.values()) {
            if (d.getDisease() == skill) {
                return d;
            }
        }
        return null;
    }

    public static final List<MapleBuffStat> getUnkBuffStats() {
        List<MapleBuffStat> stats = new ArrayList<>();
        for (MapleBuffStat d : MapleBuffStat.values()) {
            if (d.name().startsWith("UnkBuff")) {
                stats.add(d);
            }
        }
        return stats;
    }

    public static final MapleBuffStat getRandom() {
        while (true) {
            for (MapleBuffStat dis : MapleBuffStat.values()) {
                if (Randomizer.nextInt(MapleBuffStat.values().length) == 0) {
                    return dis;
                }
            }
        }
    }

    public static boolean isEncode4Byte(Map<MapleBuffStat, Pair<Integer, Integer>> statups) {
        MapleBuffStat[] stats
                = {
                    CarnivalDefence,
                    SpiritLink,
                    DojangLuckyBonus,
                    SoulGazeCriDamR,
                    PowerTransferGauge,
                    ReturnTeleport,
                    ShadowPartner,
                    SetBaseDamage,
                    QuiverCatridge,
                    ImmuneBarrier,
                    NaviFlying,
                    Dance,
                    AranSmashSwing,
                    DotHealHPPerSecond,
                    SetBaseDamageByBuff,
                    MagnetArea,
                    MegaSmasher,
                    RwBarrier,
                    VampDeath,
                    RideVehicle,
                    RideVehicleExpire,
                    Protective,
                    BlitzShield,};
        for (MapleBuffStat stat : stats) {
            if (statups.containsKey(stat)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSpecialBuff() {
        switch (this) {
            case EnergyCharged:
            case DashSpeed:
            case DashJump:
            case RideVehicle:
            case PartyBooster:
            case GuidedBullet:
            case Undead:
            case RideVehicleExpire:
            case RelikGauge:
            case Grave:
                return true;
            default:
                return false;
        }
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public boolean isItemEffect() {
        switch (this) {
            case DropItemRate:
            case ItemUpByItem:
            case MesoUpByItem:
            case ExpBuffRate:
            case WealthOfUnion:
            case LuckOfUnion:
                return true;
            default:
                return false;
        }
    }

    public boolean SpectorEffect() {
        switch (this) {
            case SpectorGauge:
            case SpectorTransForm:
            case PlainBuff:
            case ScarletBuff:
            case GustBuff:
            case AbyssBuff:
                return true;
            default:
                return false;
        }
    }

    public int getBuffstat() {
        return buffstat;
    }

    public void setBuffstat(int buffstat) {
        this.buffstat = buffstat;
    }

    public int getFirst() {
        return first;
    }

    public void setFirst(int first) {
        this.first = first;
    }

    public boolean isStacked() {
        return stacked;
    }

    public void setStacked(boolean stacked) {
        this.stacked = stacked;
    }
}
