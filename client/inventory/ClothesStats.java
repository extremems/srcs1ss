/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.inventory;

/**
 *
 * @author KoreaDev <koreadev2@nate.com>
 */
public enum ClothesStats {
    EYE_ACC(0x1, 3),
    CAP(0x2, 1),
    FORE_HEAD(0x4, 2),
    EAR_RING(0x8, 4),
    CAPE(0x10, 9),
    CLOTHES(0x20, 5),
    GLOVES(0x40, 8),
    WEAPON(0x80, 11),
    PANTS(0x100, 6),
    SHOES(0x200, 7),
    RING_1(0x400, 12),
    RING_2(0x800, 13),;
    private final int value, order;

    private ClothesStats(int value, int order) {
        this.value = value;
        this.order = order;
    }

    public static int getValueByOrder(int order) {
        for (ClothesStats cs : ClothesStats.values()) {
            if (cs.order == order) {
                return cs.value;
            }
        }
        return 0;
    }

}
