package client.messages.commands;

import client.MapleBuffStat;
import client.skills.SkillFactory;
import client.*;
import client.inventory.Equip;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import constants.GameConstants;
import constants.ServerConstants;
import constants.ServerConstants.PlayerGMRank;
import handling.auction.handler.AuctionHandler;
import handling.channel.ChannelServer;
import handling.login.LoginServer;
import provider.MapleData;
import provider.MapleDataProviderFactory;
import provider.MapleDataTool;
import scripting.NPCScriptManager;
import server.MapleInventoryManipulator;
import server.life.MapleMonster;
import server.maps.MapleMap;
import server.maps.MapleMapObject;
import server.maps.MapleMapObjectType;
import tools.Pair;
import tools.StringUtil;
import tools.packet.CField;
import tools.packet.CWvsContext;
import tools.packet.CWvsContext.BuffPacket;
import tools.packet.CWvsContext.InfoPacket;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import server.games.BattleGroundGameHandler;
import server.games.BloomingRace;

public class PlayerCommand {

    public static PlayerGMRank getPlayerLevelRequired() {
        return PlayerGMRank.NORMAL;
    }

    public static class 힘 extends DistributeStatCommands {

        public 힘() {
            stat = MapleStat.STR;
        }
    }

    public static class 덱스 extends DistributeStatCommands {

        public 덱스() {
            stat = MapleStat.DEX;
        }
    }

    public static class 인트 extends DistributeStatCommands {

        public 인트() {
            stat = MapleStat.INT;
        }
    }

    public static class 럭 extends DistributeStatCommands {

        public 럭() {
            stat = MapleStat.LUK;
        }
    }

    public static class 초기화 extends DistributeStatCommands {

        public 초기화() {
            stat = MapleStat.AVAILABLEAP;
        }
    }

    public abstract static class DistributeStatCommands extends CommandExecute {

        protected MapleStat stat = null;
        private static int statLim = Short.MAX_VALUE;

        private void setStat(MapleCharacter player, int amount) {
            switch (stat) {
                case STR:
                    player.getStat().setStr((short) amount, player);
                    player.updateSingleStat(MapleStat.STR, player.getStat().getStr());
                    break;
                case DEX:
                    player.getStat().setDex((short) amount, player);
                    player.updateSingleStat(MapleStat.DEX, player.getStat().getDex());
                    break;
                case INT:
                    player.getStat().setInt((short) amount, player);
                    player.updateSingleStat(MapleStat.INT, player.getStat().getInt());
                    break;
                case LUK:
                    player.getStat().setLuk((short) amount, player);
                    player.updateSingleStat(MapleStat.LUK, player.getStat().getLuk());
                    break;
                case AVAILABLEAP:
                    player.setRemainingAp((short) 0);
                    player.updateSingleStat(MapleStat.AVAILABLEAP, player.getRemainingAp());
                    break;
            }
        }

        private int getStat(MapleCharacter player) {
            switch (stat) {
                case STR:
                    return player.getStat().getStr();
                case DEX:
                    return player.getStat().getDex();
                case INT:
                    return player.getStat().getInt();
                case LUK:
                    return player.getStat().getLuk();
                default:
                    throw new RuntimeException(); //Will never happen.
            }
        }

        @Override
        public int execute(MapleClient c, String[] splitted) {
            if (splitted.length < 2) {
                c.getPlayer().dropMessage(5, "잘못된 정보입니다.");
                return 0;
            }
            int change = 0;
            try {
                change = Integer.parseInt(splitted[1]);
            } catch (NumberFormatException nfe) {
                c.getPlayer().dropMessage(5, "제대로 입력되지 못했습니다.");
                return 0;
            }
            if (change <= 0) {
                c.getPlayer().dropMessage(5, "0보다 큰 숫자를 입력해야합니다.");
                return 0;
            }
            if (c.getPlayer().getRemainingAp() < change) {
                c.getPlayer().dropMessage(5, "AP포인트보다 작은 숫자를 입력해야합니다.");
                return 0;
            }
            if (getStat(c.getPlayer()) + change > statLim) {
                c.getPlayer().dropMessage(5, statLim + " 이상 스탯에 ap를 투자하실 수 없습니다.");
                return 0;
            }
            setStat(c.getPlayer(), getStat(c.getPlayer()) + change);
            c.getPlayer().setRemainingAp((short) (c.getPlayer().getRemainingAp() - change));
            c.getPlayer().updateSingleStat(MapleStat.AVAILABLEAP, c.getPlayer().getRemainingAp());
            c.getPlayer().dropMessage(5, StringUtil.makeEnumHumanReadable(stat.name()) + " 스탯이 " + change + " 만큼 증가하였습니다.");
            return 1;
        }
    }

    public static class 몬스터 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            MapleMonster mob = null;
            for (final MapleMapObject monstermo : c.getPlayer().getMap().getMapObjectsInRange(c.getPlayer().getPosition(), 100000, Arrays.asList(MapleMapObjectType.MONSTER))) {
                mob = (MapleMonster) monstermo;
                if (mob.isAlive()) {
                    c.getPlayer().dropMessage(6, "몬스터 정보 :  " + mob.toString());
                    break; //only one
                }
            }
            if (mob == null) {
                c.getPlayer().dropMessage(6, "주변에 몬스터가 없습니다.");
            }
            return 1;
        }
    }

    public static class 명성치알림 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            if (c.getPlayer().getKeyValue(5, "show_honor") > 0) {
                c.getPlayer().setKeyValue(5, "show_honor", "0");
            } else {
                c.getPlayer().setKeyValue(5, "show_honor", "1");
            }
            return 1;
        }
    }

    public abstract static class OpenNPCCommand extends CommandExecute {

        protected int npc = -1;
        private static int[] npcs = { //Ish yur job to make sure these are in order and correct ;(
            9000162,
            9000000,
            9010000};

        @Override
        public int execute(MapleClient c, String[] splitted) {
            NPCScriptManager.getInstance().start(c, npcs[npc]);
            return 1;
        }
    }

    public static class 동접 extends CommandExecute {

        @Override
        public int execute(MapleClient c, String[] splitted) {
            c.getPlayer().dropMessage(-8, "접속유저 목록입니다.");
            int ret = 0;
            for (ChannelServer csrv : ChannelServer.getAllInstances()) {
                int a = csrv.getPlayerStorage().getAllCharacters().size();
                ret += a;
                c.getPlayer().dropMessage(6, csrv.getChannel() + "채널: " + a + "명\r\n");

            }
            c.getPlayer().dropMessage(-8, "총 유저 접속 수 : " + ret);
            return 1;
        }
    }

    public static class 워프 extends CommandExecute {

        @Override
        public int execute(MapleClient c, String[] splitted) {
            c.removeClickedNPC();
            NPCScriptManager.getInstance().dispose(c);
            NPCScriptManager.getInstance().start(c, 9201305);
            return 1;
        }
    }

    public static class 코디 extends CommandExecute {

        @Override
        public int execute(MapleClient c, String[] splitted) {
            c.removeClickedNPC();
            NPCScriptManager.getInstance().dispose(c);
            NPCScriptManager.getInstance().start(c, 3003273);
            return 1;
        }
    }

    public static class 엔피시 extends OpenNPCCommand {

        public 엔피시() {
            npc = 0;
        }
    }

    public static class 이벤트엔피시 extends OpenNPCCommand {

        public 이벤트엔피시() {
            npc = 1;
        }
    }

    public static class 드롭체크 extends OpenNPCCommand {

        public 드롭체크() {
            npc = 2;
        }
    }

    public static class 렉 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            c.removeClickedNPC();
            NPCScriptManager.getInstance().dispose(c);
            c.getSession().writeAndFlush(CWvsContext.enableActions(c.getPlayer()));
            c.getPlayer().dropMessage(5, "렉이 해제되었습니다.");
            return 1;
        }
    }
    
    public static class 싸전귀직업 extends CommandExecute {
        public int execute(MapleClient c, String[] splitted) {
            c.getPlayer().setBattleGrondJobName("" + splitted[1]);
            c.getPlayer().dropMessage(5, "직업 : " + splitted[1] + "로 지정 완료.");
            return 1;
        }
    }
    
    public static class 운지 extends CommandExecute {
        public int execute(MapleClient c, String[] splitted) {
            if (c.getPlayer().getMapId() == 921174100) {
                c.getPlayer().warp(993026900);
            } else {
                BattleGroundGameHandler.Ready();
            }
            return 1;
        }
    }

    public static class 보조무기해제 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            Equip equip = null;
            equip = (Equip) c.getPlayer().getInventory(MapleInventoryType.EQUIPPED).getItem((short) -10);
            if (equip == null) {
                c.getPlayer().dropMessage(1, "장착중인 보조무기가 존재하지 않습니다.");
                c.getSession().writeAndFlush(CWvsContext.enableActions(c.getPlayer()));
                return 1;
            }
            if (GameConstants.isZero(c.getPlayer().getJob())) {
                c.getPlayer().dropMessage(1, "제로는 보조무기를 해제하실 수 없습니다.");
                c.getSession().writeAndFlush(CWvsContext.enableActions(c.getPlayer()));
                return 1;
            }
            c.getPlayer().getInventory(MapleInventoryType.EQUIPPED).removeSlot((short) -10);
            c.getPlayer().equipChanged();
            MapleInventoryManipulator.addbyItem(c, equip, false);
            c.getPlayer().getStat().recalcLocalStats(c.getPlayer());
            c.getSession().writeAndFlush(CField.getCharInfo(c.getPlayer()));
            MapleMap currentMap = c.getPlayer().getMap();
            currentMap.removePlayer(c.getPlayer());
            currentMap.addPlayer(c.getPlayer());
            return 1;
        }
    }

    public static class 보조무기장착 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            int itemid = 0;
            switch (c.getPlayer().getJob()) {
                case 5100:
                    itemid = 1098000;
                    break;
                case 3100:
                case 3101:
                    itemid = 1099000;
                    break;
                case 6100:
                    itemid = 1352500;
                    break;
                case 6500:
                    itemid = 1352600;
                    break;
            }

            if (itemid != 0) {
                Item item = MapleInventoryManipulator.addId_Item(c, itemid, (short) 1, "", null, -1, "", false);

                if (item != null) {
                    MapleInventoryManipulator.equip(c, item.getPosition(), (short) -10, GameConstants.getInventoryType(itemid));
                } else {
                    c.getPlayer().dropMessage(1, "오류가 발생했습니다.");
                }
            } else {
                c.getPlayer().dropMessage(1, "보조무기 장착이 불가능한 직업군입니다.");
            }
            return 1;
        }
    }

    public static class 저장 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            new MapleCharacterSave(c.getPlayer()).saveToDB(c.getPlayer(), false, false);
            c.getPlayer().dropMessage(5, "저장되었습니다.");
            return 1;
        }
    }

    public static class 인벤초기화 extends CommandExecute {

        @Override
        public int execute(MapleClient c, String[] splitted) {
            java.util.Map<Pair<Short, Short>, MapleInventoryType> eqs = new HashMap<Pair<Short, Short>, MapleInventoryType>();
            if (splitted[1].equals("모두")) {
                for (MapleInventoryType type : MapleInventoryType.values()) {
                    for (Item item : c.getPlayer().getInventory(type)) {
                        eqs.put(new Pair<Short, Short>(item.getPosition(), item.getQuantity()), type);
                    }
                }
            } else if (splitted[1].equals("장착")) {
                for (Item item : c.getPlayer().getInventory(MapleInventoryType.EQUIPPED)) {
                    eqs.put(new Pair<Short, Short>(item.getPosition(), item.getQuantity()), MapleInventoryType.EQUIPPED);
                }
            } else if (splitted[1].equals("장비")) {
                for (Item item : c.getPlayer().getInventory(MapleInventoryType.EQUIP)) {
                    eqs.put(new Pair<Short, Short>(item.getPosition(), item.getQuantity()), MapleInventoryType.EQUIP);
                }
            } else if (splitted[1].equals("소비")) {
                for (Item item : c.getPlayer().getInventory(MapleInventoryType.USE)) {
                    eqs.put(new Pair<Short, Short>(item.getPosition(), item.getQuantity()), MapleInventoryType.USE);
                }
            } else if (splitted[1].equals("설치")) {
                for (Item item : c.getPlayer().getInventory(MapleInventoryType.SETUP)) {
                    eqs.put(new Pair<Short, Short>(item.getPosition(), item.getQuantity()), MapleInventoryType.SETUP);
                }
            } else if (splitted[1].equals("기타")) {
                for (Item item : c.getPlayer().getInventory(MapleInventoryType.ETC)) {
                    eqs.put(new Pair<Short, Short>(item.getPosition(), item.getQuantity()), MapleInventoryType.ETC);
                }
            } else if (splitted[1].equals("캐시")) {
                for (Item item : c.getPlayer().getInventory(MapleInventoryType.CASH)) {
                    eqs.put(new Pair<Short, Short>(item.getPosition(), item.getQuantity()), MapleInventoryType.CASH);
                }
            } else {
                c.getPlayer().dropMessage(6, "[모두/장착/장비/소비/설치/기타/캐시]");
            }
            for (Entry<Pair<Short, Short>, MapleInventoryType> eq : eqs.entrySet()) {
                MapleInventoryManipulator.removeFromSlot(c, eq.getValue(), eq.getKey().left, eq.getKey().right, false, false);
            }
            return 1;
        }
    }

    public static class 데스카운트 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            c.getPlayer().dropMessage(-8, "남은 데스카운트 수 : " + c.getPlayer().getDeathCount());
            return 1;
        }
    }

    public static class 마을 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {

            final MapleMap mapz = ChannelServer.getInstance(c.getChannel()).getMapFactory().getMap(ServerConstants.WarpMap);
            c.getPlayer().setDeathCount((byte) 0);
            c.getPlayer().changeMap(mapz, mapz.getPortal(0));
            c.getPlayer().dispelDebuffs();

            c.getPlayer().Stigma = 0;
            Map<MapleBuffStat, Pair<Integer, Integer>> dds = new HashMap<>();
            dds.put(MapleBuffStat.Stigma, new Pair<>(c.getPlayer().Stigma, 0));
            c.getSession().writeAndFlush(BuffPacket.cancelBuff(dds, c.getPlayer()));
            c.getPlayer().getMap().broadcastMessage(c.getPlayer(), BuffPacket.cancelForeignBuff(c.getPlayer(), dds), false);

            c.getPlayer().addKV("bossPractice", "0");
            c.getPlayer().cancelEffectFromBuffStat(MapleBuffStat.DebuffIncHp);
            c.getPlayer().cancelEffectFromBuffStat(MapleBuffStat.FireBomb);

            return 1;
        }
    }

    public static class 페이즈스킵 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {

            if (c.getPlayer().isLeader()) {
                if (c.getPlayer().getV("bossPractice") != null && c.getPlayer().getV("bossPractice").equals("1")) {
                    MapleMonster mob;
                    MapleMap map = c.getPlayer().getMap();
                    for (MapleMapObject monstermo : map.getMapObjectsInRange(c.getPlayer().getPosition(), Double.POSITIVE_INFINITY, Arrays.asList(MapleMapObjectType.MONSTER))) {
                        mob = (MapleMonster) monstermo;
                        if (!mob.getStats().isBoss() || mob.getStats().isPartyBonus() || c.getPlayer().isGM()) {
                            map.killMonster(mob, c.getPlayer(), false, false, (byte) 1);
                        }
                    }
                } else {
                    c.getPlayer().dropMessage(5, "보스 연습모드 중에만 사용하실 수 있습니다.ㄴ");
                }
            } else {
                c.getPlayer().dropMessage(5, "파티장이 시도해 주세요.");
            }
            return 1;
        }
    }

    public static class 빙고입장 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            /*            if (c.getChannelServer().getMapFactory().getMap(922290000).isBingoGame()) {
            	if (c.getPlayer().getParty() != null) {
            		c.getPlayer().dropMessage(6, "파티를 해제해주세요.");
            	} else if (c.getPlayer().getMapId() / 1000 != 680000) {
            		c.getPlayer().dropMessage(6, "마을이나 쉼터에서 시도해주세요.");
            	} else if (c.getPlayer().getMapId() == 922290000) {
            		c.getPlayer().dropMessage(6, "이미 빙고맵에 입장했습니다.");
            	} else {
            		c.getPlayer().warp(922290000);
            	}
            } else {
            	c.getPlayer().dropMessage(6, "이 채널에서는 빙고게임이 개설되고있지 않습니다.");
            }
             */
            return 1;
        }
    }

    public static class 창고 extends CommandExecute {

        @Override
        public int execute(MapleClient c, String[] splitted) {
            c.removeClickedNPC();
            NPCScriptManager.getInstance().dispose(c);
            NPCScriptManager.getInstance().start(c, 3003332);
            return 1;
        }
    }

    public static class 경매장 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            AuctionHandler.EnterAuction(c.getPlayer(), c);
            return 1;
        }
    }

    public static class 황금마차오류 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {

            if (c.getKeyValue("goldCount") == null) {
                c.setKeyValue("goldCount", "0");
            }
            if (c.getKeyValue("goldT") == null) {
                c.setKeyValue("goldT", "0");
            }
            if (c.getKeyValue("goldComplete") == null) {
                c.setKeyValue("goldComplete", "0");
            }
            if (c.getKeyValue("passDate") == null) {
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < 63; ++i) {
                    str.append("0");
                }
                c.setKeyValue("passDate", str.toString());
            }
            if (c.getKeyValue("passCount") == null) {
                c.setKeyValue("passCount", "63");
            }
            c.getSession().writeAndFlush(CField.getGameMessage(11, "오류가 해결되었습니다. @황금마차 를 통해 다시 시도해주세요."));
            c.getSession().writeAndFlush(CField.getGameMessage(11, "계속해서 오류가 발생한다면 사진과 함께 제보해주시길 바랍니다."));
            return 1;
        }
    }

    public static class 황금마차 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {

            Date startDate = new Date(2020, 03, 01);
            Date finishDate = new Date(2020, 11, 31);
            if (c.getKeyValue("goldCount") == null) {
                c.setKeyValue("goldCount", "0");
            }
            if (c.getKeyValue("goldT") == null || c.getKeyValue("goldT").equals("0")) {
                c.setKeyValue("goldCount", "0");
                c.setKeyValue("goldT", GameConstants.getCurrentFullDate());
                c.getSession().writeAndFlush(CField.getGameMessage(7, "황금마차 시간 기록이 시작되었습니다."));
            } else {
                String bTime = c.getKeyValue("goldT");
                String cTime = GameConstants.getCurrentFullDate();
                int bH = Integer.parseInt(bTime.substring(8, 10)); // 3시
                int bM = Integer.parseInt(bTime.substring(10, 12)); // 47분
                int cH = Integer.parseInt(cTime.substring(8, 10));
                int cM = Integer.parseInt(cTime.substring(10, 12));
                if ((cH - bH == 1 && cM >= bM) || (cH - bH > 1)) {
                    c.setKeyValue("goldCount", "3600");
                }
            }

            if (c.getKeyValue("goldDay") == null) {
                c.setKeyValue("goldDay", "0");
            }

            if (c.getKeyValue("goldComplete") == null) {
                c.setKeyValue("goldComplete", "0");
            }

            if (c.getKeyValue("passDate") == null) {
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < 63; ++i) {
                    str.append("0");
                }
                c.setKeyValue("passDate", str.toString());
            }

            if (c.getKeyValue("passCount") == null || Integer.parseInt(c.getKeyValue("passCount")) <= 63) {
                c.setKeyValue("passCount", String.valueOf(Integer.parseInt(c.getKeyValue("passCount")) + 72));
            }

            c.setKeyValue("bMaxDay", "135");
            c.setKeyValue("cMaxDay", "135");
            c.setKeyValue("lastDate", "20/12/31");

            c.getSession().writeAndFlush(CField.getGameMessage(7, "골든패스는 후원포인트 3000을 소모하여 대기시간 없이 출석 가능한 시스템입니다."));

            c.getSession().writeAndFlush(InfoPacket.updateClientInfoQuest(238, "count=" + c.getKeyValue("goldCount") + ";T=" + c.getKeyValue("goldT")));
            c.getSession().writeAndFlush(InfoPacket.updateClientInfoQuest(239, "complete=" + c.getKeyValue("goldComplete") + ";day=" + c.getKeyValue("goldDay") + ";passCount=" + c.getKeyValue("passCount") + ";bMaxDay=" + c.getKeyValue("bMaxDay") + ";lastDate=" + c.getKeyValue("lastDate") + ";cMaxDay=" + c.getKeyValue("cMaxDay")));
            c.getSession().writeAndFlush(InfoPacket.updateClientInfoQuest(240, "passDate=" + c.getKeyValue("passDate")));
            c.getSession().writeAndFlush(CField.onUIEventSet(100208, 1254));
            c.getSession().writeAndFlush(CField.onUIEventInfo(100208, finishDate.getTime(), startDate.getTime(), 135, "chariotInfo", GameConstants.chariotItems, 1254));
            return 1;
        }
    }

    public static class 스킬마스터 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            MapleData data = MapleDataProviderFactory.getDataProvider(MapleDataProviderFactory.fileInWZPath("Skill.wz")).getData(StringUtil.getLeftPaddedStr("" + c.getPlayer().getJob(), '0', 3) + ".img");
            c.getPlayer().dropMessage(5, "스킬마스터가 완료되었습니다.");
            if (c.getPlayer().getLevel() < 10) {
                c.getPlayer().dropMessage(1, "레벨 10 이상 부터 사용 할 수 있습니다.");
                return 1;
            }
            for (int i = 0; i < (c.getPlayer().getJob() % 10) + 1; i++) {
                c.getPlayer().maxskill(((i + 1) == ((c.getPlayer().getJob() % 10) + 1)) ? c.getPlayer().getJob() - (c.getPlayer().getJob() % 100) : c.getPlayer().getJob() - (i + 1));
            }
            c.getPlayer().maxskill(c.getPlayer().getJob());
            if (GameConstants.isDemonAvenger(c.getPlayer().getJob())) {
                c.getPlayer().maxskill(3101);
            }
            if (GameConstants.isZero(c.getPlayer().getJob())) {
                int jobs[] = {10000, 10100, 10110, 10111, 10112};
                for (int job : jobs) {
                    data = MapleDataProviderFactory.getDataProvider(MapleDataProviderFactory.fileInWZPath("Skill.wz")).getData(job + ".img");
                    for (MapleData skill : data) {
                        if (skill != null) {
                            for (MapleData skillId : skill.getChildren()) {
                                if (!skillId.getName().equals("icon")) {
                                    byte maxLevel = (byte) MapleDataTool.getIntConvert("maxLevel", skillId.getChildByPath("common"), 0);
                                    if (maxLevel < 0) { // 배틀메이지 데스는 왜 만렙이 250이지?
                                        maxLevel = 1;
                                    }
                                    if (MapleDataTool.getIntConvert("invisible", skillId, 0) == 0) { //스킬창에 안보이는 스킬은 올리지않음
                                        if (c.getPlayer().getLevel() >= MapleDataTool.getIntConvert("reqLev", skillId, 0)) {
                                            c.getPlayer().changeSingleSkillLevel(SkillFactory.getSkill(Integer.parseInt(skillId.getName())), maxLevel, maxLevel);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (c.getPlayer().getLevel() >= 200) {
                        c.getPlayer().changeSingleSkillLevel(SkillFactory.getSkill(100001005), (byte) 1, (byte) 1);
                    }
                }
            }
            if (GameConstants.isKOC(c.getPlayer().getJob()) && c.getPlayer().getLevel() >= 100) {
                c.getPlayer().changeSkillLevel(11121000, (byte) 30, (byte) 30);
                c.getPlayer().changeSkillLevel(12121000, (byte) 30, (byte) 30);
                c.getPlayer().changeSkillLevel(13121000, (byte) 30, (byte) 30);
                c.getPlayer().changeSkillLevel(14121000, (byte) 30, (byte) 30);
                c.getPlayer().changeSkillLevel(15121000, (byte) 30, (byte) 30);
            }
            return 1;
        }
    }

    public static class 도움말 extends CommandExecute {

        public int execute(MapleClient c, String[] splitted) {
            c.getPlayer().dropMessage(5, "@힘, @덱스, @인트, @럭 <추가 할 양>");
            c.getPlayer().dropMessage(5, "@몬스터 < 근처 몬스터에 대한 정보 확인 >");
            c.getPlayer().dropMessage(5, "@엔피시 < 도우미 엔피시 출력 >");
            c.getPlayer().dropMessage(5, "@이벤트엔피시 < 이벤트 엔피시 >");
            c.getPlayer().dropMessage(5, "@드롭체크  < 드롭 정보 확인 >");
            c.getPlayer().dropMessage(5, "@렉 < NPC관련 오류 발생시 사용 >");
            c.getPlayer().dropMessage(5, "@저장 < 캐릭터 정보를 저장 >");
            c.getPlayer().dropMessage(5, "@마을 < 마을로 이동 >");
            c.getPlayer().dropMessage(5, "@동접 < 서버의 접속 유저 수 확인 >");
            c.getPlayer().dropMessage(5, "@인벤초기화 < 인벤토리를 초기화 >");
            c.getPlayer().dropMessage(5, "@온라인 < 현재 채널의 접속 유저 목록 확인 >");
            c.getPlayer().dropMessage(5, "@명성치알림 < 명성치 메세지 알림 여부 확인. 현재 상태 :  " + (c.getPlayer().getKeyValue(5, "show_honor") > 0 ? ("알림") : ("미알림")) + ">");
            c.getPlayer().dropMessage(5, "@UI알림 < UI 띄우기 체크. 현재 상태 :  " + (c.getPlayer().getKeyValue(5, "openUI") > 0 ? ("적용") : ("미적용")) + ">");
            c.getPlayer().dropMessage(5, "~할말 < 전채 채팅  >");
            c.getPlayer().dropMessage(5, "@보조무기장착 < 보조무기장착  >");
            return 1;
        }
    }
}
