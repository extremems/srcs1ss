/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.android;

import client.MapleCharacter;
import client.MapleClient;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import constants.ServerConstants;
import handling.channel.handler.MovementParse;
import java.awt.Point;
import java.util.List;
import server.MapleInventoryManipulator;
import server.movement.LifeMovementFragment;
import tools.data.LittleEndianAccessor;
import tools.packet.CField;
import tools.packet.CWvsContext;

public class AndroidHandler {

    public static boolean isAndroid(final int itemId) {
        return itemId / 10000 == 166;
    }

    public static int getAndroidType(int itemid) {
        switch (itemid) {
            case 1662000: //보급
            case 1662001:
            case 1662002: //고급
            case 1662003:
                return itemid - 1661999;
            case 1662004: //눈꽃
            case 1662005:
                return itemid - 1662003;
            case 1662006: //오마이프린세스
                return itemid - 1662001;
            case 1662007: //보급
            case 1662008:
                return itemid - 1662006;
            case 1662009: //발렌타인
            case 1662010:
                return itemid - 1662008;
            case 1662011: //판타스틱
            case 1662012:
                return itemid - 1662005;
            case 1662013: //9주년
            case 1662014:
                return itemid - 1662010;
            case 1662015: //메소레인저
            case 1662016:
            case 1662017: //썸머
            case 1662018:
                return itemid - 1662007;
            case 1662019: //썸머
            case 1662020:
            case 1662021: //TOP 스쿨
            case 1662022:
                return itemid - 1662009;
            case 1662024: //베릴
            case 1662025: //행복한
            case 1662026:
                return itemid - 1662010;
            case 1662027: //메이드
                return itemid - 1662022;
            case 1662035: //제로
            case 1662036:
                return itemid - 1662018;
            case 1662115:
                return 38;
            case 1662116:
                return 39;
            case 1662111:
                return 36;
            case 1662092:
                return 22;
            case 1662141:
                return 46;
            case 1662130:
                return 42;
            case 1662131:
                return 43;
            case 1662140:
                return 45;
            case 1662125:
                return 32;
            case 1662126:
                return 33;
            case 1662039: //오르카
            case 1662093:
                return 107;
            case 1662041: //할로윈
                return 110;
            case 1662032: //서큐버스
                return 101;
            case 1662043: //은월
            case 1662044:
                return itemid - 1662024;
            case 1662046: //무르무르
                return itemid - 1662025;
            case 1662114:
                return 37;
        }
        return 1;
    }

    public static final void MoveAndroid(final LittleEndianAccessor slea, final MapleClient c, final MapleCharacter chr) {
        slea.skip(8);
        int unk1 = slea.readInt();
        int unk2 = slea.readInt();
        final List<LifeMovementFragment> res = MovementParse.parseMovement(slea, 3);

        if (res != null && chr != null && res.size() != 0 && chr.getMap() != null && chr.getAndroid() != null) { // map crash hack
            if (chr.getMapId() == ServerConstants.WarpMap) {
                return;
            }
            final Point pos = new Point(chr.getAndroid().getPos());
            chr.getAndroid().updatePosition(res);
            chr.getMap().broadcastMessage(chr, CField.moveAndroid(chr.getId(), pos, res, unk1, unk2), false);
        }
    }

    public static final void ChangeAndroidEmotion(final int emote, final MapleCharacter chr) {
        if (emote > 0 && chr != null && chr.getMap() != null && !chr.isHidden() && emote <= 17 && chr.getAndroid() != null) { //O_o
            chr.getMap().broadcastMessage(CField.showAndroidEmotion(chr.getId(), emote));
        }
    }

    public static void AndroidEar(MapleClient c, final LittleEndianAccessor slea) {
        MapleAndroid android = c.getPlayer().getAndroid();
        if (android == null) {
            c.getPlayer().dropMessage(1, "알 수 없는 오류가 발생 하였습니다.");
            c.getSession().writeAndFlush(CWvsContext.enableActions(c.getPlayer()));
            return;
        }
        short slot = slea.readShort();
        final Item item = c.getPlayer().getInventory(MapleInventoryType.USE).getItem(slot);
        if (item != null && item.getItemId() == 2892000) {
            android.setEar(!android.getEar());
            c.getPlayer().updateAndroid();
            MapleInventoryManipulator.removeFromSlot(c, MapleInventoryType.USE, slot, (short) 1, true);
            c.getSession().writeAndFlush(CWvsContext.enableActions(c.getPlayer()));
        } else {
            c.getPlayer().dropMessage(1, "알 수 없는 오류가 발생 하였습니다.");
            c.getSession().writeAndFlush(CWvsContext.enableActions(c.getPlayer()));
        }
    }

}
