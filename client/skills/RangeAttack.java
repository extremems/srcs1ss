/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.skills;

import java.awt.Point;

/**
 *
 * @author qkral
 */
public class RangeAttack {

    public RangeAttack(int skillId, Point position, int type, int delay, int attackCount) {
        this.skillId = skillId;
        this.position = position;
        this.type = (short) type;
        this.delay = delay;
        this.attackCount = attackCount;
    }

    public Point getPosition() {
        return position;
    }

    private Point position;

    public short getType() {
        return type;
    }

    private short type;

    public int getSkillId() {
        return skillId;
    }

    private int skillId;

    public int getDelay() {
        return delay;
    }

    private int delay;

    public int getAttackCount() {
        return attackCount;
    }

    private int attackCount;
}
