/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.skills;

import tools.data.MaplePacketLittleEndianWriter;

public class KainDeathBlessing {

    private int objectId;
    private int stack;
    private int passedTime;
    private int duration;

    public KainDeathBlessing(int objectId, int stack, int passedTime, int duration) {
        this.setObjectId(objectId);
        this.setStack(stack);
        this.setPassedTime(passedTime);
        this.setDuration(duration);
    }

    public void encode(MaplePacketLittleEndianWriter mplew) {
        mplew.writeInt(this.getObjectId());
        mplew.writeInt(this.getStack());
        mplew.writeInt(this.getPassedTime());
        mplew.writeInt(this.getDuration());
    }

    public int getObjectId() {
        return this.objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    public int getStack() {
        return this.stack;
    }

    public void setStack(int stack) {
        this.stack = stack;
    }

    public int getPassedTime() {
        return this.passedTime;
    }

    public void setPassedTime(int passedTime) {
        this.passedTime = passedTime;
    }

    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
