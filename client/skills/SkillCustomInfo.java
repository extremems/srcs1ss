/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.skills;

/**
 *
 * @author qkral
 */
public class SkillCustomInfo {

    private int value;
    private long endtime = 0;

    public SkillCustomInfo(int value, long time) {
        this.value = value;
        if (time > 0) {
            this.endtime = System.currentTimeMillis() + time;
        }
    }

    public boolean canCancel(long now) {
        return (endtime > 0 && now >= endtime);
    }

    public int getValue() {
        return value;
    }

    public long getEndTime() {
        return endtime;
    }
}
