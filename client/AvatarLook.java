package client;

import client.inventory.Equip;
import client.inventory.MapleInventoryType;
import client.inventory.MapleWeaponType;
import constants.GameConstants;
import server.Randomizer;
import tools.data.LittleEndianAccessor;
import tools.data.MaplePacketLittleEndianWriter;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class AvatarLook {

    private final Map<Byte, Integer> items;
    private final Map<Byte, Integer> codyItems;
    private final int[] pets;
    private int gender, skin, face, hair, job, mega, weaponStickerID, weaponID, subWeaponID, drawElfEar, pinkbeanChangeColor,
            demonSlayerDefFaceAcc, xenonDefFaceAcc, arkDefFaceAcc, hoyeongDefFaceAcc, isZeroBetaLook, addColor, baseProb;

    public AvatarLook() {
        items = new HashMap<>();
        codyItems = new HashMap<>();
        pets = new int[3];
    }

    public static AvatarLook makeRandomAvatar() {
        AvatarLook a = new AvatarLook();

        int[] skin = {15, 16, 18, 19};
        int[] face = {20061, 25017, 21045, 25099, 25050};

        a.gender = Randomizer.nextInt(2);

        int[] hair;

        if (a.gender == 1) {
            hair = new int[]{43980, 40670, 38620, 48370, 41750};
        } else {
            hair = new int[]{43330, 35660, 46340, 33150, 43320};
        }

        a.skin = skin[Randomizer.nextInt(skin.length)];
        a.face = face[Randomizer.nextInt(face.length)] + Randomizer.nextInt(8) * 100;
        a.job = 222;
        a.mega = 0;
        a.hair = hair[Randomizer.nextInt(hair.length)] + Randomizer.nextInt(8);

        int[][] items = {{1002186, 1003250, 1000599}, {1012104, 1012379}, {1022079, 1022285}, {1032024},
        {1052975, 1053257, 1053351}, {-1}, {1072153}, {1082102}, {-1}};

        for (int i = 0; i < items.length; ++i) {
            a.items.put((byte) (i + 1), items[i][Randomizer.nextInt(items[i].length)]);
        }

        int[] weapons = {1702174, 1702549, 1702945};

        a.weaponStickerID = weapons[Randomizer.nextInt(weapons.length)];
        a.weaponID = Randomizer.nextBoolean() ? 1442000 : 1302000;
        a.subWeaponID = 1092056;

        a.drawElfEar = 0;
        a.pinkbeanChangeColor = 0;

        for (int i = 0; i < 3; ++i) {
            a.pets[i] = 0;
        }

        return a;
    }

    public void save(int position, PreparedStatement ps) throws SQLException {
        ps.setInt(2, position);
        ps.setInt(3, gender);
        ps.setInt(4, skin);
        ps.setInt(5, face);
        ps.setInt(6, hair);
        for (int i = 1; i <= 9; ++i) {
            if (items.containsKey((byte) i)) {
                ps.setInt(6 + i, items.get((byte) i));
            } else {
                ps.setInt(6 + i, -1);
            }
        }
        ps.setInt(16, weaponStickerID);
        ps.setInt(17, weaponID);
        ps.setInt(18, subWeaponID);
    }

    public static AvatarLook init(ResultSet rs) throws SQLException {
        AvatarLook a = new AvatarLook();

        a.gender = rs.getInt("gender");
        a.skin = rs.getInt("skin");
        a.face = rs.getInt("face");
        a.hair = rs.getInt("hair");

        for (int i = 1; i <= 9; ++i) {
            a.items.put((byte) i, rs.getInt("equip" + i));
        }

        a.weaponStickerID = rs.getInt("weaponstickerid");
        a.weaponID = rs.getInt("weaponid");
        a.subWeaponID = rs.getInt("subweaponid");

        return a;
    }

    public int getHairEquip(int pos) {
        if (codyItems.containsKey((byte) pos)) {
            return codyItems.get((byte) pos);
        }

        if (items.containsKey((byte) pos)) {
            return items.get((byte) pos);
        }

        return -1;
    }

    public boolean compare(AvatarLook a) {

        if (a.gender != gender || a.skin != skin || a.face != face || a.hair != hair || a.subWeaponID != subWeaponID || a.weaponStickerID != weaponStickerID) {
            return false;
        }

        for (int i = 1; i <= 9; ++i) {
            if (a.getHairEquip(i) != getHairEquip(i)) {
                return false;
            }
        }

        return true;
    }

    public static void decodeUnpackAvatarLook(AvatarLook a, LittleEndianAccessor slea) throws IOException {
        a.gender = slea.readBit(1);
        a.skin = slea.readBit(10);

        boolean is5thFace = slea.readBit(1) == 1;

        int face1 = slea.readBit(10), face2 = slea.readBit(4);

        if (face1 == 1023) {
            a.face = 0;
        } else {
            a.face = face1 + (1000 * (face2 + (is5thFace ? 50 : 20)));
        }

        boolean is4thHair = slea.readBit(1) == 1;

        int hair1 = slea.readBit(10), hair2 = slea.readBit(4);

        if (hair1 == 1023) {
            a.hair = 0;
        } else {
            a.hair = hair1 + 1000 * (hair2 + 30) + (is4thHair ? 10000 : 0);
        }

        int equip1_1 = slea.readBit(10), equip1_2 = slea.readBit(3);

        if (equip1_1 != 1023) {
            a.items.put((byte) 1, equip1_1 + 1000 * (equip1_2 + 1000));
        }

        int equip2_1 = slea.readBit(10), equip2_2 = slea.readBit(2);

        if (equip2_1 != 1023) {
            a.items.put((byte) 2, equip2_1 + 1000 * (equip2_2 + 1010));
        }

        int equip3_1 = slea.readBit(10), equip3_2 = slea.readBit(2);

        if (equip3_1 != 1023) {
            a.items.put((byte) 3, equip3_1 + 1000 * (equip3_2 + 1020));
        }

        int equip4_1 = slea.readBit(10), equip4_2 = slea.readBit(2);

        if (equip4_1 != 1023) {
            a.items.put((byte) 4, equip4_1 + 1000 * (equip4_2 + 1030));
        }

        boolean equip5_check = slea.readBit(1) == 1;

        int equip5_1 = slea.readBit(10), equip5_2 = slea.readBit(4);

        if (equip5_1 != 1023) {
            a.items.put((byte) 5, equip5_1 + 1000 * (equip5_2 + 10 * (equip5_check ? 105 : 104)));
        }

        int equip6_1 = slea.readBit(10), equip6_2 = slea.readBit(2);

        if (equip6_1 != 1023) {
            a.items.put((byte) 6, equip6_1 + 1000 * (equip6_2 + 1060));
        }

        int equip7_1 = slea.readBit(10), equip7_2 = slea.readBit(2);

        if (equip7_1 != 1023) {
            a.items.put((byte) 7, equip7_1 + 1000 * (equip7_2 + 1070));
        }

        int equip8_1 = slea.readBit(10), equip8_2 = slea.readBit(2);

        if (equip8_1 != 1023) {
            a.items.put((byte) 8, equip8_1 + 1000 * (equip8_2 + 1080));
        }

        int equip9_1 = slea.readBit(10), equip9_2 = slea.readBit(2);

        if (equip9_1 != 1023) {
            a.items.put((byte) 9, equip9_1 + 1000 * (equip9_2 + 1100));
        }

        int equip10_check = slea.readBit(2);

        int equip10_1 = slea.readBit(10), equip10_2 = slea.readBit(4);
        int v53 = equip10_check == 3 ? 135 : equip10_check == 2 ? 134 : equip10_check == 1 ? 109 : 0;

        if (equip10_1 != 1023) {
            a.subWeaponID = (equip10_1 + 1000 * (equip10_2 + 2 * (5 * v53)));
        }

        boolean weaponStickerCheck = slea.readBit(1) == 1;

        int equip11_1 = slea.readBit(10), equip11_2 = slea.readBit(2);

        if (weaponStickerCheck) {
            a.weaponStickerID = (1000 * (equip11_2 + 1700) + equip11_1);
        } else {
//            a.weaponID = (1000 * (equip11_2 + 1000) + equip11_1);
        }
    }

    public void encodeUnpackAvatarLook(final MaplePacketLittleEndianWriter mplew) {
        mplew.writeBit(gender, 1); // gender
        mplew.writeBit(skin, 10); // skin

        //21278550
        mplew.writeBit((face / 10000 == 5) ? 1 : 0, 1); // checkFaceGender
        mplew.writeBit((face % 1000), 10); // face
        mplew.writeBit((face / 1000 % 10), 4); // face

        mplew.writeBit((Integer.toUnsignedLong(hair - 40000) < 10000) ? 1 : 0, 1); // checkHairGender
        mplew.writeBit((hair % 1000), 10); // hairEquip[0]
        mplew.writeBit((hair / 1000 % 10), 4); // hairEquip[0]

        mplew.writeBit((getHairEquip(1) % 1000), 10); // hairEquip[1]
        mplew.writeBit(getHairEquip(1) / 1000 % 10, 3); // hairEquip[1]

        mplew.writeBit((getHairEquip(2) % 1000), 10); // hairEquip[2]
        mplew.writeBit(getHairEquip(2) / 1000 % 10, 2); // hairEquip[2]

        mplew.writeBit((getHairEquip(3) % 1000), 10); // hairEquip[3]
        mplew.writeBit((getHairEquip(3) / 1000 % 10), 2); // hairEquip[3]

        mplew.writeBit((getHairEquip(4) % 1000), 10); // hairEquip[4]
        mplew.writeBit(getHairEquip(4) / 1000 % 10, 2); // hairEquip[4]

        mplew.writeBit(Integer.toUnsignedLong(getHairEquip(5) - 1050000) < 10000 ? 1 : 0, 1); // hairEquip[5] check
        mplew.writeBit((getHairEquip(5) % 1000), 10); // hairEquip[5]
        mplew.writeBit((getHairEquip(5) / 1000 % 10), 4); // hairEquip[5]

        mplew.writeBit((getHairEquip(6) % 1000), 10); // hairEquip[6]
        mplew.writeBit((getHairEquip(6) / 1000 % 10), 2); // hairEquip[6]

        mplew.writeBit((getHairEquip(7) % 1000), 10); // hairEquip[7]
        mplew.writeBit(getHairEquip(7) / 1000 % 10, 2); // hairEquip[7]

        mplew.writeBit((getHairEquip(8) % 1000), 10); // hairEquip[8]
        mplew.writeBit((getHairEquip(8) / 1000 % 10), 2); // hairEquip[8]

        mplew.writeBit((getHairEquip(9) % 1000), 10); // hairEquip[9]
        mplew.writeBit(getHairEquip(9) / 1000 % 10, 2); // hairEquip[9]

        int v39 = 0;

        if (subWeaponID > 0) {
            if (subWeaponID / 10000 == 109) {
                v39 = 1;
            } else {
                v39 = Integer.toUnsignedLong(subWeaponID - 1340000) < 10000 ? 2 : 3;
            }
        }

        mplew.writeBit(v39, 2); //hairEquip[10]
        mplew.writeBit((subWeaponID % 1000), 10); //hairEquip[10]
        mplew.writeBit((subWeaponID / 1000 % 10), 4); //hairEquip[10]

        int weapon = weaponStickerID > 0 ? weaponStickerID : weaponID;

        mplew.writeBit(weapon / 100000 == 17 ? 1 : 0, 1);  // hairEquip[11]
        mplew.writeBit((weapon % 1000), 10);  // hairEquip[11]
        mplew.writeBit((weapon / 1000 % 10), 2);  // hairEquip[11]

        int weaponType = (weaponID / 10000) % 100;

        if (GameConstants.getWeaponType(weaponID) == MapleWeaponType.TUNER) {
            weaponType = 213;
        }

        Integer[] wt = {0x1E, 0x1F, 0x20, 0x21, 0x25, 0x26, 0x28, 0x29, 0x2A, 0x2B, 0x2C,
            0x2D, 0x2E, 0x2F, 0x30, 0x31, 0x27, 0x22, 0x34, 0x35, 0x23, 0x24,
            0x15, 0x16, 0x17, 0x18, 0x38, 0x39, 0x1A, 0x3A, 0x1B, 0x1C, 0x3B,
            0x1D, 0x0D5, 0x1, 0};

        int index = Arrays.asList(wt).indexOf(weaponType);

        mplew.writeBit(index == -1 ? wt.length : (index + 1), 8); // weaponType 5 -> 8 bit because of 213

        mplew.writeBit((drawElfEar & 1), 4); // MercedesElfEar
        mplew.writeBit(addColor, 4);
        mplew.writeBit(baseProb, 8);

        mplew.writeBit(hoyeongDefFaceAcc, 8);

        mplew.writeBit((GameConstants.isPinkBean(this.job) || GameConstants.isYeti(this.job)) ? 1 : 0, 1);

        mplew.writeBit(0, 1); // what

        //28 bytes
        mplew.writeZeroBytes(92);

        mplew.write(19); // unPackToVersion
    }

    public void encodeAvatarLook(final MaplePacketLittleEndianWriter mplew) {
        mplew.write(gender);
        mplew.write(skin);
        mplew.writeInt(face);
        mplew.writeInt(job);
        mplew.write(mega);
        mplew.writeInt(hair);

        for (Map.Entry<Byte, Integer> entry : items.entrySet()) {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }

        mplew.write(-1);
        for (Map.Entry<Byte, Integer> entry : codyItems.entrySet()) {
            mplew.write(entry.getKey());
            mplew.writeInt(entry.getValue());
        }
        mplew.write(-1);

        mplew.writeInt(weaponStickerID);
        mplew.writeInt(weaponID);
        mplew.writeInt(subWeaponID);

        mplew.writeInt(drawElfEar); // 324++
        mplew.writeInt(pinkbeanChangeColor); //324++ 핑크빈 색 바꾸기
        mplew.write(0);

        for (int i = 0; i < 3; i++) {
            mplew.writeInt(pets[i]);
        }

        if (GameConstants.isDemonSlayer(job) || GameConstants.isDemonAvenger(job)) {
            mplew.writeInt(demonSlayerDefFaceAcc);
        } else if (GameConstants.isXenon(job)) {
            mplew.writeInt(xenonDefFaceAcc);
        } else if (GameConstants.isArk(job)) {
            mplew.writeInt(arkDefFaceAcc);
        } else if (GameConstants.isHoyeong(job)) {
            mplew.writeInt(hoyeongDefFaceAcc);
        } else if (GameConstants.isZero(job)) {
            mplew.write(isZeroBetaLook);
        }

        mplew.write(addColor);
        mplew.write(baseProb);

        mplew.writeInt(0);
    }

    public static void encodeAvatarLook(final MaplePacketLittleEndianWriter mplew, final MapleCharacter chr, final boolean mega, boolean second) {
        boolean isAlpha = GameConstants.isZero(chr.getJob()) && chr.getGender() == 0 && chr.getSecondGender() == 1;
        boolean isBeta = GameConstants.isZero(chr.getJob()) && chr.getGender() == 1 && chr.getSecondGender() == 0;
        mplew.write(second || isBeta ? chr.getSecondGender() : chr.getGender());
        mplew.write(second || isBeta ? chr.getSecondSkinColor() : chr.getSkinColor());
        mplew.writeInt(second || isBeta ? chr.getSecondFace() : chr.getFace());
        mplew.writeInt(chr.getJob());
        mplew.write(mega ? 0 : 1);
        if (second || isBeta) {
            int hair = chr.getSecondHair();
            if (chr.getSecondBaseColor() != -1) {
                hair = chr.getSecondHair() / 10 * 10 + chr.getSecondBaseColor();
            }
            mplew.writeInt(hair);
        } else {
            int hair = chr.getHair();
            if (chr.getBaseColor() != -1) {
                hair = chr.getHair() / 10 * 10 + chr.getBaseColor();
            }
            mplew.writeInt(hair);
        }
        final Map<Byte, Integer> myEquip = new LinkedHashMap<>();
        final Map<Byte, Integer> maskedEquip = new LinkedHashMap<>();
        final Map<Byte, Integer> totemEquip = new LinkedHashMap<>();
        final Map<Short, Integer> equip = second ? chr.getSecondEquips() : chr.getEquips();
        for (final Map.Entry<Short, Integer> item : equip.entrySet()) {
            if (item.getKey() < -2000) {
                continue;
            }
            short pos = (short) (item.getKey() * -1);
            Equip item_ = (Equip) chr.getInventory(MapleInventoryType.EQUIPPED).getItem((short) -pos);
            if (item_ == null) {
                continue;
            }
            if (GameConstants.isAngelicBuster(chr.getJob()) && second) {
                if ((pos >= 1300) && (pos < 1400)) {
                    pos = (short) (pos - 1300);
                    switch (pos) {
                        case 0:
                            pos = 1;
                            break;
                        case 1:
                            pos = 9;
                            break;
                        case 4:
                            pos = 8;
                            break;
                        case 5:
                            pos = 3;
                            break;
                        case 6:
                            pos = 4;
                            break;
                        case 7:
                            pos = 5;
                            break;
                        case 8:
                            pos = 6;
                            break;
                        case 9:
                            pos = 7;
                            break;
                    }
                    if (myEquip.get((byte) pos) != null) {
                        maskedEquip.put((byte) pos, myEquip.get((byte) pos));
                    }
                    String lol = ((Integer) item.getValue()).toString();
                    String ss = lol.substring(0, 3);
                    int moru = Integer.parseInt(ss + ((Integer) item_.getMoru()).toString());
                    myEquip.put((byte) pos, item_.getMoru() != 0 ? moru : item.getValue());
                } else if (((pos > 100) && (pos < 200)) && (pos != 111)) {
                    pos = (short) (pos - 100);
                    switch (pos) {
                        case 10:
                        case 12:
                        case 13:
                        case 15:
                        case 16:
                            if (myEquip.get((byte) pos) != null) {
                                maskedEquip.put((byte) pos, myEquip.get((byte) pos));
                            }
                            String lol = ((Integer) item.getValue()).toString();
                            String ss = lol.substring(0, 3);
                            int moru = Integer.parseInt(ss + ((Integer) item_.getMoru()).toString());
                            myEquip.put((byte) pos, item_.getMoru() != 0 ? moru : item.getValue());
                            break;
                    }
                }
                if ((pos < 100)) {
                    if (myEquip.get((byte) pos) == null) {
                        String lol = ((Integer) item.getValue()).toString();
                        String ss = lol.substring(0, 3);
                        int moru = Integer.parseInt(ss + ((Integer) item_.getMoru()).toString());
                        myEquip.put((byte) pos, item_.getMoru() != 0 ? moru : item.getValue());
                    } else {
                        maskedEquip.put((byte) pos, item.getValue());
                    }
                }
            } else if (isBeta) {
                //제로이면서 베타일 때
                if ((pos < 100) && (myEquip.get((byte) pos) == null)) {
                    String lol = ((Integer) item.getValue()).toString();
                    String ss = lol.substring(0, 3);
                    int moru = Integer.parseInt(ss + ((Integer) item_.getMoru()).toString());
                    myEquip.put((byte) pos, item_.getMoru() != 0 ? moru : item.getValue());
                } else if (pos > 1500 && pos != 1511) {
                    if (pos > 1500) {
                        pos = (short) (pos - 1500);
                    }
                    myEquip.put((byte) pos, item.getValue());
                }

            } else if (isAlpha || (GameConstants.isAngelicBuster(chr.getJob()) && !second) || (!GameConstants.isZero(chr.getJob()) && !GameConstants.isAngelicBuster(chr.getJob()))) {
                //엔버 드레스업이 아니거나, 제로 알파이거나, 나머지 직업일 때
                if ((pos < 100) && (myEquip.get((byte) pos) == null)) {
                    String lol = ((Integer) item.getValue()).toString();
                    String ss = lol.substring(0, 3);
                    int moru = Integer.parseInt(ss + ((Integer) item_.getMoru()).toString());
                    myEquip.put((byte) pos, item_.getMoru() != 0 ? moru : item.getValue());
                    //myEquip.put((byte) pos, item.getValue());
                } else if ((pos > 100) && (pos != 111)) {

                    pos -= 100;
                    if (myEquip.get((byte) pos) != null) {
                        maskedEquip.put((byte) pos, myEquip.get((byte) pos));
                    }
                    String lol = ((Integer) item.getValue()).toString();
                    String ss = lol.substring(0, 3);
                    int moru = Integer.parseInt(ss + ((Integer) item_.getMoru()).toString());
                    myEquip.put((byte) pos, item_.getMoru() != 0 ? moru : item.getValue());

                    /*pos = (byte) (pos - 100);
                     if (myEquip.get(pos) != null) {
                     maskedEquip.put((byte) pos, myEquip.get(pos));
                     }
                     myEquip.put((byte) pos, item.getValue());*/
                } else if (myEquip.get((byte) pos) != null) {
                    maskedEquip.put((byte) pos, item.getValue());
                }
            }
        }
        for (final Map.Entry<Byte, Integer> totem : chr.getTotems().entrySet()) {
            byte pos = (byte) ((totem.getKey()).byteValue() * -1);
            if (pos < 0 || pos > 2) { //3 totem slots
                continue;
            }
            if (totem.getValue() < 1200000 || totem.getValue() >= 1210000) {
                continue;
            }
            totemEquip.put(Byte.valueOf(pos), totem.getValue());
        }

        for (Map.Entry<Byte, Integer> entry : myEquip.entrySet()) {
            int weapon = ((Integer) entry.getValue()).intValue();

            if (isAlpha && (GameConstants.getWeaponType(weapon) == MapleWeaponType.BIG_SWORD)) {
                continue;
            } else if (isBeta && (GameConstants.getWeaponType(weapon) == MapleWeaponType.LONG_SWORD)) {
                continue;
            } else if (isBeta && (GameConstants.getWeaponType(weapon) == MapleWeaponType.BIG_SWORD)) {
                mplew.write(11);
                mplew.writeInt(((Integer) entry.getValue()).intValue());
            } else {
                mplew.write(((Byte) entry.getKey()).byteValue());
                mplew.writeInt(((Integer) entry.getValue()).intValue());
            }
        }
        mplew.write(-1);

        for (Map.Entry<Byte, Integer> entry : maskedEquip.entrySet()) {
            mplew.write(((Byte) entry.getKey()).byteValue());
            mplew.writeInt(((Integer) entry.getValue()).intValue());
        }
        mplew.write(-1);

        if (isBeta) {
            Integer cWeapon = equip.get((short) -1511);
            mplew.writeInt(cWeapon != null ? cWeapon.intValue() : 0);
            Integer Weapon = equip.get((short) -11);
            mplew.writeInt(Weapon != null ? Weapon.intValue() : 0);
            mplew.writeInt(0);
        } else {
            Integer cWeapon = equip.get((short) -111);
            mplew.writeInt(cWeapon != null ? cWeapon.intValue() : 0);
            Integer Weapon = equip.get((short) -11);
            mplew.writeInt(Weapon != null ? Weapon.intValue() : 0);
            Integer Shield = equip.get((short) -10);
            if (GameConstants.isZero(chr.getJob()) || Shield == null) {
                mplew.writeInt(0);
            } else {
                mplew.writeInt(Shield.intValue());
            }
        }

        mplew.writeInt(0);//엘프귀
        mplew.writeInt(chr.getKeyValue(100229, "hue")); //324++ 핑크빈 색 바꾸기
        mplew.write(second ? chr.getSecondBaseColor() : chr.getBaseColor());//

        for (int i = 0; i < 3; i++) {
            if (chr.getPet(i) != null) {
                mplew.writeInt(chr.getPet(i).getPetItemId());
            } else {
                mplew.writeInt(0);
            }
        }

        if (GameConstants.isDemonSlayer(chr.getJob()) || GameConstants.isXenon(chr.getJob()) || GameConstants.isDemonAvenger(chr.getJob())
                || GameConstants.isArk(chr.getJob())) {
            mplew.writeInt(chr.getDemonMarking());
        } else if (GameConstants.isHoyeong(chr.getJob())) {
            mplew.writeInt(chr.getDemonMarking());
        } else if (GameConstants.isZero(chr.getJob())) {
            mplew.write(second || isBeta ? chr.getSecondGender() : chr.getGender());
        }

        mplew.write(second || isBeta ? chr.getSecondAddColor() : chr.getAddColor());
        mplew.write(second || isBeta ? chr.getSecondBaseProb() : chr.getBaseProb());
        mplew.writeInt(0);
    }

    public static void decodeAvatarLook(AvatarLook a, LittleEndianAccessor slea) {
        a.gender = slea.readByte();
        a.skin = slea.readByte();
        a.face = slea.readInt();
        a.job = slea.readInt();
        a.mega = slea.readByte();
        a.hair = slea.readInt();

        byte pos = slea.readByte();
        while (pos != -1) {
            int itemId = slea.readInt();
            a.items.put(pos, itemId);
            pos = slea.readByte();
        }

        byte pos2 = slea.readByte();
        while (pos2 != -1) {
            int itemId = slea.readInt();
            a.codyItems.put(pos2, itemId);

            pos2 = slea.readByte();
        }

        a.weaponStickerID = slea.readInt();
        a.weaponID = slea.readInt();
        a.subWeaponID = slea.readInt();

        a.drawElfEar = slea.readInt();
        a.pinkbeanChangeColor = slea.readInt();

        slea.readByte();

        for (int i = 0; i < 3; ++i) {
            a.pets[i] = slea.readInt();
        }

        a.addColor = slea.readByte();
        a.baseProb = slea.readByte();
    }
    
    /*       */   public static boolean isContentsMap(int mapid) {
/* 15070 */     switch (mapid) {
/*       */       case 450001400:
/*       */       case 921174002:
/*       */       case 921174100:
/*       */       case 921174101:
/*       */       case 921174102:
/*       */       case 921174103:
/*       */       case 921174104:
/*       */       case 921174105:
/*       */       case 921174106:
/*       */       case 921174107:
/*       */       case 921174108:
/*       */       case 921174109:
/*       */       case 921174110:
/*       */       case 993026900:
/*       */       case 993192600:
/*       */       case 993192601:
/*       */       case 993192800:
/* 15088 */         return true;
/*       */     } 
/* 15090 */     if (mapid / 10000000 == 95) {
/* 15091 */       return true;
/*       */     }
/* 15093 */     if (mapid / 10000 == 92507) {
/* 15094 */       return true;
/*       */     }
/* 15096 */     return false;
/*       */   }
/*       */   
}
