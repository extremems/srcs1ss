package client.damage;

import client.MapleCharacter;
import client.skills.SkillFactory;
import handling.channel.handler.AttackInfo;
import client.skills.MapleStatEffect;
import server.life.MapleMonster;
import tools.AttackPair;
import tools.Pair;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class CalcDamage {

    CRand32 rndGenForCharacter;
    //CRand32 rndForCheckDamageMiss;//not implement yet
    //CRand32 rndGenForMob;//not implement yet
    int invalidCount;

    public CalcDamage() {
        rndGenForCharacter = new CRand32();
        invalidCount = 0;
    }

    public void SetSeed(int seed1, int seed2, int seed3) {
        rndGenForCharacter.Seed(seed1, seed2, seed3);
        //rndForCheckDamageMiss.Seed(seed1, seed2, seed3);//not implement yet
        //rndGenForMob.Seed(seed1, seed2, seed3);//not implement yet
    }

    private int numRand = 11; //A number of random number for calculate damage (KMST 1029 기준 11번)

    public List<Pair<Long, Boolean>> PDamage(MapleCharacter chr, AttackInfo attack) {
        ArrayList<Pair<Long, Boolean>> realDamageList = new ArrayList<Pair<Long, Boolean>>();
        for (AttackPair eachMob : attack.allDamage) {
            MapleMonster monster = chr.getMap().getMonsterByOid(eachMob.objectid);
            long[] rand = new long[this.numRand];
            for (int i = 0; i < this.numRand; ++i) {
                rand[i] = this.rndGenForCharacter.Random();
            }
            int index = 0;
            for (Pair<Long, Boolean> att : eachMob.attack) {
                double realDamage = 0.0;
                boolean critical = false;
                int n = index = (int) ((byte) (index + 1));
                index = (byte) (index + 1);
                long unkRand1 = rand[n % this.numRand];
                long maxDamage = 0L;
                long minDamage = Long.MAX_VALUE;
                int n2 = index;
                index = (byte) (index + 1);
                double adjustedRandomDamage = this.RandomInRange(rand[n2 % this.numRand], maxDamage, minDamage);
                realDamage += adjustedRandomDamage;
                if (monster == null) {
                    chr.dropMessageGM(6, "monster null");
                    continue;
                }
                if (monster.getStats() == null) {
                    chr.dropMessageGM(6, "stat null");
                    continue;
                }
                double monsterPDRate = monster.getStats().getPDRate();
                double percentDmgAfterPDRate = Math.max(0.0, 100.0 - monsterPDRate);
                realDamage = percentDmgAfterPDRate * realDamage / 100.0;
                MapleStatEffect skillEffect = null;
                if (attack.skill > 0) {
                    skillEffect = SkillFactory.getSkill(attack.skill).getEffect(chr.getTotalSkillLevel(attack.skill));
                }
                if (skillEffect != null) {
                    chr.dropMessageGM(6, "skillDamage : " + skillEffect.getDamage());
                    realDamage = realDamage * (double) skillEffect.getDamage() / 100.0;
                }
                int n3 = index;
                index = (byte) (index + 1);
                if (this.RandomInRange(rand[n3 % this.numRand], 100L, 0L) < 0.0) {
                    critical = true;
                    short maxCritDamage = chr.getStat().critical_damage;
                    short minCritDamage = chr.getStat().critical_damage;
                    int n4 = index;
                    index = (byte) (index + 1);
                    int criticalDamageRate = (int) this.RandomInRange(rand[n4 % this.numRand], maxCritDamage, minCritDamage);
                    realDamage += (double) criticalDamageRate / 100.0 * (double) ((int) realDamage);
                }
                realDamageList.add(new Pair<Long, Boolean>((long) realDamage, critical));
            }
        }
        return realDamageList;
    }

    public double RandomInRange(long randomNum, long maxDamage, long minDamage) {
        //java not have unsigned long, so i used BigInteger
        BigInteger ECX = new BigInteger("" + randomNum);//random number from Crand32::Random()
        BigInteger EAX = new BigInteger("1801439851");//0x6B5FCA6B; <= this is const
        //ECX * EAX = EDX:EAX (64bit register)
        BigInteger multipled = ECX.multiply(EAX);
        //get EDX from EDX:EAX
        long highBit = multipled.shiftRight(32).longValue();//get 32bit high
        long rightShift = highBit >>> 22;//SHR EDX,16
        double newRandNum = randomNum - (rightShift * 10000000.0);

        double value;
        if (minDamage != maxDamage) {
            if (minDamage > maxDamage) {//swap
                long temp = maxDamage;
                maxDamage = minDamage;
                minDamage = temp;
            }
            value = (maxDamage - minDamage) * newRandNum / 9999999.0 + minDamage;
        } else {
            value = maxDamage;
        }
        return value;
    }
}
