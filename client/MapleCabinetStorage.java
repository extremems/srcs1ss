/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import database.DatabaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import tools.ArrayMap;
import tools.FileoutputUtil;

/**
 *
 * @author 윤정환
 */
public class MapleCabinetStorage {
    private Map<Integer, MapleCabinet> storage;
    private int cid, size;
    private boolean changed;

    public MapleCabinetStorage(int cid) {
        storage = new ArrayMap<>();
        this.cid = cid;
        size = 0;
        changed = false;
    }
    
    public Map<Integer, MapleCabinet> getStorage() {
        return storage;
    }
    
    public void addMessage(MapleCabinet message) {
        storage.put(size++, message);
        changed = true;
    }
    
    public void removeMessage(int id) {
        storage.remove(id);
        changed = true;
    }
    
    public void loadFromDb() throws SQLException {
        Connection con = DatabaseConnection.getConnection();
        PreparedStatement ps = con.prepareStatement("SELECT * FROM cabinet WHERE cid = ?");
        ps.setInt(1, cid);
        ResultSet rs = ps.executeQuery();
        while (rs.next())
            storage.put(size++, new MapleCabinet(rs.getString("title"), rs.getString("content"), rs.getInt("itemId"), rs.getInt("quantity"), rs.getLong("expiredate")));

        rs.close();
        ps.close();
        con.close();
    }
    
    public void saveToDB(Connection con) throws SQLException {
        try {
            if (!changed)
                return;
            
            PreparedStatement ps = con.prepareStatement("DELETE FROM cabinet WHERE cid = ?");
            ps.setInt(1, cid);
            ps.executeUpdate();
            ps.close();

            for (MapleCabinet message : storage.values()) {
                ps = con.prepareStatement("INSERT INTO cabinet (cid, title, content, itemId, quantity, expiredate) VALUES (?, ?, ?, ?, ?, ?)");
                ps.setInt(1, cid);
                ps.setString(2, message.getTitle());
                ps.setString(3, message.getContent());
                ps.setInt(4, message.getItemId());
                ps.setInt(5, message.getQuantity());
                ps.setLong(6, message.getExpiredate());
                ps.executeUpdate();
                ps.close();
            }
            changed = false;
        } catch (Exception e) {
            FileoutputUtil.outputFileError(FileoutputUtil.PacketEx_Log, e);
            e.printStackTrace();
        }
       
    }
}
