/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

/**
 *
 * @author 펠로스
 */
public class MapleCabinet {
    private String title, content;
    private int itemId, quantity;
    private long expiredate;
    
    public MapleCabinet(String title, String content, int itemId, int quantity, long expiredate) {
        this.title = title;
        this.content = content;
        this.itemId = itemId;
        this.quantity = quantity;
        this.expiredate = expiredate;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public long getExpiredate() {
        return expiredate;
    }

    public void setExpiredate(long expiredate) {
        this.expiredate = expiredate;
    }
}
