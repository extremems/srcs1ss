package client.union;

import constants.GameConstants;
import database.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UnionList {

    private List<MapleUnion> unions;
    private String unionKey;

    public UnionList() {
        unions = new ArrayList<>();
    }

    public void loadFromDb(int accId) throws SQLException {
        Connection con = DatabaseConnection.getConnection();
        PreparedStatement ps = con.prepareStatement("SELECT u.unk1, u.unk3, c.name as unionname, c.id as id, c.job as unionjob, c.level as unionlevel, u.position, u.position2 FROM unions as u, characters as c WHERE c.id = u.id && c.accountid = ?");
        ps.setInt(1, accId);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            if (GameConstants.isZero(rs.getInt("unionjob"))) { // zero Exception
                if (rs.getInt("unionlevel") < 130) {
                    continue;
                }
            } else {
                if (rs.getInt("unionlevel") < 60) {
                    continue;
                }
            }
            unions.add(new MapleUnion(rs.getInt("id"), rs.getInt("unionlevel"), rs.getInt("unionjob"), rs.getInt("unk1"), rs.getInt("position2"), rs.getInt("position"), rs.getInt("unk3"), rs.getString("unionname")));
        }
        rs.close();
        ps.close();

        ps = con.prepareStatement("SELECT * FROM unionkey WHERE accid = ?");
        ps.setInt(1, accId);
        rs = ps.executeQuery();
        if (rs.next()) {
            unionKey = rs.getString("UnionKey");
        }
        rs.close();
        ps.close();
        con.close();
    }

    public void loadFromTransfer(List<MapleUnion> unions) {
        this.unions.addAll(unions);
    }

    public void savetoDB(Connection con, int accId) throws SQLException {

        for (MapleUnion union : unions) {
            PreparedStatement ps = con.prepareStatement("DELETE FROM unions WHERE id = ?");
            ps.setInt(1, union.getCharid());
            ps.executeUpdate();
            ps.close();

            ps = con.prepareStatement("INSERT INTO unions (id, unk1, unk3, position, position2) VALUES (?, ?, ?, ?, ?)");
            ps.setInt(1, union.getCharid());
            ps.setInt(2, union.getUnk1());
            ps.setInt(3, union.getUnk3());
            ps.setInt(4, union.getPosition());
            ps.setInt(5, union.getUnk2());
            ps.executeUpdate();
            ps.close();
        }
    }

    public void saveToKeyDB() {
        Connection con = null;
        PreparedStatement ps = null;
        PreparedStatement pse = null;
        ResultSet rs = null;
        int accid = -1;
        try {
            for (MapleUnion union : unions) {
                con = DatabaseConnection.getConnection();
                ps = con.prepareStatement("SELECT * FROM characters WHERE id = ?");
                ps.setInt(1, union.getCharid());
                rs = ps.executeQuery();
                if (rs.next()) {
                    accid = rs.getInt("accountid");
                    pse = con.prepareStatement("DELETE FROM unionkey WHERE accid = ?");
                    pse.setInt(1, accid);
                    pse.executeUpdate();
                    pse.close();
                }
                ps.close();
                rs.close();

                ps = con.prepareStatement("INSERT INTO unionkey (accid, UnionKey) VALUES (?, ?)");
                ps.setInt(1, accid);
                ps.setString(2, getUnionKey());
                ps.execute();
                ps.close();
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (pse != null) {
                    pse.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<MapleUnion> getUnions() {
        return unions;
    }

    public void setUnions(List<MapleUnion> unions) {
        this.unions = unions;
    }

    public String getUnionKey() {
        return unionKey;
    }

    public void setUnionKey(String key) {
        this.unionKey = key;
    }
}
