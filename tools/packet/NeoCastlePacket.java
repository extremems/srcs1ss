/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools.packet;

import client.MapleCharacter;
import handling.SendPacketOpcode;
import server.Randomizer;
import server.life.MapleMonster;
import tools.data.MaplePacketLittleEndianWriter;

public class NeoCastlePacket {

    public static byte[] NeoCoinUI() {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.STARDUST_UI.getValue());
        mplew.write(1);
        mplew.writeInt(100711);
        mplew.writeInt(100711);
        mplew.writeMapleAsciiString("UI/UIWindowEvent.img/2020neoCoin");

        mplew.writeLong(0);
        mplew.writeLong(0);
        mplew.write(false);
        mplew.writeLong(0);
        mplew.writeLong(PacketHelper.getTime(PacketHelper.MAX_TIME));
        return mplew.getPacket();
    }

    public static byte[] showOrgel(MapleMonster mob) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.SHOW_EFFECT.getValue());
        mplew.write(80);
        mplew.writeInt(80003023);
        mplew.writeInt(0);
        mplew.writePosInt(mob.getPosition());
        return mplew.getPacket();
    }

    public static byte[] gainOrgelPoint(MapleMonster mob) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(980);
        mplew.writePosInt(mob.getPosition());
        mplew.writeInt(6);
        mplew.writeInt(Randomizer.rand(5, 8));
        return mplew.getPacket();
    }

    public static byte[] ShowOrgelMessage(String msg) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DETAIL_SHOW_INFO.getValue());
        mplew.writeInt(3);
        mplew.writeInt(0x14);
        mplew.writeInt(0x14);
        mplew.writeInt(0);
        mplew.write(false);
        mplew.writeMapleAsciiString(msg);

        return mplew.getPacket();
    }

    public static byte[] OrgelEventStart(int type) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(1042);
        mplew.writeInt(80003023);
        mplew.writeInt(type);
        mplew.writeInt(20);
        return mplew.getPacket();
    }

    public static byte[] OrgelTime(int time, int type) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(1043);
        mplew.writeInt(80003023);
        mplew.writeInt(time);
        mplew.writeInt(type);
        return mplew.getPacket();
    }

    public static byte[] OrgelEventEnd() {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(1044);
        mplew.writeInt(80003023);
        return mplew.getPacket();
    }

    public static byte[] OrgelCount(int count) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(1045);
        mplew.writeInt(80003023);
        mplew.writeInt(count);
        return mplew.getPacket();
    }
}
