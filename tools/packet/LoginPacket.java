/*
 This file is part of the OdinMS Maple Story Server
 Copyright (C) 2008 ~ 2010 Patrick Huy <patrick.huy@frz.cc> 
 Matthias Butz <matze@odinms.de>
 Jan Christian Meyer <vimes@odinms.de>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License version 3
 as published by the Free Software Foundation. You may not use, modify
 or distribute this program under any other version of the
 GNU Affero General Public License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tools.packet;

import client.AvatarLook;
import client.MapleCharacter;
import client.MapleClient;
import constants.GameConstants;
import constants.JobConstants;
import constants.JobConstants.LoginJob;
import constants.ServerConstants;
import handling.SendPacketOpcode;
import handling.channel.ChannelServer;
import handling.login.LoginServer;
import tools.HexTool;
import tools.data.MaplePacketLittleEndianWriter;

import java.util.List;
import java.util.Map;
import java.util.Set;
import server.Randomizer;

public class LoginPacket {

    public static byte[] OnOpcodeEncryption(int nBlockSize, byte[] aBuffer) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(43);

        mplew.writeInt(nBlockSize);
        mplew.writeInt(aBuffer.length);
        mplew.write(aBuffer);
        return mplew.getPacket();
    }

    private static final String version;

    static {
        int ret = 0;
        ret ^= (ServerConstants.MAPLE_VERSION & 0x7FFF);
        ret ^= (1 << 15);
        ret ^= ((ServerConstants.MAPLE_PATCH & 0xFF) << 16);
        version = String.valueOf(ret);
    }

    public static final byte[] initializeConnection(final short mapleVersion, final byte[] sendIv, final byte[] recvIv, final boolean ingame) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        int ret = 0;
        ret ^= (mapleVersion & 0x7FFF);
        ret ^= (ServerConstants.check << 15);
        ret ^= ((ServerConstants.MAPLE_PATCH & 0xFF) << 16);
        String version = String.valueOf(ret);

        int packetsize = ingame ? 15 : 49;

        mplew.writeShort(packetsize);

        if (!ingame) {
            mplew.writeShort(291);
            mplew.writeMapleAsciiString(version);
            mplew.write(recvIv);
            mplew.write(sendIv);
            mplew.write(1); // locale
            mplew.write(0); // single thread loading
        }

        mplew.writeShort(291);
        mplew.writeInt(mapleVersion);
        mplew.write(recvIv);
        mplew.write(sendIv);
        mplew.write(1); // locale

        if (!ingame) {
            mplew.writeInt(mapleVersion * 100 + ServerConstants.MAPLE_PATCH); //1.2.342
            mplew.writeInt(mapleVersion * 100 + ServerConstants.MAPLE_PATCH); //1.2.342
            mplew.writeInt(0); // unknown
            mplew.write(false);
            mplew.write(false);
        }
        return mplew.getPacket();
    }

    public static final byte[] getHotfix() {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.HOTFIX.getValue());
        mplew.write(0);

        return mplew.getPacket();
    }

    public static final byte[] WHITE_BACKGROUND_LODING() {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.WHITE_BACKGROUND_LODING.getValue());
        mplew.write(0);
        return mplew.getPacket();
    }

    public static final byte[] SessionCheck(int value) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter(6);

        mplew.writeShort(SendPacketOpcode.SESSION_CHECK.getValue());
        mplew.writeInt(value);

        return mplew.getPacket();
    }

    public static final byte[] HackShield() {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter(3);

        mplew.writeShort(SendPacketOpcode.HACKSHIELD.getValue());
        mplew.write(1);
        mplew.write(0); // 324 ++

        return mplew.getPacket();
    }

    public static final byte[] EnableLogin() {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter(3);

        mplew.writeShort(SendPacketOpcode.ENABLE_LOGIN.getValue());

        return mplew.getPacket();
    }

    public static final byte[] getPing() {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter(2);

        mplew.writeShort(SendPacketOpcode.PING.getValue());

        return mplew.getPacket();
    }

    public static final byte[] getAuthSuccessRequest(MapleClient client, final String id, String pwd) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.LOGIN_STATUS.getValue());
        mplew.write(0);
        mplew.writeShort(0); //351++
        mplew.writeMapleAsciiString(id);
        mplew.writeLong(-1); // 324 ++, Auction에도 쓰임 1090907166
        mplew.writeInt(client.getAccID());
        mplew.write(/*client.isGm() ? 1 : */0); // Admin byte - Find, Trade, etc.
        mplew.writeInt(client.isGm() ? 0xA2030 : 0); // gm flag
        mplew.writeInt(/*client.getVIPGrade() */0); // MVP Grade
        mplew.writeInt(20); //  nAge
        mplew.write(/*client.purchaseExp()*/0); //nPurchaseExp
        mplew.write(client.getChatBlockedTime() > 0 ? 1 : 0); //ChatBlockReason
        mplew.writeLong(client.getChatBlockedTime()); //ChatUnBlockDate
        mplew.write(0); //고정값!
        mplew.writeMapleAsciiString(id); // pw
        mplew.write(0);
        mplew.writeMapleAsciiString(pwd);
        mplew.write(JobConstants.enableJobs);
        if (JobConstants.enableJobs) {
            mplew.write(JobConstants.jobOrder); //Job Order (orders are located in wz)

            for (LoginJob j : LoginJob.values()) { // 개수 1개 적음.
                mplew.write(j.getFlag());
                mplew.writeShort(j.getFlag()); //2 = 레벨 제한
            }
        }
        mplew.write(0);
        mplew.writeInt(-1);

        return mplew.getPacket();
    }

    public static final byte[] getLoginFailed(final int reason) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter(16);

        mplew.writeShort(SendPacketOpcode.LOGIN_STATUS.getValue());
        mplew.writeInt(reason);
        mplew.writeShort(0);

        return mplew.getPacket();
    }

    public static final byte[] getPermBan(final byte reason) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter(16);

        mplew.writeShort(SendPacketOpcode.LOGIN_STATUS.getValue());
        mplew.writeShort(2); // Account is banned
        mplew.writeInt(0);
        mplew.writeShort(reason);
        mplew.write(HexTool.getByteArrayFromHexString("01 01 01 01 00"));

        return mplew.getPacket();
    }

    public static final byte[] getTempBan(final long timestampTill, final byte reason) {

        final MaplePacketLittleEndianWriter w = new MaplePacketLittleEndianWriter(17);

        w.writeShort(SendPacketOpcode.LOGIN_STATUS.getValue());
        w.write(2);
        w.write(HexTool.getByteArrayFromHexString("00 00 00 00 00"));
        w.write(reason);
        w.writeLong(timestampTill); // Tempban date is handled as a 64-bit long, number of 100NS intervals since 1/1/1601. Lulz.

        return w.getPacket();
    }

    public static final byte[] deleteCharResponse(final int cid, final int state) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DELETE_CHAR_RESPONSE.getValue());
        mplew.writeInt(cid);
        mplew.write(state);
        if (state == 69) {
            mplew.write(0);
            mplew.write(0);
            mplew.write(0);
            mplew.write(0);
            mplew.writeInt(0); // > 0 -> BUffer(16);
        } else if (state == 71) {
            mplew.write(0);
        }
        mplew.write(0); // 307++
        mplew.write(0); // 307++

        return mplew.getPacket();
    }

    public static final byte[] secondPwError(final byte mode) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter(3);

        mplew.writeShort(SendPacketOpcode.SECONDPW_ERROR.getValue());
        mplew.write(mode);
        return mplew.getPacket();
    }

    public static byte[] enableRecommended() {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.ENABLE_RECOMMENDED.getValue());
        mplew.writeInt(0); //worldID with most characters
        return mplew.getPacket();
    }

    public static byte[] sendRecommended(int world, String message) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.SEND_RECOMMENDED.getValue());
        mplew.write(message != null ? 1 : 0); //amount of messages
        if (message != null) {
            mplew.writeInt(world);
            mplew.writeMapleAsciiString(message);
        }
        return mplew.getPacket();
    }

    public static final byte[] getServerList(final int serverId, final Map<Integer, Integer> channelLoad) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVERLIST.getValue());
        mplew.write(serverId);
        final String worldName = LoginServer.getServerName(); // remove the SEA
        mplew.writeMapleAsciiString(worldName);
        mplew.writeInt(0);
        mplew.writeInt(0);
        mplew.write(0);
        mplew.write(0);
        mplew.write(LoginServer.getFlag());
        mplew.writeMapleAsciiString(LoginServer.getEventMessage());
        int lastChannel = 1;
        Set<Integer> channels = channelLoad.keySet();
        for (int i = 30; i > 0; i--) {
            if (channels.contains(i)) {
                lastChannel = i;
                break;
            }
        }
        mplew.write(lastChannel);
        int load;
        for (int i = 1; i <= lastChannel; i++) {
            if (ChannelServer.getInstance(i) != null) {
                load = Math.max(1, ChannelServer.getInstance(i).getPlayerStorage().getAllCharacters().size());
            } else {
                load = 1;
            }
            mplew.writeMapleAsciiString(worldName + ((i == 1) ? "-" + i : ((i == 2) ? "-20세이상" : "-" + (i - 1))));
            mplew.writeInt(load);
            mplew.write(serverId);
            mplew.write(i - 1);
            mplew.write(i == 2 ? 1 : 0);// i == 2 ? 1 : 0); //20세 이상 입장.
        }
        mplew.writeShort(0); // size: (short x, short y, string msg)
        mplew.writeInt(0); // Offset?
        mplew.write(0); // 키면 버튼 빛남
        mplew.write(0); // 324++
        mplew.write(0); // 332++
        return mplew.getPacket();
    }

    public static final byte[] LeavingTheWorld() {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.LEAVING_WORLD.getValue());
        mplew.write(3);
        mplew.writeMapleAsciiString("main");
        mplew.write(1);
        mplew.writeZeroBytes(8);
        mplew.writeMapleAsciiString("sub");
        mplew.writeZeroBytes(9);
        mplew.writeMapleAsciiString("sub_2");
        mplew.writeZeroBytes(9);
        mplew.write(1);
        return mplew.getPacket();
    }

    public static final byte[] getEndOfServerList() {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVERLIST.getValue());
        mplew.write(0xFF);
        /* 1.2.287 광고 추가 */
        int advertisement = 0; //1.2.287(1) 기준.
        mplew.write(advertisement);
        for (int i = 0; i < advertisement; i++) {
            mplew.writeMapleAsciiString("");//http://maplestory.nexon.com/MapleStory/news/2016/login_Banner.html"); //띄울 이미지.
            mplew.writeMapleAsciiString("");//http://maplestory.nexon.com/MapleStory/news/2016/login_Banner.html"); //이동할 주소.
            mplew.writeInt(5000); // 시간
            mplew.writeInt(310); // width
            /* 광고 width, weight 고정 */
            mplew.writeInt(60); // height
            mplew.writeInt(235); // x
            mplew.writeInt(538); // y
        }
        /* 1.2.287 광고 추가 */
        mplew.writeShort(0); // NotActiveAccountDlgFocus
        mplew.writeLong(-1); // lockAccount Connection count.

        return mplew.getPacket();
    }

    public static final byte[] getServerStatus(final int status) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SERVERSTATUS.getValue());
        mplew.writeShort(status);

        return mplew.getPacket();
    }

  public static final byte[] getCharList(final String secondpw, final List<MapleCharacter> chars, int charslots, byte nameChange) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CHARLIST.getValue());
        mplew.write(0);
        mplew.writeMapleAsciiString("");
        mplew.writeInt(1);
        mplew.writeInt(1);
        mplew.writeInt(charslots); //trunkchar slotcount
        mplew.write(0);
        mplew.write(0); // 324 ++
        mplew.writeInt(0); //캐릭터 지워질때 유령되는 패킷ㅇㅇ
        mplew.writeLong(PacketHelper.getKoreanTimestamp(System.currentTimeMillis())); //v216
        mplew.write(0); //IsEditedList
        mplew.writeInt(chars.size());

        for (final MapleCharacter chr : chars) {
            mplew.writeInt(chr.getId());
        }

        mplew.write(chars.size());

        for (final MapleCharacter chr : chars) {
            addCharEntry(mplew, chr, !chr.isGM() && chr.getLevel() >= 30, false);
        }

        mplew.write(secondpw != null && secondpw.length() > 0 ? 1 : (secondpw != null && secondpw.length() <= 0 ? 2 : 0)); // second pw request
        mplew.write(0); // 2차 비번 여부 
        mplew.write(1); // 캐릭터 생성 2차비번체크여부 1 = 활성화, 0 = 비활성
        mplew.writeInt(charslots);

        mplew.writeInt(0); // 더블 클릭 UI 활성화 캐릭터 사이즈.
        mplew.writeInt(-1); // 신캐 이벤트. 카이저/엔버/듀얼블레이드/메카닉/데몬
        mplew.writeLong(PacketHelper.getKoreanTimestamp(System.currentTimeMillis()));
        mplew.writeShort(0);
        mplew.writeShort(1);
        mplew.writeInt(0); //273 ++
        mplew.writeInt(2); //273 ++
        mplew.writeInt(0); //332 ++
        return mplew.getPacket();
    }
  
    public static final byte[] addNewCharEntry(final MapleCharacter chr, final boolean worked) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ADD_NEW_CHAR_ENTRY.getValue());
        mplew.write(worked ? 0 : 1);
        mplew.writeInt(0);
        addCharEntry(mplew, chr, false, false);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static final byte[] charNameResponse(final String charname, final boolean nameUsed) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CHAR_NAME_RESPONSE.getValue());
        mplew.writeMapleAsciiString(charname);
        mplew.write(nameUsed ? 1 : 0);

        return mplew.getPacket();
    }

    private static final void addCharEntry(final MaplePacketLittleEndianWriter mplew, final MapleCharacter chr, boolean ranking, boolean viewAll) {
        PacketHelper.addCharStats(mplew, chr);
        mplew.writeInt(0); //273 ++
        mplew.writeInt(0);
        mplew.writeLong(0);
        mplew.writeInt(0);
        if (GameConstants.isZero(chr.getJob())) {
            byte gender = chr.getGender(), secondGender = chr.getSecondGender();
            chr.setGender((byte) 0);
            chr.setSecondGender((byte) 1);
            AvatarLook.encodeAvatarLook(mplew, chr, true, false);

            chr.setGender((byte) 1);
            chr.setSecondGender((byte) 0);
            AvatarLook.encodeAvatarLook(mplew, chr, true, false);

            chr.setGender(gender);
            chr.setSecondGender(secondGender);

        } else {
            AvatarLook.encodeAvatarLook(mplew, chr, true, false);
        }
    }

    public static final byte[] getSecondPasswordConfirm(byte op) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.AUTH_STATUS_WITH_SPW.getValue());
        mplew.write(op); // 0x48 "오늘은 더이상 캐릭터를 생성할 수 없습니다."
        if (op == 0) {
            // Jobs
            mplew.write(JobConstants.enableJobs ? 1 : 0); //toggle
            mplew.write(JobConstants.jobOrder); //Job Order (orders are located in wz)
            for (LoginJob j : LoginJob.values()) {
                mplew.write(j.getFlag());
                mplew.writeShort(j.getFlag()); //2 = 레벨 제한
            }
            // End of Jobs
        }
        return mplew.getPacket();
    }

    public static byte[] NewSendPasswordWay(MapleClient c) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.NEW_PASSWORD_CHECK.getValue());
        mplew.write(c.getSecondPassword() != null ? 1 : 0);
        mplew.write(0);

        return mplew.getPacket();
    }

    public static final byte[] getSecondPasswordResult(final boolean success) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.AUTH_STATUS_WITH_SPW_RESULT.getValue());
        mplew.write(success ? 0 : 0x14);
        return mplew.getPacket();
    }

    public static final byte[] MapleExit() {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MAPLE_EXIT.getValue());
        return mplew.getPacket();
    }

    public static byte[] ChannelBackImg(boolean isSunday) {
        return ChannelBackImg(isSunday, false);
    }

    public static byte[] ChannelBackImg(boolean isSunday, boolean event) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CHANNEL_BACK_IMG.getValue());
        mplew.write(1);
        if (isSunday && event) {
            mplew.writeMapleAsciiString("Lara");
        } else {
            mplew.writeMapleAsciiString("Lara");
        }
        mplew.writeInt(1);
        mplew.writeInt(0);
        mplew.write(0);
        return mplew.getPacket();
    }

    public static byte[] getSelectedChannelFailed(byte data, int ch) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.SELECT_CHANNEL_LIST.getValue());
        mplew.write(data);
        mplew.writeInt(ch);
        mplew.writeInt(0);
        mplew.writeInt(-1); // 332++
        return mplew.getPacket();
    }

    public static byte[] getSelectedChannelResult(int ch) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.SELECT_CHANNEL_LIST.getValue());
        mplew.write(0);
        mplew.writeInt(ch);
        mplew.writeInt(0);
        mplew.writeInt(-1); // 332++    
        return mplew.getPacket();
    }

    public static byte[] getSelectedWorldResult(int world) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.SELECTED_WORLD.getValue());
        mplew.writeInt(world);
        return mplew.getPacket();
    }

    public static final byte[] getKeyGuardResponse(String Key) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.LOG_OUT.getValue());
        mplew.writeMapleAsciiString(Key);
        return mplew.getPacket();
    }

    public static final byte[] getAuthSuccessRequest(final MapleClient client) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.LOGIN_STATUS.getValue());
        mplew.write(0);
        mplew.writeMapleAsciiString(client.getAccountName()); //sID
        mplew.writeLong(0); // 324 ++
        mplew.writeInt(client.getAccID());//dwAccountID
        mplew.write(client.getGender()); //Gender
        mplew.write(client.isGm() ? 1 : 0); //Admin byte
        mplew.writeInt(0); // admin flag
        mplew.writeInt(0); // vip grade
        mplew.writeInt(0); // nBlockReason
        mplew.write(client.getChatBlockedTime() > 0 ? 1 : 0); //pBlockReason.Interface //ChatBlockReason
        mplew.writeLong(client.getChatBlockedTime()); //ChatUnBlockDate
        if (true) { // NexonLogin
            mplew.write(1);
        } else {
            mplew.write(0);
            mplew.writeMapleAsciiString(client.getAccountName());
        }
        mplew.write(0);
        mplew.writeMapleAsciiString("");
        mplew.write(JobConstants.enableJobs);
        if (JobConstants.enableJobs) {
            mplew.write(JobConstants.jobOrder); //Job Order (orders are located in wz)

            for (LoginJob j : LoginJob.values()) { // 개수 1개 적음.
                mplew.write(j.getFlag());
                mplew.writeShort(j.getFlag()); //2 = 레벨 제한
            }
        }
        mplew.write(0);
        mplew.writeInt(-1);
        return mplew.getPacket();
    }

    public static final byte[] getCharEndRequest(final MapleClient client, String Acc, String Pwd, boolean Charlist) {
        final MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CHAR_END_REQUEST.getValue());
        mplew.write(0);
        mplew.writeInt(client.getAccID());
        mplew.write(client.getGender());
        mplew.write(client.isGm() ? 1 : 0); // Admin byte
        mplew.writeZeroBytes(21);
        mplew.writeMapleAsciiString(Pwd); //패스워드.
        mplew.writeMapleAsciiString(Acc); //게임 아이디.
        mplew.writeShort(0);
        mplew.write(1);

        mplew.write(0x20); //1.2.250+
        for (int i = 0; i < 32; i++) {
            mplew.write(1);
            mplew.writeShort(1);
        }
        mplew.writeInt(-1);
        mplew.writeShort(Charlist ? 1 : 0);

        return mplew.getPacket();
    }
}
