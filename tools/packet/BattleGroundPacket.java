/*     */ package tools.packet;
/*     */ 
/*     */ import client.MapleCharacter;
/*     */ import constants.GameConstants;
/*     */ import handling.SendPacketOpcode;
/*     */ import java.awt.Point;
/*     */ import java.util.List;
/*     */ import server.events.MapleBattleGroundCharacter;
/*     */ import server.events.MapleBattleGroundMobInfo;
/*     */ import server.life.MapleMonster;
/*     */ import tools.Triple;
/*     */ import tools.data.MaplePacketLittleEndianWriter;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BattleGroundPacket
/*     */ {
/*     */   public static byte[] UpgradeMainSkill(MapleBattleGroundCharacter chr) {
/*  29 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/*  30 */     mplew.writeShort(SendPacketOpcode.BATTLEGROUND_UPGRADE_SKILL.getValue());
/*  31 */     mplew.writeInt(5);
/*  32 */     mplew.writeInt(chr.getAttackUp());
/*  33 */     mplew.writeInt(460);
/*  34 */     mplew.writeInt(100 + chr.getHpMpUp());
/*  35 */     mplew.writeInt(460);
/*  36 */     mplew.writeInt(200 + chr.getCriUp());
/*  37 */     mplew.writeInt(460);
/*  38 */     mplew.writeInt(300 + chr.getSpeedUp());
/*  39 */     mplew.writeInt(460);
/*  40 */     mplew.writeInt(400 + chr.getRegenUp());
/*  41 */     mplew.writeInt(460);
/*  42 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] UpgradeSkillEffect(MapleBattleGroundCharacter chr, int skillid) {
/*  46 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/*  47 */     mplew.writeShort(SendPacketOpcode.BATTLEGROUND_UPGRADE_EFFECT.getValue());
/*  48 */     mplew.writeInt(chr.getId());
/*  49 */     mplew.writeInt(skillid);
/*  50 */     mplew.writeInt(840);
/*  51 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] AvaterSkill(MapleBattleGroundCharacter chr, int type) {
/*  55 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/*  56 */     mplew.writeShort(SendPacketOpcode.USE_SKILL_WITH_UI.getValue());
/*  57 */     mplew.writeInt(type);
/*  58 */     mplew.writeInt(type);
/*  59 */     mplew.write(0);
/*  60 */     mplew.writeInt(chr.getSkillList().size());
/*  61 */     for (Triple<Integer, Integer, Integer> skill : (Iterable<Triple<Integer, Integer, Integer>>)chr.getSkillList()) {
/*  62 */       mplew.write(((Integer)skill.getLeft()).intValue());
/*  63 */       mplew.writeInt(((Integer)skill.getMid()).intValue());
/*  64 */       mplew.writeInt(((Integer)skill.getRight()).intValue());
/*  65 */       mplew.writeInt(0);
/*  66 */       mplew.writeInt(0);
/*  67 */       mplew.writeInt(0);
/*  68 */       mplew.writeInt(0);
/*  69 */       mplew.write(0);
/*  70 */       mplew.writeInt(5000);
/*  71 */       mplew.write(0);
/*  72 */       mplew.write(0);
/*     */     } 
/*  74 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] ChangeAvater(MapleBattleGroundCharacter chr, int type) {
/*  78 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/*  79 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_CHANGE_AVATER.getValue());
/*  80 */     mplew.writeInt(3);
/*  81 */     mplew.writeInt(chr.getId());
/*  82 */     mplew.writeInt(0);
/*  83 */     mplew.writeInt(chr.getExp());
/*  84 */     mplew.writeInt(chr.getMoney());
/*  85 */     mplew.writeInt(0);
/*  86 */     mplew.writeInt(0);
/*  87 */     mplew.writeInt(0);
/*  88 */     mplew.writeInt(0);
/*  89 */     mplew.writeInt(type);
/*  90 */     mplew.writeInt(chr.getKill());
/*  91 */     mplew.writeInt(chr.getDeath());
/*  92 */     mplew.write(0);
/*  93 */     mplew.writeInt(chr.getTeam());
/*  94 */     mplew.writeInt(chr.getJobType());
/*  95 */     mplew.writeInt(chr.getLevel());
/*  96 */     mplew.writeInt(chr.getMindam());
/*  97 */     mplew.writeInt(chr.getMaxdam());
/*  98 */     mplew.writeInt(chr.getAttackSpeed());
/*  99 */     mplew.writeInt(0);
/* 100 */     mplew.writeInt(0);
/* 101 */     mplew.writeInt(chr.getCritical());
/* 102 */     mplew.writeInt(chr.getHpRegen());
/* 103 */     mplew.writeInt(chr.getMp());
/* 104 */     mplew.writeInt(chr.getMpRegen());
/* 105 */     mplew.writeInt(chr.getMp());
/* 106 */     mplew.writeInt(chr.getSpeed());
/* 107 */     mplew.writeInt(chr.getJump());
/* 108 */     mplew.writeInt(chr.getMaxHp());
/* 109 */     mplew.writeInt(chr.getMaxMp());
/* 110 */     mplew.writeInt(0);
/* 111 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] UpdateAvater(MapleBattleGroundCharacter chr, int type) {
/* 115 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 116 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_UPDATE_AVATER.getValue());
/* 117 */     mplew.writeInt(chr.getId());
/* 118 */     mplew.writeInt(13);
/* 119 */     mplew.writeInt(chr.getId());
/* 120 */     mplew.writeInt(0);
/* 121 */     mplew.writeInt(chr.getExp());
/* 122 */     mplew.writeInt(chr.getMoney());
/* 123 */     mplew.writeInt(0);
/* 124 */     mplew.writeInt(0);
/* 125 */     mplew.writeInt(0);
/* 126 */     mplew.writeInt(chr.getChr().getSkillCustomValue0(80001741));
/* 127 */     mplew.writeInt(type);
/* 128 */     mplew.writeInt(chr.getKill());
/* 129 */     mplew.writeInt(chr.getDeath());
/* 130 */     mplew.write(1);
/* 131 */     mplew.writeInt(chr.getTeam());
/* 132 */     mplew.writeInt(chr.getJobType());
/* 133 */     mplew.writeInt(chr.getLevel());
/* 134 */     mplew.writeInt(chr.getMindam());
/* 135 */     mplew.writeInt(chr.getMaxdam());
/* 136 */     mplew.writeInt(chr.getAttackSpeed());
/* 137 */     mplew.writeInt(0);
/* 138 */     mplew.writeInt(0);
/* 139 */     mplew.writeInt(chr.getCritical());
/* 140 */     mplew.writeInt(chr.getHpRegen());
/* 141 */     mplew.writeInt(chr.getMp());
/* 142 */     mplew.writeInt(chr.getMpRegen());
/* 143 */     mplew.writeInt(chr.getMp());
/* 144 */     mplew.writeInt(chr.getSpeed());
/* 145 */     mplew.writeInt(chr.getJump());
/* 146 */     mplew.writeInt(chr.getMaxHp());
/* 147 */     mplew.writeInt(chr.getMaxMp());
/* 148 */     mplew.writeInt(0);
/* 149 */     mplew.write(0);
/* 150 */     mplew.write(0);
/* 151 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] AttackSkill(MapleCharacter chr, Point oldpos, Point newpos, int skillid, int attackimg, int delay, int delay2, int imgafter, int speed, int left, int range, List<Integer> mob, int moving) {
/* 155 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 156 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_ATTACK.getValue());
/* 157 */     mplew.writeInt(chr.getId());
/* 158 */     mplew.writeInt(skillid);
/* 159 */     mplew.write(1);
/* 160 */     mplew.writeInt(attackimg);
/* 161 */     mplew.writeInt(delay);
/* 162 */     mplew.writeInt(delay2);
/* 163 */     mplew.writeInt(moving);
/* 164 */     mplew.writeInt(1);
/* 165 */     mplew.writeInt(imgafter);
/* 166 */     mplew.writeInt((skillid == 80003009) ? 300 : 0);
/* 167 */     mplew.writeInt(mob.isEmpty() ? 1 : mob.size());
/* 168 */     if (mob.isEmpty()) {
/* 169 */       mplew.writeInt(chr.getSkillCustomValue0(156789));
/* 170 */       mplew.write(speed);
/* 171 */       mplew.write((skillid == 80001739) ? 2 : 0);
/* 172 */       mplew.write(left);
/* 173 */       mplew.writeInt(1);
/* 174 */       mplew.writePosInt(oldpos);
/* 175 */       mplew.writePosInt(newpos);
/* 176 */       Point bpos = new Point(newpos.x + ((left == 0) ? range : -range), newpos.y);
/* 177 */       if (skillid == 80001736) {
/* 178 */         bpos = new Point(600, 500);
/* 179 */       } else if (skillid == 80002678) {
/* 180 */         bpos = new Point(newpos.x + ((left == 0) ? range : -range), newpos.y + 75);
/*     */       } 
/* 182 */       mplew.writePosInt(bpos);
/* 183 */       mplew.writeInt((skillid == 80001736) ? 15 : 0);
/* 184 */       mplew.writeInt((skillid == 80001736) ? 15 : 0);
/* 185 */       mplew.writeInt(0);
/* 186 */       mplew.writeInt(0);
/* 187 */       mplew.writeInt(0);
/* 188 */       mplew.writeInt(0);
/* 189 */       mplew.writeInt(0);
/* 190 */       mplew.writeInt(0);
/* 191 */       int rangespeed = 0;
/* 192 */       if (range > 0) {
/* 193 */         rangespeed = 100;
/* 194 */         if (skillid == 80001736 || skillid == 80002339 || skillid == 80003003) {
/* 195 */           rangespeed = 40;
/* 196 */         } else if (skillid == 80001735) {
/* 197 */           rangespeed = 80;
/* 198 */         } else if (skillid == 80001664) {
/* 199 */           rangespeed = 5;
/*     */         } 
/*     */       } 
/* 202 */       mplew.writeInt(rangespeed);
/* 203 */       mplew.writeInt(0);
/* 204 */       mplew.writeInt((skillid == 80001736) ? 1 : 0);
/* 205 */       mplew.writeInt(0);
/*     */     } else {
/* 207 */       for (Integer mobs : mob) {
/* 208 */         mplew.writeInt(chr.getSkillCustomValue0(156789));
/* 209 */         mplew.write(speed);
/* 210 */         mplew.write((chr.getMap().getCharacter(mobs.intValue()) != null) ? 1 : ((skillid == 80001650 || skillid == 80001739) ? 2 : 0));
/* 211 */         mplew.write(left);
/* 212 */         mplew.writeInt(mobs.intValue());
/* 213 */         MapleMonster monster = chr.getMap().getMonsterByOid(mobs.intValue());
/* 214 */         if (skillid == 80001650) {
/* 215 */           mplew.writePosInt(oldpos);
/* 216 */           mplew.writePosInt(oldpos);
/* 217 */           mplew.writePosInt(newpos);
/* 218 */         } else if (skillid == 80001739) {
/* 219 */           if (monster == null) {
/* 220 */             MapleCharacter dchr = chr.getMap().getCharacter(mobs.intValue());
/* 221 */             mplew.writePosInt(new Point((dchr.getPosition()).x, (dchr.getPosition()).y));
/* 222 */             mplew.writePosInt(new Point((dchr.getPosition()).x, (dchr.getPosition()).y - 10));
/* 223 */             mplew.writePosInt(new Point((dchr.getPosition()).x, (dchr.getPosition()).y));
/*     */           } else {
/* 225 */             mplew.writePosInt(new Point((monster.getPosition()).x, (monster.getPosition()).y));
/* 226 */             mplew.writePosInt(new Point((monster.getPosition()).x, (monster.getPosition()).y - 10));
/* 227 */             mplew.writePosInt(new Point((monster.getPosition()).x, (monster.getPosition()).y));
/*     */           } 
/*     */         } else {
/* 230 */           mplew.writePosInt(oldpos);
/* 231 */           if (monster != null) {
/* 232 */             mplew.writePosInt(monster.getPosition());
/*     */           } else {
/* 234 */             mplew.writePosInt(newpos);
/*     */           } 
/* 236 */           Point bpos = new Point(newpos.x + ((left == 0) ? range : -range), newpos.y);
/* 237 */           mplew.writePosInt(oldpos);
/*     */         } 
/* 239 */         mplew.writeInt(0);
/* 240 */         mplew.writeInt(0);
/* 241 */         mplew.writeInt(0);
/* 242 */         mplew.writeInt(0);
/*     */         
/* 244 */         mplew.writeInt((skillid == 80001739) ? 216352990 : 0);
/* 245 */         mplew.writeInt(0);
/* 246 */         mplew.writeInt(0);
/* 247 */         mplew.writeInt(0);
/* 248 */         mplew.writeInt(0);
/* 249 */         mplew.writeInt(0);
/* 250 */         mplew.writeInt(0);
/* 251 */         mplew.writeInt(0);
/* 252 */         chr.setSkillCustomInfo(156789, chr.getSkillCustomValue0(156789) + 1, 0);
/*     */       } 
/*     */     } 
/* 255 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] AttackSkillStack(MapleCharacter chr, Point oldpos, Point newpos, int skillid, int attackimg, int delay, int delay2, int imgafter, int speed, int left, int range, int stack) {
/* 259 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 260 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_ATTACK.getValue());
/* 261 */     mplew.writeInt(chr.getId());
/* 262 */     mplew.writeInt(skillid);
/* 263 */     mplew.write(1);
/* 264 */     mplew.writeInt(attackimg);
/* 265 */     mplew.writeInt(delay);
/* 266 */     mplew.writeInt(delay2);
/* 267 */     mplew.writeInt(0);
/* 268 */     mplew.writeInt(stack);
/* 269 */     for (int i = 0; i < stack; i++) {
/* 270 */       mplew.writeInt((skillid == 80003006 && i == 1) ? (imgafter + 450) : imgafter);
/* 271 */       mplew.writeInt((skillid == 80003006 && i == 0) ? 30 : ((skillid == 80003006 && i == 1) ? 480 : 90));
/* 272 */       mplew.writeInt(1);
/* 273 */       mplew.writeInt(chr.getSkillCustomValue0(156789));
/* 274 */       mplew.write(speed);
/* 275 */       mplew.write(0);
/* 276 */       mplew.write(left);
/* 277 */       mplew.writeInt(0);
/* 278 */       mplew.writePosInt(oldpos);
/* 279 */       mplew.writePosInt(newpos);
/* 280 */       Point bpos = new Point(newpos.x + ((left == 0) ? range : -range), newpos.y);
/* 281 */       if (skillid == 80002680) {
/* 282 */         if (i == 0) {
/* 283 */           bpos.y -= 157;
/* 284 */         } else if (i == 2) {
/* 285 */           bpos.y += 157;
/*     */         } 
/* 287 */       } else if (skillid != 80003006 && skillid != 80001741) {
/* 288 */         if (i == 0) {
/* 289 */           bpos.y += 190;
/* 290 */         } else if (i == 1) {
/* 291 */           bpos.y += 95;
/* 292 */         } else if (i == 3) {
/* 293 */           bpos.y -= 95;
/* 294 */         } else if (i == 4) {
/* 295 */           bpos.y -= 190;
/*     */         } 
/*     */       } 
/* 298 */       mplew.writePosInt(bpos);
/* 299 */       mplew.writeInt(0);
/* 300 */       mplew.writeInt(0);
/* 301 */       mplew.writeInt(0);
/* 302 */       mplew.writeInt(0);
/* 303 */       mplew.writeInt(0);
/* 304 */       mplew.writeInt(0);
/* 305 */       mplew.writeInt((skillid == 80001741) ? ((left == 0) ? (-1 * (276 + i * 276)) : (276 + i * 276)) : 0);
/* 306 */       mplew.writeInt((skillid == 80001741) ? ((left == 0) ? (-1 * (55 + i * 55)) : (55 + i * 55)) : 0);
/* 307 */       int rangespeed = 0;
/* 308 */       if (range > 0) {
/* 309 */         rangespeed = 100;
/* 310 */         if (skillid == 80001736 || skillid == 80002339 || skillid == 80003003) {
/* 311 */           rangespeed = 40;
/* 312 */         } else if (skillid == 80001735 || skillid == 80003006) {
/* 313 */           rangespeed = 80;
/* 314 */         } else if (skillid == 80001664) {
/* 315 */           rangespeed = 5;
/* 316 */         } else if (skillid == 80001741) {
/* 317 */           rangespeed = 55;
/*     */         } 
/*     */       } 
/* 320 */       mplew.writeInt(rangespeed);
/* 321 */       mplew.writeInt((skillid == 80001741) ? ((left == 0) ? -20 : 20) : 0);
/* 322 */       mplew.writeInt((skillid == 80001741) ? ((left == 0) ? -10 : 10) : 0);
/* 323 */       mplew.writeInt(0);
/* 324 */       chr.setSkillCustomInfo(156789, chr.getSkillCustomValue0(156789) + 1, 0);
/*     */     } 
/*     */     
/* 327 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] MoveAttack(MapleCharacter chr, Point pos, Point pos1, int attackcount) {
/* 331 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 332 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_MOVE_ATTACK.getValue());
/* 333 */     mplew.writeInt(chr.getId());
/* 334 */     mplew.writeInt(attackcount);
/* 335 */     mplew.writeInt(4);
/* 336 */     mplew.write(0);
/* 337 */     mplew.write(0);
/* 338 */     mplew.write(0);
/* 339 */     mplew.writePosInt(pos);
/* 340 */     mplew.writePosInt(pos1);
/* 341 */     Point pos2 = new Point(800, 600);
/* 342 */     mplew.writePosInt(pos2);
/* 343 */     mplew.writeInt(15);
/* 344 */     mplew.writeInt(15);
/* 345 */     mplew.writeInt(0);
/* 346 */     mplew.writeInt(0);
/* 347 */     mplew.writeInt(0);
/* 348 */     mplew.writeInt(0);
/* 349 */     mplew.writeInt(0);
/* 350 */     mplew.writeInt(0);
/* 351 */     mplew.writeInt(40);
/* 352 */     mplew.writeInt(0);
/* 353 */     mplew.writeInt(1);
/* 354 */     mplew.writeInt(0);
/* 355 */     mplew.writeInt(1);
/* 356 */     return mplew.getPacket();
/*     */   }
/*     */ 
/*     */   
/*     */   public static byte[] AttackRefresh(MapleCharacter chr, int stype, int count, int type, int... args) {
/* 361 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 362 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_ATTACK_REFRESH.getValue());
/* 363 */     mplew.writeInt(chr.getId());
/* 364 */     mplew.writeInt(stype);
/* 365 */     if (stype == 1) {
/* 366 */       mplew.writeInt(count);
/* 367 */       mplew.writeInt(type);
/*     */     } else {
/* 369 */       mplew.writeInt(count);
/* 370 */       mplew.writeInt(type);
/* 371 */       mplew.writeInt(args[0]);
/* 372 */       mplew.writeInt(args[1]);
/*     */     } 
/* 374 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] CoolDown(int skillid, int cool, int maxcool) {
/* 378 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 379 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_COOLDOWN.getValue());
/* 380 */     mplew.writeInt(skillid);
/* 381 */     mplew.writeInt(cool);
/* 382 */     mplew.writeInt(maxcool);
/* 383 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] BonusAttack(int skillid) {
/* 387 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 388 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_ATTACK_BONUS.getValue());
/* 389 */     mplew.writeInt(skillid);
/* 390 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] SkillOn(MapleBattleGroundCharacter chr, int skillid, int skilllv, int unk, int skillid2, int skillid3, int cooltime) {
/* 394 */     return SkillOn(chr, skillid, skilllv, unk, skillid2, skillid3, cooltime, 0, 0);
/*     */   }
/*     */   
/*     */   public static byte[] SkillOn(MapleBattleGroundCharacter chr, int skillid, int skilllv, int unk, int skillid2, int skillid3, int cooltime, int nowstack, int maxstack) {
/* 398 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 399 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_SKILLON.getValue());
/* 400 */     mplew.writeInt(GameConstants.BattleGroundJobType(chr));
/* 401 */     mplew.writeShort(1);
/* 402 */     mplew.writeInt(skillid);
/* 403 */     mplew.write(skilllv);
/* 404 */     mplew.writeInt(unk);
/* 405 */     mplew.writeInt(skillid2);
/* 406 */     mplew.writeInt(skillid3);
/* 407 */     mplew.writeInt(nowstack);
/* 408 */     mplew.writeInt(maxstack);
/* 409 */     mplew.writeInt(cooltime);
/* 410 */     mplew.writeShort(0);
/* 411 */     mplew.write(0);
/* 412 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] SkillOnList(MapleBattleGroundCharacter chr, int type, List<Triple<Integer, Integer, Integer>> info) {
/* 416 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 417 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_SKILLON.getValue());
/* 418 */     mplew.writeInt(type);
/* 419 */     for (Triple<Integer, Integer, Integer> info1 : info) {
/* 420 */       mplew.writeShort(1);
/* 421 */       mplew.writeInt(((Integer)info1.getLeft()).intValue());
/* 422 */       mplew.write(((Integer)info1.getMid()).intValue());
/* 423 */       mplew.writeInt(0);
/* 424 */       mplew.writeInt(0);
/* 425 */       mplew.writeInt(0);
/* 426 */       mplew.writeInt(0);
/* 427 */       mplew.writeInt(0);
/* 428 */       mplew.writeInt(((Integer)info1.getRight()).intValue());
/* 429 */       mplew.writeShort(0);
/*     */     } 
/* 431 */     mplew.write(0);
/* 432 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] AttackDamage(MapleCharacter chr, Point pos1, Point pos2, Point pos3, Point pos4, Point pos5, List<MapleBattleGroundMobInfo> minfo, boolean people, int... args) {
/* 436 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 437 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_ATTACK_DAMAGE.getValue());
/* 438 */     mplew.writeInt(chr.getId());
/* 439 */     mplew.writeInt(args[0]);
/* 440 */     mplew.writeInt(args[1]);
/* 441 */     mplew.writeShort(args[2]);
/* 442 */     mplew.write(args[3]);
/* 443 */     mplew.writeInt(0);
/* 444 */     mplew.writePosInt(pos1);
/* 445 */     mplew.writePosInt(pos2);
/* 446 */     mplew.writePosInt(pos3);
/* 447 */     mplew.writePosInt(pos4);
/* 448 */     mplew.writePosInt(pos5);
/* 449 */     mplew.writeInt(args[5]);
/* 450 */     mplew.writeInt(args[6]);
/* 451 */     mplew.writeInt(args[7]);
/* 452 */     mplew.writeInt(args[8]);
/*     */     
/* 454 */     mplew.writeInt(args[9]);
/* 455 */     mplew.writeInt(args[10]);
/* 456 */     mplew.writeInt(args[11]);
/* 457 */     mplew.writeInt(args[12]);
/* 458 */     mplew.writeInt(people ? minfo.size() : 0);
/*     */     
/* 460 */     if (!people) {
/* 461 */       mplew.writeInt(people ? 0 : minfo.size());
/*     */     }
/* 463 */     int i = 0;
/* 464 */     for (MapleBattleGroundMobInfo m : minfo) {
/* 465 */       mplew.writeInt(i);
/* 466 */       mplew.writeInt(m.getDamage());
/* 467 */       mplew.writeInt(0);
/* 468 */       mplew.writeInt(0);
/* 469 */       mplew.write(m.getCritiCal() ? 1 : 0);
/* 470 */       mplew.write(0);
/* 471 */       mplew.writeInt((m.getSkillid() == 80001662) ? 300 : 0);
/* 472 */       mplew.writeInt(0);
/* 473 */       mplew.write(1);
/* 474 */       mplew.writeInt(m.getSkillid());
/* 475 */       mplew.write(1);
/* 476 */       mplew.writeInt(m.getCid());
/* 477 */       mplew.writeInt(m.getOid());
/* 478 */       mplew.writeInt(m.getUnk1());
/* 479 */       mplew.writeInt(m.getUnk2());
/* 480 */       mplew.writeInt(m.getUnk3());
/* 481 */       mplew.write(m.getUnk4());
/* 482 */       mplew.writePosInt(m.getPos1());
/* 483 */       mplew.writePosInt(m.getPos2());
/* 484 */       i++;
/*     */     } 
/* 486 */     mplew.writeZeroBytes(100);
/* 487 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] TakeDamage(MapleCharacter chr, int oid, int damage, int type, int heal) {
/* 491 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 492 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_TAKEDAMAGE.getValue());
/* 493 */     mplew.writeInt(chr.getId());
/* 494 */     mplew.writeInt(oid);
/* 495 */     mplew.writeInt(damage);
/* 496 */     mplew.writeInt(heal);
/* 497 */     mplew.writeInt(-1);
/* 498 */     mplew.writeInt(type);
/* 499 */     mplew.write(0);
/* 500 */     mplew.writeInt((chr.getBuffedValue(80001655) || chr.getBuffedValue(80001740)) ? 1 : 0);
/* 501 */     if (chr.getBuffedValue(80001655)) {
/* 502 */       mplew.writeInt(80001655);
/* 503 */     } else if (chr.getBuffedValue(80001740)) {
/* 504 */       mplew.writeInt(80001740);
/*     */     } 
/* 506 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] ShowPoint(MapleMonster monster, int point) {
/* 510 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 511 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_SHOW_POINT.getValue());
/* 512 */     mplew.writeInt(monster.getObjectId());
/* 513 */     mplew.writeInt(point);
/* 514 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] Respawn(int cid, int type) {
/* 518 */     MaplePacketLittleEndianWriter packet = new MaplePacketLittleEndianWriter();
/* 519 */     packet.writeShort(SendPacketOpcode.RESPAWN.getValue());
/* 520 */     packet.writeInt(cid);
/* 521 */     packet.writeInt(type);
/* 522 */     return packet.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] Death(int type) {
/* 526 */     MaplePacketLittleEndianWriter packet = new MaplePacketLittleEndianWriter();
/* 527 */     packet.writeShort(SendPacketOpcode.BATTLE_GROUND_DEATH.getValue());
/* 528 */     packet.writeInt(type);
/* 529 */     return packet.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] DeathEffect(MapleBattleGroundCharacter chr) {
/* 533 */     MaplePacketLittleEndianWriter packet = new MaplePacketLittleEndianWriter();
/* 534 */     packet.writeShort(SendPacketOpcode.BATTLE_GROUND_DEATH_EFFECT.getValue());
/* 535 */     packet.writeInt(chr.getId());
/* 536 */     return packet.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] TakeDamageEffect(MapleBattleGroundCharacter chr, int stack) {
/* 540 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 541 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_EFFECT.getValue());
/* 542 */     mplew.writeInt(chr.getId());
/* 543 */     mplew.writeZeroBytes(54);
/* 544 */     mplew.writeInt(1);
/* 545 */     mplew.writeZeroBytes(66);
/* 546 */     mplew.writeInt(stack);
/* 547 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] SelectAvater() {
/* 551 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 552 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_SELECT_AVATER.getValue());
/* 553 */     mplew.writeInt(13);
/* 554 */     for (int i = 1; i <= 13; i++) {
/* 555 */       mplew.writeInt((i == 13) ? 0 : i);
/* 556 */       mplew.writeInt((i == 13) ? 0 : ((i == 8 || i == 12) ? 2 : 1));
/*     */     } 
/* 558 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] SelectAvaterOther(MapleCharacter chr, int type, int unk) {
/* 562 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 563 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_SELECT_AVATER_OTHER.getValue());
/* 564 */     mplew.writeInt(chr.getId());
/* 565 */     mplew.writeInt(type);
/* 566 */     mplew.writeInt(unk);
/* 567 */     return mplew.getPacket();
/*     */   }
/*     */   
/*     */   public static byte[] SelectAvaterClock(int time) {
/* 571 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 572 */     mplew.writeShort(SendPacketOpcode.BATTLE_GROUND_SELECT_AVATER_CLOCK.getValue());
/* 573 */     mplew.writeInt(time);
/* 574 */     mplew.write(0);
/* 575 */     return mplew.getPacket();
/*     */   }
/*     */ }