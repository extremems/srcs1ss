/*
 This file is part of the OdinMS Maple Story Server
 Copyright (C) 2008 ~ 2010 Patrick Huy <patrick.huy@frz.cc> 
 Matthias Butz <matze@odinms.de>
 Jan Christian Meyer <vimes@odinms.de>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License version 3
 as published by the Free Software Foundation. You may not use, modify
 or distribute this program under any other version of the
 GNU Affero General Public License.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tools.packet;

import client.AvatarLook;
import client.MapleCharacter;
import client.skills.SkillFactory;
import client.status.MonsterStatus;
import client.status.MonsterStatusEffect;
import handling.SendPacketOpcode;
import client.skills.MapleStatEffect;
import server.Obstacle;
import server.Randomizer;
import server.field.boss.demian.FlyingSwordNode;
import server.field.boss.demian.MapleDelayedAttack;
import server.field.boss.demian.MapleFlyingSword;
import server.field.boss.demian.MapleIncinerateObject;
import server.field.boss.lotus.MapleEnergySphere;
import server.field.boss.lucid.Butterfly;
import server.field.boss.lucid.FairyDust;
import server.field.boss.will.SpiderWeb;
import server.life.Ignition;
import server.life.MapleMonster;
import server.life.MobSkill;
import server.maps.MapleMap;
import server.movement.LifeMovementFragment;
import tools.HexTool;
import tools.Pair;
import tools.Triple;
import tools.data.MaplePacketLittleEndianWriter;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import server.field.boss.FieldSkill;
import server.maps.MapleFoothold;
import server.maps.MapleNodes;

public class MobPacket {
    
    public static byte[] Monster_Attack(MapleMonster monster, boolean afterAction, int skill, int level, int unk) {
/*   65 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/*   66 */     mplew.writeShort(SendPacketOpcode.MONSTER_ATTACK.getValue());
/*   67 */     mplew.writeInt(monster.getObjectId());
/*   68 */     mplew.writeInt(skill);
/*   69 */     mplew.writeInt(level);
/*   70 */     mplew.writeInt(monster.getId());
/*   71 */     mplew.writeInt(unk);
/*   72 */     if (skill == 170) {
/*   73 */       switch (level) {
/*      */         case 62:
/*      */         case 64:
/*      */         case 65:
/*      */         case 66:
/*      */         case 77:
/*   79 */           mplew.write(afterAction);
/*   80 */           mplew.writeInt((monster.getPosition()).x);
/*   81 */           mplew.writeInt((monster.getPosition()).y);
/*      */         case 73:
/*   83 */           mplew.writeInt((monster.getPosition()).x);
/*   84 */           mplew.writeInt((monster.getPosition()).y); break;
/*      */       } 
/*   86 */     } else if (skill == 253 && level == 1) {
/*   87 */       mplew.writeInt((monster.getPosition()).x);
/*   88 */       mplew.writeInt((monster.getPosition()).y);
/*      */     } 
/*      */     
/*   91 */     return mplew.getPacket();
/*      */   }

/*      */   public static byte[] damageMonster(int oid, long damage, boolean heal) {
/*  223 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/*      */     
/*  225 */     mplew.writeShort(SendPacketOpcode.DAMAGE_MONSTER.getValue());
/*  226 */     mplew.writeInt(oid);
/*  227 */     mplew.write(heal ? 1 : 0);
/*  228 */     if (heal) {
/*  229 */       mplew.writeInt(damage);
/*  230 */       mplew.writeInt(-1);
/*      */     } else {
/*  232 */       mplew.writeLong(damage);
/*      */     } 
/*  234 */     return mplew.getPacket();
/*      */   }

/*      */   public static byte[] getSmartNotice(int monsterid, int unk0, int unk1, int unk2, String txt) {
/* 2788 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 2789 */     mplew.writeShort(SendPacketOpcode.SMART_NOTICE.getValue());
/* 2790 */     mplew.writeInt(unk0);
/* 2791 */     mplew.writeInt(monsterid);
/* 2792 */     mplew.writeInt(unk1);
/* 2793 */     mplew.writeInt(unk2);
/* 2794 */     mplew.writeMapleAsciiString(txt);
/* 2795 */     return mplew.getPacket();
/*      */   }

/*      */   public static byte[] FieldSummonAttack(int type, boolean useskill, Point pos, int objid, List<Pair<Long, Integer>> damageinfo) {
/* 3262 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/* 3263 */     mplew.writeShort(SendPacketOpcode.SPEICAL_SUMMON_ATTACK.getValue());
/* 3264 */     mplew.writeInt(type);
/* 3265 */     mplew.writeInt(useskill ? 1 : 0);
/* 3266 */     mplew.writePosInt(pos);
/* 3267 */     mplew.writeInt(objid);
/* 3268 */     mplew.writeInt(damageinfo.size());
/* 3269 */     int i = 1;
/* 3270 */     for (Pair<Long, Integer> info : damageinfo) {
/* 3271 */       mplew.writeInt(i);
/* 3272 */       mplew.writeLong(((Long)info.getLeft()).longValue());
/* 3273 */       mplew.writeInt(((Integer)info.getRight()).intValue());
/* 3274 */       i++;
/*      */     } 
/*      */     
/* 3277 */     return mplew.getPacket();
/*      */   }

    public static byte[] damageFriendlyMob(final MapleMonster mob, final long damage, final boolean display) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAMAGE_MONSTER.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.write(display ? 1 : 2); //false for when shammos changes map!
        mplew.writeLong(damage);
        mplew.writeLong(mob.getHp());
        mplew.writeLong((int) mob.getMobMaxHp());
        return mplew.getPacket();
    }

    public static byte[] setAfterAttack(int objectid, int afterAttack, int attackCount, int attackIdx, boolean left) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.SET_MONSTER_AFTER_ATTACK.getValue());
        mplew.writeInt(objectid);
        mplew.writeShort(afterAttack);
        mplew.writeInt(attackCount);
        mplew.writeInt(attackIdx); // 274 ++
        mplew.write(left); // isLeft
        return mplew.getPacket();
    }
    
    public static byte[] LaserHandler(int oid, int type, int speed, int left) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.LASER_HANDLE.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(type);
        mplew.writeInt(speed);
        mplew.write(left);
        return mplew.getPacket();
    }

    public static byte[] killMonster(final int oid, final int animation) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.KILL_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(animation); // 0 = dissapear, 1 = fade out, 2+ = special
        mplew.writeInt(0);
        mplew.writeInt(0); // 332++
        if (animation == 9) {
            mplew.writeInt(-1);
        }

        return mplew.getPacket();
    }

    public static byte[] suckMonster(final int oid, final int chr) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.KILL_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(9);
        mplew.writeInt(0);
        mplew.writeInt(chr);

        return mplew.getPacket();
    }

    public static byte[] healMonster(final int oid, final int heal) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DAMAGE_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(0);
        mplew.writeLong(-heal);

        return mplew.getPacket();
    }

    public static byte[] showMonsterHP(int oid, int remhppercentage) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_MONSTER_HP.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(remhppercentage);
        mplew.write(1);//306 추가됨

        return mplew.getPacket();
    }

    public static byte[] showBossHP(final MapleMonster mob) { //보스 체력바
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOSS_ENV.getValue());
        mplew.write(6);
        mplew.writeInt(mob.getId()); //hack: MV cant have boss hp bar
        mplew.writeLong(mob.getHp() + mob.getBarrier());
        mplew.writeLong((long) (mob.getMobMaxHp() * mob.bonusHp() + mob.getBarrier()));
        mplew.write(mob.getStats().getTagColor());
        mplew.write(mob.getStats().getTagBgColor()); //5 = 보스 알림이
        return mplew.getPacket();
    }

    public static byte[] showBossHP(final int monsterId, final long currentHp, final long maxHp) { //보스 죽이고나서 체력바 없어짐.
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BOSS_ENV.getValue());
        mplew.write(6);
        mplew.writeInt(monsterId); //has no image
        mplew.writeLong((int) (currentHp <= 0 ? -1 : currentHp));
        mplew.writeLong((int) maxHp);
        mplew.write(6);
        mplew.write(5);

        //colour legend: (applies to both colours)
        //1 = red, 2 = dark blue, 3 = light green, 4 = dark green, 5 = black, 6 = light blue, 7 = purple
        return mplew.getPacket();
    }

    public static byte[] moveMonster(byte bOption, int skill, long targetInfo, int tEncodedGatherDuration, int oid, Point startPos, Point startWobble, List<LifeMovementFragment> moves, List<Point> multiTargetForBall, List<Short> randTimeForAreaAttack, boolean cannotUseSkill) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOVE_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.write(bOption); //?? I THINK
        mplew.write(skill); //pCenterSplit
        mplew.writeLong(targetInfo); //307 ++    

        mplew.write(multiTargetForBall.size());
        for (int i = 0; i < multiTargetForBall.size(); i++) {
            mplew.writeShort(multiTargetForBall.get(i).x);
            mplew.writeShort(multiTargetForBall.get(i).y);
        }

        mplew.write(randTimeForAreaAttack.size());
        for (int i = 0; i < randTimeForAreaAttack.size(); i++) {
            mplew.writeShort(randTimeForAreaAttack.get(i));
        }

        mplew.writeInt(0); // 1.2.332++
        mplew.writeInt(0); // 1.2.342++
        mplew.writeInt(tEncodedGatherDuration); //1.2.192+
        mplew.writePos(startPos);
        mplew.writePos(startWobble);
        PacketHelper.serializeMovementList(mplew, moves);
        mplew.write(cannotUseSkill);

        return mplew.getPacket();
    }

    public static byte[] spawnMonster(MapleMonster life, int spawnType, int link) {
        return spawnMonster(life, spawnType, link, 1);
    }

    public static byte[] spawnMonster(MapleMonster life, int spawnType, int link, int control) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_MONSTER.getValue());
        /*  410 */ mplew.write(0);
        /*  411 */ mplew.writeInt(life.getObjectId());
        /*  412 */ mplew.write(control);
        /*  413 */ mplew.writeInt(life.getId());
        /*  414 */ addMonsterStatus(mplew, life);
        /*      */
 /*  416 */ mplew.writeShort((life.getPosition()).x);
        /*  417 */ mplew.writeShort((life.getPosition()).y);
        /*  418 */ mplew.write(life.getStance());
        /*  419 */ if (life.getId() == 8910000 || life.getId() == 8910100) {
            /*  420 */ mplew.write((life.getCustomValue(8910000) != null) ? 1 : 0);
            /*      */        }
        /*      */
 /*  423 */ MapleFoothold fh = life.getMap().getFootholds().findBelow(life.getPosition());
        /*  424 */ boolean flying = false;
        /*  425 */ if (life.getStats().getName().contains("자폭")) {
            /*  426 */ flying = true;
            /*      */        }
        /*  428 */ switch (life.getStats().getName()) {
            /*      */ case "어둠의 집행자":
                /*  430 */ flying = true;
                /*      */ break;
            /*      */        }
        /*  433 */ mplew.writeShort(flying ? 0 : ((fh != null) ? fh.getId() : life.getFh()));
        /*  434 */ mplew.writeShort(flying ? 0 : ((fh != null) ? fh.getId() : life.getFh()));
        /*  435 */ mplew.write(spawnType);
        /*      */
 /*  437 */ if (spawnType == -3 || (spawnType >= 0 && spawnType != 254)) {
            /*  438 */ mplew.writeInt(link);
            /*      */        }
        /*      */
 /*  441 */ mplew.write(255);
        /*  442 */ mplew.writeLong(life.getHp());
        /*  443 */ mplew.writeInt(0);
        /*  444 */ mplew.writeInt(life.getSeperateSoul());
        /*  445 */ int per = life.getHPPercent();
        /*  446 */ mplew.writeInt(!life.getStats().isMobZone() ? 0 : ((per == 25) ? 4 : ((per == 50) ? 3 : ((per == 75) ? 2 : 1))));
        /*  447 */ mplew.writeInt(0);
        /*  448 */ mplew.write(0);
        /*  449 */ mplew.writeInt(-1);
        /*  450 */ mplew.writeInt(0);
        /*  451 */ mplew.writeInt(0);
        /*  452 */ mplew.write(life.isFacingLeft());
        /*  453 */ mplew.writeInt(0);
        /*  454 */ mplew.writeInt(life.getScale());
        /*  455 */ mplew.writeInt(life.getEliteGrade());
        /*  456 */ if (life.getEliteGrade() >= 0) {
            /*  457 */ mplew.writeInt(life.getEliteGradeInfo().size());
            /*  458 */ for (Pair<Integer, Integer> info : (Iterable<Pair<Integer, Integer>>) life.getEliteGradeInfo()) {
                /*  459 */ mplew.writeInt(((Integer) info.left).intValue());
                /*  460 */ mplew.writeInt(((Integer) info.right).intValue());
                /*      */            }
            /*  462 */ mplew.writeInt(life.getEliteType());
            /*      */        }
        /*  464 */ mplew.write(false);
        /*  465 */ mplew.write(false);
        /*  466 */ if (life.getId() == 8880101 || life.getId() == 8880111) {
            /*  467 */ int size = 0;
            /*  468 */ for (MapleMonster monster : life.getMap().getAllMonster()) {
                /*  469 */ if (monster.getId() == 8880102) {
                    /*  470 */ size++;
                    /*      */                }
                /*      */            }
            /*  473 */ mplew.writeInt(size);
            /*  474 */ for (MapleMonster monster : life.getMap().getAllMonster()) {
                /*  475 */ if (monster.getId() == 8880102) {
                    /*  476 */ mplew.writeInt(5);
                    /*  477 */ mplew.writeInt(monster.getObjectId());
                    /*      */                }
                /*      */            }
            /*      */        } else {
            /*  481 */ mplew.writeInt(0);
            /*      */        }
        /*      */
 /*  484 */ boolean avatarLook = (life.getEliteGrade() >= 0);
        /*  485 */ avatarLook = false;
        /*  486 */ mplew.write(avatarLook);
        /*  487 */ if (avatarLook) {
            /*  488 */ AvatarLook a = AvatarLook.makeRandomAvatar();
            /*  489 */ a.encodeUnpackAvatarLook(mplew);
            /*      */        }
        /*      */
 /*  492 */ boolean mulug = (life.getMap().getId() / 10000 == 92507);
        /*  493 */ mplew.writeShort(0);
        /*  494 */ mplew.write(mulug);
        /*  495 */ if (mulug) {
            /*  496 */ mplew.writeInt(5000);
            /*  497 */ mplew.write(0);
            /*  498 */ mplew.writeInt(4);
            /*  499 */ mplew.writeShort(0);
            /*  500 */ mplew.write(0);
            /*  501 */ mplew.writeInt(6000);
            /*  502 */ mplew.writeZeroBytes(12);
            /*  503 */ mplew.writeInt(3);
            /*  504 */ mplew.writeZeroBytes(12);
            /*  505 */ mplew.write(0);
            /*  506 */ mplew.writeInt(1);
            /*  507 */ mplew.writeInt(3);
            /*  508 */ mplew.writeLong(0L);
            /*      */        } else {
            /*  510 */ mplew.writeLong(0L);
            /*  511 */ mplew.write(0);
            /*      */        }
        /*      */
 /*  514 */ mplew.write(life.getStats().getSkeleton().size());
        /*      */
 /*  516 */ for (Triple<String, Integer, Integer> skeleton : (Iterable<Triple<String, Integer, Integer>>) life.getStats().getSkeleton()) {
            /*  517 */ mplew.writeMapleAsciiString((String) skeleton.left);
            /*  518 */ mplew.write(((Integer) skeleton.mid).intValue());
            /*  519 */ mplew.writeInt(((Integer) skeleton.right).intValue());
            /*      */        }
        /*  521 */ mplew.write(0);
        /*  522 */ mplew.write(0);
        /*  523 */ mplew.write(0);
        /*  524 */ mplew.writeInt((life.getId() == 8220110) ? 1 : 0);
        /*  525 */ if (life.getId() == 8220110) {
            /*  526 */ mplew.writeInt(60000);
            /*      */        }
        /*  528 */ mplew.writeInt(0);
        /*  529 */ mplew.writeInt(0);
        /*  530 */ mplew.writeInt(0);
        /*  531 */ if (life.getId() == 8880102) {
            /*  532 */ mplew.writeInt(0);
            /*      */        }

        return mplew.getPacket();
    }

    public static void addMonsterStatus(MaplePacketLittleEndianWriter mplew, MapleMonster life) {
        mplew.write(life.getChangedStats() != null ? 1 : 0);

        if (life.getChangedStats() != null) {
            mplew.writeLong(life.getChangedStats().hp);
            mplew.writeInt(life.getChangedStats().mp);
            mplew.writeInt(life.getChangedStats().exp);
            mplew.writeInt(life.getChangedStats().watk);
            mplew.writeInt(life.getChangedStats().matk);
            mplew.writeInt(life.getChangedStats().PDRate);
            mplew.writeInt(life.getChangedStats().MDRate);
            mplew.writeInt(life.getChangedStats().acc);
            mplew.writeInt(life.getChangedStats().eva);
            mplew.writeInt(life.getChangedStats().pushed);
            mplew.writeInt(life.getChangedStats().speed);
            mplew.writeInt(life.getChangedStats().level);
            mplew.writeInt(2100000000); //UserCount?
            mplew.write(0);
        }
        PacketHelper.writeMonsterMask(mplew, life.getStati());
        for (Entry<MonsterStatus, MonsterStatusEffect> ms : life.getStati().entrySet()) {
            if (ms.getKey().getFlag() <= MonsterStatus.MS_HangOver.getFlag()) {
                mplew.writeInt(ms.getValue().getValue());
                if (SkillFactory.getSkill(ms.getValue().getSkill()) == null) {
                    mplew.writeShort(ms.getValue().getSkill());
                    mplew.writeShort(ms.getValue().getLevel());
                } else {
                    mplew.writeInt(ms.getValue().getSkill());
                }
                mplew.writeShort(ms.getValue().getDuration() / 1000); // Monsterstatus는 아직 밀리 아님
            }
        }
        DecodeTemporary(mplew, life, life.getStati(), null);
    }

    public static byte[] controlMonster(MapleMonster life, boolean newSpawn, boolean aggro) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_MONSTER_CONTROL.getValue());
        mplew.write(aggro ? 2 : 1);
        mplew.writeInt(life.getObjectId());
        mplew.write(1);//1); // 1 = Control normal, 5 = Control none
        mplew.writeInt(life.getId());
        addMonsterStatus(mplew, life);

        mplew.writeShort(life.getPosition().x);
        mplew.writeShort(life.getPosition().y);
        mplew.write(life.getStance());
        if ((life.getId() == 8910000) || (life.getId() == 8910100)) {
            mplew.write(0); //1이면 뭐가 활성화됨.
            //추가바이트 필요?
        }
        mplew.writeShort(life.getFh());
        mplew.writeShort(life.getFh());
        mplew.write(newSpawn ? -2 : life.isFake() ? -4 : -1);

        mplew.write(0xFF);
        mplew.writeLong(life.getHp()); // hp
        mplew.writeInt(0); // eff ItemId
        mplew.writeInt(life.getSeperateSoul()); // seperateSoulC
        int per = life.getHPPercent();
        mplew.writeInt(!life.getStats().isMobZone() ? 0 : per == 25 ? 4 : per == 50 ? 3 : per == 75 ? 2 : 1); // MobZoneUpdate
        mplew.writeInt(0); // 1.2.273 ++
        mplew.write(0); //1.2.273 ++
        mplew.writeInt(-1); //afterAction
        mplew.writeInt(0); //1.2.273 ++
        mplew.writeInt(-1); //currentAction
        mplew.write(life.isFacingLeft()); //isLeft
        mplew.writeInt(0); //LastAttackTime
        mplew.writeInt(life.getScale()); //scale
        mplew.writeInt(life.getEliteGrade()); //elite grade
        if (life.getEliteGrade() >= 0) {
            mplew.writeInt(life.getEliteGradeInfo().size());
            for (Pair<Integer, Integer> info : life.getEliteGradeInfo()) {
                mplew.writeInt(info.left);
                mplew.writeInt(info.right);
            }
            mplew.writeInt(life.getEliteType());
        }
        mplew.write(false);
        mplew.write(false);
        mplew.writeInt(0);
        mplew.write(false); // 120Byte
        mplew.writeLong(0);
        mplew.writeInt(0); // 325 ++, size 우르스 기준 1주고 1250

        mplew.write(life.getStats().getSkeleton().size());

        for (Triple<String, Integer, Integer> skeleton : life.getStats().getSkeleton()) {
            mplew.writeMapleAsciiString(skeleton.left); // HitPartsName
            mplew.write(skeleton.mid); // unk
            mplew.writeInt(skeleton.right); // HitPartsDurability
        }

        mplew.writeInt(0);
        mplew.writeInt(0); // 332++
        mplew.writeInt(0); // 332++
        mplew.writeInt(0); // 332++

        if (life.getId() == 8880102) {
            mplew.writeInt(0);
        }

        return mplew.getPacket();
    }

    public static byte[] stopControllingMonster(int oid) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SPAWN_MONSTER_CONTROL.getValue());
        mplew.write(0);
        mplew.writeInt(oid);

        return mplew.getPacket();
    }

    public static byte[] makeMonsterReal(MapleMonster life) {
        return spawnMonster(life, -1, 0);
    }

    public static byte[] makeMonsterFake(MapleMonster life) {
        return spawnMonster(life, -4, 0);
    }

    public static byte[] makeMonsterEffect(MapleMonster life, int effect) {
        return spawnMonster(life, effect, 0);
    }
    
    public static byte[] UseSkill(int objectId, int value) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.FORCE_ACTION.getValue());
        mplew.writeInt(objectId);
        mplew.writeInt((value >= 10) ? (value - 10) : value);
        mplew.write((value >= 10) ? 1 : 0);
        return mplew.getPacket();
    }

    public static byte[] moveMonsterResponse(int objectid, short moveid, int currentMp, boolean useSkills, int skillId, int skillLevel, int attackIdx) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MOVE_MONSTER_RESPONSE.getValue());
        mplew.writeInt(objectid);
        mplew.writeShort(moveid);
        mplew.write(useSkills); // nextAttackPossible
        mplew.writeInt(currentMp);
        mplew.writeInt(skillId);
        mplew.writeShort(skillLevel);
        mplew.writeInt(attackIdx);//skill == null ? 0 : skill.isOnlyFsm() ? skill.getAction() : 0); // attack Number
        mplew.writeInt(0); // 공격 후딜레이 같음
        return mplew.getPacket();
    }

    public static byte[] applyMonsterStatus(final MapleMonster mons, final Map<MonsterStatus, MonsterStatusEffect> mse, boolean fromMonster, MapleStatEffect effect) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.APPLY_MONSTER_STATUS.getValue());
        mplew.writeInt(mons.getObjectId());
        PacketHelper.writeMonsterMask(mplew, mse);
        for (Entry<MonsterStatus, MonsterStatusEffect> ms : mse.entrySet()) {
            if (ms.getKey().getFlag() <= MonsterStatus.MS_HangOver.getFlag()) {
                mplew.writeInt(ms.getValue().getValue());
                if (fromMonster) {
                    mplew.writeShort(ms.getValue().getSkill());
                    mplew.writeShort(ms.getValue().getLevel());
                } else {
                    mplew.writeInt(ms.getValue().getSkill());
                }
                mplew.writeShort(ms.getValue().getDuration() / 1000); // Monsterstatus는 아직 밀리 아님
            }
        }
        DecodeTemporary(mplew, mons, mse, effect);
        mplew.writeShort(effect == null ? 0 : effect.getDotInterval()); // delay
        mplew.write(mse.containsKey(MonsterStatus.MS_Freeze) ? 2 : 0);
        for (Entry<MonsterStatus, MonsterStatusEffect> stat : mse.entrySet()) {
            if (MonsterStatus.IsMovementAffectingStat(stat.getKey())) { //
                mplew.write(1);
                break;
            }
        }

        mplew.writeLong(0);
        mplew.writeLong(0);
        mplew.writeLong(0);
        mplew.writeLong(0);
        mplew.writeLong(0);

        return mplew.getPacket();
    }

    private static void DecodeTemporary(MaplePacketLittleEndianWriter mplew, MapleMonster mob, Map<MonsterStatus, MonsterStatusEffect> mse, MapleStatEffect effect) {

        if (mse.containsKey(MonsterStatus.MS_Pdr)) { // 0x8000000
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_Mdr)) { // 0x2000000
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_Speed)) { // 0x400000
            mplew.write(mob.getFreezingOverlap());
        }

        if (mse.containsKey(MonsterStatus.MS_Freeze)) { // 0x100000
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_PCounter)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_MCounter)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_PCounter) || mse.containsKey(MonsterStatus.MS_MCounter)) {
            mplew.writeInt(0);
            mplew.write(0);
            mplew.writeInt(1); // 3?
        }

        if (mse.containsKey(MonsterStatus.MS_AdddamParty)) { // 0x8000000
            MonsterStatusEffect eff = null;
            for (Entry<MonsterStatus, MonsterStatusEffect> stat : mse.entrySet()) {
                if (stat.getKey() == MonsterStatus.MS_AdddamParty) {
                    eff = stat.getValue();
                    break;
                }
            }
            mplew.writeInt(eff != null ? eff.getChr() != null ? eff.getChr().getId() : 0 : 0);
            mplew.writeInt(eff != null ? eff.getChr() != null ? eff.getChr().getParty() == null ? 0 : eff.getChr().getParty().getId() : 0 : 0);
            mplew.writeInt(effect == null ? 0 : effect.getX()); // 피니투라 페투치아 한정. (파티원 데미지 증가 값임)
        }

        if (mse.containsKey(MonsterStatus.MS_Lifting)) { // 0x4000000
            MonsterStatusEffect eff = null;
            for (Entry<MonsterStatus, MonsterStatusEffect> stat : mse.entrySet()) {
                if (stat.getKey() == MonsterStatus.MS_Lifting) {
                    eff = stat.getValue();
                    break;
                }
            }
            mplew.writeInt(eff != null ? eff.getChr() != null ? eff.getChr().getId() : 0 : 0);
            mplew.writeInt(eff != null ? eff.getChr() != null ? eff.getChr().getParty() == null ? 0 : eff.getChr().getParty().getId() : 0 : 0);
            mplew.writeInt(effect == null ? 0 : effect.getX()); // 피니투라 페투치아 한정. (파티원 데미지 증가 값임)
            mplew.writeInt(0);
        }

        /*        if (mse.containsKey(MonsterStatus.MS_DeadlyCharge)) { // 0x1000000
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeInt(0);
        }*/
        if (mse.containsKey(MonsterStatus.MS_FixdamRBuff)) { // 0x1000
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_RWChoppingHammer)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_ElementDarkness)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_DeadlyCharge)) {
            mplew.writeInt(0);
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_Incizing)) {
            mplew.writeInt(mob.getObjectId()); // ObjectId
            mplew.writeInt(10); // ?
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_BMageDebuff)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_DarkLightning)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_PvPHelenaMark)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_MultiPMDR)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_UnkFlameWizard)) {
            MonsterStatusEffect eff = null;
            for (Entry<MonsterStatus, MonsterStatusEffect> stat : mse.entrySet()) {
                if (stat.getKey() == MonsterStatus.MS_UnkFlameWizard) {
                    eff = stat.getValue();
                    break;
                }
            }
            mplew.writeInt(eff != null ? eff.getChr() != null ? eff.getChr().getId() : 0 : 0);
            mplew.writeInt(0);
            mplew.writeInt(100);
            mplew.writeInt(1);
        }

        if (mse.containsKey(MonsterStatus.MS_ElementResetBySummon)) {
            mplew.writeInt(0);
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_DragonStrike)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_CurseMark)) {
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeInt(0);
        }
        /* 1001 */ if (mse.containsKey(MonsterStatus.MS_PVPRude_Stack)) {
            /* 1002 */ mplew.writeInt(1);
            /*      */        }

        if (mse.containsKey(MonsterStatus.MS_PopulatusTimer)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_BossPropPlus)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_Poison)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_Ambush)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_RWChoppingHammer)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_Calabash)) {
            mplew.writeInt(mse.get(MonsterStatus.MS_Calabash).getChr() == null ? 0 : mse.get(MonsterStatus.MS_Calabash).getChr().getId());
        }

        if (mse.containsKey(MonsterStatus.MS_Gathering)) {
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_Invincible)) {
            mplew.writeInt(37284630);
        }

        if (mse.containsKey(MonsterStatus.MS_Burned)) {
            mplew.write(mob.getIgnitions().size()); // count for special packet
            for (Ignition ignition : mob.getIgnitions()) {
                mplew.writeInt(ignition.getOwnerId()); // Mob ownerId
                mplew.writeInt(0); // 324++
                mplew.writeInt(ignition.getSkill());
                mplew.writeLong(ignition.getDamage()); // damage
                mplew.writeInt(ignition.getInterval());
                mplew.writeInt(ignition.getIgnitionKey()); // ?
                mplew.writeInt(12000);
                mplew.writeInt(ignition.getDuration() / 1000);
                mplew.writeInt(0);
                mplew.writeInt(0);
                mplew.writeInt(0);
                mplew.writeInt(0);
                mplew.writeInt(0);
                mplew.writeInt(300);
            }
        }

        if (mse.containsKey(MonsterStatus.MS_BalogDisable)) {
            mplew.write(0);
            mplew.write(0);
        }

        if (mse.containsKey(MonsterStatus.MS_ExchangeAttack)) {
            mplew.write(1);
        }

        if (mse.containsKey(MonsterStatus.MS_AddBuffStat)) {
            mplew.write(0);
        }

        if (mse.containsKey(MonsterStatus.MS_LinkTeam)) {
            mplew.writeMapleAsciiString("");
        }

        //line 3
        //0x200000 -> int 
        //0x100000 -> long
        //0x8000 -> int 
        //0x80000 -> int 
        //0x40000 -> int 
        //0x20000 -> int 
        //0x10000 -> int
        if (mse.containsKey(MonsterStatus.MS_SoulExplosion)) {
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_SeperateSoulP)) {
            mplew.writeInt(50);
            mplew.writeInt(mob.getObjectId()); // oid
            mplew.writeShort(17425);
            mplew.writeInt(43);
            mplew.writeInt(mse.get(MonsterStatus.MS_SeperateSoulP).getSkill()); // skillid
        }

        if (mse.containsKey(MonsterStatus.MS_SeperateSoulC)) {
            mplew.writeInt(50);
            mplew.writeInt(mob.getSeperateSoul()); // 본체의 objectId
            mplew.writeShort(17425);
            mplew.writeInt(43);
        }

        if (mse.containsKey(MonsterStatus.MS_Ember)) {
            MonsterStatusEffect eff = null;
            for (Entry<MonsterStatus, MonsterStatusEffect> stat : mse.entrySet()) {
                if (stat.getKey() == MonsterStatus.MS_Ember) {
                    eff = stat.getValue();
                    break;
                }
            }
            mplew.writeInt(eff != null ? eff.getChr() != null ? eff.getChr().getBuffedEffect(12101024) != null ? eff.getChr().getBuffedEffect(12101024).getDOTTime() : 0 : 0 : 0);
            mplew.writeInt(mse.get(MonsterStatus.MS_Ember).getSkill());
            mplew.writeInt(0);
            mplew.writeInt(mse.get(MonsterStatus.MS_Ember).getChr() == null ? 0 : mse.get(MonsterStatus.MS_Ember).getChr().getId());
            mplew.writeInt(5);
        }

        if (mse.containsKey(MonsterStatus.MS_TrueSight)) {
            mplew.writeInt(0);
            mplew.writeInt(mse.get(MonsterStatus.MS_TrueSight).getSkill());
            mplew.writeInt(0); // CD D8 27 10 오브젝트 아이디, 캐릭터 아이디 아님
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.writeInt(0);
        }

        if (mse.containsKey(MonsterStatus.MS_Laser)) {
            mplew.writeInt(1);
            mplew.writeShort(mse.get(MonsterStatus.MS_Laser).getSkill());
            mplew.writeShort(mse.get(MonsterStatus.MS_Laser).getLevel());
            mplew.writeInt(1955733479); // unk
            mplew.writeInt(1);
            mplew.writeInt(0x8D);
        }
    }

    public static byte[] cancelMonsterStatus(final MapleMonster mons, final Map<MonsterStatus, MonsterStatusEffect> mse) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CANCEL_MONSTER_STATUS.getValue());
        mplew.writeInt(mons.getObjectId());
        PacketHelper.writeMonsterMask(mplew, mse);

        if (mse.containsKey(MonsterStatus.MS_Burned)) {
            mplew.writeInt(0);
            mplew.writeInt(mons.getIgnitions().size());
            for (Ignition ig : mons.getIgnitions()) {
                mplew.writeInt(ig.getOwnerId());
                mplew.writeInt(ig.getSkill());
                mplew.writeInt(0);
            }
            mplew.write(0x0A);
        }

        for (Entry<MonsterStatus, MonsterStatusEffect> stat : mse.entrySet()) {
            if (MonsterStatus.IsMovementAffectingStat(stat.getKey())) { //
                mplew.write(1);
                break;
            }
        }

        mplew.writeInt(0);
        mplew.writeInt(0);
        return mplew.getPacket();
    }

    public static byte[] applyStatusAttack(int oid, int skillId, MapleStatEffect effect) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeInt(oid);
        mplew.writeInt(skillId);
        mplew.writeShort(effect == null ? 0 : effect.getDotInterval());
        mplew.write(1);
        mplew.writeInt(effect == null ? 0 : effect.getDuration() / 1000);

        return mplew.getPacket();
    }

    public static byte[] talkMonster(int oid, int itemId, String msg) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.TALK_MONSTER.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(500); //?
        mplew.writeInt(itemId);
        mplew.write(itemId <= 0 ? 0 : 1);
        mplew.write(msg == null || msg.length() <= 0 ? 0 : 1);
        if (msg != null && msg.length() > 0) {
            mplew.writeMapleAsciiString(msg);
        }
        mplew.writeInt(1); //?

        return mplew.getPacket();
    }

    public static byte[] removeTalkMonster(int oid) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.REMOVE_TALK_MONSTER.getValue());
        mplew.writeInt(oid);

        return mplew.getPacket();
    }

    public static final byte[] SpeakingMonster(MapleMonster mob, int type, int unk2) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.SPEAK_MONSTER.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.writeInt(type);
        mplew.writeInt(unk2);

        return mplew.getPacket();
    }

    public static byte[] showMagnet(int mobid, boolean success) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.SHOW_MAGNET.getValue());
        mplew.writeInt(mobid);
        mplew.write(success ? 1 : 0);
        mplew.write(0); // times, 0 = once, > 0 = twice

        return mplew.getPacket();
    }

    public static byte[] catchMonster(int mobid, byte success) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.CATCH_MONSTER.getValue());
        mplew.writeInt(mobid);
        mplew.write(success);
        mplew.write(success);

        return mplew.getPacket();
    }

    public static byte[] changePhase(MapleMonster mob) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CHANGE_PHASE.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.write(mob.getPhase());
        mplew.writeInt(0);
        return mplew.getPacket();
    }

    public static byte[] changeMobZone(MapleMonster mob) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CHANGE_MOBZONE.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.writeInt(mob.getPhase());
        return mplew.getPacket();
    }

    public static byte[] dropStone(String name, List<Point> data) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.DROP_STONE.getValue());
        mplew.writeMapleAsciiString(name);
        mplew.writeInt(0);
        mplew.writeInt(data.size());
        for (Point a : data) {
            mplew.writeInt(a.x);
            mplew.writeInt(a.y);
        }
        return mplew.getPacket();
    }
    
    public static byte[] createObstacle(MapleMonster mob, List<Obstacle> obs, byte type) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CREATE_OBSTACLE.getValue());
        mplew.writeInt(0);
        mplew.writeInt(obs.size());
        mplew.write(type);
        
        if (type == 1) {
            mplew.writeInt(mob.getId());
            mplew.write(0);
            mplew.writeInt((mob.getPosition()).x);
            mplew.writeInt((((Obstacle) obs.get(0)).getOldPosition()).y);
            mplew.writeInt(((Obstacle) obs.get(0)).getHeight());
            mplew.writeInt(0);
        }  
        if (type == 4) {
            mplew.writeInt(mob.getId());
            mplew.writeInt((mob.getPosition()).x);
            mplew.writeInt((mob.getPosition()).y);
            mplew.writeInt(2817);
            mplew.writeInt(0);
        }
        for (Obstacle ob : obs) {
            mplew.write(1);
            mplew.writeInt(ob.getKey());
            mplew.writeInt((ob.getObjectId() == 0) ? Randomizer.nextInt() : ob.getObjectId());
            mplew.writeInt((ob.getOldPosition()).x);
            mplew.writeInt((ob.getOldPosition()).y);
            mplew.writeInt((ob.getNewPosition()).x);
            mplew.writeInt((ob.getNewPosition()).y);
            mplew.writeInt(ob.getRangeed());
            mplew.writeInt(ob.getTrueDamage());
            mplew.writeInt(ob.getDelay());
            mplew.writeInt(ob.getHeight());
            mplew.writeInt(ob.getVperSec());
            mplew.writeInt(ob.getMaxP());
            mplew.writeInt(ob.getLength());
            mplew.writeInt(ob.getAngle());
            mplew.writeInt(ob.getUnk());
            mplew.writeInt(109); // 1.2.351
            mplew.writeInt(2); // 1.2.351
            mplew.writeInt(700); //운석 떨어지는 거 804
            mplew.writeInt(0);
            if (type == 5) {
                mplew.writeInt(mob.getId());
                mplew.writeInt((mob.getPosition()).x);
                mplew.writeInt((ob.getOldPosition()).y);
                mplew.writeInt(ob.getHeight());
                mplew.writeInt(0);
            }
        }
        mplew.write(0);
        return mplew.getPacket();
    }
    
    public static byte[] createObstacle2(MapleMonster mob, Obstacle ob, byte type) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CREATE_OBSTACLE2.getValue());
        //1개씩만 쏘는 패킷
        mplew.write(type);
        mplew.write(ob != null);
        if (ob != null) {
            mplew.writeInt(ob.getKey()); // type
            mplew.writeInt(Randomizer.nextInt()); // crc
            mplew.writeInt(ob.getOldPosition().x);
            mplew.writeInt(ob.getOldPosition().y);
            mplew.writeInt(ob.getNewPosition().x);
            mplew.writeInt(ob.getNewPosition().y);
            mplew.writeInt(ob.getRangeed());
            mplew.writeInt(ob.getTrueDamage()); // HP% Damage
            mplew.writeInt(ob.getDelay());
            mplew.writeInt(ob.getHeight());
            mplew.writeInt(ob.getVperSec());
            mplew.writeInt(ob.getMaxP()); // 바닥 도달까지 속도
            mplew.writeInt(ob.getLength());
            mplew.writeInt(ob.getAngle());
            mplew.writeInt(ob.getUnk());
            mplew.write(false);
        }
        return mplew.getPacket();
    }
    
    public static byte[] CreateObstacle2(List<Obstacle> obs) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CREATE_OBSTACLE2.getValue());
        mplew.write(5);
        
        for (Obstacle ob : obs) {
            mplew.write(1);
            mplew.writeInt(ob.getKey());
            mplew.writeInt(Randomizer.nextInt());
            mplew.writeInt((ob.getOldPosition()).x);
            mplew.writeInt((ob.getOldPosition()).y);
            mplew.writeInt((ob.getNewPosition()).x);
            mplew.writeInt((ob.getNewPosition()).y);
            mplew.writeInt(ob.getRangeed());
            mplew.writeInt(ob.getTrueDamage());
            mplew.writeInt(ob.getHeight());
            mplew.writeInt(ob.getVperSec());
            mplew.writeInt(ob.getMaxP());
            mplew.writeInt(ob.getLength());
            mplew.writeInt(ob.getAngle());
            mplew.writeInt(ob.getUnk());
            mplew.writeInt(ob.getDelay());
            mplew.writeLong(0L);
        } 
        mplew.write(0);
        mplew.writeInt(4212352);
        return mplew.getPacket();
    }

    public static byte[] BlockAttack(MapleMonster mob, List<Integer> ids) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.BLOCK_ATTACK.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.writeInt(ids.size());
        for (Integer a : ids) {
            mplew.writeInt(a);
        }
        return mplew.getPacket();
    }

    public static byte[] TeleportMonster(MapleMonster monster_, boolean afterAction, int type, Point point) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.TELEPORT_MONSTER.getValue());
        mplew.writeInt(monster_.getObjectId());
        mplew.write(afterAction);
        if (!afterAction) {
            mplew.writeInt(type);
            switch (type) {
                case 3:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 14:
                    mplew.writeInt(point.x);
                    mplew.writeInt(point.y);
                    break;
                case 4:
                    mplew.writeInt(0); // ?
                    break;
            }
        }
        return mplew.getPacket();
    }

    public static byte[] createEnergySphere(int oid, int skillLevel, MapleEnergySphere mes) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.ENERGY_SPHERE.getValue()); // op
        mplew.writeInt(oid);
        mplew.writeInt(217); //id
        mplew.writeInt(skillLevel); // lv
        mplew.write(mes.isOk());

        if (mes.isOk()) {
            mplew.writeInt(mes.getSize()); // size
            mplew.write(mes.isDelayed()); // isDelayed
            mplew.writeInt(mes.getY()); // Y
            mplew.writeInt(mes.getDensity()); // 밀도?
            mplew.writeInt(mes.getFriction()); // 마찰?

            mplew.writeInt(mes.getStartDelay());
            mplew.writeInt(mes.getDestroyDelay());

            //for
            for (int i = 0; i < mes.getSize(); i++) {
                mplew.writeInt(mes.getObjectId() + i);
            }
        } else {
            mplew.writeInt(0);
            mplew.writeInt(mes.getX());
            mplew.writeInt(1); // size

            mplew.writeInt(mes.getObjectId()); // objectSN

            mplew.writeInt(25);
            mplew.writeInt(25);

            mplew.writeInt(0); // objectSN
            mplew.writeInt(mes.getRetitution()); // retitution
            mplew.writeInt(mes.getDestroyDelay()); // destroydelay
            mplew.writeInt(mes.getStartDelay()); // startdelay
            mplew.writeInt(0); // 1.2.324++
            mplew.write(mes.isNoGravity()); // nogravity
            mplew.write(mes.isNoDeleteFromOthers()); // nodeletefrom?
            if (skillLevel == 3 || skillLevel == 4 || skillLevel == 21) { // 217 AND 3 or 4
                mplew.writeInt(5); // maxspeedX
                mplew.writeInt(5); // maxspeedY
            }
            if (mes.isNoDeleteFromOthers()) { // nodeletefrom?
                mplew.writeInt(mes.getRetitution());
                mplew.writeInt(skillLevel == 17 ? 250 : skillLevel == 15 ? 200 : 300);
                mplew.writeInt(24);
                mplew.writeInt(8);
            }
        }

        return mplew.getPacket();
    }

    public static byte[] blockMoving(int objectId, int value) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.BLOCK_MOVING.getValue());
        mplew.writeInt(objectId);
        mplew.writeInt(value);
        mplew.write(0);
        return mplew.getPacket();
    }

    public static byte[] MobSkillDelay(int objectId, int skillID, int skillLv, int skillAfter, short sequenceDelay, List<Rectangle> skillRectInfo) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.MOBSKILL_DELAY.getValue());
        mplew.writeInt(objectId);
        mplew.writeInt(skillAfter);
        mplew.writeInt(skillID);
        mplew.writeInt(skillLv);
        mplew.writeInt(skillID == 230 ? 900 : 0); // 307++
        if (skillRectInfo == null || skillRectInfo.isEmpty()) {
            mplew.writeInt(0);
            mplew.writeInt(0);
        } else {
            mplew.writeInt(sequenceDelay);
            mplew.writeInt(skillRectInfo.size());
            for (Rectangle rect : skillRectInfo) {
                mplew.writeInt(rect.x);
                mplew.writeInt(rect.y);
                mplew.writeInt(rect.x + rect.width);
                mplew.writeInt(rect.y + rect.height);
            }
        }
        return mplew.getPacket();
    }

    public static byte[] incinerateObject(MapleIncinerateObject mio, boolean isSpawn) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.STIGMA_INCINERATE_OBJECT.getValue());
        mplew.writeInt(isSpawn ? 0 : 1);
        if (isSpawn) {
            mplew.writeInt(mio.getX());
            mplew.writeInt(mio.getY());
            mplew.writeInt(2500); // delay?
            mplew.writeInt(1);//mio.getObjectId()); // 아마 objectId?
            mplew.writeMapleAsciiString("Map/Obj/BossDemian.img/demian/altar");
            mplew.write(0);
        }
        return mplew.getPacket();
    }

    public static byte[] FlyingSword(MapleFlyingSword mfs, boolean isCreate) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FLYING_SWORD_CREATE.getValue());
        mplew.write(isCreate);
        mplew.writeInt(mfs.getObjectId());
        if (isCreate) {
            mplew.write(mfs.getObjectType());
            mplew.write(4);
            mplew.writeInt(mfs.getOwner().getId());
            mplew.writeInt(mfs.getOwner().getTruePosition().x);
            mplew.writeInt(mfs.getOwner().getTruePosition().y);
        }
        return mplew.getPacket();
    }

    public static byte[] FlyingSwordNode(MapleFlyingSword mfs) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FLYING_SWORD_NODE.getValue());
        mplew.writeInt(mfs.getObjectId());
        mplew.writeInt(mfs.getTarget() == null ? 0 : mfs.getTarget().getId());
        mplew.write(mfs.isStop());
        mplew.writeInt(mfs.getNodes().size());

        for (FlyingSwordNode fsn : mfs.getNodes()) {
            mplew.write(fsn.getNodeType());
            mplew.writeShort(fsn.getPathIndex());
            mplew.writeShort(fsn.getNodeIndex());
            mplew.writeShort(fsn.getV());
            mplew.writeInt(fsn.getStartDelay());
            mplew.writeInt(fsn.getEndDelay());
            mplew.writeInt(fsn.getDuration());
            mplew.write(fsn.isHide());
            mplew.write(fsn.getCollisionType());
            mplew.writeInt(fsn.getPos().x);
            mplew.writeInt(fsn.getPos().y);
        }

        return mplew.getPacket();
    }

    public static byte[] FlyingSwordTarget(MapleFlyingSword mfs) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FLYING_SWORD_TARGET.getValue());
        mplew.writeInt(mfs.getObjectId());
        mplew.writeInt(mfs.getTarget().getId());
        return mplew.getPacket();
    }

    public static byte[] FlyingSwordMakeEnterRequest(MapleFlyingSword mfs) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.FLYING_SWORD_MAKE_ENTER_REQUEST.getValue());
        mplew.writeInt(mfs.getObjectId());
        mplew.writeInt(mfs.getTarget().getId());
        return mplew.getPacket();
    }

    public static byte[] ZakumAttack(MapleMonster mob, String skeleton) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.ZAKUM_ATTACK.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.writeMapleAsciiString(skeleton);

        return mplew.getPacket();
    }

    public static byte[] onDemianDelayedAttackCreate(int skillId, int skillLevel, MapleDelayedAttack mda) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DEMIAN_ATTACK_CREATE.getValue());
        mplew.writeInt(mda.getOwner().getObjectId());
        mplew.writeInt(skillId);
        mplew.writeInt(skillLevel);
        switch (skillLevel) {
            /*        	case 42: {
        		mplew.writeInt(mob.isFacingLeft() ? 1 : 0);
        	}*/
            case 44:
            case 45:
            case 46:
            case 47: {
                mplew.write(mda.isIsfacingLeft()); // bFlip
                mplew.writeInt(1);
                mplew.writeInt(mda.getObjectId());
                mplew.writeInt(mda.getPos().x); // 
                mplew.writeInt(mda.getPos().y); // 
                break;
            }
        }

        return mplew.getPacket();
    }

    public static byte[] onDemianDelayedAttackCreate(MapleMonster mob, int skillId, int skillLevel, List<MapleDelayedAttack> mda) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();

        mplew.writeShort(SendPacketOpcode.DEMIAN_ATTACK_CREATE.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.writeInt(skillId);
        mplew.writeInt(skillLevel);
        switch (skillLevel) {
            case 42: {
                mplew.write(mob.isFacingLeft() ? 1 : 0);
                mplew.writeInt(1); // AttackIdx

                mplew.writeInt(mda.size());
                for (MapleDelayedAttack att : mda) {
                    mplew.writeInt(att.getObjectId());
                    mplew.writeInt(att.getPos().x);
                    mplew.writeInt(att.getPos().y);
                    mplew.writeInt(att.getAngle());
                }
                break;
            }
        }

        return mplew.getPacket();
    }

    public static class BossLucid {

        public static byte[] createButterfly(int initId, List<Butterfly> butterflies) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_BUTTERFLY_CREATE.getValue());
            mplew.writeInt(initId);//not sure
            mplew.writeInt(butterflies.size());
            for (Butterfly butterfly : butterflies) {
                mplew.writeInt(butterfly.type);
                mplew.writeInt(butterfly.pos.x);
                mplew.writeInt(butterfly.pos.y);
            }
            return mplew.getPacket();
        }
        
        public static byte[] SpawnLucidDream(List<Point> pos) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_DO_SKILL.getValue());
            mplew.writeInt(238);
            mplew.writeInt(12);
            mplew.writeInt(pos.size());
            for (Point p : pos) {
                mplew.writeInt(p.x);
                mplew.writeInt(p.y);
                mplew.writeInt(0);
                mplew.writeInt(0);
                mplew.writeInt(p.x);
            } 
            return mplew.getPacket();
        }

        /**
         * @param mode Butterfly.Mode ADD/MOVE/ATTACK/ERASE
         * @param args ADD: initId(not sure), typeId, posX, posY MOVE: posX,
         * poxY ATTACK: count, startDelay
         * @return
         */
        public static byte[] setButterflyAction(Butterfly.Mode mode, int... args) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_BUTTERFLY_ACTION.getValue());
            mplew.writeInt(mode.code);
            switch (mode) {
                case ADD:
                    mplew.writeInt(args[0]);
                    mplew.writeInt(args[1]);
                    mplew.writeInt(args[2]);
                    mplew.writeInt(args[3]);
                    break;
                case MOVE:
                    mplew.writeInt(args[0]);
                    mplew.writeInt(args[1]);
                    break;
                case ATTACK:
                    mplew.writeInt(args[0]);
                    mplew.writeInt(args[1]);
                    break;
                default:
                    break;
            }
            return mplew.getPacket();
        }

        public static byte[] createDragon(int phase, int posX, int posY, int createPosX, int createPosY, boolean isLeft) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_DRAGON_CREATE.getValue());
            mplew.writeInt(phase);
            mplew.writeInt(posX);
            mplew.writeInt(posY);
            mplew.writeInt(createPosX);
            mplew.writeInt(createPosY);
            mplew.write(isLeft);
            return mplew.getPacket();
        }

        public static byte[] doFlowerTrapSkill(int level, int pattern, int x, int y, boolean flip) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_DO_SKILL.getValue());
            mplew.writeInt(238);
            mplew.writeInt(level);//1~3
            mplew.writeInt(pattern);//0~2
            mplew.writeInt(x);
            mplew.writeInt(y);
            mplew.write(flip);//not sure
            return mplew.getPacket();
        }

        public static byte[] doLaserRainSkill(int startDelay, List<Integer> intervals) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_DO_SKILL.getValue());
            mplew.writeInt(238);
            mplew.writeInt(5);
            mplew.writeInt(startDelay);
            mplew.writeInt(intervals.size());
            for (int interval : intervals) {
                mplew.writeInt(interval);
            }
            return mplew.getPacket();
        }

        public static byte[] doFairyDustSkill(int level, List<FairyDust> fairyDust) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_DO_SKILL.getValue());
            mplew.writeInt(238);
            mplew.writeInt(level);//4 or 10
            mplew.writeInt(fairyDust.size());
            for (FairyDust fd : fairyDust) {
                mplew.writeInt(fd.scale);
                mplew.writeInt(fd.createDelay);
                mplew.writeInt(fd.moveSpeed);
                mplew.writeInt(fd.angle);
            }
            return mplew.getPacket();
        }

        public static byte[] doForcedTeleportSkill(int splitId) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_DO_SKILL.getValue());
            mplew.writeInt(238);
            mplew.writeInt(6);
            mplew.writeInt(splitId);//0~7
            return mplew.getPacket();
        }

        public static byte[] doRushSkill() {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_DO_SKILL.getValue());
            mplew.writeInt(238);
            mplew.writeInt(8);
            mplew.writeInt(0);//only path0 exists o.O
            return mplew.getPacket();
        }

        public static byte[] setStainedGlassOnOff(boolean enable, List<String> tags) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID2_STAINED_GLASS_ON_OFF.getValue());
            mplew.write(enable);
            mplew.writeInt(tags.size());
            for (String name : tags) {
                mplew.writeMapleAsciiString(name);
            }
            return mplew.getPacket();
        }

        public static byte[] breakStainedGlass(List<String> tags) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID2_STAINED_GLASS_BREAK.getValue());
            mplew.writeInt(tags.size());
            for (String name : tags) {
                mplew.writeMapleAsciiString(name);
            }
            return mplew.getPacket();
        }

        public static byte[] setFlyingMode(boolean enable) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID2_SET_FLYING_MODE.getValue());
            mplew.write(enable);
            return mplew.getPacket();
        }

        /**
         * @param placement true:show/hide the statue, false:update the gauge
         * @param gauge 0~3
         * @param enable when the 'placement' is true, a boolean means
         * put/remove, otherwise it means displaying horn effect.
         * @return
         */
        public static byte[] changeStatueState(boolean placement, int gauge, boolean used) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID_STATUE_STATE_CHANGE.getValue());
            mplew.writeInt(placement ? 1 : 0);
            if (placement) {
                mplew.write(used);
            } else {
                mplew.writeInt(gauge);
                mplew.write(used);
            }
            return mplew.getPacket();
        }

        public static byte[] doShoot(int angle, int speed) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID2_WELCOME_BARRAGE.getValue());
            mplew.writeInt(0);
            mplew.writeInt(angle);
            mplew.writeInt(speed);
            return mplew.getPacket();
        }

        public static byte[] doBidirectionShoot(int angleRate, int speed, int interval, int shotCount) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID2_WELCOME_BARRAGE.getValue());
            mplew.writeInt(3);
            mplew.writeInt(angleRate);
            mplew.writeInt(speed);
            mplew.writeInt(interval);
            mplew.writeInt(shotCount);
            return mplew.getPacket();
        }

        public static byte[] doSpiralShoot(int angle, int angleRate, int angleDiff, int speed, int interval, int shotCount, int bulletAngleRate, int bulletSpeedRate) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID2_WELCOME_BARRAGE.getValue());
            mplew.writeInt(4);
            mplew.writeInt(angle);
            mplew.writeInt(angleRate);
            mplew.writeInt(angleDiff);
            mplew.writeInt(speed);
            mplew.writeInt(interval);
            mplew.writeInt(shotCount);
            mplew.writeInt(bulletAngleRate);
            mplew.writeInt(bulletSpeedRate);
            return mplew.getPacket();
        }

        /**
         * @param type should be 1/2/5. 1 : ??? 2 : Start skill action 5 : Stop
         * skill action (for delaying or... idk) 0/3/4 see below
         */
        public static byte[] doWelcomeBarrageSkill(int type) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.LUCID2_WELCOME_BARRAGE.getValue());
            mplew.writeInt(type);
            return mplew.getPacket();
        }
    }

    public static class BossWill {

        public static byte[] setMoonGauge(int max, int min) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_SET_MOONGAUGE.getValue());
            mplew.writeInt(max); // 최댓값
            mplew.writeInt(min); // 사용하기 위한 최솟값

            return mplew.getPacket();
        }

        public static byte[] addMoonGauge(int add) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_MOONGAUGE.getValue());
            mplew.writeInt(add);

            return mplew.getPacket();
        }

        public static byte[] cooldownMoonGauge(int length) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_COOLTIME_MOONGAUGE.getValue());
            mplew.writeInt(length);

            return mplew.getPacket();
        }

        public static byte[] createBulletEyes(int... args) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_CREATE_BULLETEYE.getValue());
            mplew.writeInt(args[0]);
            mplew.writeInt(args[1]);
            mplew.writeInt(args[2]);
            mplew.writeInt(args[3]);
            if (args[0] == 1) {
                mplew.writeInt(1800);
                mplew.writeInt(5);
                mplew.write(true);
                mplew.writeInt(args[4]);
                mplew.writeInt(args[5]);
                mplew.writeInt(args[6]);
                mplew.writeInt(args[7]);
            }
            return mplew.getPacket();
        }

        public static byte[] setWillHp(List<Integer> counts, MapleMap map, int mobId1, int mobId2, int mobId3) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_SET_HP.getValue());
            mplew.writeInt(counts.size());
            for (int i : counts) {
                mplew.writeInt(i);
            }

            MapleMonster life1 = map.getMonsterById(mobId1);
            MapleMonster life2 = map.getMonsterById(mobId2);
            MapleMonster life3 = map.getMonsterById(mobId3);

            mplew.write(life1 != null);
            if (life1 != null) {
                mplew.writeInt(life1.getId());
                mplew.writeLong(life1.getHp());
                mplew.writeLong((long) (life1.getMobMaxHp() * life1.bonusHp()));
            }

            mplew.write(life2 != null);
            if (life2 != null) {
                mplew.writeInt(life2.getId());
                mplew.writeLong(life2.getHp());
                mplew.writeLong((long) (life2.getMobMaxHp() * life2.bonusHp()));
            }

            mplew.write(life3 != null);
            if (life3 != null) {
                mplew.writeInt(life3.getId());
                mplew.writeLong(life3.getHp());
                mplew.writeLong((long) (life3.getMobMaxHp() * life3.bonusHp()));
            }

            return mplew.getPacket();
        }

        public static byte[] setWillHp(List<Integer> counts) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_SET_HP2.getValue());
            mplew.writeInt(counts.size());
            for (int i : counts) {
                mplew.writeInt(i);
            }

            return mplew.getPacket();
        }

        public static byte[] WillSpiderAttack(int id, int skill, int level, int type, List<Triple<Integer, Integer, Integer>> values) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_SPIDER_ATTACK.getValue());
            mplew.writeInt(id);
            mplew.writeInt(skill);
            mplew.writeInt(level);

            switch (level) {
                case 1:
                case 2:
                case 3:
                case 14: {
                    if (level == 14) {
                        mplew.writeInt(type); // 거울 깨짐 여부
                    }
                    mplew.writeInt(level == 14 ? 9 : 4);
                    mplew.writeInt(1200); // 공격 맞을 때까지 딜레이
                    mplew.writeInt(level == 14 ? 5000 : 9000); // duration

                    //lt, rb
                    mplew.writeInt((level == 14 && type == 2) ? - 60 : -40);
                    mplew.writeInt(-600);
                    mplew.writeInt((level == 14 && type == 2) ? 60 : 40);
                    mplew.writeInt(10);

                    mplew.writeInt(values.size());

                    for (Triple<Integer, Integer, Integer> value : values) {
                        mplew.writeInt(value.left);
                        //level == 14 ? (1500 * (1 + (i / 4))) : (1800 * (1 + (i / 6)))
                        mplew.writeInt(value.mid); // 300++, 딜레이
                        //-650 + (130 * Randomizer.rand(1, 9))
                        mplew.writeInt(value.right); // 130++, 시작 x좌표
                        mplew.writeInt(0);
                    }
                    break;
                }
                case 4: {
                    //1이면 위쪽, 나머지는 아래쪽으로 워프
                    mplew.writeInt(type);
                    mplew.write(type != 0);
                    break;
                }
                case 5: {
                    mplew.writeInt(2);

                    if (type == 0) {
                        mplew.write(0);

                        mplew.writeInt(-690);
                        mplew.writeInt(-455);
                        mplew.writeInt(695);
                        mplew.writeInt(160);

                        mplew.write(1);

                        mplew.writeInt(-690);
                        mplew.writeInt(-2378);
                        mplew.writeInt(695);
                        mplew.writeInt(-2019);
                    } else {
                        mplew.write(0);

                        mplew.writeInt(-690);
                        mplew.writeInt(-2378);
                        mplew.writeInt(695);
                        mplew.writeInt(-2019);

                        mplew.write(1);

                        mplew.writeInt(-690);
                        mplew.writeInt(-455);
                        mplew.writeInt(695);
                        mplew.writeInt(160);
                    }
                    break;
                }
            }

            return mplew.getPacket();
        }

        public static byte[] willUseSpecial() {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_USE_SPECIAL.getValue());

            return mplew.getPacket();
        }

        public static byte[] willStun() {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_STUN.getValue());

            return mplew.getPacket();
        }

        public static byte[] willThirdOne() {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_THIRD_ONE.getValue());
            mplew.writeInt(0);

            return mplew.getPacket();
        }

        public static byte[] willSpider(int type, SpiderWeb web) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_SPIDER.getValue());
            mplew.writeInt(type); // 3 : create, 4 : destroy, 5 : disable
            if (type == 3 || type == 4) {
                mplew.writeInt(web.getObjectId());

                mplew.writeInt(web.getPattern());

                mplew.writeInt(web.getX1());
                mplew.writeInt(web.getY1());

                switch (web.getPattern()) {
                    case 0:
                        mplew.writeInt(100);
                        mplew.writeInt(100);
                        break;
                    case 1:
                        mplew.writeInt(160);
                        mplew.writeInt(160);
                        break;
                    case 2:
                        mplew.writeInt(270);
                        mplew.writeInt(270);
                        break;
                    default:
                        mplew.writeInt(0);
                        mplew.writeInt(0);
                        break;
                }
            }
            return mplew.getPacket();
        }

        public static byte[] teleport() {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_TELEPORT.getValue());
            mplew.writeInt(1);

            return mplew.getPacket();
        }

        public static byte[] poison(int objectId, int ownerId) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_POISON.getValue());
            mplew.writeInt(objectId); // objectId
            mplew.writeInt(ownerId);
            mplew.writeInt(0);
            mplew.writeInt(0);
            mplew.write(0);

            return mplew.getPacket();
        }

        public static byte[] removePoison(int objectId) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.WILL_POSION_REMOVE.getValue());
            mplew.writeInt(1);
            mplew.writeInt(objectId); // objectId

            return mplew.getPacket();
        }

    }
    
    public static byte[] demianRunaway(MapleMonster monster, byte type, MobSkill mobSkill, int duration, boolean suc) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.DEMIAN_RUNAWAY.getValue());
        mplew.writeInt(monster.getObjectId());
        mplew.write(type);
        
        switch (type) {
            case 0:
                mplew.writeInt(mobSkill.getSkillLevel());
                mplew.writeInt(duration);
                mplew.writeShort(0);
                mplew.write(1);
                break;
            case 1:
                mplew.write(suc ? 1 : 0);
                mplew.writeInt(suc ? 13 : 30);
                mplew.writeInt(suc ? 6 : 170);
                mplew.writeInt(suc ? 0 : 51);
                break;
        } 
        return mplew.getPacket();
    }

    public static byte[] enableOnlyFsmAttack(MapleMonster mob, int skill, int unk) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.ENABLE_ONLYFSM_ATTACK.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.writeInt(skill);
        mplew.writeInt(unk);
        return mplew.getPacket();
    }

    public static byte[] ChangePhaseDemian(MapleMonster mob, int unk) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.DEMIAN_PHASE_CHANGE.getValue());
        mplew.writeInt(mob.getObjectId());
        mplew.writeInt(unk);
        return mplew.getPacket();
    }

    public static byte[] CorruptionChange(byte phase, int qty) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CORRUPTION_CHANGE.getValue());
        mplew.write(phase);
        mplew.writeInt(qty);
        return mplew.getPacket();
    }

    public static byte[] Lucid3rdPhase(int type) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.LUCID3_PHASE.getValue());
        mplew.writeInt(type);
        return mplew.getPacket();
    }

    public static byte[] jinHillahBlackHand(int objectId, int skillId, int skillLv, List<Triple<Point, Integer, List<Rectangle>>> points) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.JINHILLAH_BLACK_HAND.getValue());
        mplew.writeInt(objectId);
        mplew.writeInt(points.size());

        int i = 0;
        for (Triple<Point, Integer, List<Rectangle>> point : points) {
            i++;
            mplew.writeInt(1);

            mplew.writeInt(i);
            mplew.writeInt(skillId);
            mplew.writeInt(skillLv);
            mplew.write(true);

            mplew.writeInt(1);

            mplew.writePos(point.left);

            mplew.writeInt(point.mid); // 딜레이
            mplew.writeInt(0);
            mplew.writeInt(0);

            mplew.writeInt(point.right.size());

            for (Rectangle rect : point.right) {
                mplew.writeRect(rect);
            }
        }
        return mplew.getPacket();
    }

    public static byte[] jinHillahSpirit(int objectId, int cid, Rectangle rect, Point pos, int skillLv) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.JINHILLAH_SPIRIT.getValue());
        mplew.writeInt(objectId);
        mplew.writeInt(cid); // ?

        mplew.writeInt(rect.x);
        mplew.writeInt(rect.y);
        mplew.writeInt(rect.width);
        mplew.writeInt(rect.height);

        mplew.writePos(pos);

        mplew.writeInt(1); // Idx?
        mplew.writeInt(246); // skillId
        mplew.writeInt(skillLv);
        mplew.write(true);

        mplew.writeInt(1);

        mplew.writePos(new Point(280 * (Randomizer.rand(-3, 3)), -260)); // ?

        int z = Randomizer.rand(4, 6);

        mplew.writeInt(250 * z); // 딜레이
        mplew.writeInt(0);
        mplew.writeInt(0);

        List<Rectangle> rectz = new ArrayList<>();
        for (int j = 0; j < z; ++j) {
            rectz.add(new Rectangle(Randomizer.rand(-100, 150), -80, Randomizer.rand(10, 15) * 5, 640));
        }

        mplew.writeInt(rectz.size());

        for (Rectangle recta : rectz) {
            mplew.writeRect(recta);
        }

        return mplew.getPacket();
    }
    
    public static byte[] useFieldSkill(FieldSkill fieldSkill) {
        int result, i;
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.FIELD_SKILL.getValue());
        mplew.writeInt(fieldSkill.getSkillId());
        mplew.writeInt(fieldSkill.getSkillLevel());
        
        switch (fieldSkill.getSkillId()) {
            case 100008:
                mplew.writeInt(fieldSkill.getSummonedSequenceInfoList().size());
                for (FieldSkill.SummonedSequenceInfo info : fieldSkill.getSummonedSequenceInfoList()) {
                    mplew.writeInt((info.getPosition()).x);
                    mplew.writeInt((info.getPosition()).y);
                }
                break;
            case 100011:
                mplew.writeInt(fieldSkill.getLaserInfoList().size());
                for (FieldSkill.LaserInfo info : fieldSkill.getLaserInfoList()) {
                    mplew.writeInt((info.getPosition()).x);
                    mplew.writeInt((info.getPosition()).y);
                    mplew.writeInt(info.getUnk1());
                    mplew.writeInt(info.getUnk2());
                }
                break;
            case 100013:
                mplew.writeInt(fieldSkill.getEnvInfo().size());
                for (MapleNodes.Environment env : fieldSkill.getEnvInfo()) {
                    mplew.writeInt(env.getX());
                    mplew.writeInt(env.getY());
                    mplew.writeMapleAsciiString(env.getName());
                    mplew.writeInt(env.isShow() ? 1 : 0);
                }
                break;
            case 100014:
            case 100016:
                mplew.writeInt(fieldSkill.getThunderInfo().size());
                mplew.writeInt((fieldSkill.getSkillId() == 100016) ? 1400 : 2700);
                mplew.write(1);
                for (FieldSkill.ThunderInfo th : fieldSkill.getThunderInfo()) {
                    mplew.writePosInt(th.getStartPosition());
                    mplew.writePosInt(th.getEndPosition());
                    mplew.writeInt(th.getInfo());
                    mplew.writeInt(th.getDelay());
                }
            case 100020:
                mplew.writeInt(0);
                mplew.writeInt(0);
                for (FieldSkill.FieldFootHold fh : fieldSkill.getFootHolds()) {
                    mplew.write(true);
                    mplew.writeInt(fh.getDuration());
                    mplew.writeInt(fh.getInterval());
                    mplew.writeInt(fh.getAngleMin());
                    mplew.writeInt(fh.getAngleMax());
                    mplew.writeInt(fh.getAttackDelay());
                    mplew.writeInt(fh.getZ() + fh.getSet());
                    mplew.writeInt(fh.getZ());
                    mplew.writeMapleAsciiString("");
                    mplew.writeMapleAsciiString("");
                    mplew.writeRect(fh.getRect());
                    mplew.writeInt((fh.getPos()).x);
                    mplew.writeInt((fh.getPos()).y);
                    mplew.write(fh.isFacingLeft());
                }
                mplew.write(false);
                break;
            case 100023:
                result = 2;
                mplew.writeInt(8880608);
                mplew.writeInt(1);
                mplew.writeInt(0);
                mplew.writeInt(100);
                mplew.writeInt(80);
                mplew.writeInt(240);
                mplew.writeInt(1530);
                mplew.writeInt(250);
                mplew.writeInt(result);
                for (i = 0; i < result; i++) {
                    if (i == 0) {
                        mplew.writeInt(Randomizer.rand(-864, 10));
                        mplew.writeInt(Randomizer.rand(30, 915));
                        mplew.writeInt(Randomizer.rand(810, 3420));
                    } else {
                        mplew.writeInt(Randomizer.rand(300, 915));
                        mplew.writeInt(Randomizer.rand(-864, 10));
                        mplew.writeInt(Randomizer.rand(810, 3420));
                    }
                }
                break;
            case 100024:
                mplew.writeInt(7);
                mplew.writeInt(Randomizer.rand(0, 6));
                mplew.writeInt(3060);
                mplew.writeInt(2700);
                mplew.writeInt(0);
                mplew.writeInt(0);
                mplew.writeInt(0);
                mplew.writeInt(0);
                mplew.writeInt(0);
                break;
        }     
        return mplew.getPacket();
    }
    
    public static byte[] fieldSkillRemove(int skillId, int skillLevel) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.FIELD_SKILL_REMOVE.getValue());
        mplew.writeInt(skillId);
        mplew.writeInt(skillLevel);
        return mplew.getPacket();
    }

    public static byte[] useFieldSkill(int skillId, int skillLevel) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.FIELD_SKILL.getValue());
        mplew.writeInt(skillId);
        mplew.writeInt(skillLevel);
        switch (skillId) {
            case 100008: {
                Rectangle rect = new Rectangle(-1998, 84, 4000, 84);
                mplew.writeInt(2);
                mplew.writeRect(rect);
                break;
            }

            /*      */
 /*      */ case 100023:
                /* 2616 */ int result = 2;
                /* 2617 */ mplew.writeInt(8880608);
                /* 2618 */ mplew.writeInt(1);
                /* 2619 */ mplew.writeInt(0);
                /* 2620 */ mplew.writeInt(100);
                /* 2621 */ mplew.writeInt(80);
                /* 2622 */ mplew.writeInt(240);
                /* 2623 */ mplew.writeInt(1530);
                /* 2624 */ mplew.writeInt(250);
                /* 2625 */ mplew.writeInt(result);
                /* 2626 */ for (int i = 0; i < result; i++) {
                    /* 2627 */ if (i == 0) {
                        /* 2628 */ mplew.writeInt(Randomizer.rand(-864, 10));
                        /* 2629 */ mplew.writeInt(Randomizer.rand(30, 915));
                        /* 2630 */ mplew.writeInt(Randomizer.rand(810, 3420));
                        /*      */                    } else {
                        /* 2632 */ mplew.writeInt(Randomizer.rand(300, 915));
                        /* 2633 */ mplew.writeInt(Randomizer.rand(-864, 10));
                        /* 2634 */ mplew.writeInt(Randomizer.rand(810, 3420));
                        /*      */                    }
                    /*      */                }
                /*      */ break;
            /*      */
 /*      */
 /*      */
 /*      */ case 100024:
                /* 2642 */ mplew.writeInt(7);
                /* 2643 */ mplew.writeInt(Randomizer.rand(0, 6));
                /* 2644 */ mplew.writeInt(3060);
                /* 2645 */ mplew.writeInt(2700);
                /* 2646 */ mplew.writeInt(0);
                /* 2647 */ mplew.writeInt(0);
                /* 2648 */ mplew.writeInt(0);
                /* 2649 */ mplew.writeInt(0);
                /* 2650 */ mplew.writeInt(0);
                /*      */ break;
        }
        return mplew.getPacket();
    }
    
    public static byte[] HillaDrainStart(int oid) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.HILLA_HP_DRAIN_START.getValue());
        mplew.writeInt(oid);
        return mplew.getPacket();
    }
    
    public static byte[] HillaDrainEffect(int oid, List<Integer> mob) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.HILLA_HP_DRAIN_EFFECT.getValue());
        mplew.writeInt(oid);
        mplew.writeInt(mob.size());
        for (Integer mobid : mob) {
            mplew.writeInt(mobid.intValue());
        }
        return mplew.getPacket();
    }
    
    public static byte[] HillaDrainActive(int oid) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.HILLA_HP_DRAIN_ACTIVE.getValue());
        mplew.writeInt(oid);
        return mplew.getPacket();
    }

    public static byte[] mobBarrier(int objectId, int percent) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MONSTER_BARRIER.getValue());
        mplew.writeInt(objectId);
        mplew.writeInt(percent);

        return mplew.getPacket();
    }
    

    /*      */ public static byte[] mobBarrier(MapleMonster monster) {
        /* 2667 */ MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        /* 2668 */ mplew.writeShort(SendPacketOpcode.MONSTER_BARRIER.getValue());
        /* 2669 */ mplew.writeInt(monster.getObjectId());
        /* 2670 */ mplew.writeInt(monster.getShieldPercent());
        /* 2671 */ mplew.writeLong(monster.getStats().getHp());
        /* 2672 */ return mplew.getPacket();
        /*      */    }

    public static byte[] mobBarrierEffect(int objectId, String eff, String sound, String ui) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MONSTER_BARRIER_EFFECT.getValue());
        mplew.writeInt(objectId);
        mplew.write(true);
        mplew.writeMapleAsciiString(eff);

        mplew.writeInt(1);
        mplew.write(true);
        mplew.writeMapleAsciiString(sound);

        mplew.write(true);
        mplew.writeMapleAsciiString(ui);

        mplew.writeInt(-1);
        mplew.writeShort(0);
        return mplew.getPacket();
    }

    public static byte[] monsterResist(MapleMonster monster, MapleCharacter player, int time, int skill) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.MONSTER_RESIST.getValue());
        mplew.writeInt(monster.getObjectId());
        mplew.writeInt(2); // type
        mplew.writeInt(skill);
        mplew.writeShort(time);//time);
        mplew.writeInt(player.getId());
        mplew.write(false);
        mplew.writeInt(0); // 329++

        player.dropMessageGM(5, HexTool.toString(mplew.getPacket()));
        return mplew.getPacket();
    }

    public static class BossDusk {

        public static byte[] handleDuskGauge(boolean decrease, int gauge, int full) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.DUSK_GAUGE.getValue());
            mplew.write(decrease);
            mplew.writeInt(gauge);
            mplew.writeInt(full);
            return mplew.getPacket();
        }

        public static byte[] spawnDrillAttack(int x, boolean left) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.FIELD_SKILL.getValue());
            mplew.writeInt(100020);
            mplew.writeInt(2);
            mplew.writeInt(0);

            mplew.writeInt(1501); // duration
            mplew.write(true);
            mplew.writeInt(1501); // duration
            mplew.writeInt(1);
            mplew.writeInt(0x23);
            mplew.writeInt(0x4B);
            mplew.writeInt(1020);
            mplew.writeInt(6);
            mplew.writeInt(0);
            mplew.writeInt(0);

            mplew.writeInt(-2185);
            mplew.writeInt(300);
            mplew.writeInt(-120);
            mplew.writeInt(120);
            mplew.writeInt(x);
            mplew.writeInt(-157); //y;
            mplew.write(left);
            mplew.write(0);

            return mplew.getPacket();
        }

        public static byte[] spawnTempFoothold() {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.FIELD_SKILL.getValue());
            mplew.writeInt(100020);
            mplew.writeInt(1);
            mplew.writeInt(3);
            mplew.writeInt(23160); // duration;
            mplew.write(true);
            mplew.writeInt(20400); // duration;
            mplew.writeInt(2760); // delay (duration1 - duration2);
            mplew.writeInt(0x4B); // angle
            mplew.writeInt(0x4B); // angle
            mplew.writeInt(1980);
            mplew.writeInt(5);
            mplew.writeInt(3);
            mplew.writeShort(2);

            mplew.writeInt(12642);
            mplew.writeInt(-2185);
            mplew.writeInt(300);
            mplew.writeInt(-100);
            mplew.writeInt(100);
            mplew.writeInt(-610);
            mplew.writeInt(-159);
            mplew.write(0);
            mplew.write(1);

            mplew.writeInt(21900);
            mplew.writeInt(660);
            mplew.writeInt(-81);
            mplew.writeInt(-81);
            mplew.writeInt(1980);
            mplew.writeInt(6);
            mplew.writeInt(3);
            mplew.write(2);
            mplew.write(0);

            mplew.writeInt(12898);
            mplew.writeInt(-2185);
            mplew.writeInt(300);
            mplew.writeInt(-100);
            mplew.writeInt(100);
            mplew.writeInt(600);
            mplew.writeInt(-159);
            mplew.write(0);
            mplew.write(0);
            return mplew.getPacket();
        }
    }
    
    public static class BossSeren {
        
        public static byte[] SerenChangeBackground(int code) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.SEREN_BACKGROUND_CHANGE.getValue());
            mplew.writeInt(code);
            return mplew.getPacket();
        }
        
        public static byte[] SerenUserStunGauge(int max, int now) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.SEREN_STUN_GAUGE.getValue());
            mplew.writeInt(max);
            mplew.writeInt(now);
            System.err.println(mplew.toString());
            return mplew.getPacket();
        }
        
        public static byte[] SerenMobLazer(MapleMonster mob, int skilllv, int delaytime) {
            MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            mplew.writeShort(SendPacketOpcode.SEREN_MOB_LASER.getValue());
            mplew.writeInt(mob.getObjectId());
            mplew.writeInt(skilllv);
            mplew.writeInt(delaytime);
            List<Point> pos = new ArrayList<>();
            if (skilllv == 1) {
                int rand = Randomizer.rand(2, 6);
                if (rand == 0) {
                    pos.add(new Point(-2511, -780));
                    pos.add(new Point(-1300, -2359));
                    pos.add(new Point(673, -2618));
                    pos.add(new Point(2252, -1407));
                    pos.add(new Point(2511, 566));
                    pos.add(new Point(1300, 2145));
                    pos.add(new Point(-673, 2404));
                    pos.add(new Point(-2252, 1193));
                } else if (rand == 1) {
                    /* 3088 */ pos.add(new Point(-2252, -1407));
                    /* 3089 */ pos.add(new Point(-673, -2618));
                    /* 3090 */ pos.add(new Point(1300, -2359));
                    /* 3091 */ pos.add(new Point(2511, -780));
                    /* 3092 */ pos.add(new Point(2252, 1193));
                    /* 3093 */ pos.add(new Point(673, 2404));
                    /* 3094 */ pos.add(new Point(-1300, 2145));
                    /* 3095 */ pos.add(new Point(-2511, 566));
                    /* 3096 */                } else if (rand == 2) {
                    /* 3097 */ pos.add(new Point(-2600, -107));
                    /* 3098 */ pos.add(new Point(-1838, -1945));
                    /* 3099 */ pos.add(new Point(0, -2707));
                    /* 3100 */ pos.add(new Point(1838, -1945));
                    /* 3101 */ pos.add(new Point(2600, -107));
                    /* 3102 */ pos.add(new Point(1838, 1731));
                    /* 3103 */ pos.add(new Point(0, 2493));
                    /* 3104 */ pos.add(new Point(-1838, 1731));
                    /* 3105 */                } else if (rand == 3) {
                    /* 3106 */ pos.add(new Point(-2561, -558));
                    /* 3107 */ pos.add(new Point(-1491, -2237));
                    /* 3108 */ pos.add(new Point(451, -2668));
                    /* 3109 */ pos.add(new Point(2130, -1598));
                    /* 3110 */ pos.add(new Point(2561, 344));
                    /* 3111 */ pos.add(new Point(1491, 2023));
                    /* 3112 */ pos.add(new Point(-451, 2454));
                    /* 3113 */ pos.add(new Point(-2130, 1384));
                    /* 3114 */                } else if (rand == 4) {
                    /* 3115 */ pos.add(new Point(-2130, -1598));
                    /* 3116 */ pos.add(new Point(-451, -2668));
                    /* 3117 */ pos.add(new Point(1491, -2237));
                    /* 3118 */ pos.add(new Point(2561, -558));
                    /* 3119 */ pos.add(new Point(2130, 1384));
                    /* 3120 */ pos.add(new Point(451, 2454));
                    /* 3121 */ pos.add(new Point(-1491, 2023));
                    /* 3122 */ pos.add(new Point(-2561, 344));
                    /* 3123 */                } else if (rand == 5) {
                    /* 3124 */ pos.add(new Point(-1992, -1778));
                    /* 3125 */ pos.add(new Point(-227, -2697));
                    /* 3126 */ pos.add(new Point(1671, -2099));
                    /* 3127 */ pos.add(new Point(2590, -334));
                    /* 3128 */ pos.add(new Point(1992, 1564));
                    /* 3129 */ pos.add(new Point(227, 2483));
                    /* 3130 */ pos.add(new Point(-1671, 1885));
                    /* 3131 */ pos.add(new Point(-2590, 120));
                    /* 3132 */                } else if (rand == 6) {
                    /* 3133 */ pos.add(new Point(-2443, -996));
                    /* 3134 */ pos.add(new Point(-1099, -2463));
                    /* 3135 */ pos.add(new Point(889, -2550));
                    /* 3136 */ pos.add(new Point(2356, -1206));
                    /* 3137 */ pos.add(new Point(2443, 782));
                    /* 3138 */ pos.add(new Point(1099, 2249));
                    /* 3139 */ pos.add(new Point(-889, 2336));
                    /* 3140 */ pos.add(new Point(-2356, 992));
                    /*      */                }
                /*      */            } else {
                /* 3143 */ pos.add(new Point(1300, 0));
                /* 3144 */ pos.add(new Point(1800, -651));
                /* 3145 */ pos.add(new Point(1300, -1400));
                /* 3146 */ pos.add(new Point(750, -3500));
                /* 3147 */ pos.add(new Point(-750, -3500));
                /* 3148 */ pos.add(new Point(-1900, -1700));
                /* 3149 */ pos.add(new Point(-1900, -500));
                /* 3150 */ pos.add(new Point(-800, 200));
                /* 3151 */ pos.add(new Point(-800, 900));
                /* 3152 */ pos.add(new Point(2200, 1600));
                /* 3153 */ pos.add(new Point(-10, 0));
                /* 3154 */ pos.add(new Point(40, 0));
                /*      */            }
            /* 3156 */ mplew.writeInt(pos.size());
            /* 3157 */ for (int i = 0; i < pos.size(); i++) {
                /* 3158 */ mplew.writeInt(0);
                /* 3159 */ mplew.writeInt(-107);
                /* 3160 */ mplew.writePosInt(pos.get(i));
                /*      */            }
            /* 3162 */ return mplew.getPacket();
            /*      */        }

        /*      */
 /*      */ public static byte[] SerenTimer(int value, int... info) {
            /* 3166 */ MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            /* 3167 */ mplew.writeShort(SendPacketOpcode.SEREN_TIMER.getValue());
            /* 3168 */ mplew.writeInt(value);
            /* 3169 */ switch (value) {
                /*      */ case 0:
                    /* 3171 */ mplew.writeInt(info[0]);
                    /* 3172 */ mplew.writeInt(info[1]);
                    /* 3173 */ mplew.writeInt(info[2]);
                    /* 3174 */ mplew.writeInt(info[3]);
                    /* 3175 */ mplew.writeInt(info[4]);
                    /*      */ break;
                /*      */ case 1:
                    /* 3178 */ mplew.writeInt(info[0]);
                    /* 3179 */ mplew.writeInt(info[1]);
                    /* 3180 */ mplew.writeInt(info[2]);
                    /* 3181 */ mplew.writeInt(info[3]);
                    /* 3182 */ mplew.writeInt(info[4]);
                    /*      */ break;
                /*      */ case 2:
                    /* 3185 */ mplew.write(info[0]);
                    /*      */ break;
                /*      */ case 3:
                    /* 3188 */ mplew.writeInt(0);
                    /*      */ break;
                /*      */            }
            System.err.println(mplew.toString());
            /* 3191 */ return mplew.getPacket();
            /*      */        }

        /*      */
 /*      */ public static byte[] SerenSpawnOtherMist(int oid, boolean left, Point pos) {
            /* 3195 */ MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            /* 3196 */ mplew.writeShort(SendPacketOpcode.SEREN_SPAWN_OTHER_MIST.getValue());
            /* 3197 */ mplew.write(0);
            /* 3198 */ mplew.writeInt(263);
            /* 3199 */ mplew.writeInt(1);
            /* 3200 */ mplew.writeInt(oid);
            /* 3201 */ mplew.write(left);
            /* 3202 */ mplew.writePosInt(pos);
            /* 3203 */ return mplew.getPacket();
            /*      */        }

        /*      */
 /*      */ public static byte[] SerenUnk(int max, int now) {
            /* 3207 */ MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            /* 3208 */ mplew.writeShort(SendPacketOpcode.SEREN_UNK.getValue());
            /* 3209 */ mplew.writeInt(max);
            /* 3210 */ mplew.writeInt(now);
            /* 3211 */ return mplew.getPacket();
            /*      */        }


        /*      */
 /*      */ public static byte[] SerenChangePhase(String str, int type, MapleMonster mob) {
            /* 3215 */ MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
            /* 3216 */ mplew.writeShort(SendPacketOpcode.SEREN_CHANGE_PHASE.getValue());
            /* 3217 */ mplew.writeInt(mob.getObjectId());
            /* 3218 */ mplew.writeMapleAsciiString(str);
            /* 3219 */ mplew.writeInt(type);
            /* 3220 */ mplew.writeInt(0);
            /* 3221 */ mplew.writeInt(1);
            /* 3222 */ mplew.writeInt(mob.getId());
            /* 3223 */ return mplew.getPacket();
            /*      */        }
 
 /*      */   public static byte[] ShowBlackMageSkill(int type) {
/* 3229 */     MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
/*      */     
/* 3231 */     mplew.writeShort(SendPacketOpcode.SHOW_EFFECT.getValue());
/* 3232 */     mplew.writeShort((type == 42570) ? 42570 : 45386);
/* 3233 */     mplew.writeShort(390);
/* 3234 */     mplew.write(0);
/* 3235 */     mplew.writeInt(2);
/* 3236 */     mplew.writeShort((type == 42570) ? 0 : type);
/* 3237 */     return mplew.getPacket();
/*      */   }
        /*      */    }
    /*      */
}
