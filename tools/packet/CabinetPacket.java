/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools.packet;

import client.MapleCabinet;
import client.MapleCabinetStorage;
import client.inventory.Item;
import handling.SendPacketOpcode;
import java.util.Map;
import java.util.Map.Entry;
import tools.data.MaplePacketLittleEndianWriter;

/**
 *
 * @author 펠로스
 */
public class CabinetPacket {
    

    public static final byte[] CabinetAction(MapleCabinetStorage cabinet, int... args) {
        MaplePacketLittleEndianWriter mplew = new MaplePacketLittleEndianWriter();
        mplew.writeShort(SendPacketOpcode.CABINET_ACTION.getValue());
        
        mplew.writeInt(args[0]);
        
        switch (args[0]) {
            case 8: // 알림 메시지
                mplew.writeInt(0);
                break;
            case 9: // 열기
                Map<Integer, MapleCabinet> storage = cabinet.getStorage();
                mplew.writeInt(storage.size());
                for (Entry<Integer, MapleCabinet> message : storage.entrySet()) {
                    mplew.write(1);
                    mplew.writeInt(message.getKey());
                    mplew.writeInt(0);
                    mplew.writeInt(0);
                    mplew.writeInt(0);
                    mplew.writeInt(0);
                    mplew.writeLong(PacketHelper.getTime(message.getValue().getExpiredate()));
                    mplew.writeMapleAsciiString(message.getValue().getTitle());
                    mplew.writeMapleAsciiString(message.getValue().getContent());

                    mplew.write(1);
                    PacketHelper.addItemInfo(mplew, new Item(message.getValue().getItemId(), (byte) 0, (byte) message.getValue().getQuantity(), (byte) 0));
                }
            
                break;
            case 10: // 2차 비번 확인 창
                mplew.write(1);
                break;
            case 12: // 수령
                mplew.writeInt(args[1]);
                break;
            case 11: // 알림 메시지
            case 13: // 창 닫는듯? (maybe)
            case 14: // 잘못된 요청
            case 15: // 슬롯 꽉참
            case 16: // 긴급 점검
                break;
        }
        

        return mplew.getPacket();
    }
}
