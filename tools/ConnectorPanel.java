/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import client.MapleCabinet;
import client.MapleCharacter;
import client.inventory.Item;
import client.inventory.MapleInventoryType;
import client.messages.commands.AdminCommand;
import constants.GMConstants;
import constants.ServerConstants;
import database.DatabaseConnection;
import handling.channel.ChannelServer;
import handling.discord.DiscordSetting;
import handling.discord.Discordcommand;
import handling.world.World;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import scripting.NPCScriptManager;
import scripting.PortalScriptManager;
import scripting.ReactorScriptManager;
import server.MapleItemInformationProvider;
import server.Timer.EventTimer;
import server.Timer.EtcTimer;
import server.ShutdownServer;
import server.control.MapleHotTimeControl;
import server.control.MapleRateEventControl;
import server.life.MapleMonsterInformationProvider;
import server.maps.MapleMap;
import server.shops.MapleShopFactory;
import tools.packet.CField;
import tools.packet.CWvsContext;
import tools.packet.CabinetPacket;

public class ConnectorPanel extends javax.swing.JFrame implements ActionListener {

    static String txtMsg, txtOpt;
    static int intOpt;
    public static DefaultListModel accounts = new DefaultListModel();
    public static DefaultListModel characters = new DefaultListModel();
    public static DefaultListModel addItem = new DefaultListModel();
    public static DefaultListModel addItem2 = new DefaultListModel();
    public static DefaultListModel ChatList = new DefaultListModel();
    public static DefaultListModel admin = new DefaultListModel();

    public ConnectorPanel() {
        initComponents();

        ButtonGroup group1 = new ButtonGroup();
        group1.add(jRadioButton1);
        group1.add(jRadioButton2);

        ButtonGroup group2 = new ButtonGroup();
        group2.add(jRadioButton3);
        group2.add(jRadioButton4);

        EtcTimer.getInstance().register(new Runnable() {

            @Override
            public void run() {
                jLabel17.setText(DatabaseConnection.getIdleConnections() + "");
                jLabel19.setText(DatabaseConnection.getActiveConnections() + "");
                jLabel21.setText(Runtime.getRuntime().availableProcessors() + " / " + Thread.activeCount());
            }
        }, 500);

        for (int i = 0; i < Discordcommand.getGM().size(); i++) {
            addAdmin(Discordcommand.getGM().get(i).getLeft());
        }
        /*
         String filePath = "KickList.txt";
         DefaultTableModel model = (DefaultTableModel) jTable2.getModel();
         try (
         InputStream inStream = new FileInputStream(filePath);
         InputStreamReader reader = new InputStreamReader(inStream, "UTF-8");
         BufferedReader bufReader = new BufferedReader(reader);) {
         StringBuilder sb = new StringBuilder();
         String line = null;
         while ((line = bufReader.readLine()) != null) {
         sb.append(line).append('\n');
         model.addRow(new Object[]{line});
         }
         if (sb.length() > 0) {
         sb.setLength(sb.length() - 1);
         }
         } catch (FileNotFoundException e) {
         e.printStackTrace();
         } catch (IOException e) {
         e.printStackTrace();
         };
         */

    }

    public static void addaccount(String account) {
        try {
            accounts.addElement(account);
            jList3.setModel(accounts);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeaccount(String account) {
        accounts.removeElement(account);
        jList3.setModel(accounts);
    }

    public static void addcharacter(String chr) {
        try {
            characters.addElement(chr);
            jList4.setModel(characters);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removecharacter(String chr) {
        characters.removeElement(chr);
        jList4.setModel(characters);
    }

    public static void addItem(String item) {
        try {
            addItem.addElement(item);
            for (int i = 0; i < addItem.size(); i++) {
                if (!addItem2.contains(addItem.get(i))) {
                    addItem2.addElement(addItem.get(i));
                }
            }
            jList5.setModel(addItem2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeItem(boolean t) {
        if (t) {
            addItem.clear();
            addItem2.clear();
            jList5.removeAll();
        }
    }

    public static void addAdmin(String admins) {
        try {
            admin.addElement(admins);
            jList8.setModel(admin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeAdmin(String admins) {
        admin.removeElement(admins);
        jList8.setModel(admin);
    }

    public static void removeAdminAll() {
        admin.clear();
        jList8.removeAll();
    }

    public static class resetAccountOnline extends TimerTask {

        @Override
        public void run() {
            for (ChannelServer cs : ChannelServer.getAllInstances()) {
                for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                    if (!accounts.contains(player.getName())) {
                        addaccount(player.getName());
                        jLabel25.setText(String.valueOf((int) (Integer.parseInt(jLabel25.getText()) + 1)));
                    }
                }
            }
        }
    }

    public static class resetCharacterOnline extends TimerTask {

        @Override
        public void run() {
            for (ChannelServer cs : ChannelServer.getAllInstances()) {
                for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                    if (!characters.contains(player.getName())) {
                        addcharacter(player.getName());
                        jLabel26.setText(String.valueOf((int) (Integer.parseInt(jLabel26.getText()) + 1)));
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame2 = new javax.swing.JFrame();
        jButton5 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jButton8 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jButton9 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jButton11 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jButton12 = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jButton15 = new javax.swing.JButton();
        jTextField11 = new javax.swing.JTextField();
        jScrollPane6 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jButton16 = new javax.swing.JButton();
        jButton17 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jLabel7 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jTextField13 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jButton20 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jLabel27 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jButton19 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jTextField14 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jButton23 = new javax.swing.JButton();
        jComboBox2 = new javax.swing.JComboBox<>();
        jTextField17 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextField18 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextField19 = new javax.swing.JTextField();
        jButton24 = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        jTextField20 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jTextField21 = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel35 = new javax.swing.JLabel();
        jButton10 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jButton14 = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jList7 = new javax.swing.JList<>();
        jLabel36 = new javax.swing.JLabel();
        jButton25 = new javax.swing.JButton();
        jButton26 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jList6 = new javax.swing.JList<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList3 = new javax.swing.JList<>();
        jLabel24 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList4 = new javax.swing.JList<>();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList5 = new javax.swing.JList<>();
        jLabel28 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        jLabel30 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jList8 = new javax.swing.JList<>();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        jTextField22 = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        jTextField23 = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jTextField24 = new javax.swing.JTextField();
        jButton28 = new javax.swing.JButton();
        jButton29 = new javax.swing.JButton();
        jButton30 = new javax.swing.JButton();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        jTextField25 = new javax.swing.JTextField();
        jTextField26 = new javax.swing.JTextField();
        jButton27 = new javax.swing.JButton();

        jFrame2.setName("jFrame2"); // NOI18N
        jFrame2.setResizable(false);

        jButton5.setText("강제종료");
        jButton5.setName("jButton5"); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Process Name", "Window Title"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTable3.setName("jTable3"); // NOI18N
        jTable3.getTableHeader().setReorderingAllowed(false);
        jTable3.getTableHeader().setResizingAllowed(false);
        jScrollPane3.setViewportView(jTable3);

        org.jdesktop.layout.GroupLayout jFrame2Layout = new org.jdesktop.layout.GroupLayout(jFrame2.getContentPane());
        jFrame2.getContentPane().setLayout(jFrame2Layout);
        jFrame2Layout.setHorizontalGroup(
            jFrame2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jFrame2Layout.createSequentialGroup()
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 475, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, Short.MAX_VALUE))
            .add(jFrame2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jButton5)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jFrame2Layout.setVerticalGroup(
            jFrame2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jFrame2Layout.createSequentialGroup()
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 449, Short.MAX_VALUE)
                .add(18, 18, 18)
                .add(jButton5)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("Form"); // NOI18N
        setResizable(false);

        jTabbedPane1.setName("서버관련"); // NOI18N

        jPanel5.setName("jPanel5"); // NOI18N

        jButton8.setText("지급");
        jButton8.setName("jButton8"); // NOI18N
        jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton8MouseClicked(evt);
            }
        });
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jLabel1.setText("후원 포인트 지급");
        jLabel1.setName("jLabel1"); // NOI18N

        jTextField2.setText("유저 이름");
        jTextField2.setName("jTextField2"); // NOI18N
        jTextField2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField2MouseClicked(evt);
            }
        });
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jTextField3.setText("아이템 코드");
        jTextField3.setName("jTextField3"); // NOI18N
        jTextField3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField3MouseClicked(evt);
            }
        });
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        jTextField5.setText("개수");
        jTextField5.setName("jTextField5"); // NOI18N
        jTextField5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextField5MouseEntered(evt);
            }
        });
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });

        jLabel2.setText("아이템 지급");
        jLabel2.setName("jLabel2"); // NOI18N

        jTextField6.setText("유저 이름");
        jTextField6.setName("jTextField6"); // NOI18N
        jTextField6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField6MouseClicked(evt);
            }
        });
        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });

        jTextField7.setText("포인트");
        jTextField7.setName("jTextField7"); // NOI18N
        jTextField7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField7MouseClicked(evt);
            }
        });
        jTextField7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField7ActionPerformed(evt);
            }
        });

        jButton9.setText("지급");
        jButton9.setName("jButton9"); // NOI18N
        jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton9MouseClicked(evt);
            }
        });
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jLabel3.setText("홍보 포인트 지급");
        jLabel3.setName("jLabel3"); // NOI18N

        jTextField8.setText("유저 이름");
        jTextField8.setName("jTextField8"); // NOI18N
        jTextField8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField8MouseClicked(evt);
            }
        });
        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });

        jTextField9.setText("포인트");
        jTextField9.setName("jTextField9"); // NOI18N
        jTextField9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField9MouseClicked(evt);
            }
        });
        jTextField9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField9ActionPerformed(evt);
            }
        });

        jButton11.setText("지급");
        jButton11.setName("jButton11"); // NOI18N
        jButton11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton11MouseClicked(evt);
            }
        });
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jLabel4.setText("데이터 리셋");
        jLabel4.setName("jLabel4"); // NOI18N

        jButton12.setText("리셋");
        jButton12.setName("jButton12"); // NOI18N
        jButton12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton12MouseClicked(evt);
            }
        });
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "옵코드", "드롭", "포탈", "엔피시", "상점", "이벤트" }));
        jComboBox1.setName("jComboBox1"); // NOI18N
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel5.setText("핫타임 (자동)");
        jLabel5.setName("jLabel5"); // NOI18N

        jTextField10.setText("아이템 코드");
        jTextField10.setName("jTextField10"); // NOI18N
        jTextField10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField10MouseClicked(evt);
            }
        });
        jTextField10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField10ActionPerformed(evt);
            }
        });

        jButton15.setText("추가");
        jButton15.setName("jButton15"); // NOI18N
        jButton15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton15MouseClicked(evt);
            }
        });
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        jTextField11.setText("개수");
        jTextField11.setName("jTextField11"); // NOI18N
        jTextField11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField11MouseClicked(evt);
            }
        });
        jTextField11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField11ActionPerformed(evt);
            }
        });

        jScrollPane6.setName("jScrollPane6"); // NOI18N

        jList1.setName("jList1"); // NOI18N
        jScrollPane6.setViewportView(jList1);

        jButton16.setText("지급");
        jButton16.setName("jButton16"); // NOI18N
        jButton16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton16MouseClicked(evt);
            }
        });
        jButton16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton16ActionPerformed(evt);
            }
        });

        jButton17.setText("전체 삭제");
        jButton17.setName("jButton17"); // NOI18N
        jButton17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton17MouseClicked(evt);
            }
        });
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });

        jButton18.setText("선택 삭제");
        jButton18.setName("jButton18"); // NOI18N
        jButton18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton18MouseClicked(evt);
            }
        });
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });

        jLabel6.setText("핫타임 (수동)");
        jLabel6.setName("jLabel6"); // NOI18N

        jScrollPane7.setName("jScrollPane7"); // NOI18N

        jList2.setName("jList2"); // NOI18N
        jScrollPane7.setViewportView(jList2);

        jLabel7.setText("지급 시간");
        jLabel7.setName("jLabel7"); // NOI18N

        jTextField12.setEditable(false);
        jTextField12.setName("jTextField12"); // NOI18N
        jTextField12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField12MouseClicked(evt);
            }
        });
        jTextField12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField12ActionPerformed(evt);
            }
        });

        jTextField13.setEditable(false);
        jTextField13.setName("jTextField13"); // NOI18N
        jTextField13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextField13MouseClicked(evt);
            }
        });
        jTextField13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField13ActionPerformed(evt);
            }
        });

        jLabel8.setText("시");
        jLabel8.setName("jLabel8"); // NOI18N

        jLabel9.setText("분");
        jLabel9.setName("jLabel9"); // NOI18N

        jButton20.setText("로드");
        jButton20.setName("jButton20"); // NOI18N
        jButton20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton20MouseClicked(evt);
            }
        });
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });

        jButton21.setText("<html>\n<center>프로퍼티<br>\n리로드</center>\n</html>");
        jButton21.setActionCommand("hottime.txt 리로드");
        jButton21.setName("jButton21"); // NOI18N
        jButton21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton21MouseClicked(evt);
            }
        });
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton21ActionPerformed(evt);
            }
        });

        jLabel27.setText("GM설정");
        jLabel27.setName("jLabel27"); // NOI18N

        jTextField1.setText("유저 이름");
        jTextField1.setName("jTextField1"); // NOI18N

        jTextField4.setText("레벨");
        jTextField4.setName("jTextField4"); // NOI18N

        jButton3.setText("설정");
        jButton3.setName("jButton3"); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jComboBox1, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jScrollPane7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane6)
                            .add(jPanel5Layout.createSequentialGroup()
                                .add(jLabel5)
                                .add(0, 0, Short.MAX_VALUE))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel5Layout.createSequentialGroup()
                                .add(jTextField10)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jTextField11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel5Layout.createSequentialGroup()
                                .add(jTextField12, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jLabel8)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jTextField13, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 6, Short.MAX_VALUE)
                                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel5Layout.createSequentialGroup()
                                        .add(jLabel7)
                                        .add(19, 19, 19))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel9)))
                            .add(jButton12, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jButton17, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jButton18, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jButton15, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jButton16, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jButton20, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jButton21)))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jTextField8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 116, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jTextField9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 198, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton11, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jTextField6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 116, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jTextField7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 197, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel3)
                            .add(jLabel1)
                            .add(jLabel2)
                            .add(jLabel4)
                            .add(jLabel6)
                            .add(jLabel27))
                        .add(0, 0, Short.MAX_VALUE))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel5Layout.createSequentialGroup()
                                .add(jTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 116, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 95, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 218, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jTextField5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 90, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jTextField4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 90, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButton8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jButton3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel27)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextField4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton3))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextField5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton8))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextField7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton9))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel3)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextField9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton11))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel6)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextField11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jButton18)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton17)
                        .add(103, 103, 103)
                        .add(jButton16))
                    .add(jScrollPane6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 166, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel5Layout.createSequentialGroup()
                                .add(jLabel7)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(jLabel9))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel5Layout.createSequentialGroup()
                                .add(0, 0, Short.MAX_VALUE)
                                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(jTextField12, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(jLabel8))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jTextField13, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jButton21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton20))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jLabel5)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 90, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(10, 10, 10)
                .add(jLabel4)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jButton12)
                    .add(jComboBox1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("서버 관련 1", jPanel5);

        jPanel6.setName("jPanel6"); // NOI18N

        jLabel10.setText("배율 변경");
        jLabel10.setName("jLabel10"); // NOI18N

        jButton19.setText("로드");
        jButton19.setName("jButton19"); // NOI18N
        jButton19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton19MouseClicked(evt);
            }
        });
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });

        jButton22.setText("이벤트 시작");
        jButton22.setName("jButton22"); // NOI18N
        jButton22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton22MouseClicked(evt);
            }
        });

        jTextField14.setName("jTextField14"); // NOI18N
        jTextField14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField14ActionPerformed(evt);
            }
        });

        jLabel11.setText("경험치");
        jLabel11.setName("jLabel11"); // NOI18N

        jLabel12.setText("메소");
        jLabel12.setName("jLabel12"); // NOI18N

        jTextField15.setName("jTextField15"); // NOI18N

        jLabel13.setText("드롭");
        jLabel13.setName("jLabel13"); // NOI18N

        jTextField16.setName("jTextField16"); // NOI18N

        jButton23.setText("변경");
        jButton23.setName("jButton23"); // NOI18N
        jButton23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton23MouseClicked(evt);
            }
        });
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton23ActionPerformed(evt);
            }
        });

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "경험치", "메소", "드롭" }));
        jComboBox2.setName("jComboBox2"); // NOI18N

        jTextField17.setText("배율");
        jTextField17.setName("jTextField17"); // NOI18N

        jLabel14.setText("배율 이벤트");
        jLabel14.setName("jLabel14"); // NOI18N

        jTextField18.setText("시간 (분)");
        jTextField18.setName("jTextField18"); // NOI18N

        jLabel15.setText("리붓");
        jLabel15.setName("jLabel15"); // NOI18N

        jTextField19.setText("시간 (분)");
        jTextField19.setName("jTextField19"); // NOI18N

        jButton24.setText("리붓하기");
        jButton24.setToolTipText("");
        jButton24.setName("jButton24"); // NOI18N
        jButton24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton24MouseClicked(evt);
            }
        });

        jLabel29.setText("공지");
        jLabel29.setName("jLabel29"); // NOI18N

        jTextField20.setText("공지사항");
        jTextField20.setName("jTextField20"); // NOI18N

        jButton4.setText("팝업");
        jButton4.setName("jButton4"); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton6.setText("엔피시");
        jButton6.setName("jButton6"); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("쇼인포");
        jButton7.setName("jButton7"); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jRadioButton1.setSelected(true);
        jRadioButton1.setText("일반");
        jRadioButton1.setName("jRadioButton1"); // NOI18N

        jRadioButton2.setText("나눔고딕");
        jRadioButton2.setName("jRadioButton2"); // NOI18N

        jTextField21.setText("9062004");
        jTextField21.setName("jTextField21"); // NOI18N

        jLabel31.setText("엔피시 공지 세부 설정");
        jLabel31.setName("jLabel31"); // NOI18N

        jLabel32.setText("엔피시코드");
        jLabel32.setName("jLabel32"); // NOI18N

        jLabel33.setText("글꼴선택");
        jLabel33.setName("jLabel33"); // NOI18N

        jLabel34.setText("타입선택");
        jLabel34.setName("jLabel34"); // NOI18N

        jRadioButton3.setSelected(true);
        jRadioButton3.setText("일반");
        jRadioButton3.setName("jRadioButton3"); // NOI18N

        jRadioButton4.setText("OnAddPopupSay");
        jRadioButton4.setName("jRadioButton4"); // NOI18N

        jLabel35.setText("기타 기능");
        jLabel35.setName("jLabel35"); // NOI18N

        jButton10.setText("점검 ON");
        jButton10.setName("jButton10"); // NOI18N
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton13.setText("모두 광장으로 이동");
        jButton13.setName("jButton13"); // NOI18N
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jButton14.setText("모두 로그인 서버로 이동");
        jButton14.setName("jButton14"); // NOI18N
        jButton14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton14ActionPerformed(evt);
            }
        });

        jScrollPane5.setName("jScrollPane5"); // NOI18N

        jList7.setName("jList7"); // NOI18N
        jScrollPane5.setViewportView(jList7);

        jLabel36.setText("GM 아이디 목록");
        jLabel36.setName("jLabel36"); // NOI18N

        jButton25.setText("<html> <center>프로퍼티<br> 리로드</center> </html>");
        jButton25.setName("jButton25"); // NOI18N
        jButton25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton25MouseClicked(evt);
            }
        });

        jButton26.setText("<html> <center>프로퍼티<br> 로드</center> </html>");
        jButton26.setName("jButton26"); // NOI18N
        jButton26.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton26MouseClicked(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel6Layout.createSequentialGroup()
                        .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(jComboBox2, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel6Layout.createSequentialGroup()
                                .add(jLabel11)
                                .add(10, 10, 10)
                                .add(jTextField14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel12)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jTextField15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel13)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jTextField16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 88, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton23, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
                    .add(jPanel6Layout.createSequentialGroup()
                        .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel6Layout.createSequentialGroup()
                                .add(94, 94, 94)
                                .add(jTextField18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 100, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jTextField17))
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jTextField19, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jButton24, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(jPanel6Layout.createSequentialGroup()
                                .add(jButton22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 124, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(0, 0, Short.MAX_VALUE))))
                    .add(jTextField20)
                    .add(jButton10, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jButton13, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jButton14, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel6Layout.createSequentialGroup()
                        .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel10)
                            .add(jLabel14)
                            .add(jLabel15)
                            .add(jLabel29)
                            .add(jPanel6Layout.createSequentialGroup()
                                .add(jButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jButton6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jButton7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 96, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jLabel31, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 142, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jPanel6Layout.createSequentialGroup()
                                .add(10, 10, 10)
                                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(jTextField21)
                                    .add(jLabel32, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .add(21, 21, 21)
                                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(jRadioButton1)
                                    .add(jLabel33))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jRadioButton2)
                                .add(8, 8, 8)
                                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jPanel6Layout.createSequentialGroup()
                                        .add(jRadioButton3)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jRadioButton4))
                                    .add(jLabel34)))
                            .add(jLabel35)
                            .add(jLabel36, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 107, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(0, 0, Short.MAX_VALUE))
                    .add(jPanel6Layout.createSequentialGroup()
                        .add(jScrollPane5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 344, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButton25)
                            .add(jButton26))))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel10)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel11)
                    .add(jLabel12)
                    .add(jTextField15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel13)
                    .add(jTextField16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton19)
                    .add(jButton23))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel14)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 13, Short.MAX_VALUE)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBox2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextField17, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextField18, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton22))
                .add(18, 18, 18)
                .add(jLabel15)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton24))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel29)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jTextField20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButton4)
                    .add(jButton7)
                    .add(jButton6))
                .add(19, 19, 19)
                .add(jLabel31)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel32)
                    .add(jLabel33)
                    .add(jLabel34))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jRadioButton1)
                    .add(jRadioButton2)
                    .add(jRadioButton3)
                    .add(jRadioButton4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel35)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButton10)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButton13)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButton14)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel36)
                .add(4, 4, 4)
                .add(jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 138, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel6Layout.createSequentialGroup()
                        .add(jButton25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 69, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton26, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 63, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("서버 관련 2", jPanel6);

        jPanel4.setName("jPanel4"); // NOI18N

        jScrollPane8.setName("jScrollPane8"); // NOI18N

        jList6.setName("jList6"); // NOI18N
        jScrollPane8.setViewportView(jList6);

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("채팅로그", jPanel4);

        jPanel1.setName("jPanel1"); // NOI18N

        jLabel16.setText("Idle DB Connection :");
        jLabel16.setName("jLabel16"); // NOI18N

        jLabel17.setText("jLabel17");
        jLabel17.setName("jLabel17"); // NOI18N

        jLabel18.setText("Active DB Connection : ");
        jLabel18.setName("jLabel18"); // NOI18N

        jLabel19.setText("jLabel19");
        jLabel19.setName("jLabel19"); // NOI18N

        jLabel20.setText("Active Threads : ");
        jLabel20.setName("jLabel20"); // NOI18N

        jLabel21.setText("jLabel21");
        jLabel21.setName("jLabel21"); // NOI18N

        jLabel22.setText("DataBase Connection");
        jLabel22.setName("jLabel22"); // NOI18N

        jLabel23.setText("Connection account : ");
        jLabel23.setName("jLabel23"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        jList3.setName("jList3"); // NOI18N
        jScrollPane1.setViewportView(jList3);

        jLabel24.setText("Connection Character :");
        jLabel24.setName("jLabel24"); // NOI18N

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        jList4.setName("jList4"); // NOI18N
        jList4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jList4MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jList4);

        jLabel25.setText("0");
        jLabel25.setName("jLabel25"); // NOI18N

        jLabel26.setText("0");
        jLabel26.setName("jLabel26"); // NOI18N

        jButton1.setText("새로고침");
        jButton1.setName("jButton1"); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("새로고침");
        jButton2.setName("jButton2"); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jScrollPane4.setName("jScrollPane4"); // NOI18N

        jList5.setName("jList5"); // NOI18N
        jList5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jList5MouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(jList5);

        jLabel28.setText("Item 목록");
        jLabel28.setName("jLabel28"); // NOI18N

        jScrollPane9.setName("jScrollPane9"); // NOI18N

        jTextArea3.setColumns(20);
        jTextArea3.setRows(5);
        jTextArea3.setName("jTextArea3"); // NOI18N
        jTextArea3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextArea3MouseClicked(evt);
            }
        });
        jScrollPane9.setViewportView(jTextArea3);

        jLabel30.setText("Item 세부 능력치");
        jLabel30.setName("jLabel30"); // NOI18N

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 219, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jScrollPane9)
                        .addContainerGap())
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jLabel22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 192, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(jPanel1Layout.createSequentialGroup()
                                        .add(jLabel20)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jLabel21))
                                    .add(jPanel1Layout.createSequentialGroup()
                                        .add(jLabel16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 116, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jLabel17)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                        .add(jLabel18)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jLabel19)))
                                .add(0, 0, Short.MAX_VALUE))
                            .add(jPanel1Layout.createSequentialGroup()
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel28, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1Layout.createSequentialGroup()
                                        .add(jLabel23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 125, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jLabel25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jButton1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jPanel1Layout.createSequentialGroup()
                                        .add(10, 10, 10)
                                        .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jButton2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                            .add(jPanel1Layout.createSequentialGroup()
                                                .add(jLabel24)
                                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                                .add(jLabel26)
                                                .add(0, 90, Short.MAX_VALUE))))
                                    .add(jPanel1Layout.createSequentialGroup()
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jLabel30, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 133, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(0, 0, Short.MAX_VALUE)))))
                        .add(12, 12, 12))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel22)
                .add(9, 9, 9)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel16)
                    .add(jLabel17)
                    .add(jLabel18)
                    .add(jLabel19))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel21)
                    .add(jLabel20))
                .add(18, 18, 18)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel23)
                    .add(jLabel24)
                    .add(jLabel25)
                    .add(jLabel26))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 111, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 111, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButton1)
                    .add(jButton2))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel28)
                    .add(jLabel30))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jScrollPane9)
                    .add(jScrollPane4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Connection", jPanel1);

        jPanel2.setName("jPanel2"); // NOI18N

        jScrollPane10.setName("jScrollPane10"); // NOI18N

        jList8.setName("jList8"); // NOI18N
        jScrollPane10.setViewportView(jList8);

        jLabel37.setText("관리자 목록");
        jLabel37.setName("jLabel37"); // NOI18N

        jLabel38.setText("디스코드 정보");
        jLabel38.setName("jLabel38"); // NOI18N

        jLabel39.setText("토큰");
        jLabel39.setName("jLabel39"); // NOI18N

        jTextField22.setName("jTextField22"); // NOI18N

        jLabel40.setText("전체채팅");
        jLabel40.setName("jLabel40"); // NOI18N

        jTextField23.setName("jTextField23"); // NOI18N

        jLabel41.setText("확성기");
        jLabel41.setName("jLabel41"); // NOI18N

        jTextField24.setName("jTextField24"); // NOI18N

        jButton28.setText("변경하기");
        jButton28.setName("jButton28"); // NOI18N
        jButton28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton28MouseClicked(evt);
            }
        });

        jButton29.setText("변경하기");
        jButton29.setName("jButton29"); // NOI18N
        jButton29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton29MouseClicked(evt);
            }
        });

        jButton30.setText("로그아웃");
        jButton30.setName("jButton30"); // NOI18N
        jButton30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton30MouseClicked(evt);
            }
        });

        jLabel42.setText("ID :");
        jLabel42.setName("jLabel42"); // NOI18N

        jLabel43.setText("PW :");
        jLabel43.setName("jLabel43"); // NOI18N

        jTextField25.setName("jTextField25"); // NOI18N

        jTextField26.setName("jTextField26"); // NOI18N

        jButton27.setText("변경하기");
        jButton27.setName("jButton27"); // NOI18N
        jButton27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton27MouseClicked(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jScrollPane10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 208, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanel2Layout.createSequentialGroup()
                                .add(26, 26, 26)
                                .add(jLabel42))
                            .add(jPanel2Layout.createSequentialGroup()
                                .add(18, 18, 18)
                                .add(jLabel43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 32, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jTextField25)
                            .add(jTextField26, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton27))
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jLabel37, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 72, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(47, 47, 47)
                        .add(jButton30))
                    .add(jLabel39)
                    .add(jLabel38)
                    .add(jLabel40)
                    .add(jLabel41)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jTextField24, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jTextField23)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jTextField22))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButton28)
                            .add(jButton29))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel37)
                    .add(jButton30))
                .add(4, 4, 4)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(jButton27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jPanel2Layout.createSequentialGroup()
                            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(jLabel42)
                                .add(jTextField25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(jLabel43)
                                .add(jTextField26, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                .add(15, 15, 15)
                .add(jLabel38)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel39)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jTextField22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel40)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField23, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton28))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel41)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jButton29))
                .addContainerGap(272, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("디스코드", jPanel2);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void logChat(String a) {

    }

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        String procn = jTable3.getValueAt(jTable3.getSelectedRow(), 0).toString();

    }//GEN-LAST:event_jButton5ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jTextField2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField2MouseClicked
        jTextField2.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField2MouseClicked

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void jTextField3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField3MouseClicked
        jTextField3.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3MouseClicked

    private void jTextField5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField5MouseEntered

    }//GEN-LAST:event_jTextField5MouseEntered

    private void jTextField5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField5MouseClicked
        jTextField5.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField5MouseClicked

    private void jButton8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton8MouseClicked
        for (ChannelServer cs : ChannelServer.getAllInstances()) {
            for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                if (player.getName().equals(jTextField2.getText())) {
                    player.gainItem(Integer.parseInt(jTextField3.getText()), Integer.parseInt(jTextField5.getText()));
                    player.dropMessage(1, "아이템이 지급되었습니다. 인벤토리를 확인해 주세요!");
                    JOptionPane.showMessageDialog(null, "지급이 완료되었습니다.");
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null, "캐릭터를 찾을 수 없습니다.");
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton8MouseClicked

    private void jTextField6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField6MouseClicked
        jTextField6.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6MouseClicked

    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField6ActionPerformed

    private void jTextField7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField7MouseClicked
        jTextField7.setText("");    // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7MouseClicked

    private void jTextField7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField7ActionPerformed

    private void jButton9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseClicked
        for (ChannelServer cs : ChannelServer.getAllInstances()) {
            for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                if (player.getName().equals(jTextField6.getText())) {
                    player.gainDonationPoint(Integer.parseInt(jTextField7.getText()));
                    player.dropMessage(1, "후원포인트가 지급되었습니다.");
                    JOptionPane.showMessageDialog(null, "지급이 완료되었습니다.");
                    return;
                }
            }
        }

    }//GEN-LAST:event_jButton9MouseClicked

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jTextField8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField8MouseClicked
        jTextField8.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8MouseClicked

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void jTextField9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField9MouseClicked
        jTextField9.setText("");          // TODO add your handling code here:
    }//GEN-LAST:event_jTextField9MouseClicked

    private void jTextField9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField9ActionPerformed

    private void jButton11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton11MouseClicked
        for (ChannelServer cs : ChannelServer.getAllInstances()) {
            for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                if (player.getName().equals(jTextField8.getText())) {
                    player.gainHPoint(Integer.parseInt(jTextField9.getText()));
                    player.dropMessage(1, "홍보포인트가 지급되었습니다.");
                    JOptionPane.showMessageDialog(null, "지급이 완료되었습니다.");
                    return;
                }
            }
        }
    }//GEN-LAST:event_jButton11MouseClicked

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton12MouseClicked
        switch (jComboBox1.getSelectedIndex()) {
            case 0:
                handling.SendPacketOpcode.reloadValues();
                handling.RecvPacketOpcode.reloadValues();
                break;
            case 1:
                MapleMonsterInformationProvider.getInstance().clearDrops();
                ReactorScriptManager.getInstance().clearDrops();
                break;
            case 2:
                PortalScriptManager.getInstance().clearScripts();
                break;
            case 3:
                NPCScriptManager.getInstance().scriptClear();
                break;
            case 4:
                MapleShopFactory.getInstance().clear();
                break;
            case 5:
                for (ChannelServer instance : ChannelServer.getAllInstances()) {
                    instance.reloadEvents();
                }
                break;
            default:
                break;
        }// TODO add your handling code here:
        JOptionPane.showMessageDialog(null, jComboBox1.getSelectedItem() + "리셋이 완료되었습니다.");
    }//GEN-LAST:event_jButton12MouseClicked

    private void jTextField10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField10MouseClicked
        jTextField10.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10MouseClicked

    private void jTextField10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField10ActionPerformed

    private void jButton15MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton15MouseClicked
        String data = Integer.parseInt(jTextField10.getText()) + "," + Integer.parseInt(jTextField11.getText());
        DefaultListModel model = new DefaultListModel();
        for (int i = 0; i < jList1.getModel().getSize(); i++) {
            model.addElement(jList1.getModel().getElementAt(i));
        }
        model.addElement(data);
        jList1.setModel(model);
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton15MouseClicked

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jTextField11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField11MouseClicked
        jTextField11.setText(""); // TODO add your handling code here:
    }//GEN-LAST:event_jTextField11MouseClicked

    private void jTextField11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField11ActionPerformed

    private void jButton16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton16MouseClicked
        String players = "";
        long now = System.currentTimeMillis();
        for (ChannelServer cs : ChannelServer.getAllInstances()) {
            for (MapleCharacter achr : cs.getPlayerStorage().getAllCharacters().values()) {
                for (int j = 0; j < jList1.getModel().getSize(); j++) {
                    String singleItem = jList1.getModel().getElementAt(j);
                    SimpleDateFormat format = new SimpleDateFormat ( "yyyy년 MM월 dd일 HH시 mm분");
                    Date time = new Date();
                    String time2 = format.format(time);
                    achr.getCabinet().addMessage(new MapleCabinet("[핫타임 보상]", time2 + "에 지급된 핫타임 보상입니다.", Integer.parseInt(singleItem.split(",")[0]), Integer.parseInt(singleItem.split(",")[1]), now + 7 * 24 * 60 * 60 * 1000));
                }
                achr.getClient().getSession().writeAndFlush(CabinetPacket.CabinetAction(null, 11));
                if (achr != null) {
                    players += ", ";
                }
                players += achr.getName();
            }
        }
        JOptionPane.showMessageDialog(null, "핫타임이 지급되었습니다. 아래는 핫타임을 지급받은 유저의 닉네임입니다.\n\n" + players);
    }//GEN-LAST:event_jButton16MouseClicked

    private void jButton16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton16ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton16ActionPerformed

    private void jButton17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton17MouseClicked
        DefaultListModel model = new DefaultListModel();
        jList1.setModel(model);
    }//GEN-LAST:event_jButton17MouseClicked

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton18MouseClicked
        String data = Integer.parseInt(jTextField10.getText()) + "," + Integer.parseInt(jTextField11.getText());
        DefaultListModel model = new DefaultListModel();
        for (int i = 0; i < jList1.getModel().getSize(); i++) {
            if (i != jList1.getSelectedIndex()) {
                model.addElement(jList1.getModel().getElementAt(i));
            }
        }

        jList1.setModel(model);// TODO add your handling code here:
    }//GEN-LAST:event_jButton18MouseClicked

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton18ActionPerformed

    private void jTextField12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField12MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField12MouseClicked

    private void jTextField12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField12ActionPerformed

    private void jTextField13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextField13MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField13MouseClicked

    private void jTextField13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField13ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField13ActionPerformed

    private void jButton20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton20MouseClicked
        DefaultListModel model = new DefaultListModel();
        for (int i = 0; i < MapleHotTimeControl.getHotTimeItemIds().split(",").length; i++) {
            model.addElement(MapleHotTimeControl.getHotTimeItemIds().split(",")[i] + "," + MapleHotTimeControl.getHotTimeItemQtys().split(",")[i]);
        }
        jList2.setModel(model);
        jTextField12.setText(MapleHotTimeControl.getHotTimeHour() + "");
        jTextField13.setText(MapleHotTimeControl.getHotTimeMinute() + "");
    }//GEN-LAST:event_jButton20MouseClicked

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed

    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton21MouseClicked
        MapleHotTimeControl.reloadProperty();
        DefaultListModel model = new DefaultListModel();
        for (int i = 0; i < MapleHotTimeControl.getHotTimeItemIds().split(",").length; i++) {
            model.addElement(MapleHotTimeControl.getHotTimeItemIds().split(",")[i] + "," + MapleHotTimeControl.getHotTimeItemQtys().split(",")[i]);
        }
        jList2.setModel(model);
        jTextField12.setText(MapleHotTimeControl.getHotTimeHour() + "");
        jTextField13.setText(MapleHotTimeControl.getHotTimeMinute() + ""); // TODO add your handling code here:
    }//GEN-LAST:event_jButton21MouseClicked

    private void jButton21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton21ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton21ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jButton23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton23ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton23ActionPerformed

    private void jTextField14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField14ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField14ActionPerformed

    private void jButton19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton19MouseClicked
        jTextField14.setText(ChannelServer.getInstance(1).getExpRate() + "");
        jTextField15.setText(ChannelServer.getInstance(1).getMesoRate() + "");
        jTextField16.setText(ChannelServer.getInstance(1).getDropRate() + "");

    }//GEN-LAST:event_jButton19MouseClicked

    private void jButton23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton23MouseClicked
        for (ChannelServer cserv : ChannelServer.getAllInstances()) {
            if (MapleRateEventControl.getExpMin() == 0) {
                cserv.setExpRate(Integer.parseInt(jTextField14.getText()));
            }
            if (MapleRateEventControl.getMesoMin() == 0) {
                cserv.setMesoRate(Integer.parseInt(jTextField15.getText()));
            }
            if (MapleRateEventControl.getDropMin() == 0) {
                cserv.setDropRate(Integer.parseInt(jTextField16.getText()));
            }
        }
        JOptionPane.showMessageDialog(null, "변경이 완료되었습니다.\n\n(배율이벤트가 진행중일 경우 변경되지 않습니다.)");
    }//GEN-LAST:event_jButton23MouseClicked

    private void jButton22MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton22MouseClicked
        switch (jComboBox2.getSelectedIndex()) {
            case 0:
                if (MapleRateEventControl.getExpMin() == 0) {
                    MapleRateEventControl.setExpMin(Integer.parseInt(jTextField18.getText()));
                    for (ChannelServer cserv : ChannelServer.getAllInstances()) {
                        cserv.setExpRate(Integer.parseInt(jTextField17.getText()));
                    }
                    World.Broadcast.broadcastMessage(CField.getGameMessage(8, "[경험치 이벤트] " + jTextField18.getText() + "분동안 경험치 배율이 " + jTextField17.getText() + "배로 변경됩니다!"));
                } else {
                    JOptionPane.showMessageDialog(null, "이벤트가 이미 진행중입니다.");
                }
                break;
            case 1:
                if (MapleRateEventControl.getMesoMin() == 0) {
                    MapleRateEventControl.setMesoMin(Integer.parseInt(jTextField18.getText()));
                    for (ChannelServer cserv : ChannelServer.getAllInstances()) {
                        cserv.setMesoRate(Integer.parseInt(jTextField17.getText()));
                    }
                    World.Broadcast.broadcastMessage(CField.getGameMessage(8, "[메소 이벤트] " + jTextField18.getText() + "분동안 메소 배율이 " + jTextField17.getText() + "배로 변경됩니다!"));
                } else {
                    JOptionPane.showMessageDialog(null, "이벤트가 이미 진행중입니다.");
                }
                break;
            case 2:
                if (MapleRateEventControl.getDropMin() == 0) {
                    MapleRateEventControl.setDropMin(Integer.parseInt(jTextField18.getText()));
                    for (ChannelServer cserv : ChannelServer.getAllInstances()) {
                        cserv.setDropRate(Integer.parseInt(jTextField17.getText()));
                    }
                    World.Broadcast.broadcastMessage(CField.getGameMessage(8, "[드롭 이벤트] " + jTextField18.getText() + "분동안 드롭 배율이 " + jTextField17.getText() + "배로 변경됩니다!"));
                } else {
                    JOptionPane.showMessageDialog(null, "이벤트가 이미 진행중입니다.");
                }
                break;
            default:
                break;

        }
    }//GEN-LAST:event_jButton22MouseClicked

    private int minutesLeft = 0;
    private void jButton24MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton24MouseClicked
        if (AdminCommand.getts() == null && (AdminCommand.gett() == null || !AdminCommand.gett().isAlive())) {
            minutesLeft = Integer.parseInt(jTextField19.getText());
            AdminCommand.sett(new Thread(ShutdownServer.getInstance()));
            AdminCommand.setts(EventTimer.getInstance().register(new Runnable() {
                public void run() {
                    if (minutesLeft == 0) {
                        ShutdownServer.getInstance().shutdown();
                        AdminCommand.gett().start();
                        AdminCommand.getts().cancel(false);
                    }
                    World.Broadcast.broadcastMessage(CWvsContext.serverNotice(0, "", "서버가 " + minutesLeft + "분 뒤 종료될 예정입니다. 안전한 저장을 위해 로그아웃 해주세요."));
                    minutesLeft--;
                }
            }, 60000));
            JOptionPane.showMessageDialog(null, "리붓 설정이 완료되었습니다.");
        } else {
            JOptionPane.showMessageDialog(null, "리붓이 이미 진행중입니다.\n\n" + minutesLeft + " ~ " + (minutesLeft + 1) + "분 뒤에 서버가 종료됩니다.");
        }        // TODO add your handling code here:
    }//GEN-LAST:event_jButton24MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        for (ChannelServer cs : ChannelServer.getAllInstances()) {
            for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                if (accounts.contains(player.getName())) {
                    removeaccount(player.getName());
                    jLabel25.setText(String.valueOf((int) (Integer.parseInt(jLabel25.getText()) - 1)));
                }
            }
        }
        Timer timer = new Timer();
        timer.schedule(new resetAccountOnline(), 5000);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        for (ChannelServer cs : ChannelServer.getAllInstances()) {
            for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                if (characters.contains(player.getName())) {
                    removecharacter(player.getName());
                    jLabel26.setText(String.valueOf((int) (Integer.parseInt(jLabel26.getText()) - 1)));
                }
            }
        }
        Timer timer = new Timer();
        timer.schedule(new resetCharacterOnline(), 5000);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        boolean check = false;
        for (ChannelServer cs : ChannelServer.getAllInstances()) {
            MapleCharacter hp = null;
            hp = cs.getPlayerStorage().getCharacterByName(this.jTextField1.getText());
            if (hp == null && !check) {
                check = false;
            } else if (hp != null) {
                if (hp.getGMLevel() <= 0) {
                    check = true;
                    hp.setGMLevel(Integer.valueOf(jTextField4.getText()).byteValue());
                    hp.getClient().getSession().writeAndFlush(CWvsContext.getTopMsg("[알림] 해당 플레이어가 GM " + Integer.valueOf(jTextField4.getText()).byteValue() + "레벨이 되었습니다."));
                    JOptionPane.showMessageDialog(null, "GM설정을 하였습니다.");
                    jTextField1.setText("유저 이름");
                    jTextField4.setText("레벨");
                    return;
                } else {
                    check = true;
                    hp.setGMLevel((byte) 0);
                    hp.getClient().getSession().writeAndFlush(CWvsContext.getTopMsg("[알림] GM설정이 해제 되었습니다."));
                    JOptionPane.showMessageDialog(null, "GM설정이 해제 되었습니다.");
                    jTextField1.setText("유저 이름");
                    jTextField4.setText("레벨");
                    return;
                }
            }
        }
        if (!check) {
            JOptionPane.showMessageDialog(null, "플레이어가 접속 중이지 않습니다.");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jList4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jList4MouseClicked
        try {
            if (jList4.getModel() != null) {
                String name = jList4.getModel().getElementAt(jList4.getSelectedIndex());
                jTextField1.setText(name);
                jTextField2.setText(name);
                jTextField6.setText(name);
                jTextField8.setText(name);
                removeItem(true);
            }
        } catch (Exception e) {
        }
        try {
            for (ChannelServer cs : ChannelServer.getAllInstances()) {
                MapleCharacter hp = null;
                hp = cs.getPlayerStorage().getCharacterByName(jList4.getModel().getElementAt(jList4.getSelectedIndex()));
                if (hp != null) {
                    addItem("* 장착중인 아이템 *");
                    // 장착중인 아이템 리스트 추가
                    for (int i = -1900; i <= 0; i++) {
                        Item tempItem = hp.getInventory(MapleInventoryType.EQUIPPED).getItem((short) i);
                        if (tempItem != null) {
                            addItem(getItemName(tempItem.getItemId()) + "(" + tempItem.getItemId() + ")");
                        }
                    }
                    addItem("* 장비아이템 *");
                    // 장비 아이템 리스트 추가
                    for (int i = 1; i <= hp.getInventory(MapleInventoryType.EQUIP).getSlotLimit(); i++) {
                        Item tempItem = hp.getInventory(MapleInventoryType.EQUIP).getItem((short) i);
                        if (tempItem != null) {
                            addItem(getItemName(tempItem.getItemId()) + "(" + tempItem.getItemId() + ")");
                        }
                    }
                    addItem("* 소비아이템 *");
                    // 소비 아이템 리스트 추가
                    for (int i = 1; i <= hp.getInventory(MapleInventoryType.USE).getSlotLimit(); i++) {
                        Item tempItem = hp.getInventory(MapleInventoryType.USE).getItem((short) i);
                        if (tempItem != null) {
                            addItem(getItemName(tempItem.getItemId()) + "(" + tempItem.getItemId() + ")");
                        }
                    }
                    addItem("* 기타아이템 *");
                    // 기타 아이템 리스트 추가
                    for (int i = 1; i <= hp.getInventory(MapleInventoryType.ETC).getSlotLimit(); i++) {
                        Item tempItem = hp.getInventory(MapleInventoryType.ETC).getItem((short) i);
                        if (tempItem != null) {
                            addItem(getItemName(tempItem.getItemId()) + "(" + tempItem.getItemId() + ")");
                        }
                    }
                    addItem("* 설치아이템 *");
                    // 설치 아이템 리스트 추가
                    for (int i = 1; i <= hp.getInventory(MapleInventoryType.SETUP).getSlotLimit(); i++) {
                        Item tempItem = hp.getInventory(MapleInventoryType.SETUP).getItem((short) i);
                        if (tempItem != null) {
                            addItem(getItemName(tempItem.getItemId()) + "(" + tempItem.getItemId() + ")");
                        }
                    }
                    addItem("* 캐시아이템 *");
                    // 캐시 아이템 리스트 추가
                    for (int i = 1; i <= hp.getInventory(MapleInventoryType.CASH).getSlotLimit(); i++) {
                        Item tempItem = hp.getInventory(MapleInventoryType.CASH).getItem((short) i);
                        if (tempItem != null) {
                            addItem(getItemName(tempItem.getItemId()) + "(" + tempItem.getItemId() + ")");
                        }
                    }
                }
            }
        } catch (Exception ex) {
        }
    }//GEN-LAST:event_jList4MouseClicked
    public static boolean isNumeric(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private void jList5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jList5MouseClicked
        if (jList5.getModel() != null) {
            for (ChannelServer cs : ChannelServer.getAllInstances()) {
                MapleCharacter hp = null;
                hp = cs.getPlayerStorage().getCharacterByName(jList4.getModel().getElementAt(jList4.getSelectedIndex()));
                if (hp != null) {
                    String name = jList5.getModel().getElementAt(jList5.getSelectedIndex());
                    int idx = name.indexOf("(");
                    int idx2 = name.indexOf(")");
                    int itemid = 0;
                    if (idx != -1) {
                        if (!isNumeric(name.substring(idx + 1, idx2))) {
                            itemid = Integer.valueOf(name.substring(name.lastIndexOf("(") + 1, name.lastIndexOf(")")));
                        } else {
                            itemid = Integer.valueOf(name.substring(idx + 1, idx2));
                        }
                    }
                    jTextArea3.setText("");
                    if (itemid != 0) {
                        jTextArea3.append(String.valueOf("Name : " + MapleItemInformationProvider.getInstance().getItemInformation(itemid).name));
                        jTextArea3.append("\r\n");
                        jTextArea3.append(String.valueOf("ItemCode : " + itemid));
                        jTextArea3.append("\r\n");
                        jTextArea3.append(String.valueOf("Quantity : " + hp.getItemQuantity(itemid, false)));
                        jTextArea3.append("\r\n");

                        if (hp.getItemOption(itemid, "str") != 0) {
                            jTextArea3.append(String.valueOf("Str : " + hp.getItemOption(itemid, "str")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "dex") != 0) {
                            jTextArea3.append(String.valueOf("Dex : " + hp.getItemOption(itemid, "dex")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "int") != 0) {
                            jTextArea3.append(String.valueOf("Int : " + hp.getItemOption(itemid, "int")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "luk") != 0) {
                            jTextArea3.append(String.valueOf("Luk : " + hp.getItemOption(itemid, "luk")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "watk") != 0) {
                            jTextArea3.append(String.valueOf("watk : " + hp.getItemOption(itemid, "watk")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "matk") != 0) {
                            jTextArea3.append(String.valueOf("matk : " + hp.getItemOption(itemid, "matk")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "arc") != 0) {
                            jTextArea3.append(String.valueOf("Arc : " + hp.getItemOption(itemid, "arc")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "arcexp") != 0) {
                            jTextArea3.append(String.valueOf("ArcExp : " + hp.getItemOption(itemid, "arcexp")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "arclevel") != 0) {
                            jTextArea3.append(String.valueOf("ArcLevel : " + hp.getItemOption(itemid, "arclevel")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "bossDamage") != 0) {
                            jTextArea3.append(String.valueOf("BossDamage : " + hp.getItemOption(itemid, "bossDamage")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "allStat") != 0) {
                            jTextArea3.append(String.valueOf("AllStat : " + hp.getItemOption(itemid, "allStat")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "amazingequipscroll") != 0) {
                            jTextArea3.append(String.valueOf("AmazingEquipScroll : " + hp.getItemOption(itemid, "amazingequipscroll")));
                            jTextArea3.append("\r\n");
                        }

                        if (hp.getItemOption(itemid, "ViciousHammer") != 0) {
                            jTextArea3.append(String.valueOf("ViciousHammer : " + hp.getItemOption(itemid, "ViciousHammer")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "karmaCount") != 0) {
                            jTextArea3.append(String.valueOf("karmaCount : " + hp.getItemOption(itemid, "karmaCount")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "enhance") != 0) {
                            jTextArea3.append(String.valueOf("Enhance : " + hp.getItemOption(itemid, "enhance")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "state") != 0) {
                            jTextArea3.append(String.valueOf("State : " + hp.getItemOption(itemid, "state")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "potential1") != 0) {
                            jTextArea3.append(String.valueOf("potential1 : " + hp.getItemOption(itemid, "potential1")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "potential2") != 0) {
                            jTextArea3.append(String.valueOf("potential2 : " + hp.getItemOption(itemid, "potential2")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "potential3") != 0) {
                            jTextArea3.append(String.valueOf("potential3 : " + hp.getItemOption(itemid, "potential3")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "potential4") != 0) {
                            jTextArea3.append(String.valueOf("potential4 : " + hp.getItemOption(itemid, "potential4")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "potential5") != 0) {
                            jTextArea3.append(String.valueOf("potential5 : " + hp.getItemOption(itemid, "potential5")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "potential6") != 0) {
                            jTextArea3.append(String.valueOf("potential6 : " + hp.getItemOption(itemid, "potential6")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "soulname") != 0) {
                            jTextArea3.append(String.valueOf("SoulName : " + hp.getItemOption(itemid, "soulname")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "soulenchanter") != 0) {
                            jTextArea3.append(String.valueOf("SoulEnchanter : " + hp.getItemOption(itemid, "soulenchanter")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "soulpotential") != 0) {
                            jTextArea3.append(String.valueOf("SoulPotential : " + hp.getItemOption(itemid, "soulpotential")));
                            jTextArea3.append("\r\n");
                        }
                        if (hp.getItemOption(itemid, "soulskill") != 0) {
                            jTextArea3.append(String.valueOf("SoulSkill : " + hp.getItemOption(itemid, "soulskill")));
                            jTextArea3.append("\r\n");
                        }
                    }
                }
            }
        }
    }//GEN-LAST:event_jList5MouseClicked

    private void jTextArea3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextArea3MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextArea3MouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if (!"공지사항".equals(this.jTextField20.getText())) {
            World.Broadcast.broadcastMessage(CWvsContext.serverNotice(1, "", this.jTextField20.getText()));
            JOptionPane.showMessageDialog(null, "[" + this.jTextField20.getText() + "] 내용을 모든 플레이어에게 알렸습니다.");
            this.jTextField20.setText("공지사항");
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        if (!"공지사항".equals(this.jTextField20.getText())) {
            World.Broadcast.broadcastMessage(CWvsContext.getTopMsg(this.jTextField20.getText()));
            JOptionPane.showMessageDialog(null, "[" + this.jTextField20.getText() + "] 내용을 모든 플레이어에게 알렸습니다.");
            this.jTextField20.setText("공지사항");
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if (!"공지사항".equals(this.jTextField20.getText())) {
            if (this.jRadioButton3.isSelected()) {
                if (isNumeric(this.jTextField21.getText())) {
                    if (this.jRadioButton1.isSelected()) {
                        World.Broadcast.broadcastMessage(CField.NPCPacket.getNPCTalk(Integer.valueOf(this.jTextField21.getText()), (byte) 0, this.jTextField20.getText(), "00 00", (byte) 0));
                    } else if (this.jRadioButton2.isSelected()) {
                        World.Broadcast.broadcastMessage(CField.NPCPacket.getNPCTalk(Integer.valueOf(this.jTextField21.getText()), (byte) 0, "#fn나눔고딕#" + this.jTextField20.getText(), "00 00", (byte) 0));
                    }
                } else {
                    if (this.jRadioButton1.isSelected()) {
                        World.Broadcast.broadcastMessage(CField.NPCPacket.getNPCTalk(9062004, (byte) 0, this.jTextField20.getText(), "00 00", (byte) 0));
                    } else if (this.jRadioButton2.isSelected()) {
                        World.Broadcast.broadcastMessage(CField.NPCPacket.getNPCTalk(9062004, (byte) 0, "#fn나눔고딕#" + this.jTextField20.getText(), "00 00", (byte) 0));
                    }
                }
            } else {
                if (isNumeric(this.jTextField21.getText())) {
                    World.Broadcast.broadcastMessage(CField.NPCPacket.OnAddPopupSay(Integer.valueOf(this.jTextField21.getText()), 3500, this.jTextField20.getText(), ""));
                } else {
                    World.Broadcast.broadcastMessage(CField.NPCPacket.OnAddPopupSay(9062004, 3500, this.jTextField20.getText(), ""));
                }
            }
            JOptionPane.showMessageDialog(null, "[" + this.jTextField20.getText() + "] 내용을 모든 플레이어에게 알렸습니다.");
            this.jTextField20.setText("공지사항");
            this.jTextField21.setText("9062004");
            this.jRadioButton1.setSelected(true);
            this.jRadioButton3.setSelected(true);
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        if (ServerConstants.serverCheck) {
            ServerConstants.serverCheck = false;
            this.jButton10.setText("점검 ON");
            JOptionPane.showMessageDialog(null, "서버점검이 비활성화되었습니다.");
        } else {
            ServerConstants.serverCheck = true;
            this.jButton10.setText("점검 OFF");
            JOptionPane.showMessageDialog(null, "서버점검이 활성화되었습니다.");
        }
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        MapleMap map = null;
        for (ChannelServer cs : ChannelServer.getAllInstances()) {
            for (MapleCharacter hp : cs.getPlayerStorage().getAllCharacters().values()) {
                hp.warp(100000000);
            }
        }
        JOptionPane.showMessageDialog(null, "모든 플레이어를 광장으로 이동 시켰습니다.");
    }//GEN-LAST:event_jButton13ActionPerformed

    private void jButton14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton14ActionPerformed
        for (ChannelServer cserv : ChannelServer.getAllInstances()) {
            cserv.getPlayerStorage().disconnectAll();
        }
        JOptionPane.showMessageDialog(null, "모든 플레이어가 로그인 서버로 이동 되었습니다.");
    }//GEN-LAST:event_jButton14ActionPerformed

    private void jButton26MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton26MouseClicked
        jList7.removeAll();
        DefaultListModel model = new DefaultListModel();
        for (int i = 0; i < GMConstants.getGMID().size(); i++) {
            model.addElement(GMConstants.getGMID().get(i));
        }
        jList7.setModel(model);
    }//GEN-LAST:event_jButton26MouseClicked

    private void jButton25MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton25MouseClicked
        jList7.removeAll();
        GMConstants.reloadProperty();
        DefaultListModel model1 = new DefaultListModel();
        for (int i = 0; i < GMConstants.getGMID().size(); i++) {
            model1.addElement(GMConstants.getGMID().get(i));
        }
        jList7.setModel(model1);
    }//GEN-LAST:event_jButton25MouseClicked

    private void jButton30MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton30MouseClicked
        if (jList8.getModel() != null) {
            String name = jList8.getModel().getElementAt(jList8.getSelectedIndex());
            boolean check = false;
            for (int i = 0; i < Discordcommand.getGM().size(); i++) {
                if (Discordcommand.getGM().get(i).getLeft().equals(name)) {
                    Discordcommand.getGM().remove(i);
                    JOptionPane.showMessageDialog(null, name + "님을 로그아웃 시켰습니다.");
                    check = true;
                }
            }
            if (check) {
                removeAdminAll();
                for (int i = 0; i < Discordcommand.getGM().size(); i++) {
                    addAdmin(Discordcommand.getGM().get(i).getLeft());
                }
            }
        }
    }//GEN-LAST:event_jButton30MouseClicked

    private void jButton28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton28MouseClicked
        Triple<String, Long, Long> oldDiscordSetting = DiscordSetting.discordBotSetting;
        if (jTextField23.getText() != null && jTextField24.getText() != null) {
            DiscordSetting.discordBotSetting = new Triple<String, Long, Long>(oldDiscordSetting.getLeft(), Long.valueOf(jTextField23.getText()), oldDiscordSetting.getRight());
            JOptionPane.showMessageDialog(null, "변경이 완료되었습니다.");
        }
    }//GEN-LAST:event_jButton28MouseClicked

    private void jButton29MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton29MouseClicked
        Triple<String, Long, Long> oldDiscordSetting = DiscordSetting.discordBotSetting;
        if (jTextField23.getText() != null && jTextField24.getText() != null) {
            DiscordSetting.discordBotSetting = new Triple<String, Long, Long>(oldDiscordSetting.getLeft(), oldDiscordSetting.getMid(), Long.valueOf(jTextField24.getText()));
            JOptionPane.showMessageDialog(null, "변경이 완료되었습니다.");
        }
    }//GEN-LAST:event_jButton29MouseClicked

    private void jButton27MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton27MouseClicked

        if (jTextField25.getText() != null && jTextField26.getText() != null) {
            DiscordSetting.AdminLogin = new Pair<String, String>(jTextField25.getText(), jTextField26.getText());
        }
    }//GEN-LAST:event_jButton27MouseClicked

    public static String getItemName(final int id) {
        return MapleItemInformationProvider.getInstance().getName(id);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        JMenuItem menu = (JMenuItem) evt.getSource();
        jFrame2.setVisible(true);
        jFrame2.setSize(480, 440);
    }

    class Alert extends Thread {

        @Override
        public void run() {
            try {
                Alert(txtMsg, txtOpt, intOpt);
            } catch (Exception e) {

            }
        }
    }

    private void Alert(String msg, String opt, int opt2) {
        JOptionPane.showMessageDialog(null, msg, opt, opt2);
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConnectorPanel().setVisible(true);
            }
        });
    }

    public static int getModelId(String ip) {
        /*DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            if (model.getValueAt(i, 2).toString().equals(ip)) {
                return i;
            }
        }*/
        return 1;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup2;
    public static javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton14;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton16;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    public static javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    private javax.swing.JButton jButton25;
    private javax.swing.JButton jButton26;
    private javax.swing.JButton jButton27;
    private javax.swing.JButton jButton28;
    public javax.swing.JButton jButton29;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton30;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    public static javax.swing.JFrame jFrame2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    public static javax.swing.JLabel jLabel25;
    public static javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList2;
    public static javax.swing.JList<String> jList3;
    public static javax.swing.JList<String> jList4;
    public static javax.swing.JList<String> jList5;
    public static javax.swing.JList<String> jList6;
    public static javax.swing.JList<String> jList7;
    public static javax.swing.JList<String> jList8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTabbedPane jTabbedPane1;
    public static final javax.swing.JTable jTable3 = new javax.swing.JTable();
    public javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField17;
    private javax.swing.JTextField jTextField18;
    private javax.swing.JTextField jTextField19;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField20;
    private javax.swing.JTextField jTextField21;
    public javax.swing.JTextField jTextField22;
    public javax.swing.JTextField jTextField23;
    private javax.swing.JTextField jTextField24;
    private javax.swing.JTextField jTextField25;
    private javax.swing.JTextField jTextField26;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables
}
