/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools.wztosql;

import constants.GameConstants;
import database.DatabaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import server.MapleItemInformationProvider;

public class DumpItemGather {

    public static void main() {
        System.out.println("[알림] 장비, 치장 분리를 시작합니다.");
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM inventoryitems WHERE inventorytype = ?");
            ps.setInt(1, 1);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int item = rs.getInt("itemid");
                byte type = GameConstants.getInventoryType(item).getType();
                PreparedStatement pse = con.prepareStatement("UPDATE inventoryitems SET inventorytype = ? WHERE itemid = ?");
                pse.setByte(1, type);
                pse.setInt(2, item);
                pse.executeUpdate();
                pse.close();
                if (ii.isCash(item)) {
                    System.out.println(ii.getName(item) + "(" + item + ") 변환");
                }
            }
            ps.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            System.out.println("[알림] 장비, 치장 분리를 종료합니다.");
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }
}
