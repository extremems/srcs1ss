/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handling.netty;

import client.MapleClient;
import constants.ServerConstants;
import handling.SendPacketOpcode;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import tools.HexTool;
import tools.MapleAESOFB;
import java.util.concurrent.locks.Lock;
import tools.data.ByteArrayByteStream;
import tools.data.LittleEndianAccessor;

/**
 *
 * @author csproj
 */
public class MapleNettyEncoder extends MessageToByteEncoder<byte[]> {

    @Override
    protected void encode(ChannelHandlerContext ctx, byte[] msg, ByteBuf buffer) throws Exception {

        final MapleClient client = ctx.channel().attr(MapleClient.CLIENTKEY).get();

        if (client != null) {
            final Lock mutex = client.getLock();

            mutex.lock();
            LittleEndianAccessor slea = new LittleEndianAccessor(new ByteArrayByteStream(msg));
            int header_num = slea.readShort();
            if (header_num != 1094
                    && header_num != 1057
                    && header_num != SendPacketOpcode.SPAWN_MONSTER.getValue()
                    && header_num != SendPacketOpcode.SPAWN_MONSTER_CONTROL.getValue()) {
                //    System.out.println("SEND " + header_num + " : " + SendPacketOpcode.getOpcodeName(header_num) + " : " + slea.toString());
            }
            try {
                if (ServerConstants.DEBUG_SEND) {
                    if (header_num != 100) {
                        System.out.println("[SEND] : Opcode " + header_num + " / Data " + HexTool.toString(msg));
                        System.out.println(HexTool.toStringFromAscii(msg) + "\n");
                    }
                }

                final MapleAESOFB send_crypto = client.getSendCrypto();
                buffer.writeBytes(send_crypto.getPacketHeader(msg.length));
                buffer.writeBytes(send_crypto.crypt(msg));
                ctx.flush();
            } finally {
                mutex.unlock();
            }
        } else {
            buffer.writeBytes(msg);
        }
    }
}
