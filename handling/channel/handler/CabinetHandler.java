/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handling.channel.handler;

import client.MapleCabinet;
import client.MapleClient;
import client.inventory.Item;
import tools.data.LittleEndianAccessor;
import tools.data.MaplePacketLittleEndianWriter;
import tools.packet.CWvsContext;
import tools.packet.CabinetPacket;
import tools.packet.PacketHelper;

/**
 *
 * @author 펠로스
 */
public class CabinetHandler {
    
    public static void CabinetAction(final LittleEndianAccessor slea, final MapleClient c) {
        int action = slea.readInt();
        
        switch (action) {
            case 0:
                c.getSession().writeAndFlush(CabinetPacket.CabinetAction(c.getPlayer().getCabinet(), 9));
                break;
            case 4: // 아이템 수령    
                int id = slea.readInt();
                MapleCabinet message = c.getPlayer().getCabinet().getStorage().get(id);
                if (message != null) {
                    c.getPlayer().gainItem(message.getItemId(), message.getQuantity());
                    c.getSession().writeAndFlush(CWvsContext.InfoPacket.getShowItemGain(message.getItemId(), (short) message.getQuantity(), false));
                    c.getPlayer().getCabinet().removeMessage(id);
                    c.getSession().writeAndFlush(CabinetPacket.CabinetAction(null, 12, id));
                }
                break;
        }
    }
}
