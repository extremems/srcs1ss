/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handling.discord;

import client.inventory.Equip;
import client.inventory.Item;
import constants.GameConstants;
import java.awt.Color;
import java.util.Optional;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import server.MapleItemInformationProvider;

/**
 *
 * @author KoreaDev <koreadev2@nate.com>
 *
 */
public class DiscordGeneralChat implements ChatListener {

    @Override
    public void generalChat(final String name, final String medalname, final String ChrChannel, final String message) {
        Optional<TextChannel> channel = DiscordSetting.api.getTextChannelById(DiscordSetting.discordBotSetting.getMid());
        if (channel.isPresent()) {
            EmbedBuilder em = new EmbedBuilder();
            em.setTitle(name + " " + ChrChannel);
            em.addField(medalname, message);
            em.setColor(Color.WHITE);
            channel.get().sendMessage(em);
        }

    }

    @Override
    public void itemMegaphoneChat(final String name, final String medalname, final String ChrChannel, final String message, Item item) {
        MapleItemInformationProvider ii = MapleItemInformationProvider.getInstance();
        String msg = "";
        msg += "\r\n [아이템 정보]\r\n ";
        switch (GameConstants.getInventoryType(item.getItemId())) {
            case EQUIP:
            case DECORATION:
            case EQUIPPED:
                msg += EquipMsg(ii, item);
                break;
            default:
                msg += "\r\n 아이템 이름 : " + ii.getName(item.getItemId());
                msg += "\r\n 아이템 설명 : " + ii.getDesc(item.getItemId());
                break;
        }
        Optional<TextChannel> channel = DiscordSetting.api.getTextChannelById(DiscordSetting.discordBotSetting.getRight());
        if (channel.isPresent()) {
            EmbedBuilder em = new EmbedBuilder();
            em.setTitle(name + " " + ChrChannel);
            em.addField(medalname, message + msg);
            em.setColor(Color.WHITE);
            em.setThumbnail("https://maplestory.io/api/KMS/336/item/" + item.getItemId() + "/icon"); //이건머지? 신경안써두될듯 보내드렷음
            channel.get().sendMessage(em);
        }
    }

    public String EquipMsg(MapleItemInformationProvider ii, Item item) {
        String msg = "";
        Equip eq = (Equip) item;
        for (int i = 0; i < eq.getEnhance(); i++) {
            msg += "★";
        }
        msg += "\r\n 아이템 이름 : " + ii.getName(item.getItemId()) + "(+" + eq.getLevel() + ")";
        if (eq.getStr() > 0) {
            msg += "\r\n Str +" + eq.getStr();
        }
        if (eq.getDex() > 0) {
            msg += "\r\n Dex +" + eq.getDex();
        }
        if (eq.getLuk() > 0) {
            msg += "\r\n Luk +" + eq.getLuk();
        }
        if (eq.getInt() > 0) {
            msg += "\r\n Int +" + eq.getInt();
        }
        if (eq.getWatk() > 0) {
            msg += "\r\n 공격력 +" + eq.getWatk();
        }
        if (eq.getMatk() > 0) {
            msg += "\r\n 마력 +" + eq.getMatk();
        }
        if (eq.getTotalDamage() > 0) {
            msg += "\r\n 데미지 +" + eq.getTotalDamage() + "%";
        }
        if (eq.getBossDamage() > 0) {
            msg += "\r\n 보스 몬스터 공격 시 데미지 +" + eq.getBossDamage() + "%";
        }
        if (eq.getAllStat() > 0) {
            msg += "\r\n 올스텟 +" + eq.getAllStat() + "%";
        }
        if (eq.getPotential1() > 0) {
            msg += "\r\n 잠재능력 1 : " + ii.getPotentialName(item, eq.getPotential1());
        }
        if (eq.getPotential2() > 0) {
            msg += "\r\n 잠재능력 2 : " + ii.getPotentialName(item, eq.getPotential2());
        }
        if (eq.getPotential3() > 0) {
            msg += "\r\n 잠재능력 3 : " + ii.getPotentialName(item, eq.getPotential3());
        }
        if (eq.getPotential4() > 0) {
            msg += "\r\n 잠재능력 4 : " + ii.getPotentialName(item, eq.getPotential4());
        }
        if (eq.getPotential5() > 0) {
            msg += "\r\n 잠재능력 5 : " + ii.getPotentialName(item, eq.getPotential5());
        }
        if (eq.getPotential6() > 0) {
            msg += "\r\n 잠재능력 6 : " + ii.getPotentialName(item, eq.getPotential6());
        }
        int faildslot = 0;
        if (eq.getViciousHammer() == 0 && ii.getSlots(item.getItemId()) == eq.getUpgradeSlots()) {
            faildslot = 0;
        } else if (eq.getViciousHammer() == 0) {
            faildslot = ii.getSlots(item.getItemId()) - eq.getLevel() + eq.getUpgradeSlots();
        } else {
            faildslot = ii.getSlots(item.getItemId()) - eq.getLevel() + eq.getUpgradeSlots() + 1;
        }
        msg += "\r\n 업그레이드 가능 횟수 : " + (eq.getViciousHammer() > 0 ? eq.getUpgradeSlots() + 1 : eq.getUpgradeSlots()) + "(복구 가능 횟수 : " + faildslot + ")";
        return msg;
    }
}
