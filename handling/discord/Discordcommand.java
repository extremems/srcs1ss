/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handling.discord;

import client.MapleCharacter;
import database.DatabaseConnection;
import handling.RecvPacketOpcode;
import handling.SendPacketOpcode;
import handling.channel.ChannelServer;
import handling.world.World;
import static handling.world.World.Guild.getGuild;
import handling.world.guild.MapleGuild;
import java.awt.Color;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import scripting.NPCScriptManager;
import scripting.PortalScriptManager;
import scripting.ReactorScriptManager;
import server.life.MapleMonsterInformationProvider;
import server.shops.MapleShopFactory;
import tools.ConnectorPanel;
import tools.Pair;
import tools.packet.CField;

/**
 *
 * @author KoreaDev <koreadev2@nate.com>
 *
 */
public class Discordcommand implements MessageCreateListener {

    private MessageCreateEvent event;
    private Long Id;
    private String Name;
    private static List<Pair<String, Long>> GMList = new ArrayList<>();

    public void setGM(String Name, Long Id) {
        GMList.add(new Pair<>(Name, Id));
    }

    public void removeGM(String Name) {
        for (int i = 0; i < GMList.size(); i++) {
            if (GMList.get(i).left.equals(Name)) {
                GMList.remove(i);
            }
        }
    }

    public boolean getGM(String Name) {
        for (Pair<String, Long> name : GMList) {
            if (name.left.equals(Name)) {
                return true;
            }
        }
        return false;
    }

    public static List<Pair<String, Long>> getGM() {
        return GMList;
    }

    public boolean getId(Long id) {
        for (Pair<String, Long> gm : GMList) {
            if (gm.right.equals(id)) {
                return true;
            }
        }
        return false;
    }

    public Long getId(String Name) {
        for (Pair<String, Long> gm : GMList) {
            if (gm.left.equals(Name)) {
                return gm.right;
            }
        }
        return -1L;
    }

    public void updateGM() {
        if (!getGM(getName()) && getId(getId())) {
            for (int i = 0; i < GMList.size(); i++) {
                if (Objects.equals(GMList.get(i).right, getId()) && !GMList.get(i).left.equals(getName())) {
                    GMList.set(i, new Pair<>(getName(), getId()));
                }
            }
            ConnectorPanel.removeAdminAll();
            for (int i = 0; i < GMList.size(); i++) {
                ConnectorPanel.addAdmin(GMList.get(i).getLeft());
            }
        }
    }

    public void setId(Long id) {
        this.Id = id;
    }

    public Long getId() {
        return Id;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getName() {
        return Name;
    }

    public boolean isBot() {
        return event.getMessage().getUserAuthor().get().isBot();
    }

    public void setEvent(MessageCreateEvent event) {
        this.event = event;
    }

    public boolean isChatChannel() {
        return event.getChannel().getId() == DiscordSetting.discordBotSetting.getMid();
    }

    public void dropMessage(int type, String message) {
        if (type == 1) {
            event.getChannel().sendMessage("```" + message + "```");
        } else if (type == 2) {
            EmbedBuilder em = new EmbedBuilder();
            em.setTitle("< 연 >");
            em.setDescription(message);
            em.setColor(Color.WHITE);
            event.getChannel().sendMessage(em);
        } else {
            event.getChannel().sendMessage(message);
        }
    }

    public String getCustomData(int id, int type) {
        Connection con = null;
        String s = "";
        try {
            con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM questinfo WHERE characterid = ? AND quest = ?");
            ps.setInt(1, id);
            ps.setInt(2, type);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                s = rs.getString("customData");
            }
            ps.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return s;
    }

    public long getKeyValue(int id, int type, String key) {
        String s = getCustomData(id, type);
        String[] data = s.split(";");
        for (int i = 0; i < data.length; i++) {
            if (data[i].contains(key)) {
                String newkey = data[i].replace(key + "=", "");
                String newkey2 = newkey.replace(";", "");
                Long ss = Long.valueOf(newkey2);
                return ss;
            }
        }
        return -1;
    }

    public void onUserCommand() {
        Message message = event.getMessage();
        String[] splitted = message.getContent().split(" ");
        switch (splitted[0]) {
            case "@로그인": {
                String id = splitted[1];
                String password = splitted[2];
                if (!id.equals(DiscordSetting.AdminLogin.getLeft())) {
                    dropMessage(2, "존재하지 않는 아이디입니다.");
                    break;
                }
                if (!password.equals(DiscordSetting.AdminLogin.getRight())) {
                    dropMessage(2, "비밀번호가 틀렸습니다.");
                    break;
                }
                if (!getGM(getName()) && !getId(getId())) {
                    setGM(getName(), getId());
                    dropMessage(2, "관리자로 로그인 되었습니다.");
                    ConnectorPanel.addAdmin(getName());
                } else {
                    dropMessage(2, "이미 로그인 되어있습니다.");
                }
                break;
            }
            case "@로그아웃": {
                if (getGM(getName()) && getId(getId())) {
                    removeGM(getName());
                    ConnectorPanel.removeAdmin(getName());
                    dropMessage(2, "로그아웃 되었습니다.");
                }
                break;
            }
            case "@서버안내": {;
                String msg = "[서버안내]\r\n";
                msg += "버전 :▶: 1.2.343\r\n";
                msg += "배율 :▶: 경험치 ▼드롭 : 1 메소 : 1\r\n";
                msg += "배율 :▶: 1~200 경험치 :200배\r\n";
                msg += "배율 :▶: 200~210 경험치 :100배\r\n";
                msg += "배율 :▶: 210~220 경험치 :80배\r\n";
                msg += "배율 :▶: 220~230 경험치 :70배\r\n";
                msg += "배율 :▶: 230~240 경험치 :50배\r\n";
                msg += "배율 :▶: 240~250 경험치 :35배\r\n";
                msg += "배율 :▶: 250~260 경험치 :20배\r\n";
                msg += "배율 :▶: 260~270 경험치 :15배\r\n";
                msg += "배율 :▶: 270~280 경험치 :10배\r\n";
                msg += "배율 :▶: 280~300 경험치 :7배\r\n";
                dropMessage(2, msg);
                break;
            }
            case "@동접": {
                int ret = 0;
                for (ChannelServer csrv : ChannelServer.getAllInstances()) {
                    int a = csrv.getPlayerStorage().getAllCharacters().size();
                    ret += a;
                }
                dropMessage(2, "총 유저 접속 수 : " + ret);
                break;
            }
            default:
                if (isBot()) {
                    break;
                }
                if (!isBot() && !getGM(getName()) && !getId(getId()) && splitted[0].charAt(0) != '@' && isChatChannel()) {
                    World.Broadcast.broadcastSmega(CField.getGameMessage(1, "[디스코드] " + getName() + "(유저) : " + message.getContent()));
                }
                break;
        }
    }

    public void onAdministratorCommand() {
        onUserCommand();
        Message message = event.getMessage();
        String[] splitted = message.getContent().split(" ");
        switch (splitted[0]) {
            case "@총온라인": {
                String msg = "";
                for (ChannelServer cs : ChannelServer.getAllInstances()) {
                    msg += cs.getChannel() + "채널 : " + cs.getPlayerStorage().getOnlinePlayers(true) + "\r\n";
                }
                dropMessage(2, msg);
                break;
            }
            case "@핫타임": {
                String players = "";
                int itemid = Integer.valueOf(splitted[1]);
                int itemQ = Integer.valueOf(splitted[2]);
                for (ChannelServer cs : ChannelServer.getAllInstances()) {
                    for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                        player.gainItem(itemid, itemQ);
                        player.dropMessage(1, "핫타임이 지급되었습니다.\r\n인벤토리를 확인해 주세요!");
                        if (player != null) {
                            players += ", ";
                        }
                        players += player.getName();
                    }
                }
                dropMessage(2, "핫타임이 지급되었습니다.\r\n아래는 핫타임을 지급받은 유저의 닉네임입니다.\n\n" + players);
                break;
            }
            case "@관리자목록": {
                String msg = "";
                for (int i = 0; i < GMList.size(); i++) {
                    msg += GMList.get(i).left + ",";
                }
                dropMessage(2, msg);
                break;
            }
            case "@회원가입": {
                boolean register = false;
                String UserId = splitted[1];
                String UserPw = splitted[2];
                Connection con = null;
                try {
                    con = DatabaseConnection.getConnection();
                    PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts WHERE name= ?");
                    ps.setString(1, UserId);
                    ResultSet rs = ps.executeQuery();
                    if (!rs.next()) {
                        PreparedStatement ps2 = con.prepareStatement("INSERT INTO accounts (name, password) VALUES (?, ?)");
                        ps2.setString(1, UserId);
                        ps2.setString(2, UserPw);
                        ps2.executeUpdate();
                        ps2.close();
                        register = true;
                    }
                    rs.close();
                    ps.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (register) {
                    dropMessage(2, "회원가입이 완료되었습니다.");
                } else {
                    dropMessage(2, "이미 가입 되어있는 아이디 입니다.");
                }
                break;
            }
            case "@유저검색": {
                Connection con = null;
                String text = "";
                try {
                    con = DatabaseConnection.getConnection();
                    PreparedStatement ps = con.prepareStatement("SELECT * FROM characters where name = ?");
                    ps.setString(1, splitted[1]);
                    ResultSet rs = ps.executeQuery();
                    if (rs.next()) {
                        text += "닉네임 : " + rs.getString("name") + "\r\n";
                        text += "레벨 : " + rs.getInt("level") + "\r\n";
                        text += "Str : " + rs.getInt("Str") + "\r\n";
                        text += "Dex : " + rs.getInt("Dex") + "\r\n";
                        text += "Luk : " + rs.getInt("Luk") + "\r\n";
                        text += "Int : " + rs.getInt("Int") + "\r\n";
                        text += "Meso : " + rs.getLong("meso") + "\r\n";
                        final MapleGuild mga = getGuild(rs.getInt("guildid"));
                        text += "GuildName : " + mga.getName() + "\r\n";
                        text += "GuildRank : " + mga.getRankTitle(rs.getInt("guildrank")) + "(" + rs.getInt("guildrank") + ")" + "\r\n";
                    } else {
                        text += "[Error] 존재하지 않는 닉네임입니다.";
                    }
                    rs.close();
                    ps.close();
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
                dropMessage(2, text);
                break;
            }
            case "@관리자권한부여": {
                for (ChannelServer cserv : ChannelServer.getAllInstances()) {
                    MapleCharacter player = null;
                    player = cserv.getPlayerStorage().getCharacterByName(splitted[1]);
                    if (player != null) {
                        byte number = Byte.parseByte(splitted[2]);
                        player.setGMLevel(number);
                        player.dropMessage(5, "[알림] " + splitted[1] + " 플레이어가 GM레벨 " + splitted[2] + " (으)로 설정되었습니다.");
                        dropMessage(2, splitted[1] + " 플레이어가 GM레벨 " + splitted[2] + " (으)로 설정되었습니다.");
                    } else {
                        dropMessage(2, "존재하지 않는 플레이어입니다.");
                    }
                }
                break;
            }
            case "@옵코드리셋": {
                SendPacketOpcode.reloadValues();
                RecvPacketOpcode.reloadValues();
                break;
            }
            case "@드롭리셋": {
                MapleMonsterInformationProvider.getInstance().clearDrops();
                ReactorScriptManager.getInstance().clearDrops();
                break;
            }
            case "@포탈리셋": {
                PortalScriptManager.getInstance().clearScripts();
                break;
            }
            case "@엔피시리셋": {
                NPCScriptManager.getInstance().scriptClear();
                break;
            }
            case "@상점리셋": {
                MapleShopFactory.getInstance().clear();
                break;
            }
            default:
                if (isBot()) {
                    break;
                }
                if (!isBot() && splitted[0].charAt(0) != '@' && isChatChannel()) {
                    World.Broadcast.broadcastSmega(CField.getGameMessage(1, "[디스코드] " + getName() + "(관리자) : " + message.getContent()));
                }
                break;
        }
    }

    @Override
    public void onMessageCreate(MessageCreateEvent event) {
        Message message = event.getMessage();
        setEvent(event);
        setName(message.getAuthor().getDisplayName());
        setId(message.getAuthor().getId());
        if (!GMList.isEmpty()) {
            updateGM();
            if (getGM(getName()) && getId(getId())) {
                onAdministratorCommand();
            } else {
                onUserCommand();
            }
        } else {
            onUserCommand();
        }
    }

}
