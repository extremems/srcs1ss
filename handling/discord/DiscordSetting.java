/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handling.discord;

import handling.channel.handler.ChatHandler;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.activity.ActivityType;
import tools.Pair;
import tools.Triple;

/**
 *
 * @author KoreaDev <koreadev2@nate.com>
 *
 */
public class DiscordSetting {

    public static Triple<String, Long, Long> discordBotSetting;
    public static Pair<String, String> AdminLogin;
    public static DiscordApi api;

    public void run() {
        try {
            System.out.println("[알림] 디스코드 봇 설정을 로딩합니다.");
            FileInputStream setting = new FileInputStream("discordBot.korea");
            Properties setting_ = new Properties();
            setting_.load(setting);
            setting.close();
            String token = setting_.getProperty(toUni("token"));
            Long generalChat = Long.parseLong(setting_.getProperty(toUni("generalChat")));
            Long itemMega = Long.parseLong(setting_.getProperty(toUni("itemMega")));
            String ID = setting_.getProperty(toUni("ID"));
            String PW = setting_.getProperty(toUni("PW"));
            discordBotSetting = new Triple(token, generalChat, itemMega);
            AdminLogin = new Pair(ID, PW);
        } catch (Exception e) {
            System.err.println("[오류] 디스코드 설정을 불러오는데 실패하였습니다.");
            e.printStackTrace();
        }

        DiscordApiBuilder builder = new DiscordApiBuilder();
        builder.setToken(DiscordSetting.discordBotSetting.getLeft());
        api = builder.login().join();
        api.addListener(new Discordcommand());
        api.updateActivity(ActivityType.PLAYING, "PANDORA | @도움말");
        DiscordGeneralChat chat = new DiscordGeneralChat();
        ChatHandler.addListener(chat);
    }

    protected static String toUni(String kor) throws UnsupportedEncodingException {
        return new String(kor.getBytes("KSC5601"), "8859_1");
    }

    public static final void println(final String text, final int color) {
        System.out.println(text);
    }

}
