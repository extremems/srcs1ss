/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handling.discord;

import client.inventory.Item;

/**
 *
 * @author KoreaDev <koreadev2@nate.com>
 *
 */
public interface ChatListener {

    void generalChat(final String name, final String medalname, final String channel, final String message);

    void itemMegaphoneChat(final String name, final String medalname, final String channel, final String message, Item item);
}
