/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handling.world.guild;

public enum MapleGuildType {
    // 1.2.343 [+5]
    showInfo(55), // -7
    lookInfo(56), // -7
    InfoRes(56), // -7
    getInfo(57), // -7
    newInfo(62), // -7
    newGuildMember(70), // -7
    Request(79), // -7
    delayRequest(84), // -7
    memberLeft(88), // -7
    Expelled(91), // -7
    Disband(94), // -7
    DenyReq(99), // -7
    cancelRequest(100), // -7
    InviteDeny(101), // -7
    Invite(103), // -7
    CapacityChange(111), // -7
    memberUpdate(115), // -7
    memberOnline(116), // -7
    rankTitleChange(122), // -6
    rankChange(128), // -2
    Contribution(130), // -2
    CustomEmblem(132), // -2
    Notice(141),
    Setting(143),
    updatePoint(151),
    rankRequest(152),
    removeDisband(157),
    Skill(159),
    UseNoblessSkill(162),
    ChangeLeader(174),
    Attendance(182);

    private final int type;

    private MapleGuildType(int i) {
        this.type = i;
    }

    public final int getType() {
        return type;
    }

}
