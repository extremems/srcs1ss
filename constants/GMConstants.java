package constants;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class GMConstants {

    private static GMConstants instance = new GMConstants();
    public static List<String> GM_ID = new ArrayList<String>();

    public static GMConstants getInstance() {
        return instance;
    }

    protected static String toUni(String kor) throws UnsupportedEncodingException {
        return new String(kor.getBytes("KSC5601"), "8859_1");
    }

    public static List<String> getGMID() {
        return GM_ID;
    }

    public static void reloadProperty() {
        try {
            FileInputStream setting = new FileInputStream("GM.korea");
            Properties setting_ = new Properties();
            setting_.load(setting);
            setting.close();
            GM_ID.removeAll(GM_ID);
            String gmid = setting_.getProperty(toUni("gm_id"));
            if (!gmid.isEmpty()) {
                String gmsid[] = gmid.split(",");
                for (int i = 0; i < gmsid.length; i++) {
                    GM_ID.add(gmsid[i]);
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    static {
        try {
            FileInputStream setting = new FileInputStream("GM.korea");
            Properties setting_ = new Properties();
            setting_.load(setting);
            setting.close();

            String gmid = setting_.getProperty(toUni("gm_id"));
            if (!gmid.isEmpty()) {
                String gmsid[] = gmid.split(",");
                for (int i = 0; i < gmsid.length; i++) {
                    GM_ID.add(gmsid[i]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
