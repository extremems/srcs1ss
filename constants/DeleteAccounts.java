package constants;

import client.MapleCharacter;
import client.MapleClient;
import database.DatabaseConnection;
import handling.world.World;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import scripting.NPCConversationManager;
import tools.FileoutputUtil;

public class DeleteAccounts {

    public static void main(String[] args) {

        DatabaseConnection.init();

        List<Integer> deletes = new ArrayList<>();

        try {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Timestamp ts = rs.getTimestamp("lastlogin");
                if (ts != null) {
                    if (ts.getYear() <= 2019 && ts.getDate() <= 31 && ts.getMonth() <= 4) {
                        deletes.add(rs.getInt("id"));
                    }
                } else {
                    deletes.add(rs.getInt("id"));
                }
            }
            ps.close();
            rs.close();

            for (int delete : deletes) {
                ps = con.prepareStatement("DELETE FROM accounts WHERE id = ?");
                ps.setInt(1, delete);
                ps.executeUpdate();
                ps.close();

                ps = con.prepareStatement("SELECT * FROM characters WHERE accountid = ?");
                ps.setInt(1, delete);
                rs = ps.executeQuery();
                while (rs.next()) {
                    deleteCharacter(rs.getInt("id"), delete);
                }

                ps.close();
                rs.close();
            }

            con.close();

        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    public static final int deleteCharacter(final int cid, final int accId) {
        try {
            final Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT guildid, guildrank, familyid, name FROM characters WHERE id = ? AND accountid = ?");
            ps.setInt(1, cid);
            ps.setInt(2, accId);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                rs.close();
                ps.close();
                return 9;
            }
            String name = rs.getString("name");
            if (rs.getInt("guildid") > 0) { // is in a guild when deleted
                if (rs.getInt("guildrank") == 1) { //cant delete when leader
                    rs.close();
                    ps.close();
                    return 10; // 22 : error
                }
                World.Guild.deleteGuildCharacter(rs.getInt("guildid"), cid);
            }
            rs.close();
            ps.close();
            PreparedStatement ps2 = con.prepareStatement("SELECT email, macs FROM accounts WHERE id = ?");
            ps2.setInt(1, accId);
            ResultSet rs2 = ps2.executeQuery();
            if (!rs2.next()) {
                ps2.close();
                rs2.close();
            }
            ps2.close();
            rs2.close();
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM characters WHERE id = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM hiredmerch WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM mountdata WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM inventoryitems WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM inventoryitemsuse WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM inventoryitemssetup WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM inventoryitemsetc WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM inventoryitemscash WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM famelog WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM famelog WHERE characterid_to = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM dueypackages WHERE RecieverId = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM wishlist WHERE characterid = ?", cid);

            ps = con.prepareStatement("DELETE FROM buddies WHERE repname = ?");
            ps.setString(1, name);
            ps.executeUpdate();
            ps.close();

            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM keymap WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM trocklocations WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM regrocklocations WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM hyperrocklocations WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM savedlocations WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM skills WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM mountdata WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM skillmacros WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM trocklocations WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM questinfo WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM queststatus WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM inventoryslot WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM extendedSlots WHERE characterid = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM `unions` WHERE id = ?", cid);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM `unionkey` WHERE accid = ?", accId);
            MapleCharacter.deleteWhereCharacterId(con, "DELETE FROM `keyvalue` WHERE id = ?", cid);
            con.close();
            return 0;
        } catch (Exception e) {
            FileoutputUtil.outputFileError(FileoutputUtil.PacketEx_Log, e);
            e.printStackTrace();
        }
        return 10;
    }

}
