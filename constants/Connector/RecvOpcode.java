/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants.Connector;

/**
 *
 * @author KoreaDev <koreadev2@nate.com> & sakura24<x-s-s-@nate.com>
 *
 */
public enum RecvOpcode {

    LOGIN_PASSWORD(0x01),
    Register(0x02),
    LOGIN_REQUEST(0x03),
    SESSION_CHECK(0x04),
    Login_DEL_REQUEST(0x05),
    Heart_Beat(0x06);

    private final int code;

    private RecvOpcode(int i) {
        this.code = i;
    }

    public final int getValue() {
        return code;
    }
}
