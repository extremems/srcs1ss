/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants.Connector;

import constants.ServerConstants;
import database.DatabaseConnection;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.CharsetUtil;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import tools.Triple;

/**
 *
 * @author KoreaDev <koreadev2@nate.com> & sakura24<x-s-s-@nate.com>
 *
 */
public class KuronekoHandler {

    private static ChannelHandlerContext session;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static String[] readString(ByteBuf msg) {
        return msg.toString(CharsetUtil.UTF_8).replace("POST /", "").split("HTTP/1.")[0].split("-S-p-r-i-t-s-");
    }

    public static ChannelHandlerContext getSession() {
        return session;
    }

    public static void setSession(ChannelHandlerContext ctx) {
        session = ctx;
    }

    public static String getSessionIp() {
        return getSession().channel().remoteAddress().toString().split(":")[0];
    }

    public static long UnixTime() {
        Calendar c = Calendar.getInstance();
        return c.getTimeInMillis() / 1000;
    }

    public static void getPacket() {
        getPacket(true, false);
    }

    public static void getPacket(boolean reple) {
        getPacket(reple, false);
    }

    public static void getPacket(boolean reple, boolean overlap) {
        String Reple = "";
        String packet = "";
        if (overlap) {
            Reple = "Overlap";
        } else if (reple) {
            Reple = "OK";
        } else {
            Reple = "Failed";
        }
        packet += "HTTP/1.0 200 Document Follows \r\n";
        packet += "Content-Type: Text/plain \r\n";
        packet += "Content-Length: " + Reple.length() + "\r\n";
        packet += "\r\n";
        packet += Reple;
        getSession().writeAndFlush(Unpooled.copiedBuffer(packet, CharsetUtil.UTF_8));
    }

    public static String CurrentReadable_Time() {
        return sdf.format(Calendar.getInstance().getTime());
    }

    public static String getDCurrentTime() {
        Calendar calz = Calendar.getInstance(TimeZone.getTimeZone("KST"), Locale.KOREAN);
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
        String time = simpleTimeFormat.format(calz.getTime());
        return time;
    }

    public static void log(final String file, final String msg) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file, true);
            out.write(("\n------------------------ " + CurrentReadable_Time() + " ------------------------\n\r\n").getBytes());
            out.write(msg.getBytes());
        } catch (IOException ess) {
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ignore) {
            }
        }
    }

    public static void Loginok(ByteBuf msg) {
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts WHERE name=? AND password=?");
            ps.setString(1, readString(msg)[1]);
            ps.setString(2, readString(msg)[2]);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                getPacket(true);
            } else {
                getPacket(false);
            }
            rs.close();
            ps.close();
            con.close();
            System.out.println("[알림] " + getSessionIp() + " 에서 접속기 서버에 접속했습니다.");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void Register(ByteBuf msg) {
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts WHERE name= ?");
            ps.setString(1, readString(msg)[1]);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                con = DatabaseConnection.getConnection();
                PreparedStatement query = null;
                query = con.prepareStatement("INSERT INTO accounts (name, password) VALUES (?,?)");
                query.setString(1, readString(msg)[1]);
                query.setString(2, readString(msg)[2]);
                query.executeUpdate();
                query.close();

                query = con.prepareStatement("INSERT INTO sakurareg (ip) VALUES (?)");
                query.setString(1, readString(msg)[3]);
                query.executeUpdate();
                query.close();
                getPacket(true);
                System.out.println("[알림] " + getSessionIp() + " 에서 접속기 서버에 회원가입을 했습니다. ID : " + readString(msg)[1] + " PW : " + readString(msg)[2]);
                log("LogFile/커넥터로그/" + getDCurrentTime() + ".txt", "IPAddress : " + getSessionIp() + "/ ID : " + readString(msg)[1] + "/ PW : " + readString(msg)[2]);
            } else {
                getPacket(false);
            }

            ps.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void LoginRequest(ByteBuf msg) {
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM accounts WHERE name=? AND password=?");
            ps.setString(1, readString(msg)[1]);
            ps.setString(2, readString(msg)[2]);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                PreparedStatement query = con.prepareStatement("SELECT * FROM sakuralogin WHERE id = ?");
                query.setString(1, readString(msg)[1]);
                ResultSet rs2 = query.executeQuery();
                if (!rs2.next()) {
                    PreparedStatement query2 = con.prepareStatement("INSERT INTO sakuralogin (id, ip, mapleip, time) VALUES (?, ?, ?, ?)");
                    query2.setString(1, readString(msg)[1]);
                    query2.setString(2, readString(msg)[3]);
                    query2.setString(3, readString(msg)[4]);
                    query2.setLong(4, UnixTime());
                    query2.executeUpdate();
                    query2.close();
                } else {
                    getPacket(false);
                }
                query.close();
                ServerConstants.cons.put(readString(msg)[4], new Triple<>(readString(msg)[1], readString(msg)[2], readString(msg)[3]));
                System.out.println("[알림] " + getSessionIp() + " 에서 인게임 서버에 접속했습니다.");
                getPacket(true);
            } else {
                getPacket(false);
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void sessionCheck(ByteBuf msg) {
        Connection con = null;
        try {
            int rowcount = 0;
            con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM sakurareg WHERE ip=?");
            ps.setString(1, readString(msg)[1]);
            ResultSet rs = ps.executeQuery();
            rs.last();
            rowcount = rs.getRow();
            rs.beforeFirst();
            if (rowcount < Integer.parseInt(readString(msg)[2])) {
                getPacket(true);
            } else {
                getPacket(false);
            }
            ps.close();
            rs.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public static void DelLoginok(ByteBuf msg) {
        Connection con = null;
        try {
            con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("DELETE FROM sakuralogin WHERE id = ?");
            ps.setString(1, readString(msg)[1]);
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
