package constants.Connector;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.CharsetUtil;
import java.io.IOException;

/**
 *
 * @author KoreaDev <koreadev2@nate.com> & sakura24<x-s-s-@nate.com>
 *
 */
public class KuronekoNettyHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        final String address = ctx.channel().remoteAddress().toString().split(":")[0];
        final String port = ctx.channel().remoteAddress().toString().split(":")[1];
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws IOException {
        ByteBuf in = (ByteBuf) msg;
        for (final RecvOpcode recv : RecvOpcode.values()) {
            if (recv.getValue() == CalculateRecv(in)) {
                try {
                    KuronekoHandler.setSession(ctx);
                    handle(recv, in);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }

    public static int CalculateRecv(ByteBuf msg) {
        int opcode = -1;
        String[] PacketRead = msg.toString(CharsetUtil.UTF_8).replace("POST /", "").split("HTTP/1.")[0].split("-S-p-r-i-t-s-");
        switch (PacketRead[0]) {
            case "Login":
                opcode = 0x01;
                break;
            case "Reg":
                opcode = 0x02;
                break;
            case "LoginReg":
                opcode = 0x03;
                break;
            case "RegIPcheck":
                opcode = 0x04;
                break;
            case "LoginDel":
                opcode = 0x05;
                break;
            case "HeartBeat":
                opcode = 0x06;
                break;
        }
        return opcode;
    }

    public static void handle(RecvOpcode header, ByteBuf msg) {
        switch (header) {
            case LOGIN_PASSWORD:
                KuronekoHandler.Loginok(msg);
                break;
            case Register:
                KuronekoHandler.Register(msg);
                break;
            case LOGIN_REQUEST:
                KuronekoHandler.LoginRequest(msg);
                break;
            case SESSION_CHECK:
                KuronekoHandler.sessionCheck(msg);
                break;
            case Login_DEL_REQUEST:
                KuronekoHandler.DelLoginok(msg);
                break;
        }
    }
}
