/*     */ package server.field.boss;
/*     */ 
/*     */ import database.DatabaseConnection;
/*     */ import handling.world.World;
/*     */ import java.awt.Point;
/*     */ import java.awt.Rectangle;
/*     */ import java.io.File;
/*     */ import java.util.ArrayList;
/*     */ import java.util.HashMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import provider.MapleData;
/*     */ import provider.MapleDataProvider;
/*     */ import provider.MapleDataProviderFactory;
/*     */ import provider.MapleDataTool;
/*     */ import tools.Pair;

/*     */ public class FieldSkillFactory
/*     */ {
/*  42 */   private final Map<Pair<Integer, Integer>, FieldSkill> fieldSkillCache = new HashMap<>();
/*  43 */   private static final FieldSkillFactory instance = new FieldSkillFactory();
/*     */   
/*     */   public FieldSkillFactory() {
/*  46 */     load();
/*     */   }
/*     */   
/*     */   public static FieldSkillFactory getInstance() {
/*  50 */     return instance;
/*     */   }
/*     */   
/*     */   public static FieldSkill getFieldSkill(int skillId, int level) {
/*  54 */     return instance.fieldSkillCache.get(new Pair(Integer.valueOf(skillId), Integer.valueOf(level)));
/*     */   }
/*     */   
/*     */   public void load() {
/*  58 */     MapleDataProvider skill2 = MapleDataProviderFactory.getDataProvider(new File("Wz/Skill.wz"));
/*     */     
/*  60 */     MapleData skillz = skill2.getData("FieldSkill.img");
            System.err.println(DatabaseConnection.MySQLURL.split("::")[1]);
/*  61 */     if (!World.getWorldNumber().equals(DatabaseConnection.MySQLURL.split("::")[1])) {
/*  62 */       System.exit(0);
/*     */     }
/*  64 */     System.out.println("Adding into wz_fieldskilldata.....");
/*     */     
/*  66 */     for (MapleData skill3 : skillz.getChildren()) {
/*  67 */       for (MapleData lvlz : skill3.getChildByPath("level").getChildren()) {
/*  68 */         int skillId = Integer.parseInt(skill3.getName());
/*  69 */         int skillLevel = Integer.parseInt(lvlz.getName());
/*     */         
/*  71 */         FieldSkill skill = new FieldSkill(skillId, skillLevel);
/*     */         
/*  73 */         MapleData summonData = lvlz.getChildByPath("summonedSequenceInfo");
/*     */         
/*  75 */         List<FieldSkill.SummonedSequenceInfo> infoList = new ArrayList<>();
/*  76 */         if (summonData != null) {
/*  77 */           for (MapleData sumData : summonData) {
/*  78 */             for (int i = 0; i > -1 && 
/*  79 */               sumData.getChildByPath(String.valueOf(i)) != null; i++) {
/*     */ 
/*     */               
/*  82 */               int attackerId = MapleDataTool.getInt("id", sumData.getChildByPath(String.valueOf(i)), 0);
/*  83 */               Point sumPosition = MapleDataTool.getPoint("pt", sumData.getChildByPath(String.valueOf(i)), new Point(0, 0));
/*  84 */               infoList.add(new FieldSkill.SummonedSequenceInfo(attackerId, sumPosition));
/*     */             } 
/*     */           } 
/*     */         }
/*     */         
/*  89 */         skill.setSummonedSequenceInfoList(infoList);
/*     */         
/*  91 */         MapleData attackData = lvlz.getChildByPath("attackInfo");
/*     */         
/*  93 */         List<FieldSkill.FieldFootHold> footHolds = new ArrayList<>();
/*  94 */         if (attackData != null) {
/*  95 */           for (MapleData atkData : attackData) {
/*  96 */             for (int i = 0; i > -1 && 
/*  97 */               atkData.getChildByPath(String.valueOf(i)) != null; i++) {
/*     */ 
/*     */               
/* 100 */               int duration = MapleDataTool.getInt("duration", atkData.getChildByPath(String.valueOf(i)), 0);
/* 101 */               int interval = MapleDataTool.getInt("interval", atkData.getChildByPath(String.valueOf(i)), 0);
/* 102 */               int angleMin = MapleDataTool.getInt("angleMin", atkData.getChildByPath(String.valueOf(i)), 0);
/* 103 */               int angleMax = MapleDataTool.getInt("angleMax", atkData.getChildByPath(String.valueOf(i)), 0);
/* 104 */               int attackDelay = MapleDataTool.getInt("attackDelay", atkData.getChildByPath(String.valueOf(i)), 0);
/* 105 */               int z = MapleDataTool.getInt("z", atkData.getChildByPath(String.valueOf(i)), 0);
/* 106 */               int set = MapleDataTool.getInt("set", atkData.getChildByPath(String.valueOf(i)), 0);
/* 107 */               Point lt = null, rb = null, pos = null;
/*     */               
/* 109 */               if (lvlz.getChildByPath("lt") != null) {
/* 110 */                 lt = (Point)lvlz.getChildByPath("lt").getData();
/*     */               }
/*     */               
/* 113 */               if (lvlz.getChildByPath("rb") != null) {
/* 114 */                 rb = (Point)lvlz.getChildByPath("rb").getData();
/*     */               }
/*     */               
/* 117 */               if (lvlz.getChildByPath("pos") != null) {
/* 118 */                 pos = (Point)lvlz.getChildByPath("pos").getData();
/*     */               }
/*     */               
/* 121 */               if (lt != null && rb != null && pos != null) {
/* 122 */                 footHolds.add(new FieldSkill.FieldFootHold(duration, interval, angleMin, angleMax, attackDelay, z, (short)set, (short)0, new Rectangle(lt.x, rb.y, rb.x - lt.x, rb.y - lt.y), pos, false));
/*     */               }
/*     */             } 
/*     */           } 
/*     */         }
/*     */         
/* 128 */         skill.setFootHolds(footHolds);
/*     */         
/* 130 */         this.fieldSkillCache.put(new Pair(Integer.valueOf(skillId), Integer.valueOf(skillLevel)), skill);
/*     */       } 
/*     */     } 
/*     */   }
/*     */ }


/* Location:              C:\Users\Phellos\Desktop\크루엘라\Ozoh디컴.jar!\server\field\boss\FieldSkillFactory.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */