package server.field.boss;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.skills.SkillFactory;
import server.Obstacle;
import server.Randomizer;
import server.Timer.MobTimer;
import server.field.boss.demian.MapleFlyingSword;
import server.life.*;
import server.maps.MapleMap;
import tools.Pair;
import tools.packet.CField;
import tools.packet.MobPacket;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import server.Timer;
import tools.Triple;

public class MapleBossManager {

    public static void changePhase(MapleMonster monster) {
        if (monster.getId() >= 8800102 && monster.getId() <= 8800110) {
            int[] arms = {8800103, 8800104, 8800105, 8800106, 8800107, 8800108, 8800109, 8800110};
            if (monster.getPhase() == 0) {
                monster.setPhase((byte) 1);
            }
            if (monster.getId() == 8800102) {
                boolean nextPhase = true;
                for (int arm : arms) {
                    if (monster.getMap().getMonsterById(arm) != null) {
                        nextPhase = false;
                        break;
                    }
                }
                if (nextPhase) {
                    monster.setPhase((byte) 3);
                }
                if (monster.getHPPercent() <= 20) {
                    monster.setPhase((byte) 4);
                    for (int arm : arms) {
                        monster.getMap().killMonster(arm);
                    }
                }
            }
            monster.getMap().broadcastMessage(MobPacket.changePhase(monster));
        } else if (monster.getId() == 8880000 || monster.getId() == 8880002 || monster.getId() == 8880010) {
            byte phase;
            if (monster.getHPPercent() <= 25) {
                phase = 4;
            } else if (monster.getHPPercent() <= 50) {
                phase = 3;
            } else if (monster.getHPPercent() <= 75) {
                phase = 2;
            } else {
                phase = 1;
            }
            monster.setPhase(phase);
            monster.getMap().broadcastMessage(MobPacket.changePhase(monster));
        }
    }

    public static void setBlockAttack(MapleMonster monster) {
        List<Integer> blocks = new ArrayList<>();
        switch (monster.getId()) {
            case 8800102: {
                if (monster.getPhase() != 2) {
                    monster.getMap().killMonster(8800117);
                    List<String> updateLists = new ArrayList<>();
                    monster.getMap().updateEnvironment(updateLists);
                    blocks.add(1);
                }
                if (monster.getPhase() != 3) {
                    blocks.add(2);
                    blocks.add(3);
                    blocks.add(4);
                    blocks.add(5);
                }
                if (monster.getPhase() != 4) {
                    blocks.add(6);
                    blocks.add(7);
                    blocks.add(8);
                }
                break;
            }
            case 8850011:
            case 8850111: {
                blocks.add(4);
                break;
            }
            case 8910000:
            case 8910100: {
                if (monster.getHPPercent() > 10) {
                    blocks.add(3);
                    blocks.add(4);
                    blocks.add(5);
                    blocks.add(6);
                    blocks.add(7);
                }
                if (monster.getHPPercent() > 70) {
                    blocks.add(2);
                }
                break;
            }
            case 8930000:
            case 8930100: {
                if (monster.getId() == 8930100 && monster.getHPPercent() > 70) {
                    blocks.add(2);
                    blocks.add(3);
                    blocks.add(4);
                }
                if (monster.getHPPercent() > 40) {
                    blocks.add(8);
                    blocks.add(9);
                    blocks.add(10);
                    blocks.add(12);
                    blocks.add(13);
                    blocks.add(14);
                    blocks.add(15);
                }
                break;
            }
            case 8880300:
            case 8880301:
            case 8880303:
            case 8880304:
            case 8880321:
            case 8880322:
            case 8880325:
            case 8880326:
            case 8880340:
            case 8880341:
            case 8880343:
            case 8880344:
            case 8880351:
            case 8880352:
            case 8880355:
            case 8880356:
                return;
        }
        monster.getMap().broadcastMessage(MobPacket.BlockAttack(monster, blocks));
    }

    public static void ZakumBodyHandler(MapleMonster monster, MapleCharacter chr, boolean facingLeft) {
        long time = System.currentTimeMillis();
        List<MobSkill> useableSkills = new ArrayList<>();
        for (MobSkill msi : monster.getSkills()) {
            if ((time - monster.getLastSkillUsed(msi.getSkillId(), msi.getSkillLevel())) >= 0) {
                if (monster.getHPPercent() <= msi.getHP() && !msi.isOnlyOtherSkill()) {
                    if (msi.isOnlyFsm()) {
                        if (monster.getPhase() == 2) {
                            if (msi.getSkillId() == 201 && msi.getSkillLevel() == 162) {
                                useableSkills.add(msi);
                            }
                        } else if (monster.getPhase() == 3) {
                            if (msi.getSkillId() == 176 && msi.getSkillLevel() == 27) {
                                if (msi.getSkillId() != 201 && msi.getSkillLevel() != 162) {
                                    useableSkills.add(msi);
                                }
                            }
                        }
                    } else {
                        useableSkills.add(msi);
                    }
                }
            }
        }

        if (!useableSkills.isEmpty()) {
            MobSkill msi = useableSkills.get(Randomizer.nextInt(useableSkills.size()));
            monster.setLastSkillUsed(msi, time, msi.getInterval());
            monster.setNextSkill(msi.getSkillId());
            monster.setNextSkillLvl(msi.getSkillLevel());
        }
    }

    public static void ZakumArmHandler(MapleMonster monster, MapleCharacter chr, short moveid, boolean movingAttack, boolean facingLeft) {
        long time = System.currentTimeMillis();
        List<MobSkill> useableSkills = new ArrayList<>();
        for (MobSkill msi : monster.getSkills()) {
            if ((time - monster.getLastSkillUsed(msi.getSkillId(), msi.getSkillLevel())) >= 0) {
                if (monster.getHPPercent() <= msi.getHP() && !msi.isOnlyOtherSkill()) {
                    if (msi.isOnlyFsm()) {
                        if (monster.getPhase() == 1) {
                            if (msi.getSkillId() == 176 && (msi.getSkillLevel() == 25 || msi.getSkillLevel() == 26)) {
                                useableSkills.add(msi);
                            }
                        } else if (monster.getPhase() == 2) {
                            if (msi.getSkillId() == 176 && msi.getSkillLevel() == 27) {
                                if (monster.getId() <= 8800106) {
                                    if (monster.getMap().getMonsterById(monster.getId() + 4) != null) { // 반대편 팔도 존재해야 합창하지 ㅡ.ㅡ;
                                        useableSkills.add(msi);
                                    }
                                } else {
                                    if (monster.getMap().getMonsterById(monster.getId() - 4) != null) { // 반대편 팔도 존재해야 합창하지 ㅡ.ㅡ;
                                        useableSkills.add(msi);
                                    }
                                }
                            }
                        }
                    } else {
                        useableSkills.add(msi);
                    }
                }
            }
        }

        if (!useableSkills.isEmpty()) {
            MobSkill msi = useableSkills.get(Randomizer.nextInt(useableSkills.size()));

            List<MapleMonster> nextattackMobs = new ArrayList<>();

            if (msi.getSkillId() == 176) {
                switch (msi.getSkillLevel()) {
                    case 25: { // 짧게 찍기
                        for (MapleMonster mob : monster.getMap().getAllMonstersThreadsafe()) {
                            if (mob.getId() >= 8800103 && mob.getId() <= 8800110) { // 쿨타임은 8마리 전부에게, 찍는건 그 중 일부만
                                if ((mob.getId() == monster.getId() || Randomizer.isSuccess(25)) && nextattackMobs.size() < 3) {
                                    nextattackMobs.add(mob);
                                    mob.setNextSkill(msi.getSkillId());
                                    mob.setNextSkillLvl(msi.getSkillLevel());
                                }
                                mob.setLastSkillUsed(MobSkillFactory.getMobSkill(176, 25), time, 3500);
                                mob.setLastSkillUsed(MobSkillFactory.getMobSkill(176, 26), time, 3500);
                                chr.getMap().broadcastMessage(MobPacket.moveMonsterResponse(mob.getObjectId(), moveid, mob.getMp(), movingAttack, mob.getNextSkill(), mob.getNextSkillLvl(), 0));
                            }
                        }
                        break;
                    }
                    case 26: { // 길게 찍기
                        for (MapleMonster mob : monster.getMap().getAllMonstersThreadsafe()) {
                            if (mob.getId() >= 8800103 && mob.getId() <= 8800110) { // 쿨타임은 8마리 전부에게, 찍는건 그 중 일부만
                                if ((mob.getId() == monster.getId() || Randomizer.isSuccess(50)) && nextattackMobs.size() < 6) {
                                    nextattackMobs.add(mob);
                                    mob.setNextSkill(msi.getSkillId());
                                    mob.setNextSkillLvl(msi.getSkillLevel());
                                }
                                mob.setLastSkillUsed(MobSkillFactory.getMobSkill(176, 25), time, 3500);
                                mob.setLastSkillUsed(MobSkillFactory.getMobSkill(176, 26), time, 3500);
                                chr.getMap().broadcastMessage(MobPacket.moveMonsterResponse(mob.getObjectId(), moveid, mob.getMp(), movingAttack, mob.getNextSkill(), mob.getNextSkillLvl(), 0));
                            }
                        }
                        break;
                    }
                    case 27: { // 합창
                        monster.setNextSkill(msi.getSkillId());
                        monster.setNextSkillLvl(msi.getSkillLevel());
                        for (MapleMonster mob : monster.getMap().getAllMonstersThreadsafe()) {
                            if (mob.getId() >= 8800103 && mob.getId() <= 8800110) { // 쿨타임은 8마리 전부에게, 찍는건 그 중 일부만
                                if ((mob.getId() - 4 == monster.getId()) || (mob.getId() + 4 == monster.getId())) {
                                    mob.setNextSkill(msi.getSkillId());
                                    mob.setNextSkillLvl(msi.getSkillLevel());
                                } else {
                                    mob.setNextSkill(0);
                                    mob.setNextSkillLvl(0);
                                }
                                mob.setLastSkillUsed(msi, time, 7500);

                                chr.getMap().broadcastMessage(MobPacket.moveMonsterResponse(mob.getObjectId(), moveid, mob.getMp(), movingAttack, mob.getNextSkill(), mob.getNextSkillLvl(), 0));
                            }
                        }
                    }
                    break;
                }
            } else {
                monster.setLastSkillUsed(msi, time, msi.getInterval());
                monster.setNextSkill(msi.getSkillId());
                monster.setNextSkillLvl(msi.getSkillLevel());
                chr.getMap().broadcastMessage(MobPacket.moveMonsterResponse(monster.getObjectId(), moveid, monster.getMp(), movingAttack, monster.getNextSkill(), monster.getNextSkillLvl(), 0));
            }
        }
    }

    public static void magnusHandler(MapleMonster monster) {
        if (monster != null) {
            // 1 ~ 5 : 초록, 6 ~ 7 : 파랑, 8 ~ 9 : 보라
            int[] types = {4, 6, 8};
            List<Obstacle> obs = new ArrayList<>();
            int size = Randomizer.rand(4 * monster.getPhase(), 7 * monster.getPhase());

            for (int i = 0; i < size; ++i) {
                int type = types[Randomizer.nextInt(types.length)];
                Obstacle ob;
                int x = Randomizer.rand(550, 3050);
                if (type == 4) {
                    ob = new Obstacle(4, new Point(x, -2000), new Point(x, -1347), 25, 50, 1459, Randomizer.rand(50, 150), 1, 653, 0);
                } else if (type == 8) {
                    ob = new Obstacle(8, new Point(x, -2000), new Point(x, -1347), 65, 100, 542, Randomizer.rand(50, 150), 2, 653, 0);
                } else { // 6
                    ob = new Obstacle(6, new Point(x, -2000), new Point(x, -1347), 45, 100, 1481, Randomizer.rand(50, 150), 1, 653, 0);
                }

                obs.add(ob);
            }

            monster.getMap().broadcastMessage(MobPacket.createObstacle(monster, obs, (byte) 0));
        }
    }

    public static void lotusHandler(MapleMonster monster) {
        if (monster != null) {
            // 48 ~ 52
            int[] types = {48, 49, 50, 51, 52};
            List<Obstacle> obs = new ArrayList<>();
            int aa = monster.getId() % 10;
            int size = Randomizer.rand(1, 3);

            for (int i = 0; i < size; ++i) {
                int type = types[Randomizer.nextInt(aa == 2 ? 5 : aa == 1 ? 4 : 3)];
                Obstacle ob;
                int x = Randomizer.rand(-600, 650);
                if (type == 48) {
                    ob = new Obstacle(48, new Point(x, -520), new Point(x, -16), 36, 10, 157, Randomizer.rand(650, 850), 1, 504, 0);
                } else if (type == 49) {
                    ob = new Obstacle(49, new Point(x, -520), new Point(x, -16), 51, 10, 185, Randomizer.rand(500, 700), 1, 504, 0);
                } else if (type == 50) {
                    ob = new Obstacle(50, new Point(x, -520), new Point(x, -16), 51, 10, 718, Randomizer.rand(500, 700), 2, 504, 0);
                } else if (type == 51) {
                    ob = new Obstacle(51, new Point(x, -520), new Point(x, -16), 65, 20, 797, Randomizer.rand(400, 500), 2, 504, 0);
                } else { // 52
                    ob = new Obstacle(52, new Point(x, -520), new Point(x, -16), 190, 100, 136, Randomizer.rand(300, 350), 1, 504, 0);
                }

                obs.add(ob);
            }

            monster.getMap().broadcastMessage(MobPacket.createObstacle(monster, obs, (byte) 0));
        }
    }

    /*      */ public static void SerenHandler(MapleMonster monster) {
        /*      */ int time;
        /* 1412 */ switch (monster.getId()) {
            /*      */
 /*      */
 /*      */ case 8880600:
                /* 1416 */ monster.setSchedule(Timer.MobTimer.getInstance().register(() -> {
                    List<Obstacle> obs = new ArrayList<>();

                    for (int i = 0; i < 3; i++) {
                        int x = Randomizer.rand(-1030, 1030);
                        Obstacle ob = new Obstacle(84, new Point(x, -440), new Point(x, 275), 30, 15, 871, Randomizer.rand(12, 16), 3, 715, 0);
                        obs.add(ob);
                    }

                    monster.getMap().spawnObstacle(monster, obs, (byte) 0);

                    for (MapleCharacter chr : monster.getMap().getAllChracater()) {
                        if (chr.isAlive()) {
                            chr.addSerenGauge(10); // 세렌 게이지
                        }
                    }
                },
                        5000L));
                /*      */ break;

            /*      */ case 8880602:
                /* 1435 */ monster.ResetSerenTime(true);
                /* 1436 */ monster.setSchedule(Timer.MobTimer.getInstance().register(() -> {
                    if (monster.getMap().getAllChracater().size() > 0 && monster.getCustomValue0(8880603) == 0L) {
                        monster.addSkillCustomInfo(8880602, 1L);
                        if (monster.getCustomValue0(8880602) >= ((monster.getSerenTimetype() == 3) ? 1L : 5L)) {
                            monster.removeCustomInfo(8880602);
                            if (monster.getSerenTimetype() == 4) {
                                monster.gainShield(monster.getStats().getHp() / 100L, !(monster.getShield() > 0L), 0);
                            }
                            for (MapleCharacter chr : monster.getMap().getAllChracater()) {
                                if (chr.isAlive()) {
                                    chr.addSerenGauge((monster.getSerenTimetype() == 3) ? -20 : 20);
                                }
                            }
                        }
                        monster.AddSerenTimeHandler(monster.getSerenTimetype(), -1);
                    }
                },
                        1000L));
                /*      */ break;

            /*      */ case 8880603:
                /* 1459 */ monster.setSchedule(Timer.MobTimer.getInstance().register(() -> {
                    /*      */ if (monster == null || monster.getMap().getMonsterById(8880603) == null) {
                        /*      */ monster.getSchedule().cancel(true);
                        /*      */
 /*      */
 /*      */ monster.setSchedule(null);
                        /*      */                    }
                    /*      */
 /*      */ if (monster != null) {
                        /*      */ for (int i = 0; i < 2; i++) {
                            /*      */ int time1 = (i == 0) ? 10 : 1000;
                            /*      */

 /*      */                        }
                        /*      */                    }
                    /* 1474 */                },
                        Randomizer.rand(7000, 11000)
                ));
                /*      */ break;
            /*      */
 /*      */
 /*      */ case 8880607:
                /* 1479 */ monster.setSchedule(Timer.MobTimer.getInstance().register(() -> {
                    /*      */ FieldSkillFactory.getInstance();
                    monster.getMap().broadcastMessage(MobPacket.useFieldSkill(FieldSkillFactory.getFieldSkill(100023, 1)));
                    /* 1481 */                },
                        Randomizer.rand(5000, 10000)
                ));
                /*      */ break;
            /*      */
 /*      */
 /*      */ case 8880605:
                /* 1486 */ monster.setSchedule(Timer.MobTimer.getInstance().register(() -> {
                    for (MapleCharacter chr : monster.getMap().getAllChracater()) {
                        if (chr.isAlive() && (chr.getPosition()).x - 300 <= (monster.getPosition()).x && (chr.getPosition()).x + 300 >= (monster.getPosition()).x) {
                            monster.addSkillCustomInfo(8880605, 1L);
                            if (monster.getCustomValue0(8880605) >= 10L) {
                                monster.switchController(chr.getClient().getRandomCharacter(), true);
                                monster.removeCustomInfo(8880605);
                            }
                            monster.getMap().broadcastMessage(MobPacket.enableOnlyFsmAttack(monster, 1, 0));
                            break;
                        }
                    }
                },
                        2000L));
                /*      */ break;
            /*      */

 /*      */
 /*      */ case 8880610:
                /* 1504 */ monster.setSchedule(Timer.MobTimer.getInstance().register(() -> {
                    /*      */ if (monster != null) {
                        /*      */ int type = Randomizer.rand(0, 2);
                        /*      */
 /*      */ if (type == 0) {
                            /*      */ monster.getMap().spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880611), new Point(-320, 305));
                            /*      */
 /*      */ monster.getMap().spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880611), new Point(470, 305));
                            /*      */                        } else {
                            /*      */ monster.getMap().spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880611), (type == 1) ? new Point(-320, 305) : new Point(470, 305));
                            /*      */                        }
                        /*      */
 /*      */                    }
                    /* 1518 */                },
                        Randomizer.rand(30000, 50000)
                ));
                /*      */ break;
            /*      */
 /*      */
 /*      */
 /*      */ case 8880601:
            /*      */ case 8880604:
            /*      */ case 8880608:
            /*      */ case 8880613:
                /* 1527 */ time = (monster.getId() == 8880601) ? 7000 : Randomizer.rand(7000, 13000);
                /* 1528 */ monster.setSchedule(Timer.MobTimer.getInstance().register(() -> monster.getMap().broadcastMessage(MobPacket.enableOnlyFsmAttack(monster, 1, 0)), time));
                /*      */ break;
            /*      */        }
        /*      */    }

    public static void duskHandler(MapleMonster dusk, MapleMap map) {

        dusk.setSchedule(MobTimer.getInstance().register(() -> {
            if (map.getId() == 450009400) {
                long time = System.currentTimeMillis();
                MapleMonster att = map.getMonsterById(8644658);
                if (dusk != null) {
                    for (MapleCharacter cchr : map.getCharacters()) {
                        if (cchr.isDuskBlind()) {
                            cchr.setDuskGauge(Math.max(0, cchr.getDuskGauge() - 45));
                            if (cchr.getDuskGauge() <= 0) {
                                cchr.cancelEffectFromBuffStat(MapleBuffStat.DuskDarkness, 80002902);
                                cchr.setDuskBlind(false);
                            }
                        } else {
                            cchr.setDuskGauge(Math.min(1000, cchr.getDuskGauge() + 5));
                            if (cchr.getDuskGauge() >= 1000) {
                                for (int i = 0; i < 3; i++) {
                                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8644653), new Point(Randomizer.rand(-650, 650), Randomizer.rand(-500, -200)));
                                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8644654), new Point(Randomizer.rand(-650, 650), -157));
                                }
                                SkillFactory.getSkill(80002902).getEffect(1).applyTo(cchr);
                                cchr.setDuskBlind(true);
                            }
                        }
                        cchr.getClient().getSession().writeAndFlush(MobPacket.BossDusk.handleDuskGauge(cchr.isDuskBlind(), cchr.getDuskGauge(), 1000));
                    }
                    int a = 0;
                    if (dusk.getLastSpecialAttackTime() == 0) {
                        dusk.setLastSpecialAttackTime(time);
                    }
                    if (dusk.getPhase() == 1) {
                        if (time - dusk.getLastSpecialAttackTime() >= 60000) {
                            dusk.setPhase((byte) (dusk.getPhase() == 0 ? 1 : 0));
                            map.broadcastMessage(MobPacket.changePhase(dusk));
                            map.broadcastMessage(MobPacket.changeMobZone(dusk));
                            map.broadcastMessage(MobPacket.BossDusk.spawnTempFoothold());
                            map.broadcastMessage(CField.enforceMSG("방어하던 촉수로 강력한 공격을 할거예요! 버텨낸다면 드러난 공허의 눈을 공격할 수 있어요!", 250, 3000));
                            dusk.setLastSpecialAttackTime(time);
                        }
                    } else {
                        if (time - dusk.getLastSpecialAttackTime() >= 35000) {
                            dusk.setPhase((byte) (dusk.getPhase() == 0 ? 1 : 0));
                            map.broadcastMessage(MobPacket.changePhase(dusk));
                            map.broadcastMessage(MobPacket.changeMobZone(dusk));
                            dusk.setLastSpecialAttackTime(time);
                        } else if (time - dusk.getLastSpecialAttackTime() >= 25000) {
                            dusk.setNextSkill(186);
                            dusk.setNextSkillLvl(11);
                            map.broadcastMessage(MobPacket.moveMonsterResponse(dusk.getObjectId(), (short) 0, dusk.getMp(), true, dusk.getNextSkill(), dusk.getNextSkillLvl(), 0));
                            dusk.setNextSkill(0);
                            dusk.setNextSkillLvl(0);
                            map.broadcastMessage(MobPacket.moveMonsterResponse(dusk.getObjectId(), (short) 0, dusk.getMp(), true, dusk.getNextSkill(), dusk.getNextSkillLvl(), 0));
                        }
                    }
                    if (att != null) {
                        if (time - att.getLastSeedCountedTime() >= 10000) {
                            if (dusk.getPhase() == 1) {
                                map.broadcastMessage(MobPacket.BossDusk.spawnDrillAttack(Randomizer.rand(-650, 650), Randomizer.nextInt(100) < 50));
                                att.setLastSeedCountedTime(time);
                            }
                        }
                        if (time - att.getLastSpecialAttackTime() >= 8000) {
                            if (dusk.getPhase() == 1) {
                                map.broadcastMessage(MobPacket.enableOnlyFsmAttack(att, 2, 0));
                                att.setLastSpecialAttackTime(time);
                            }
                        }
                    }
                }
            }
        }, 1000));
    }

    public static void demianHandler(MapleMonster monster) {

        if (monster.getId() == 8880100 || monster.getId() == 8880110) {
            MapleFlyingSword mfs = new MapleFlyingSword(0, monster);
            monster.getMap().spawnFlyingSword(mfs);
            monster.getMap().setNewFlyingSwordNode(mfs, monster.getTruePosition());

            monster.getMap().broadcastMessage(CField.StigmaTime(30000));
            monster.getMap().broadcastMessage(CField.enforceMSG("데미안이 누구에게 낙인을 새길지 알 수 없습니다.", 216, 30000000));

            monster.setSchedule(MobTimer.getInstance().register(() -> {
                int size = monster.getMap().getAllCharactersThreadsafe().size();
                if (size > 0) {
                    MapleCharacter chr = monster.getMap().getAllCharactersThreadsafe().get(Randomizer.nextInt(size));
                    MobSkill ms = MobSkillFactory.getMobSkill(237, 1);
                    ms.applyEffect(chr, monster, true, monster.isFacingLeft(), 0);
                    monster.getMap().broadcastMessage(CField.StigmaTime(30000));
                    monster.getMap().broadcastMessage(CField.enforceMSG("데미안이 누구에게 낙인을 새길지 알 수 없습니다.", 216, 30000000));
                } else if (monster.getSchedule() != null) {
                    monster.getSchedule().cancel(false);
                }
            }, 30000));
        } else if (monster.getId() == 8880101 || monster.getId() == 8880111) {
            monster.setHp((long) (monster.getHp() * 0.3));

            monster.getMap().broadcastMessage(CField.StigmaTime(25000));
            monster.getMap().broadcastMessage(CField.enforceMSG("데미안이 누구에게 낙인을 새길지 알 수 없습니다.", 216, 30000000));

            monster.setSchedule(MobTimer.getInstance().register(() -> {
                int size = monster.getMap().getAllCharactersThreadsafe().size();
                if (size > 0) {
                    MapleCharacter chr = monster.getMap().getAllCharactersThreadsafe().get(Randomizer.nextInt(size));
                    MobSkill ms = MobSkillFactory.getMobSkill(237, 1);
                    ms.applyEffect(chr, monster, true, monster.isFacingLeft(), 0);
                    monster.getMap().broadcastMessage(CField.StigmaTime(25000));
                    monster.getMap().broadcastMessage(CField.enforceMSG("데미안이 누구에게 낙인을 새길지 알 수 없습니다.", 216, 30000000));
                } else {
                    monster.getSchedule().cancel(false);
                }
            }, 25000));
        }
    }

    public static void pierreHandler(MapleMonster monster) {

        List<Point> pos = new ArrayList<>();
        pos.add(new Point(200, 551));
        pos.add(new Point(1000, 551));
        pos.add(new Point(1400, 551));
        pos.add(new Point(0, 551));
        pos.add(new Point(600, 551));
        pos.add(new Point(1200, 551));
        pos.add(new Point(400, 551));
        pos.add(new Point(800, 551));
        pos.add(new Point(1600, 551));
        monster.getMap().broadcastMessage(MobPacket.dropStone("CapEffect", pos));

        monster.setSchedule(MobTimer.getInstance().register(() -> {

            Transform trans = monster.getStats().getTrans();

            if (monster.getId() % 10 == 0) { // 보라색 피에르
                if (monster.getHPPercent() < trans.getOn() && monster.getHPPercent() > trans.getOff()) {
                    if (System.currentTimeMillis() - monster.lastCapTime >= trans.getDuration() * 1000) {
                        MapleMonster copy = MapleLifeFactory.getMonster(Randomizer.nextBoolean() ? monster.getId() + 1 : monster.getId() + 2);

                        copy.setHp(monster.getHp());
                        copy.lastCapTime = System.currentTimeMillis();

                        monster.getMap().spawnMonsterOnGroundBelow(copy, monster.getTruePosition());

                        monster.getMap().killMonster(monster, monster.getController(), false, false, (byte) 0);

                        for (MapleCharacter chr : copy.getMap().getAllCharactersThreadsafe()) {
                            Pair<Integer, Integer> skill = trans.getSkills().get(Randomizer.nextInt(trans.getSkills().size()));

                            if (chr.isAlive()) {
                                chr.cancelDisease(MapleBuffStat.CapDebuff);
                                MobSkillFactory.getMobSkill(skill.left, skill.right).applyEffect(chr, copy, true, copy.isFacingLeft(), 0);
                            }
                        }
                    }
                } else if (monster.getHPPercent() <= trans.getOff()) { // 분열 시작
                    /*	if (System.currentTimeMillis() - monster.lastCapTime >= trans.getDuration() * 1000) {
		    			MapleMonster copy1 = MapleLifeFactory.getMonster(monster.getId() + 1);
	    				
	    				copy1.setHp(monster.getHp());
	    				copy1.lastCapTime = System.currentTimeMillis();
	    				
	    				monster.getMap().spawnMonsterOnGroundBelow(copy1, monster.getTruePosition());
	    				
	    				MapleMonster copy2 = MapleLifeFactory.getMonster(monster.getId() + 2);
	    				
	    				copy2.setHp(monster.getHp());
	    				copy2.lastCapTime = System.currentTimeMillis();
	    				
	    				monster.getMap().spawnMonsterOnGroundBelow(copy2, monster.getTruePosition());
	    				
	    				for (MapleCharacter chr : monster.getMap().getAllCharactersThreadsafe()) {
	    					Pair<Integer, Integer> skill = trans.getSkills().get(Randomizer.nextInt(trans.getSkills().size()));
	    					
	    					if (chr.isAlive()) {
	    						chr.cancelDisease(MapleBuffStat.CapDebuff);
	    						MobSkillFactory.getMobSkill(skill.left, skill.right).applyEffect(chr, monster, true, monster.isFacingLeft());
	    					}
	    				}
	    				
	    				monster.getMap().killMonster(monster, monster.getController(), false, false, (byte) 0);
	    			}*/
                }
            } else if (monster.getId() % 10 == 1 || monster.getId() % 10 == 2) { // 빨간색, 파란색 피에르
                if (monster.getHPPercent() <= trans.getOn() && monster.getHPPercent() >= trans.getOff()) { // 분열 상태
/*        			if (System.currentTimeMillis() - monster.lastCapTime >= trans.getDuration() * 1000) {
        				MapleMonster copy1 = MapleLifeFactory.getMonster(monster.getId());
    					MapleMonster copy2 = MapleLifeFactory.getMonster(monster.getId() % 10 == 1 ? monster.getId() + 1 : monster.getId() - 1);
    					
    					copy1.setHp((long) (copy1.getHp() * 0.3));
    					copy2.setHp((long) (copy2.getHp() * 0.3));
    					
    					copy1.lastCapTime = System.currentTimeMillis();
    					copy2.lastCapTime = System.currentTimeMillis();
	    				
	    				monster.getMap().spawnMonsterOnGroundBelow(copy1, monster.getTruePosition());
	    				monster.getMap().spawnMonsterOnGroundBelow(copy2, monster.getTruePosition());
	    				
	    				
	    				monster.getMap().killAllMonster(monster.getController());
	    				
	    				for (MapleCharacter chr : copy2.getMap().getAllCharactersThreadsafe()) {
	    					Pair<Integer, Integer> skill = trans.getSkills().get(Randomizer.nextInt(trans.getSkills().size()));
	    					
	    					if (chr.isAlive()) {
	    						chr.cancelDisease(MapleBuffStat.CapDebuff);
	    						MobSkillFactory.getMobSkill(skill.left, skill.right).applyEffect(chr, copy2, true, copy2.isFacingLeft());
	    					}
	    				}
        			}*/
                } else { // 미분열 상태
                    if (System.currentTimeMillis() - monster.lastCapTime >= trans.getDuration() * 1000) {
                        MapleMonster copy = MapleLifeFactory.getMonster(monster.getId() % 10 == 1 ? monster.getId() - 1 : monster.getId() - 2);
                        copy.setHp(monster.getHp());
                        copy.lastCapTime = System.currentTimeMillis();

                        monster.getMap().spawnMonsterOnGroundBelow(copy, monster.getTruePosition());

                        monster.getMap().killMonster(monster, monster.getController(), false, false, (byte) 0);

                        for (MapleCharacter chr : copy.getMap().getAllCharactersThreadsafe()) {
                            Pair<Integer, Integer> skill = trans.getSkills().get(Randomizer.nextInt(trans.getSkills().size()));

                            if (chr.isAlive()) {
                                chr.cancelDisease(MapleBuffStat.CapDebuff);
                                MobSkillFactory.getMobSkill(skill.left, skill.right).applyEffect(chr, copy, true, copy.isFacingLeft(), 0);
                            }
                        }
                    }
                }
            }
        }, 1000));
    }
    
    public static void blackMageHandler(MapleMonster monster) {
        monster.setSchedule(Timer.MobTimer.getInstance().register(() -> {
            if ((monster.getEventInstance() == null || !monster.isAlive()) && monster.getMap().getCharacters().size() <= 0) {
                monster.getSchedule().cancel(true);
                monster.setSchedule(null);
                monster.getMap().killMonsterType(monster, 0);
                return;
            }
            MapleMap map = monster.getMap();
            for (MapleCharacter chr : map.getAllChracater()) {
                if (chr.hasDisease(MapleBuffStat.CurseOfCreation) && chr.isAlive()) {
                    chr.addHP(chr.getStat().getCurrentMaxHp() / 100L * 4L);
                    chr.addMP(chr.getStat().getCurrentMaxMp(chr) / 100L * 4L);
                }
                if ((chr.getMapId() == 450013100 && chr.getMap().getMobsSize(8880500) > 0 && chr.getMap().getMobsSize(8880501) > 0) || (chr.getMapId() == 450013300 && chr.getMap().getMobsSize(8880502) > 0) || (chr.getMapId() == 450013500 && chr.getMap().getMobsSize(8880503) > 0) || (chr.getMapId() == 450013700 && chr.getMap().getMobsSize(8880504) > 0)) {
                    if (chr.getMapId() != 450013700 && chr.getMapId() != 450013500 && chr.getSkillCustomValue(45001310) == null && map.getCustomValue0(45001316) == 0) {
                        chr.setSkillCustomInfo(45001310, 0, Randomizer.rand(7000, 11000));
                        for (MapleMonster mob : chr.getMap().getAllMonster()) {
                            if (mob.getId() == 8880502) {
                                map.broadcastMessage(MobPacket.blockMoving(mob.getObjectId(), 0));
                                break;
                            }
                        }
                        chr.getClient().getSession().writeAndFlush(CField.getFieldSkillAdd(100007, Randomizer.rand(1, 3), false));
                    }
                    if (chr.getSkillCustomValue(45001311) == null && chr.hasDisease(MapleBuffStat.CurseOfCreation)) {
                        chr.setSkillCustomInfo(45001311, 0, Randomizer.rand(1000, 3000));
                        if (chr.isAlive()) {
                            chr.getClient().getSession().writeAndFlush(CField.getFieldSkillAdd(100015, (chr.getMapId() == 450013700) ? 2 : 1, false));
                        }
                    }
                }
            }
            if (map.getCustomValue0(45001317) > 0) {
                int type = map.getCustomValue0(45001317);
                int bx = 0;
                int afterx = 0;
                switch (type) {
                    case 1:
                        bx = -105;
                        afterx = 158;
                        break;
                    case 2:
                        bx = -980;
                        afterx = -781;
                        break;
                    case 3:
                        bx = 789;
                        afterx = 950;
                        break;
                }
                for (MapleCharacter chr2 : map.getAllCharactersThreadsafe()) {
                    if (((chr2.getTruePosition()).x <= bx || afterx <= (chr2.getTruePosition()).x || (chr2.getPosition()).y <= -168) && chr2.getBuffedEffect(MapleBuffStat.NotDamaged) == null && chr2.getBuffedEffect(MapleBuffStat.IndieNotDamaged) == null) {
                        chr2.getPercentDamage(monster, 999, 999, 12500, true);
                    }
                }
            } else if (map.getCustomValue0(45001360) == 1) {
                for (MapleCharacter chr2 : map.getAllCharactersThreadsafe()) {
                    if ((chr2.getPosition()).y == 85 && chr2.getMapId() == 450013500 && chr2.isAlive() && chr2.getBuffedEffect(MapleBuffStat.NotDamaged) == null && chr2.getBuffedEffect(MapleBuffStat.IndieNotDamaged) == null) {
                        chr2.addHP(-chr2.getStat().getCurrentMaxHp() * 10L);
                        chr2.getClient().getSession().writeAndFlush(CField.DamagePlayer2((int) chr2.getStat().getCurrentMaxHp() * 10));
                    }
                }
            }
            if ((map.getId() == 450013100 && map.getMobsSize(8880500) > 0 && map.getMobsSize(8880501) > 0) || (map.getId() == 450013300 && map.getMobsSize(8880502) > 0) || (map.getId() == 450013500 && map.getMobsSize(8880503) > 0 && map.getCharactersSize() > 0)) {
                if (map.getCustomValue0(45001316) == 0 && map.getCustomValue(45001310) == null) {
                    int type = Randomizer.rand(1, 2);
                    List<Obstacle> obs = new ArrayList<>();
                    map.setCustomInfo(45001310, 0, 60000);
                    for (int i = 0; i < (map.getRight() - map.getLeft()) / 150; i++) {
                        if (type == 1) {
                            Obstacle ob = new Obstacle(75, new Point(map.getRight() - i * 150, -540), new Point(map.getRight() - 300, 16), 50, 50, 0, i * 530, 82, 4, 573, 0);
                            obs.add(ob);
                        } else {
                            Obstacle ob = new Obstacle(75, new Point(map.getLeft() + i * 150, -540), new Point(map.getLeft() - 300, 16), 50, 50, 0, i * 530, 82, 4, 573, 0);
                            obs.add(ob);
                        }
                    }
                    map.CreateObstacle(monster, obs);
                } else if (map.getCustomValue(45001311) == null && map.getId() == 450013100 && map.getMobsSize(8880500) > 0 && map.getMobsSize(8880501) > 0) {
                    map.setCustomInfo(45001311, 0, 63000);
                    map.broadcastMessage(CField.enforceMSG("불길한 붉은 번개가 내리쳐 움직임을 제한한다.", 265, 3000));
                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880506), new Point(-1600, 85));
                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880506), new Point(400, 85));
                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880506), new Point(-1000, 85));
                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880506), new Point(1000, 85));
                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880506), new Point(-400, 85));
                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(8880506), new Point(1600, 85));
                } else if (map.getCustomValue(45001315) == null && map.getId() == 450013300 && map.getMobsSize(8880502) > 0) {
                    boolean use = true;
                    int randt = 2;
                    for (MapleMonster mob : map.getAllMonster()) {
                        if (mob.getId() == 8880502) {
                            if (mob.isSkillForbid()) {
                                use = false;
                                break;
                            }
                            map.broadcastMessage(MobPacket.UseSkill(mob.getObjectId(), (randt == 1) ? 11 : ((randt == 2) ? 10 : 12)));
                            break;
                        }
                    }
                    if (use) {
                        map.setCustomInfo(45001315, 0, 75000);
                        map.setCustomInfo(45001316, randt, 0);
                        map.broadcastMessage(CField.enforceMSG("검은 마법사의 붉은 번개가 모든 곳을 뒤덮는다. 피할 곳을 찾아야 한다.", 265, 4000));
                    }
                } else if (map.getCustomValue(45001312) == null && map.getId() == 450013100 && map.getMobsSize(8880500) > 0 && map.getMobsSize(8880501) > 0) {
                    List<Point> pt = new ArrayList<>();
                    int mobsize = map.getMobsSize(8880507) + map.getMobsSize(8880508);
                    int basex = 2172;
                    map.setCustomInfo(45001312, 0, 50000);
                    map.broadcastMessage(CField.enforceMSG("통곡의 장벽이 솟아올라 공간을 잠식한다.", 265, 3000));
                    if (mobsize < 16) {
                        pt.add(new Point(-basex + map.getCustomValue0(45001313) * 174, 84));
                        pt.add(new Point(basex - map.getCustomValue0(45001313) * 174, 84));
                        map.broadcastMessage(CField.getFieldSkillEffectAdd(100008, 1, pt));
                    }
                    for (MapleMonster mob : map.getAllMonster()) {
                        if (mob.getId() == 8880501 || mob.getId() == 8880500) {
                            int x = 350;
                            if (mob.getId() == 8880500) {
                                x *= -1;
                            }
                            mob.getMap().broadcastMessage(MobPacket.blockMoving(mob.getObjectId(), 0));
                            mob.setNextSkill(170);
                            mob.setNextSkillLvl((mob.getId() == 8880500) ? 62 : 64);
                            mob.getMap().broadcastMessage(MobPacket.moveMonsterResponse(mob.getObjectId(), (short) (int) mob.getCustomValue0(99991), mob.getMp(), true, 170, (mob.getId() == 8880500) ? 62 : 64, 0));
                            mob.getMap().broadcastMessage(MobPacket.TeleportMonster(mob, false, 3, new Point(x, 85)));
                        }
                    }
                } else if (map.getCustomValue0(45001316) == 0 && map.getCustomValue(45001313) == null && map.getId() == 450013300 && map.getMobsSize(8880502) > 0) {
                    map.setCustomInfo(45001313, 0, Randomizer.rand(38000, 43000));
                    map.broadcastMessage(CField.enforceMSG("파멸의 눈이 적을 쫒는다.", 265, 4000));
                    for (MapleMonster mob : map.getAllMonster()) {
                        if (mob.getId() == 8880502) {
                            map.broadcastMessage(MobPacket.UseSkill(mob.getObjectId(), 5));
                            break;
                        }
                    }
                    map.broadcastMessage(CField.getFieldSkillAdd(100012, Randomizer.rand(1, 2), false));
                } else if (map.getCustomValue0(45001316) == 0 && map.getCustomValue(45101313) == null && map.getId() == 450013300 && map.getMobsSize(8880502) > 0) {
                    for (MapleMonster mob : map.getAllMonster()) {
                        if (mob.getId() == 8880516) {
                            map.broadcastMessage(MobPacket.enableOnlyFsmAttack(mob, 1, 0));
                            map.setCustomInfo(45101313, 0, Randomizer.rand(10000, 13000));
                            break;
                        }
                    }
                } else if (map.getCustomValue0(45001316) == 0 && map.getCustomValue(45001314) == null && ((map.getId() == 450013300 && map.getMobsSize(8880502) > 0) || (map.getId() == 450013500 && map.getMobsSize(8880503) > 0))) {
                    map.setCustomInfo(45001314, 0, Randomizer.rand(45000, 51000));
                    map.getObtacles(Randomizer.rand(1, 2));
                } else if (map.getCustomValue0(45001316) == 0 && map.getCustomValue(45001320) == null && map.getId() == 450013500 && map.getMobsSize(8880503) > 0) {
                    List<Triple<Point, Integer, Integer>> skillinfo = new ArrayList<>();
                    for (int i = 0; i < Randomizer.rand(3, 5); i++) {
                        skillinfo.add(new Triple(new Point(Randomizer.rand(-970, 970), Randomizer.rand(-400, 60)), Integer.valueOf(Randomizer.rand(0, 180)), Integer.valueOf(100 + Randomizer.rand(50, 100) * i)));
                    }
                    boolean spattack = false;
                    for (MapleMonster mob : map.getAllMonster()) {
                        if (mob.getId() == 8880503) {
                            map.broadcastMessage(MobPacket.enableOnlyFsmAttack(mob, 1, 0));
                            map.setCustomInfo(45101313, 0, Randomizer.rand(10000, 13000));
                            if (Randomizer.isSuccess(30)) {
                                spattack = true;
                                map.broadcastMessage(MobPacket.UseSkill(mob.getObjectId(), 13));
                            }
                            break;
                        }
                    }
                    map.setCustomInfo(45001320, 0, Randomizer.rand(4000, 6000));
                    map.broadcastMessage(CField.getFieldLaserAdd(100011, spattack ? 1 : 2, skillinfo));
                } else if (map.getCustomValue0(45001316) == 0 && map.getCustomValue(45001321) == null && map.getId() == 450013500 && map.getMobsSize(8880503) > 0) {
                    Point pos = null;
                    int type;
                    for (type = Randomizer.rand(1, 6); map.getCustomValue0(45001350 + type) == 1; type = Randomizer.rand(1, 6));
                    switch (type) {
                        case 1:
                            pos = new Point(-792, -153);
                            break;
                        case 2:
                            pos = new Point(-568, -298);
                            break;
                        case 3:
                            pos = new Point(-289, -211);
                            break;
                        case 4:
                            pos = new Point(143, -90);
                            break;
                        case 5:
                            pos = new Point(485, -185);
                            break;
                        case 6:
                            pos = new Point(791, -309);
                            break;
                    }
                    List<Triple<Point, String, Integer>> skillinfo = new ArrayList<>();
                    map.setCustomInfo(45001321, 0, Randomizer.rand(10000, 35000));
                    skillinfo.add(new Triple(pos, (type == 1) ? "foot1" : ("foo" + type), Integer.valueOf(1)));
                    map.broadcastMessage(CField.getFieldFootHoldAdd(100013, 1, skillinfo, false));
                    map.setCustomInfo(45001350 + type, 1, 0);
                } else if (map.getCustomValue0(45001316) == 0 && (map.getCustomValue(45001322) == null || map.getCustomValue(45001323) == null) && map.getId() == 450013500 && map.getMobsSize(8880503) > 0) {
                    List<Obstacle> obs = new ArrayList<>();
                    if (map.getCustomValue(45001322) == null) {
                        Point pos = new Point(Randomizer.rand(map.getLeft(), map.getRight()), 110);
                        map.setCustomInfo(45001322, 0, Randomizer.rand(50000, 65000));
                        Obstacle ob = new Obstacle(76, pos, new Point(pos.x, 16), 50, 10, 0, 600, 82, 1, 1100, 700);
                        obs.add(ob);
                    } else if (map.getCustomValue(45001323) == null) {
                        Point pos = new Point(Randomizer.rand(map.getLeft(), map.getRight()), -491);
                        map.setCustomInfo(45001323, 0, Randomizer.rand(30000, 45000));
                        Obstacle ob = new Obstacle(79, pos, new Point(pos.x, 16), 50, 10, 390, 82, 1, 573);
                        obs.add(ob);
                    }
                    if (!obs.isEmpty()) {
                        map.CreateObstacle(monster, obs);
                    }
                } else if (map.getCustomValue(45001324) == null && map.getId() == 450013500 && map.getMobsSize(8880503) > 0) {
                    List<Triple<Point, String, Integer>> skillinfo = new ArrayList<>();
                    int type3 = 1;
                    map.broadcastMessage(CField.enforceMSG("검은 마법사가 창조와 파괴의 권능을 사용한다. 위와 아래, 어느 쪽으로 피할지 선택해야 한다.", 265, 4000));
                    map.setCustomInfo(45001324, 0, Randomizer.rand(60000, 75000));
                    map.setCustomInfo(45001316, 1, 0);
                    map.getMonsterById(8880503).setSkillForbid(true);
                    map.broadcastMessage(MobPacket.UseSkill(map.getMonsterById(8880503).getObjectId(), 11));
                    for (int type = 1; type <= 6; type++) {
                        if (map.getCustomValue0(45001350 + type) == 1) {
                            Point pos = null;
                            switch (type) {
                                case 1:
                                    pos = new Point(-792, -153);
                                    break;
                                case 2:
                                    pos = new Point(-568, -298);
                                    break;
                                case 3:
                                    pos = new Point(-289, -211);
                                    break;
                                case 4:
                                    pos = new Point(143, -90);
                                    break;
                                case 5:
                                    pos = new Point(485, -185);
                                    break;
                                case 6:
                                    pos = new Point(791, -309);
                                    break;
                            }
                            skillinfo.add(new Triple(pos, (type == 1) ? "foot1" : ("foo" + type), Integer.valueOf(0)));
                        }
                    }
                }
                if (map.getCustomValue(45021328) == null && map.getId() == 450013500 && map.getMobsSize(8880503) > 0 && map.getCustomValue0(45011328) < 3) {
                    int mobid = 0;
                    map.broadcastMessage(CField.enforceMSG("파괴의 천사가 무에서 창조된다.", 265, 4000));
                    map.setCustomInfo(45011328, map.getCustomValue0(45011328) + 1, 0);
                    if (map.getCustomValue0(45011328) < 3) {
                        map.setCustomInfo(45021328, 0, 120000);
                    }
                    Point pos = null;
                    switch (map.getCustomValue0(45011328)) {
                        case 1:
                            mobid = 8880509;
                            pos = new Point(746, 85);
                            break;
                        case 2:
                            mobid = 8880510;
                            pos = new Point(443, 85);
                            break;
                        case 3:
                            mobid = 8880511;
                            pos = new Point(209, 85);
                            break;
                    }
                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(mobid), new Point(pos.x, pos.y));
                    map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(mobid), new Point(-pos.x, pos.y));
                }
                if (map.getCustomValue(45001326) == null && map.getId() == 450013500 && map.getMobsSize(8880503) > 0) {
                    List<Pair<Long, Integer>> damage = new ArrayList<>();
                    map.setCustomInfo(45001326, 0, 5000);
                    damage.add(new Pair(Long.valueOf(3000000000L), Integer.valueOf(0)));
                    MapleCharacter chr = null;
                    Iterator<MapleCharacter> chrs = map.getCharactersThreadsafe().iterator();
                    if (chrs.hasNext()) {
                        chr = chrs.next();
                    }
                    if (chr != null) {
                        map.getMonsterById(8880503).damage(chr, 3000000000L, false);
                    }
                    map.broadcastMessage(MobPacket.FieldSummonAttack(3, false, new Point(0, 0), map.getMonsterById(8880503).getObjectId(), damage));
                }
            }
            if (map.getId() == 450013700 && map.getMobsSize(8880504) > 0) {
                if (map.getCustomValue(45004445) == null) {
                    List<Triple<Point, Point, Integer>> skillinfo = new ArrayList<>();
                    map.broadcastMessage(CField.enforceMSG("신에 가까운 자의 권능이 발현된다. 창조와 파괴, 어떤 힘을 품을지 선택해야 한다.", 265, 5000));
                    map.setCustomInfo(45004445, 0, 30000);
                    map.setCustomInfo(45004446, 1, 0);
                    skillinfo.add(new Triple(new Point(-801, -404), new Point(-681, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(-681, -404), new Point(-561, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(-561, -404), new Point(-441, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(-441, -404), new Point(-321, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(-321, -404), new Point(-201, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(-201, -404), new Point(-81, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(-81, -404), new Point(39, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(39, -404), new Point(159, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(159, -404), new Point(279, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(279, -404), new Point(399, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(399, -404), new Point(519, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(519, -404), new Point(639, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(639, -404), new Point(759, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    skillinfo.add(new Triple(new Point(759, -404), new Point(879, 238), Integer.valueOf(Randomizer.rand(0, 3))));
                    map.broadcastMessage(CField.getFieldFinalLaserAdd(100014, 2, skillinfo, 0));
                } else if (map.getCustomValue(45004444) == null && map.getCustomValue0(45004446) == 0) {
                    map.setCustomInfo(45004444, 0, Randomizer.rand(7000, 12000));
                    for (int i = 0; i < Randomizer.rand(3, 4); i++) {
                        List<Triple<Point, Point, Integer>> skillinfo = new ArrayList<>();
                        int ranx = Randomizer.rand(map.getLeft(), map.getRight());
                        int rany = Randomizer.rand(-251, 159);
                        skillinfo.add(new Triple(new Point(ranx - 150, rany - 150), new Point(ranx + 150, rany + 150), Integer.valueOf(Randomizer.rand(0, 1))));
                        map.broadcastMessage(CField.getFieldFinalLaserAdd(100016, 1, skillinfo, i * 240));
                    }
                }
            }
        } ,1000L));
    }
}
