/*     */ package server.field.boss;
/*     */ 
/*     */ import java.awt.Point;
/*     */ import java.awt.Rectangle;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import server.maps.MapleNodes;
/*     */ 
/*     */ public class FieldSkill {
/*     */   private int skillId;
/*     */   private int skillLevel;
/*  12 */   private List<SummonedSequenceInfo> summonedSequenceInfoList = new ArrayList<>();
/*  13 */   private List<FieldFootHold> footHolds = new ArrayList<>();
/*  14 */   private List<LaserInfo> laserInfoList = new ArrayList<>();
/*  15 */   private List<MapleNodes.Environment> envInfo = new ArrayList<>();
/*  16 */   private List<ThunderInfo> thunInfo = new ArrayList<>();
/*     */   
/*     */   public FieldSkill(int skillId, int skillLevel) {
/*  19 */     this.skillId = skillId;
/*  20 */     this.skillLevel = skillLevel;
/*     */   }
/*     */   
/*     */   public int getSkillId() {
/*  24 */     return this.skillId;
/*     */   }
/*     */   
/*     */   public int getSkillLevel() {
/*  28 */     return this.skillLevel;
/*     */   }
/*     */   
/*     */   public void addSummonedSequenceInfoList(SummonedSequenceInfo info) {
/*  32 */     this.summonedSequenceInfoList.add(info);
/*     */   }
/*     */   
/*     */   public List<SummonedSequenceInfo> getSummonedSequenceInfoList() {
/*  36 */     return this.summonedSequenceInfoList;
/*     */   }
/*     */   
/*     */   public void setSummonedSequenceInfoList(List<SummonedSequenceInfo> summonedSequenceInfoList) {
/*  40 */     this.summonedSequenceInfoList = summonedSequenceInfoList;
/*     */   }
/*     */   
/*     */   public List<LaserInfo> getLaserInfoList() {
/*  44 */     return this.laserInfoList;
/*     */   }
/*     */   
/*     */   public void setLaserInfoList(List<LaserInfo> infoList) {
/*  48 */     this.laserInfoList = infoList;
/*     */   }
/*     */   
/*     */   public List<FieldFootHold> getFootHolds() {
/*  52 */     return this.footHolds;
/*     */   }
/*     */   
/*     */   public void setFootHolds(List<FieldFootHold> footHolds) {
/*  56 */     this.footHolds = footHolds;
/*     */   }
/*     */   
/*     */   public List<MapleNodes.Environment> getEnvInfo() {
/*  60 */     return this.envInfo;
/*     */   }
/*     */   
/*     */   public void setEnvInfo(List<MapleNodes.Environment> envInfo) {
/*  64 */     this.envInfo = envInfo;
/*     */   }
/*     */   
/*     */   public List<ThunderInfo> getThunderInfo() {
/*  68 */     return this.thunInfo;
/*     */   }
/*     */   public void setThunderInfo(List<ThunderInfo> thunInfo) {
/*  71 */     this.thunInfo = thunInfo;
/*     */   }
/*     */   
/*     */   public void addThunderInfo(ThunderInfo info) {
/*  75 */     this.thunInfo.add(info);
/*     */   }
/*     */   
/*     */   public static class ThunderInfo { private Point StartPos;
/*     */     private Point EndPos;
/*     */     private int info;
/*     */     private int delay;
/*     */     
/*     */     public ThunderInfo(Point startPos, Point endPos, int info, int delay) {
/*  84 */       this.StartPos = startPos;
/*  85 */       this.EndPos = endPos;
/*  86 */       this.info = info;
/*  87 */       this.delay = delay;
/*     */     }
/*     */     
/*     */     public Point getStartPosition() {
/*  91 */       return this.StartPos;
/*     */     }
/*     */     
/*     */     public Point getEndPosition() {
/*  95 */       return this.EndPos;
/*     */     }
/*     */     
/*     */     public int getInfo() {
/*  99 */       return this.info;
/*     */     }
/*     */     public int getDelay() {
/* 102 */       return this.delay;
/*     */     } }
/*     */   
/*     */   public static class LaserInfo {
/*     */     private Point position;
/*     */     private int unk1;
/*     */     private int unk2;
/*     */     
/*     */     public LaserInfo(Point position, int unk1, int unk2) {
/* 111 */       this.position = position;
/* 112 */       this.unk1 = unk1;
/* 113 */       this.unk2 = unk2;
/*     */     }
/*     */     
/*     */     public Point getPosition() {
/* 117 */       return this.position;
/*     */     }
/*     */     
/*     */     public int getUnk1() {
/* 121 */       return this.unk1;
/*     */     }
/*     */     
/*     */     public int getUnk2() {
/* 125 */       return this.unk2;
/*     */     }
/*     */   }
/*     */   
/*     */   public static class SummonedSequenceInfo {
/*     */     private int id;
/*     */     private Point position;
/*     */     
/*     */     public SummonedSequenceInfo(int id, Point position) {
/* 134 */       this.id = id;
/* 135 */       this.position = position;
/*     */     }
/*     */     
/*     */     public int getId() {
/* 139 */       return this.id;
/*     */     }
/*     */     
/*     */     public Point getPosition() {
/* 143 */       return this.position;
/*     */     }
/*     */   }
/*     */ 
/*     */   
/*     */   public static class FieldFootHold
/*     */   {
/*     */     private Point pos;
/*     */     
/*     */     private Rectangle rect;
/*     */     private boolean isFacingLeft;
/*     */     private short set;
/*     */     private short unk;
/*     */     
/*     */     public FieldFootHold(int duration, int interval, int angleMin, int angleMax, int attackDelay, int z, short set, short unk, Rectangle rect, Point pos, boolean isFacingLeft) {
/* 158 */       this.duration = duration;
/* 159 */       this.interval = interval;
/* 160 */       this.angleMin = angleMin;
/* 161 */       this.angleMax = angleMax;
/* 162 */       this.attackDelay = attackDelay;
/* 163 */       this.z = z;
/* 164 */       this.set = set;
/* 165 */       this.unk = unk;
/* 166 */       this.rect = rect;
/* 167 */       this.pos = pos;
/* 168 */       this.isFacingLeft = isFacingLeft;
/*     */     }
/*     */     private int duration; private int interval; private int angleMin; private int angleMax; private int attackDelay; private int z;
/*     */     public int getDuration() {
/* 172 */       return this.duration;
/*     */     }
/*     */     
/*     */     public int getInterval() {
/* 176 */       return this.interval;
/*     */     }
/*     */     
/*     */     public int getAngleMin() {
/* 180 */       return this.angleMin;
/*     */     }
/*     */     
/*     */     public int getAngleMax() {
/* 184 */       return this.angleMax;
/*     */     }
/*     */     
/*     */     public int getAttackDelay() {
/* 188 */       return this.attackDelay;
/*     */     }
/*     */     
/*     */     public int getZ() {
/* 192 */       return this.z;
/*     */     }
/*     */     
/*     */     public short getSet() {
/* 196 */       return this.set;
/*     */     }
/*     */     
/*     */     public short getUnk() {
/* 200 */       return this.unk;
/*     */     }
/*     */     
/*     */     public boolean isFacingLeft() {
/* 204 */       return this.isFacingLeft;
/*     */     }
/*     */     
/*     */     public Point getPos() {
/* 208 */       return this.pos;
/*     */     }
/*     */     
/*     */     public Rectangle getRect() {
/* 212 */       return this.rect;
/*     */     }
/*     */   }
/*     */ }


/* Location:              C:\Users\Phellos\Desktop\크루엘라\Ozoh디컴.jar!\server\field\boss\FieldSkill.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */