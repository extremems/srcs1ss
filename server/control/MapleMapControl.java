package server.control;

import handling.channel.ChannelServer;
import java.awt.Point;
import server.Randomizer;
import server.field.boss.demian.MapleIncinerateObject;
import server.life.MapleMonster;
import server.life.MobSkill;
import server.life.MobSkillFactory;
import server.maps.MapleMap;
import server.maps.MapleMapItem;
import tools.packet.CField.EffectPacket;

import java.util.Iterator;
import server.games.BattleGroundGameHandler;
import server.life.MapleLifeFactory;
import tools.packet.BattleGroundPacket;
import tools.packet.CField;
import tools.packet.SLFCGPacket;

public class MapleMapControl implements Runnable {

    private boolean isfirst;
    private int numTimes = 0;

    public MapleMapControl() {
        this.isfirst = false;
        System.out.println("[Loading Completed] Start MapControl");
    }

    @Override
    public void run() {
        numTimes++;
        long time = System.currentTimeMillis();

        Iterator<ChannelServer> css = ChannelServer.getAllInstances().iterator();
        try {
            while (css.hasNext()) {
                ChannelServer cs = css.next();
                if (!cs.hasFinishedShutdown()) {

                    Iterator<MapleMap> maps = cs.getMapFactory().getAllLoadedMaps().iterator();

                    while (maps.hasNext()) {

                        MapleMap map = maps.next();

                        if (!map.isTown() && (map.getCharactersThreadsafe().size() > 0 || map.getId() == 931000500)) { //jaira hack
                            boolean hurt = map.canHurt(time);
//                        if (map.canSpawn(time)) {
                            map.respawn(false, time);
                            //                      }
                        }

                        if (map.getAllItemsThreadsafe().size() > 0) {
                            Iterator<MapleMapItem> items = map.getAllItemsThreadsafe().iterator();
                            while (items.hasNext()) {
                                MapleMapItem item = items.next();
                                if (item.shouldExpire(time)) {
                                    item.expire(map);
                                } else if (item.shouldFFA(time)) {
                                    item.setDropType((byte) 2);
                                }
                            }
                        }

                        if (map.getAllNormalMonstersThreadsafe().size() > 0 && !map.isTown()) {
                            if (map.getBurningIncreasetime() == 0) {
                                map.setBurningIncreasetime(System.currentTimeMillis());
                            }

                            if (map.getAllCharactersThreadsafe().size() == 0) {
                                if (map.getBurning() <= 10) {
                                    if (time - map.getBurningIncreasetime() > 600 * 1000) {
                                        map.setBurningIncreasetime(time);
                                        if (map.getBurning() < 10) {
                                            map.setBurning(map.getBurning() + 1);
                                        }
                                    }
                                }
                            } else {
                                if (map.getBurning() > 0) {
                                    if (time - map.getBurningIncreasetime() > 600 * 1000) {
                                        map.setBurningIncreasetime(time);
                                        map.setBurning(map.getBurning() - 1);
                                        if (map.getBurning() > 0) {
                                            map.broadcastMessage(EffectPacket.showBurningFieldEffect("#fn나눔고딕 ExtraBold##fs26#    버닝 " + map.getBurning() + "단계 : 경험치 " + (map.getBurning() * 10) + "% 추가지급!"));
                                        } else {
                                            map.broadcastMessage(EffectPacket.showBurningFieldEffect("#fn나눔고딕 ExtraBold##fs26#  버닝필드 소멸!"));
                                        }
                                    }
                                }
                            }
                        }

                        /*  818 */ if (map.BattleGroundMainTimer > 0) {
                            /*  819 */ map.BattleGroundMainTimer--;
                            /*  820 */ if (map.BattleGroundMainTimer == 539) {
                                /*  821 */ map.broadcastMessage(CField.ImageTalkNpc(9001153, 5000, "무적 상태가 해제 되었습니다. 플레이어들끼리 전투가 가능합니다!"));
                                /*  822 */ BattleGroundGameHandler.setNotDamage(false);
                                /*  823 */                            } else if (map.BattleGroundMainTimer == 330) {
                                /*  824 */ map.broadcastMessage(CField.environmentChange("Map/Effect2.img/PvP/Boss", 16));
                                /*  825 */ map.broadcastMessage(CField.ImageTalkNpc(9001153, 5000, "보스존에 #r홀로 드래곤#k이 스폰 되었습니다. 처치시 막대한 #b경험치와 골드#k를 얻을 수 있습니다!"));
                                /*  826 */ map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303206), new Point(-380, -58));
                                /*  827 */                            } else if (map.BattleGroundMainTimer == 4) {
                                /*      */
 /*  829 */ map.broadcastMessage(CField.environmentChange("Map/Effect2.img/PvP/Start", 16));
                                /*  830 */ map.broadcastMessage(SLFCGPacket.playSE("Sound/MiniGame.img/BattlePvp/Start"));
                                /*      */                            }
                            /*  832 */ if (map.BattleGroundMainTimer <= 0) {
                                /*  833 */ map.BattleGroundMainTimer = 0;
                                /*  834 */ BattleGroundGameHandler.EndPlayGamez(map);
                                /*      */                            }
                            /*      */                        }
                        /*  878 */ if (map.Mapcoltime > 0) {
                            /*  879 */ map.Mapcoltime--;
                            /*  880 */ if (map.Mapcoltime <= 0) {
                                /*  881 */ map.Mapcoltime = 0;
                                /*      */                            }
                            /*      */                        }
                        /*      */

 /*  885 */ if (map.BattleGroundTimer > 0) {
                            /*  886 */ map.BattleGroundTimer--;
                            /*  887 */ if (map.BattleGroundTimer <= 0) {
                                /*  888 */ map.BattleGroundTimer = 0;
                                /*      */
 /*  890 */ BattleGroundGameHandler.StartGame(map);
                                /*      */                            }
                            /*  892 */ map.broadcastMessage(BattleGroundPacket.SelectAvaterClock(map.BattleGroundTimer));
                            /*      */                        }

                        /*                    if (map.getAllCharactersThreadsafe().size() == 0 && map.getAllRune().size() > 0 && map.getRuneCurse() < 4) {
                    	MapleRune rune = map.getAllRune().get(0); // 맵에 룬은 단 1개
                    	if (time - rune.getCreateTimeMills() >= 1000 * 60 * 30) {
                    		map.setRuneCurse(map.getRuneCurse() + 1);
                    		map.broadcastMessage(CField.runeCurse("룬을 해방하여 엘리트 보스의 저주를 풀어야 합니다!!\\n저주 " + map.getRuneCurse()+ "단계 :  경험치 획득, 드롭률 " + map.getRuneCurseDecrease() + "% 감소 효과 적용 중", false));
                    	}
                    }
                         */
                        int[] demians = {8880100, 8880110, 8880101, 8880111};

                        MapleMonster demian = null;

                        for (int id : demians) {
                            demian = map.getMonsterById(id);
                            if (demian != null) {
                                if (time - map.lastIncinerateTime >= 30 * 1000) { // 값이 정확히 나와있지 않음
                                    if (map.lastIncinerateTime != 0) {
                                        map.spawnIncinerateObject(new MapleIncinerateObject(Randomizer.nextInt(map.getRight() - map.getLeft()) + map.getLeft(), 16));
                                    }
                                    map.lastIncinerateTime = time;
                                }
                                break;
                            }
                        }

                        if (map.getId() == 450008350 || map.getId() == 450008950) {

                            MapleMonster will = map.getMonsterById(8880302);
                            if (will == null) {
                                will = map.getMonsterById(8880342);
                            }

                            if (will != null) {
                                if (will.getLastSkillUsed(242, 13) == 0) {
                                    MobSkill web = MobSkillFactory.getMobSkill(242, 13);
                                    will.setLastSkillUsed(web, time, web.getInterval());
                                    web.applyEffect(null, will, true, will.isFacingLeft(), 0);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
