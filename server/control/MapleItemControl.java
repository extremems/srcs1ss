/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.control;

import client.MapleCharacter;
import handling.channel.ChannelServer;
import tools.packet.CField;

/**
 *
 * @author Administrator
 */
public class MapleItemControl implements Runnable {

    public MapleItemControl() {
        System.out.println("[Loading Completed] Start MapleItemControl");
    }

    @Override
    public void run() {
        for (final ChannelServer cs : ChannelServer.getAllInstances()) {
            for (MapleCharacter player : cs.getPlayerStorage().getAllCharacters().values()) {
                if (player != null) {
                    if (player.getGMItemCheck()) {
                        player.getGMItemListAdd();
                        player.getClient().getSession().writeAndFlush(CField.NPCPacket.OnAddPopupSay(3001604, 3500, "#face1#[시스템]\r\n보관 중인 아이템이 " + player.getGMItemList().size() + "개 있습니다.\r\n엔피시를 통해 받아가세요.", ""));
                        player.getGMItemList().clear();
                    }
                }
            }
        }
    }
}
