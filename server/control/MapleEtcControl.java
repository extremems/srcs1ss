package server.control;

import client.MapleBuffStat;
import client.MapleCharacter;
import client.MapleCharacterSave;
import client.inventory.Item;
import client.inventory.MapleInventory;
import client.inventory.MapleInventoryType;
import constants.GameConstants;
import constants.ServerConstants;
import handling.auction.AuctionServer;
import handling.channel.ChannelServer;
import handling.world.World;
import scripting.NPCScriptManager;
import server.MapleInventoryManipulator;
import server.Randomizer;
import server.life.MapleMonsterInformationProvider;
import server.maps.MapleMap;
import tools.CurrentTime;
import tools.Pair;
import tools.packet.CField;
import tools.packet.CWvsContext.BuffPacket;
import tools.packet.CWvsContext.InfoPacket;

import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import tools.packet.SLFCGPacket;

public class MapleEtcControl implements Runnable {

    public long lastClearDropTime = 0, lastResetTimerTime = 0, lastConnectorTime = 0;
    public int date;

    public MapleEtcControl() {
        lastClearDropTime = System.currentTimeMillis();
        lastConnectorTime = System.currentTimeMillis();
        date = CurrentTime.요일();
        System.out.println("[Loading Completed] Start EtcControl");
    }

    @Override
    public void run() {
        long time = System.currentTimeMillis();

        if (time - lastClearDropTime >= 1000 * 60 * 60) {
            lastClearDropTime = time;
            MapleMonsterInformationProvider.getInstance().clearDrops();
            AuctionServer.saveItems();
            System.out.println("드롭 데이터를 초기화했습니다.");
        }
        /*        if (time - lastResetTimerTime >= 1000 * 60 * 60 * 6) {
            lastResetTimerTime = time;
            MobTimer.getInstance().stop();
            MobTimer.getInstance().start();
        }*/
        Iterator<ChannelServer> channels = ChannelServer.getAllInstances().iterator();
        while (channels.hasNext()) {
            ChannelServer cs = channels.next();
            Iterator<MapleCharacter> chrs = cs.getPlayerStorage().getAllCharacters().values().iterator();
            while (chrs.hasNext()) {

                MapleCharacter chr = chrs.next();

                for (MapleInventory inv : chr.getInventorys()) { // 기간제 아이템 삭제
                    Iterator<Item> items = inv.list().iterator();
                    while (items.hasNext()) {
                        Item item = items.next();
                        if (item.getExpiration() != -1 && (item.getExpiration() <= time)) {
                            if (item.getPosition() < 0) {
                                MapleInventoryManipulator.unequip(chr.getClient(), item.getPosition(), chr.getInventory(MapleInventoryType.EQUIP).getNextFreeSlot(), GameConstants.getInventoryType(item.getItemId()));
                            }
                            MapleInventoryManipulator.removeFromSlot(chr.getClient(), GameConstants.getInventoryType(item.getItemId()), item.getPosition(), item.getQuantity(), false);
                            chr.getClient().getSession().writeAndFlush(InfoPacket.itemExpired(item.getItemId()));
                        }
                    }
                }

                if (chr.getClient().getChatBlockedTime() > 0) {
                    if (time - chr.getClient().getChatBlockedTime() >= 0) {
                        chr.getClient().setChatBlockedTime(0);
                        chr.dropMessage(5, "채팅 금지 시간이 지나 금지가 해제됩니다.");
                    }
                }

                if (time - chr.lastMacroTime >= 15 * 10 * 1000 && Randomizer.nextInt(10000) < 1 && !chr.getMap().isTown() && chr.getChair() == 0 && chr.getEventInstance() == null) {
                    chr.lastMacroTime = time;
                    //   NPCScriptManager.getInstance().start(chr.getClient(), 2007, "macro"); // 매크로
                }

                if (time - chr.lastSaveTime >= 60 * 10 * 1000 && chr.choicepotential == null && chr.returnscroll == null && chr.memorialcube == null) {
                    new MapleCharacterSave(chr).saveToDB(chr, false, false);
                    chr.dropMessage(-8, "[알림] " + CurrentTime.시() + "시 " + CurrentTime.분() + "분 캐릭터를 서버에 저장하였습니다.");

                    if (chr.getGuildId() > 0) {
                        World.Guild.gainContribution(chr.getGuildId(), 50, chr.getId());
                        chr.getClient().getSession().writeAndFlush(InfoPacket.getGPContribution(50));
                    }
                }

                if (chr.getChair() != 0 && chr.getMapId() == ServerConstants.WarpMap) {
                    if (time - chr.lastChairPointTime >= 60000) {
                        chr.lastChairPointTime = time;
                        chr.gainbreakTimePoint(5);
                        chr.getClient().getSession().writeAndFlush(SLFCGPacket.OnYellowDlg(3003302, 2500, "#face1#5포인트가 수급되었습니다.", ""));
                    }
                }

                /* 무릉 도장 */
                if (chr.getDojoStartTime() > 0) {
                    Map<MapleBuffStat, Pair<Integer, Integer>> statups = new HashMap<>();
                    statups.put(MapleBuffStat.MobZoneState, new Pair<>(1, 0));
                    chr.getClient().getSession().writeAndFlush(BuffPacket.giveBuff(statups, null, chr));
                }

                //타이머
                if (chr.getDojoStartTime() > 0) {
                    if (chr.getDojoStopTime() > 0) {
                        chr.setDojoCoolTime(chr.getDojoCoolTime() + 1000); // 1초마다 1초씩 감소해줘야 함..
                        if (time - chr.getDojoStopTime() > 10000) {
                            chr.setDojoStopTime(0);
                            chr.getClient().getSession().writeAndFlush(CField.getDojoClockStop(false, (int) (900 - ((System.currentTimeMillis() - chr.getDojoStartTime()) / 1000))));
                        }
                    } else {
                        if ((time - chr.getDojoStartTime() - chr.getDojoCoolTime()) > (chr.getMapId() / 1000 == 925070 ? 300000 : chr.getMapId() / 1000 == 925071 ? 600000 : 900000)) {
                            MapleMap to = chr.getMap().getReturnMap();
                            chr.changeMap(to, to.getPortal(0));
                            NPCScriptManager.getInstance().start(chr.getClient(), "dojo_exit");
                        }
                    }
                }
            }
        }
    }
}
