/*     */ package server;

/*     */ import client.MapleClient;
/*     */ import java.awt.Point;
import server.maps.MapleMapObject;
/*     */ import server.maps.MapleMapObjectType;

/*     */
 /*     */ public class Obstacle extends MapleMapObject {

    /*     */ private Point oldPosition;
    /*     */    private Point newPosition;
    /*     */    private int key;
    /*     */    private int range;
    /*     */    private int trueDamage;
    /*     */    private int delay;

    /*     */
 /*     */ public Obstacle(int key, Point pos1, Point pos2, int range, int trueDamage, int height, int MaxP, int length, int angle, int unk) {
        /*  15 */ this.key = key;
        /*  16 */ this.oldPosition = pos1;
        /*  17 */ this.newPosition = pos2;
        /*  18 */ this.range = range;
        /*  19 */ this.trueDamage = trueDamage;
        /*  20 */ this.delay = 0;
        /*  21 */ setHeight(height);
        /*  22 */ this.maxP = MaxP;
        /*  23 */ this.length = length;
        /*  24 */ this.angle = angle;
        /*  25 */ this.unk = unk;
        /*     */    }
    /*     */    private int height;
    private int VperSec;
    private int maxP;
    private int length;
    private int angle;
    private int unk;
    private boolean effect = true;

    /*     */ public Obstacle(int key, Point pos1, Point pos2, int range, int trueDamage, int height, int MaxP, int length, int angle) {
        /*  29 */ this.key = key;
        /*  30 */ this.oldPosition = pos1;
        /*  31 */ this.newPosition = pos2;
        /*  32 */ this.range = range;
        /*  33 */ this.trueDamage = trueDamage;
        /*  34 */ this.delay = 0;
        /*  35 */ setHeight(height);
        /*  36 */ this.maxP = MaxP;
        /*  37 */ this.length = length;
        /*  38 */ this.angle = angle;
        /*  39 */ this.unk = 0;
        /*     */    }

    /*     */
 /*     */ public Obstacle(int key, Point pos1, Point pos2, int range, int trueDamage, int dealy, int height, int MaxP, int length, int angle, int unk) {
        /*  43 */ this.key = key;
        /*  44 */ this.oldPosition = pos1;
        /*  45 */ this.newPosition = pos2;
        /*  46 */ this.range = range;
        /*  47 */ this.trueDamage = trueDamage;
        /*  48 */ this.delay = dealy;
        /*  49 */ setHeight(height);
        /*  50 */ this.maxP = MaxP;
        /*  51 */ this.length = length;
        /*  52 */ this.angle = angle;
        /*  53 */ this.unk = unk;
        /*     */    }

    /*     */
 /*     */ public Point getOldPosition() {
        /*  57 */ return this.oldPosition;
        /*     */    }

    /*     */
 /*     */ public void setOldPosition(Point oldPosition) {
        /*  61 */ this.oldPosition = oldPosition;
        /*     */    }

    /*     */
 /*     */ public Point getNewPosition() {
        /*  65 */ return this.newPosition;
        /*     */    }

    /*     */
 /*     */ public void setNewPosition(Point newPosition) {
        /*  69 */ this.newPosition = newPosition;
        /*     */    }

    /*     */
 /*     */ public int getRangeed() {
        /*  73 */ return this.range;
        /*     */    }

    /*     */
 /*     */ public void setRange(int range) {
        /*  77 */ this.range = range;
        /*     */    }

    /*     */
 /*     */ public int getTrueDamage() {
        /*  81 */ return this.trueDamage;
        /*     */    }

    /*     */
 /*     */ public void setTrueDamage(int trueDamage) {
        /*  85 */ this.trueDamage = trueDamage;
        /*     */    }

    /*     */
 /*     */ public int getDelay() {
        /*  89 */ return this.delay;
        /*     */    }

    /*     */
 /*     */ public void setDelay(int delay) {
        /*  93 */ this.delay = delay;
        /*     */    }

    /*     */
 /*     */ public int getVperSec() {
        /*  97 */ return this.VperSec;
        /*     */    }

    /*     */
 /*     */ public void setVperSec(int vperSec) {
        /* 101 */ this.VperSec = vperSec;
        /*     */    }

    /*     */
 /*     */ public int getMaxP() {
        /* 105 */ return this.maxP;
        /*     */    }

    /*     */
 /*     */ public void setMaxP(int maxP) {
        /* 109 */ this.maxP = maxP;
        /*     */    }

    /*     */
 /*     */ public int getLength() {
        /* 113 */ return this.length;
        /*     */    }

    /*     */
 /*     */ public void setLength(int length) {
        /* 117 */ this.length = length;
        /*     */    }

    /*     */
 /*     */ public int getAngle() {
        /* 121 */ return this.angle;
        /*     */    }

    /*     */
 /*     */ public void setAngle(int angle) {
        /* 125 */ this.angle = angle;
        /*     */    }

    /*     */
 /*     */ public int getUnk() {
        /* 129 */ return this.unk;
        /*     */    }

    /*     */
 /*     */ public void setUnk(int unk) {
        /* 133 */ this.unk = unk;
        /*     */    }

    /*     */
 /*     */ public int getHeight() {
        /* 137 */ return this.height;
        /*     */    }

    /*     */
 /*     */ public void setHeight(int height) {
        /* 141 */ this.height = height;
        /*     */    }

    /*     */
 /*     */ public int getKey() {
        /* 145 */ return this.key;
        /*     */    }

    /*     */
 /*     */ public void setKey(int key) {
        /* 149 */ this.key = key;
        /*     */    }

    /*     */
 /*     */ public boolean isEffect() {
        /* 153 */ return this.effect;
        /*     */    }

    /*     */
 /*     */ public void setEffect(boolean effect) {
        /* 157 */ this.effect = effect;
        /*     */    }

    /*     */
 /*     */ public int getDistancePoints() {
        /* 161 */ double x = Math.pow(this.oldPosition.getX() - this.newPosition.getX(), 2.0D);
        /* 162 */ double y = Math.pow(this.oldPosition.getY() - this.newPosition.getY(), 2.0D);
        /* 163 */ double distantce = Math.sqrt(x + y);
        /* 164 */ return (int) Math.floor(distantce);
        /*     */    }

    /*     */
 /*     */
 /*     */
 /*     */ public MapleMapObjectType getType() {
        /* 170 */ return MapleMapObjectType.OBSTACLE;
        /*     */    }

    /*     */
 /*     */ public void sendSpawnData(MapleClient client) {
    }

    /*     */
 /*     */ public void sendDestroyData(MapleClient client) {
    }
    /*     */ }


/* Location:              C:\Users\Phellos\Desktop\크루엘라\Ozoh디컴.jar!\server\Obstacle.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */
