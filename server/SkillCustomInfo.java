/*    */ package server;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SkillCustomInfo
/*    */ {
/*    */   private long value;
/* 14 */   private long endtime = 0L;
/*    */   
/*    */   public SkillCustomInfo(long value, long time) {
/* 17 */     this.value = value;
/* 18 */     if (time > 0L)
/* 19 */       this.endtime = System.currentTimeMillis() + time; 
/*    */   }
/*    */   
/*    */   public boolean canCancel(long now) {
/* 23 */     return (this.endtime > 0L && now >= this.endtime);
/*    */   }
/*    */   
/*    */   public long getValue() {
/* 27 */     return this.value;
/*    */   }
/*    */   
/*    */   public long getEndTime() {
/* 31 */     return this.endtime;
/*    */   }
/*    */ }


/* Location:              C:\Users\Phellos\Desktop\크루엘라\Ozoh디컴.jar!\server\SkillCustomInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */