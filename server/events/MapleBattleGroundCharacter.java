/*     */ package server.events;
/*     */ 
/*     */ import client.MapleCharacter;
/*     */ import client.MapleStat;
/*     */ import java.util.ArrayList;
/*     */ import java.util.EnumMap;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import server.Randomizer;
/*     */ import server.Timer;
/*     */ import server.games.BattleGroundGameHandler;
/*     */ import tools.Triple;
/*     */ import tools.packet.BattleGroundPacket;
/*     */ import tools.packet.CField;
/*     */ import tools.packet.CWvsContext;
/*     */ import tools.packet.SLFCGPacket;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class MapleBattleGroundCharacter
/*     */   implements Comparable<MapleBattleGroundCharacter>
/*     */ {
/*     */   private MapleCharacter chr;
/*  31 */   public static List<MapleBattleGroundCharacter> bchr = new ArrayList<>(); private String name; private boolean alive = true;
/*     */   private int Jobtype;
/*     */   private int id;
/*  34 */   private int attackcount = 0; private int attackup; private int hpmpup; private int criticalup; private int speedup; private int regenup; private int money;
/*  35 */   private int skillmindamup = 0, skillmaxdamup = 0, skillhpup = 0, skillmpup = 0; private int level; private int speed; private int jump; private int maxhp; private int hp; private int maxmp; private int mp;
/*     */   private int hpregen;
/*  37 */   private int deathcount = 0; private int mpregen; private int mindam; private int maxdam; private int critical; private int exp; private int team; private int kill; private int death; private int attackspeed;
/*  38 */   private List<Triple<Integer, Integer, Integer>> skilllist = new ArrayList<>();
/*     */   /*     */   
/*     */   public MapleBattleGroundCharacter(MapleCharacter chr, String job) {
/*  41 */     this.chr = chr;
/*  42 */     this.name = chr.getName();
/*  43 */     this.id = chr.getId();
/*  44 */     this.level = 1;
/*  45 */     switch (job) {
/*     */       case "만지":
/*  47 */         this.Jobtype = 1;
/*  48 */         this.mindam = 180;
/*  49 */         this.maxdam = 200;
/*  50 */         this.hp = 3000;
/*  51 */         this.mp = 1000;
/*  52 */         this.critical = 0;
/*  53 */         this.attackspeed = 400;
/*  54 */         this.hpregen = 15;
/*  55 */         this.mpregen = 10;
/*  56 */         this.speed = 120;
/*  57 */         this.jump = 120;
/*  58 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/*  59 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001647), Integer.valueOf(1)));
/*  60 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001648), Integer.valueOf(0)));
/*  61 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001649), Integer.valueOf(0)));
/*  62 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001650), Integer.valueOf(0)));
/*  63 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001651), Integer.valueOf(0)));
/*  64 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/*     */         break;
/*     */       
/*     */       case "마이크":
/*  68 */         this.Jobtype = 2;
/*  69 */         this.mindam = 126;
/*  70 */         this.maxdam = 140;
/*  71 */         this.attackspeed = 400;
/*     */         
/*  73 */         this.hp = 3000;
/*  74 */         this.mp = 1000;
/*  75 */         this.critical = 0;
/*  76 */         this.hpregen = 18;
/*  77 */         this.mpregen = 10;
/*  78 */         this.speed = 120;
/*  79 */         this.jump = 120;
/*  80 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/*  81 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001652), Integer.valueOf(1)));
/*  82 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001653), Integer.valueOf(0)));
/*  83 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001654), Integer.valueOf(0)));
/*  84 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001655), Integer.valueOf(0)));
/*  85 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001656), Integer.valueOf(0)));
/*  86 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001676), Integer.valueOf(1)));
/*  87 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001677), Integer.valueOf(1)));
/*  88 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/*     */         break;
/*     */       
/*     */       case "다크로드":
/*  92 */         this.Jobtype = 3;
/*  93 */         this.mindam = 216;
/*  94 */         this.maxdam = 240;
/*  95 */         this.attackspeed = 330;
/*  96 */         this.hp = 2400;
/*  97 */         this.mp = 1000;
/*  98 */         this.critical = 0;
/*  99 */         this.hpregen = 12;
/* 100 */         this.mpregen = 10;
/* 101 */         this.speed = 120;
/* 102 */         this.jump = 120;
/* 103 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/* 104 */         this.skilllist.add(new Triple(Integer.valueOf(1), Integer.valueOf(80000330), Integer.valueOf(0)));
/* 105 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001657), Integer.valueOf(1)));
/* 106 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001658), Integer.valueOf(0)));
/* 107 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001659), Integer.valueOf(0)));
/* 108 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001660), Integer.valueOf(0)));
/* 109 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001675), Integer.valueOf(0)));
/* 110 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/*     */         break;
/*     */       
/*     */       case "하인즈":
/* 114 */         this.Jobtype = 4;
/* 115 */         this.mindam = 234;
/* 116 */         this.maxdam = 260;
/* 117 */         this.hp = 2100;
/* 118 */         this.mp = 1000;
/* 119 */         this.attackspeed = 400;
/* 120 */         this.critical = 0;
/* 121 */         this.hpregen = 10;
/* 122 */         this.mpregen = 10;
/* 123 */         this.speed = 120;
/* 124 */         this.jump = 120;
/* 125 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/* 126 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001661), Integer.valueOf(1)));
/* 127 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001662), Integer.valueOf(0)));
/* 128 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001663), Integer.valueOf(0)));
/* 129 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001664), Integer.valueOf(0)));
/* 130 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001665), Integer.valueOf(0)));
/* 131 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/*     */         break;
/*     */       
/*     */       case "무공":
/* 135 */         this.Jobtype = 5;
/* 136 */         this.mindam = 189;
/* 137 */         this.maxdam = 210;
/* 138 */         this.hp = 3150;
/* 139 */         this.mp = 1000;
/* 140 */         this.attackspeed = 400;
/* 141 */         this.critical = 0;
/* 142 */         this.hpregen = 15;
/* 143 */         this.mpregen = 10;
/* 144 */         this.speed = 120;
/* 145 */         this.jump = 120;
/* 146 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/* 147 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001666), Integer.valueOf(1)));
/* 148 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001667), Integer.valueOf(0)));
/* 149 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001668), Integer.valueOf(0)));
/* 150 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001669), Integer.valueOf(0)));
/* 151 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001670), Integer.valueOf(0)));
/* 152 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/* 153 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001679), Integer.valueOf(1)));
/*     */         break;
/*     */       
/*     */       case "헬레나":
/* 157 */         this.Jobtype = 6;
/* 158 */         this.mindam = 252;
/* 159 */         this.maxdam = 280;
/* 160 */         this.hp = 1800;
/* 161 */         this.mp = 1000;
/* 162 */         this.attackspeed = 400;
/* 163 */         this.critical = 0;
/* 164 */         this.hpregen = 18;
/* 165 */         this.mpregen = 10;
/* 166 */         this.speed = 120;
/* 167 */         this.jump = 120;
/* 168 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/* 169 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/* 170 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001732), Integer.valueOf(1)));
/* 171 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001733), Integer.valueOf(0)));
/* 172 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001734), Integer.valueOf(0)));
/* 173 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001735), Integer.valueOf(0)));
/* 174 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001736), Integer.valueOf(0)));
/*     */         break;
/*     */       
/*     */       case "랑이":
/* 178 */         this.Jobtype = 7;
/* 179 */         this.mindam = 207;
/* 180 */         this.maxdam = 230;
/* 181 */         this.hp = 2850;
/* 182 */         this.mp = 1000;
/* 183 */         this.attackspeed = 400;
/* 184 */         this.critical = 0;
/* 185 */         this.hpregen = 14;
/* 186 */         this.mpregen = 10;
/* 187 */         this.speed = 120;
/* 188 */         this.jump = 120;
/* 189 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/* 190 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/* 191 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001737), Integer.valueOf(1)));
/* 192 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001738), Integer.valueOf(0)));
/* 193 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001739), Integer.valueOf(0)));
/* 194 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001740), Integer.valueOf(0)));
/* 195 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001741), Integer.valueOf(1)));
/*     */         break;
/*     */       
/*     */       case "류드":
/* 199 */         this.Jobtype = 9;
/* 200 */         this.mindam = 150;
/* 201 */         this.maxdam = 170;
/* 202 */         this.hp = 3450;
/* 203 */         this.mp = 1000;
/* 204 */         this.attackspeed = 400;
/* 205 */         this.critical = 0;
/* 206 */         this.hpregen = 17;
/* 207 */         this.mpregen = 10;
/* 208 */         this.speed = 120;
/* 209 */         this.jump = 120;
/* 210 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/* 211 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/* 212 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002338), Integer.valueOf(1)));
/* 213 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002339), Integer.valueOf(0)));
/* 214 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002341), Integer.valueOf(0)));
/* 215 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002342), Integer.valueOf(0)));
/* 216 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002344), Integer.valueOf(0)));
/*     */         break;
/*     */       
/*     */       case "웡키":
/* 220 */         this.Jobtype = 10;
/* 221 */         this.mindam = 207;
/* 222 */         this.maxdam = 230;
/* 223 */         this.hp = 2550;
/* 224 */         this.mp = 1000;
/* 225 */         this.attackspeed = 400;
/* 226 */         this.critical = 0;
/* 227 */         this.hpregen = 12;
/* 228 */         this.mpregen = 10;
/* 229 */         this.speed = 120;
/* 230 */         this.jump = 120;
/* 231 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/* 232 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/* 233 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002670), Integer.valueOf(1)));
/* 234 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002671), Integer.valueOf(0)));
/* 235 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002672), Integer.valueOf(0)));
/* 236 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002673), Integer.valueOf(0)));
/* 237 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80002674), Integer.valueOf(0)));
/*     */         break;
/*     */       
/*     */       case "폴로&프리토":
/* 241 */         this.Jobtype = 11;
/* 242 */         this.mindam = 225;
/* 243 */         this.maxdam = 250;
/* 244 */         this.hp = 2400;
/* 245 */         this.mp = 1000;
/* 246 */         this.critical = 0;
/* 247 */         this.attackspeed = 400;
/* 248 */         this.hpregen = 15;
/* 249 */         this.mpregen = 10;
/* 250 */         this.speed = 120;
/* 251 */         this.jump = 120;
/* 252 */         this.skilllist.add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/* 253 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/* 254 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003001), Integer.valueOf(1)));
/* 255 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003002), Integer.valueOf(0)));
/* 256 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003003), Integer.valueOf(0)));
/* 257 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003004), Integer.valueOf(0)));
/* 258 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003005), Integer.valueOf(0)));
/* 259 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003006), Integer.valueOf(0)));
/* 260 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003007), Integer.valueOf(0)));
/* 261 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003008), Integer.valueOf(0)));
/* 262 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003009), Integer.valueOf(0)));
/* 263 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003010), Integer.valueOf(0)));
/* 264 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003011), Integer.valueOf(0)));
/* 265 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003012), Integer.valueOf(0)));
/* 266 */         this.skilllist.add(new Triple(Integer.valueOf(0), Integer.valueOf(80003013), Integer.valueOf(1)));
/*     */         break;
/*     */     } 
/*     */     
/* 270 */     this.maxhp = this.hp;
/* 271 */     this.maxmp = this.mp;
/* 272 */     this.attackup = 1;
/* 273 */     this.hpmpup = 1;
/* 274 */     this.criticalup = 1;
/* 275 */     this.speedup = 1;
/* 276 */     this.regenup = 1;
/* 277 */     this.team = 1;
/* 278 */     this.money = 0;
/* 279 */     bchr.add(this);
/*     */   }
/*     */   
/*     */   public String getName() {
/* 283 */     return this.name;
/*     */   }
/*     */   
/*     */   public void setName(String name) {
/* 287 */     this.name = name;
/*     */   }
/*     */   
/*     */   public int getId() {
/* 291 */     return this.id;
/*     */   }
/*     */   
/*     */   public void setId(int id) {
/* 295 */     this.id = id;
/*     */   }
/*     */   
/*     */   public int getJobType() {
/* 299 */     return this.Jobtype;
/*     */   }
/*     */   
/*     */   public void setJobType(int Jobtype) {
/* 303 */     this.Jobtype = Jobtype;
/*     */   }
/*     */   
/*     */   public int getAttackCount() {
/* 307 */     return this.attackcount;
/*     */   }
/*     */   
/*     */   public void setAttackcount(int attackcount) {
/* 311 */     this.attackcount = attackcount;
/*     */   }
/*     */   
/*     */   public int getLevel() {
/* 315 */     return this.level;
/*     */   }
/*     */   
/*     */   public void setLevel(int level) {
/* 319 */     this.level = level;
/*     */   }
/*     */   
/*     */   public int getSpeed() {
/* 323 */     return this.speed;
/*     */   }
/*     */   
/*     */   public void setSpeed(int speed) {
/* 327 */     this.speed = speed;
/*     */   }
/*     */   
/*     */   public int getJump() {
/* 331 */     return this.jump;
/*     */   }
/*     */   
/*     */   public void setJump(int jump) {
/* 335 */     this.jump = jump;
/*     */   }
/*     */   
/*     */   public int getHp() {
/* 339 */     return this.hp;
/*     */   }
/*     */   
/*     */   public void setHp(int hp) {
/* 343 */     this.hp = hp;
/* 344 */     if (this.hp >= getMaxHp()) {
/* 345 */       this.hp = getMaxHp();
/*     */     }
/* 347 */     if (this.chr.getBattleGroundChr().getHp() <= 0) {
/* 348 */       int respawntime = 0;
/* 349 */       switch (this.level) {
/*     */         case 1:
/*     */         case 2:
/* 352 */           respawntime = 1;
/*     */           break;
/*     */         case 3:
/*     */         case 4:
/* 356 */           respawntime = 2;
/*     */           break;
/*     */         case 5:
/* 359 */           respawntime = 3;
/*     */           break;
/*     */         case 6:
/* 362 */           respawntime = 4;
/*     */           break;
/*     */         case 7:
/* 365 */           respawntime = 5;
/*     */           break;
/*     */         case 8:
/* 368 */           respawntime = 6;
/*     */           break;
/*     */         case 9:
/* 371 */           respawntime = 7;
/*     */           break;
/*     */         case 10:
/* 374 */           respawntime = 8;
/*     */           break;
/*     */         case 11:
/* 377 */           respawntime = 9;
/*     */           break;
/*     */         case 12:
/* 380 */           respawntime = 10;
/*     */           break;
/*     */         case 13:
/* 383 */           respawntime = 11;
/*     */           break;
/*     */         case 14:
/* 386 */           respawntime = 12;
/*     */           break;
/*     */         case 15:
/* 389 */           respawntime = 20;
/*     */           break;
/*     */       } 
/* 392 */       this.hp = 0;
/* 393 */       if (this.chr.getMap().getBattleGroundMainTimer() > respawntime) {
/* 394 */         this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.Respawn(getId(), 1));
/* 395 */         this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.Death(respawntime));
/*     */       } 
/* 397 */       this.chr.getMap().broadcastMessage(BattleGroundPacket.DeathEffect(this));
/* 398 */       if (BattleGroundGameHandler.isEndOfGame()) {
/*     */         return;
/*     */       }
/*     */       
/* 402 */       Timer.MapTimer.getInstance().schedule(() -> { if (BattleGroundGameHandler.isEndOfGame()) return;  Heal(); int now = Randomizer.rand(1, 12); this.chr.getClient().getSession().writeAndFlush(CField.instantMapWarp(this.chr, (byte)now)); setAlive(true); },(respawntime * 1000 + 1000));
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getMaxHp() {
/* 415 */     return this.maxhp + this.skillhpup;
/*     */   }
/*     */   
/*     */   public void setMaxHp(int maxhp) {
/* 419 */     this.maxhp = maxhp;
/*     */   }
/*     */   
/*     */   public int getMp() {
/* 423 */     return this.mp;
/*     */   }
/*     */   
/*     */   public void setMp(int mp) {
/* 427 */     this.mp = mp;
/* 428 */     if (this.mp >= getMaxMp()) {
/* 429 */       this.mp = getMaxMp();
/*     */     }
/* 431 */     if (this.chr.getBattleGroundChr().getMp() <= 0) {
/* 432 */       this.mp = 0;
/*     */     }
/*     */   }
/*     */   
/*     */   public int getMaxMp() {
/* 437 */     return this.maxmp + this.skillmpup;
/*     */   }
/*     */   
/*     */   public void setMaxMp(int maxmp) {
/* 441 */     this.maxmp = maxmp;
/*     */   }
/*     */   
/*     */   public int getHpRegen() {
/* 445 */     return this.hpregen;
/*     */   }
/*     */   
/*     */   public void setHpRegen(int hpregen) {
/* 449 */     this.hpregen = hpregen;
/*     */   }
/*     */   
/*     */   public int getMpRegen() {
/* 453 */     return this.mpregen;
/*     */   }
/*     */   
/*     */   public void setMpRegen(int mpregen) {
/* 457 */     this.mpregen = mpregen;
/*     */   }
/*     */   
/*     */   public int getMindam() {
/* 461 */     return this.mindam + this.skillmindamup;
/*     */   }
/*     */   
/*     */   public void setMindam(int mindam) {
/* 465 */     this.mindam = mindam;
/*     */   }
/*     */   
/*     */   public int getMaxdam() {
/* 469 */     return this.maxdam + this.skillmaxdamup;
/*     */   }
/*     */   
/*     */   public void setMaxdam(int maxdam) {
/* 473 */     this.maxdam = maxdam;
/*     */   }
/*     */   
/*     */   public int getCritical() {
/* 477 */     return this.critical;
/*     */   }
/*     */   
/*     */   public void setCritical(int critical) {
/* 481 */     this.critical = critical;
/*     */   }
/*     */   
/*     */   public int getAttackSpeed() {
/* 485 */     return this.attackspeed;
/*     */   }
/*     */   
/*     */   public void setAttackSpeed(int attackspeed) {
/* 489 */     this.attackspeed = attackspeed;
/*     */   }
/*     */   
/*     */   public int getAttackUp() {
/* 493 */     return this.attackup;
/*     */   }
/*     */   
/*     */   public void setAttackUp(int attackup) {
/* 497 */     this.attackup = attackup;
/*     */   }
/*     */   
/*     */   public int getHpMpUp() {
/* 501 */     return this.hpmpup;
/*     */   }
/*     */   
/*     */   public void setHpMpUp(int hpmpup) {
/* 505 */     this.hpmpup = hpmpup;
/*     */   }
/*     */   
/*     */   public int getCriUp() {
/* 509 */     return this.criticalup;
/*     */   }
/*     */   
/*     */   public void setCriUp(int criticalup) {
/* 513 */     this.criticalup = criticalup;
/*     */   }
/*     */   
/*     */   public int getSpeedUp() {
/* 517 */     return this.speedup;
/*     */   }
/*     */   
/*     */   public void setSpeedUp(int speedup) {
/* 521 */     this.speedup = speedup;
/*     */   }
/*     */   
/*     */   public int getRegenUp() {
/* 525 */     return this.regenup;
/*     */   }
/*     */   
/*     */   public void setRegenUp(int regenup) {
/* 529 */     this.regenup = regenup;
/*     */   }
/*     */   
/*     */   public int getSkillMinDamUp() {
/* 533 */     return this.skillmindamup;
/*     */   }
/*     */   
/*     */   public void setSkillMinDamUp(int skillmindamup) {
/* 537 */     this.skillmindamup = skillmindamup;
/*     */   }
/*     */   
/*     */   public int getSkillMaxDamUp() {
/* 541 */     return this.skillmaxdamup;
/*     */   }
/*     */   
/*     */   public void setSkillMaxDamUp(int skillmaxdamup) {
/* 545 */     this.skillmaxdamup = skillmaxdamup;
/*     */   }
/*     */   
/*     */   public int getSkillHPUP() {
/* 549 */     return this.skillhpup;
/*     */   }
/*     */   
/*     */   public void setSkillHPUP(int skillhpup) {
/* 553 */     this.skillhpup = skillhpup;
/*     */   }
/*     */   
/*     */   public int getSkillMPUP() {
/* 557 */     return this.skillmpup;
/*     */   }
/*     */   
/*     */   public void setSkillMPUP(int skillmpup) {
/* 561 */     this.skillmpup = skillmpup;
/*     */   }
/*     */   
/*     */   public int getMoney() {
/* 565 */     return this.money;
/*     */   }
/*     */   
/*     */   public void setMoney(int money) {
/* 569 */     this.money = money;
/*     */   }
/*     */   
/*     */   public int getKill() {
/* 573 */     return this.kill;
/*     */   }
/*     */   
/*     */   public void setKill(int kill) {
/* 577 */     this.kill = kill;
/*     */   }
/*     */   
/*     */   public int getDeath() {
/* 581 */     return this.death;
/*     */   }
/*     */   
/*     */   public void setDeath(int death) {
/* 585 */     this.death = death;
/*     */   }
/*     */   
/*     */   public int getExp() {
/* 589 */     return this.exp;
/*     */   }
/*     */   
/*     */   public void setExp(int exp) {
/* 593 */     this.exp = exp;
/* 594 */     int max = 0;
/* 595 */     switch (this.level) {
/*     */       case 1:
/* 597 */         max = 51;
/*     */         break;
/*     */       case 2:
/* 600 */         max = 76;
/*     */         break;
/*     */       case 3:
/* 603 */         max = 107;
/*     */         break;
/*     */       case 4:
/* 606 */         max = 134;
/*     */         break;
/*     */       case 5:
/* 609 */         max = 188;
/*     */         break;
/*     */       case 6:
/* 612 */         max = 245;
/*     */         break;
/*     */       case 7:
/* 615 */         max = 318;
/*     */         break;
/*     */       case 8:
/* 618 */         max = 414;
/*     */         break;
/*     */       case 9:
/* 621 */         max = 496;
/*     */         break;
/*     */       case 10:
/* 624 */         max = 596;
/*     */         break;
/*     */       case 11:
/* 627 */         max = 715;
/*     */         break;
/*     */       case 12:
/* 630 */         max = 787;
/*     */         break;
/*     */       case 13:
/* 633 */         max = 866;
/*     */         break;
/*     */       case 14:
/* 636 */         max = 952;
/*     */         break;
/*     */       case 15:
/* 639 */         max = 9999;
/*     */         break;
/*     */     } 
/* 642 */     if (this.level >= 15) {
/* 643 */       max = 9999;
/*     */     }
/* 645 */     if (this.level >= 15) {
/* 646 */       this.exp = 0;
/* 647 */       exp = 0;
/*     */     } 
/* 649 */     if (exp >= max) {
/* 650 */       levelup();
/*     */     }
/*     */   }
/*     */   
/*     */   public void levelup() {
/* 655 */     this.exp = 0;
/* 656 */     int beforelevel = this.level - 1;
/* 657 */     if (beforelevel < 0) {
/* 658 */       beforelevel = 0;
/*     */     }
/* 660 */     setLevel(this.level + 1);
/* 661 */     switch (this.Jobtype) {
/*     */       case 1:
/* 663 */         this.mindam += 6 + beforelevel * 2;
/* 664 */         this.maxdam += 6 + beforelevel * 2;
/* 665 */         this.maxhp += 300;
/* 666 */         this.hpregen += 3;
/* 667 */         if (this.level == 3) {
/* 668 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001648, 1, 0, 0, 0, 5000)); break;
/* 669 */         }  if (this.level == 5) {
/* 670 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001649, 1, 0, 0, 0, 7000)); break;
/* 671 */         }  if (this.level == 7) {
/* 672 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001650, 1, 0, 0, 0, 8000)); break;
/* 673 */         }  if (this.level == 8) {
/* 674 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001651, 1, 0, 0, 0, 70000)); break;
/* 675 */         }  if (this.level == 11) {
/* 676 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001651, 2, 0, 0, 0, 70000));
/*     */         }
/*     */         break;
/*     */       case 2:
/* 680 */         this.mindam += 4 + beforelevel * 2;
/* 681 */         this.maxdam += 4 + beforelevel * 2;
/* 682 */         this.maxhp += 320;
/* 683 */         this.hpregen += 3;
/* 684 */         if (this.level == 3) {
/* 685 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001653, 1, 0, 0, 0, 6000)); break;
/* 686 */         }  if (this.level == 5) {
/* 687 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001654, 1, 0, 0, 0, 9000)); break;
/* 688 */         }  if (this.level == 7) {
/* 689 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001655, 1, 5800, 80001676, 0, 20000)); break;
/* 690 */         }  if (this.level == 8) {
/* 691 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001656, 1, 11800, 80001677, 0, 60000)); break;
/* 692 */         }  if (this.level == 11) {
/* 693 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001656, 2, 11800, 80001677, 0, 60000));
/*     */         }
/*     */         break;
/*     */       case 3:
/* 697 */         this.mindam += 5 + beforelevel * 2;
/* 698 */         this.maxdam += 5 + beforelevel * 2;
/* 699 */         this.maxhp += 240;
/* 700 */         this.hpregen += 2;
/* 701 */         if (this.level == 3) {
/* 702 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80000330, 1, 0, 0, 0, 0)); break;
/* 703 */         }  if (this.level == 5) {
/* 704 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80000330, 2, 0, 0, 0, 0));
/* 705 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001658, 1, 0, 0, 0, 10000)); break;
/* 706 */         }  if (this.level == 7) {
/* 707 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80000330, 3, 0, 0, 0, 0));
/* 708 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001659, 1, 0, 0, 0, 12000)); break;
/* 709 */         }  if (this.level == 8) {
/* 710 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001660, 1, 0, 0, 0, 60000)); break;
/* 711 */         }  if (this.level == 9) {
/* 712 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80000330, 4, 0, 0, 0, 0)); break;
/* 713 */         }  if (this.level == 11) {
/* 714 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80000330, 5, 0, 0, 0, 0));
/* 715 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001660, 2, 0, 0, 0, 60000));
/*     */         } 
/*     */         break;
/*     */       case 4:
/* 719 */         this.mindam += 5 + beforelevel * 2;
/* 720 */         this.maxdam += 5 + beforelevel * 2;
/* 721 */         this.maxhp += 210;
/* 722 */         this.hpregen += 2;
/* 723 */         if (this.level == 3) {
/* 724 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001662, 1, 0, 0, 0, 6000)); break;
/* 725 */         }  if (this.level == 5) {
/* 726 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001663, 1, 0, 0, 0, 12000)); break;
/* 727 */         }  if (this.level == 7) {
/* 728 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001664, 1, 0, 0, 0, 10000)); break;
/* 729 */         }  if (this.level == 8) {
/* 730 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001665, 1, 0, 0, 0, 60000)); break;
/* 731 */         }  if (this.level == 11) {
/* 732 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001665, 2, 0, 0, 0, 60000));
/*     */         }
/*     */         break;
/*     */       case 5:
/* 736 */         this.mindam += 4 + beforelevel * 2;
/* 737 */         this.maxdam += 4 + beforelevel * 2;
/* 738 */         this.maxhp += 315;
/* 739 */         this.hpregen += 3;
/* 740 */         if (this.level == 3) {
/* 741 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001667, 1, 0, 0, 0, 5000)); break;
/* 742 */         }  if (this.level == 5) {
/* 743 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001668, 1, 0, 0, 0, 10000)); break;
/* 744 */         }  if (this.level == 7) {
/* 745 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001669, 1, 1000, 0, 80001679, 7000)); break;
/* 746 */         }  if (this.level == 8) {
/* 747 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001670, 1, 0, 0, 0, 50000)); break;
/* 748 */         }  if (this.level == 11) {
/* 749 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001670, 2, 0, 0, 0, 50000));
/*     */         }
/*     */         break;
/*     */       case 6:
/* 753 */         this.mindam += 6 + beforelevel * 3;
/* 754 */         this.maxdam += 6 + beforelevel * 3;
/* 755 */         this.maxhp += 180;
/* 756 */         this.hpregen += 2;
/* 757 */         if (this.level == 3) {
/* 758 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001733, 1, 0, 0, 0, 10000)); break;
/* 759 */         }  if (this.level == 5) {
/* 760 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001734, 1, 0, 0, 0, 15000)); break;
/* 761 */         }  if (this.level == 7) {
/* 762 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001735, 1, 0, 0, 0, 15000)); break;
/* 763 */         }  if (this.level == 8) {
/* 764 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001736, 1, 0, 0, 0, 45000)); break;
/* 765 */         }  if (this.level == 11) {
/* 766 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001736, 2, 0, 0, 0, 45000));
/*     */         }
/*     */         break;
/*     */       case 7:
/* 770 */         this.mindam += 4 + beforelevel * 2;
/* 771 */         this.maxdam += 4 + beforelevel * 2;
/* 772 */         this.maxhp += 285;
/* 773 */         this.hpregen += 3;
/* 774 */         if (this.level == 3) {
/* 775 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001738, 1, 0, 0, 0, 3000)); break;
/* 776 */         }  if (this.level == 5) {
/* 777 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001739, 1, 0, 0, 0, 10000, 3, 3)); break;
/* 778 */         }  if (this.level == 7) {
/* 779 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001740, 1, 0, 0, 0, 17000)); break;
/* 780 */         }  if (this.level == 8) {
/* 781 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001741, 2, 0, 0, 0, 0)); break;
/* 782 */         }  if (this.level == 11) {
/* 783 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80001741, 3, 0, 0, 0, 0));
/*     */         }
/*     */         break;
/*     */       
/*     */       case 9:
/* 788 */         this.mindam += 3 + beforelevel * 2;
/* 789 */         this.maxdam += 3 + beforelevel * 2;
/* 790 */         this.maxhp += 350;
/* 791 */         this.hpregen += 3;
/* 792 */         if (this.level == 3) {
/* 793 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002339, 1, 0, 0, 0, 7000)); break;
/* 794 */         }  if (this.level == 5) {
/* 795 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002341, 1, 0, 0, 0, 20000)); break;
/* 796 */         }  if (this.level == 7) {
/* 797 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002342, 1, 0, 0, 0, 20000)); break;
/* 798 */         }  if (this.level == 8) {
/* 799 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002344, 1, 0, 0, 0, 70000)); break;
/* 800 */         }  if (this.level == 11) {
/* 801 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002344, 2, 0, 0, 0, 70000));
/*     */         }
/*     */         break;
/*     */       case 10:
/* 805 */         this.mindam += 5 + beforelevel * 2;
/* 806 */         this.maxdam += 5 + beforelevel * 2;
/* 807 */         this.maxhp += 255;
/* 808 */         this.hpregen += 2;
/* 809 */         if (this.level == 3) {
/* 810 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002671, 1, 0, 0, 0, 15000)); break;
/* 811 */         }  if (this.level == 5) {
/* 812 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002673, 1, 0, 0, 0, 12000)); break;
/* 813 */         }  if (this.level == 7) {
/* 814 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002672, 1, 0, 0, 0, 20000)); break;
/* 815 */         }  if (this.level == 8) {
/* 816 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002674, 1, 0, 0, 0, 50000)); break;
/* 817 */         }  if (this.level == 11) {
/* 818 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80002674, 2, 0, 0, 0, 50000));
/*     */         }
/*     */         break;
/*     */       case 11:
/*     */       case 12:
/* 823 */         this.mindam += 5 + beforelevel * 2;
/* 824 */         this.maxdam += 5 + beforelevel * 2;
/* 825 */         this.maxhp += 250;
/* 826 */         this.hpregen += 2;
/* 827 */         if (this.level == 3) {
/* 828 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003002, 1, 0, 0, 0, 4000));
/* 829 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003007, 1, 0, 0, 0, 4000)); break;
/* 830 */         }  if (this.level == 5) {
/* 831 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003003, 1, 0, 0, 0, 5000));
/* 832 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003008, 1, 0, 0, 0, 15000)); break;
/* 833 */         }  if (this.level == 7) {
/* 834 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003004, 1, 0, 0, 0, 15000));
/* 835 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003009, 1, 0, 0, 0, 20000)); break;
/* 836 */         }  if (this.level == 8) {
/* 837 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003005, 1, 0, 0, 0, 50000));
/* 838 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003012, 1, 0, 0, 0, 50000)); break;
/* 839 */         }  if (this.level == 11) {
/* 840 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003005, 2, 0, 0, 0, 50000));
/* 841 */           this.chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SkillOn(this, 80003012, 2, 0, 0, 0, 50000));
/*     */         } 
/*     */         break;
/*     */     } 
/* 845 */     if (this.level == 6) {
/* 846 */       this.chr.getClient().getSession().writeAndFlush(CField.ImageTalkNpc(9001153, 3000, "아바타 상태 분석 완료. 다음 사냥터로 이동하는 것을 추천드립니다."));
/* 847 */     } else if (this.level == 8 && this.Jobtype != 7) {
/* 848 */       this.chr.getClient().getSession().writeAndFlush(CField.ImageTalkNpc(9001153, 3000, "아바타 상태 분석 완료. 궁극기를 사용할 수 있게 되었습니다."));
/* 849 */     } else if (this.level == 15) {
/* 850 */       this.chr.getClient().getSession().writeAndFlush(CField.ImageTalkNpc(9001153, 3000, "시스템 성장 임계점에 도달하였습니다. 더욱 적극적으로 상대방을 격파 하십시오."));
/*     */     } 
/* 852 */     this.maxmp += 100;
/* 853 */     this.mpregen++;
/* 854 */     this.skillmindamup = 0;
/* 855 */     this.skillmaxdamup = 0;
/* 856 */     this.chr.getClient().getSession().writeAndFlush(CField.EffectPacket.showEffect(this.chr, 0, 0, 29, 0, 0, (byte)(this.chr.isFacingLeft() ? 1 : 0), true, this.chr.getTruePosition(), "Effect/BasicEff.img/PvpLevelUp", null));
/* 857 */     this.chr.getMap().broadcastMessage(this.chr, CField.EffectPacket.showEffect(this.chr, 0, 0, 29, 0, 0, (byte)(this.chr.isFacingLeft() ? 1 : 0), false, this.chr.getTruePosition(), "Effect/BasicEff.img/PvpLevelUp", null), false);
/* 858 */     this.chr.getMap().broadcastMessage(SLFCGPacket.playSE("Sound/Game.img/PvpLevelUp"));
/* 859 */     int attackup = (getAttackUp() == 1) ? 2 : ((getAttackUp() == 2) ? 5 : ((getAttackUp() == 3) ? 10 : ((getAttackUp() == 4) ? 15 : ((getAttackUp() == 5) ? 20 : 0))));
/* 860 */     setSkillMinDamUp(Math.round(getSkillMinDamUp() + (getMindam() / 100 * attackup)));
/* 861 */     setSkillMaxDamUp(Math.round(getSkillMaxDamUp() + (getMaxdam() / 100 * attackup)));
/* 862 */     int hpmpup = (getHpMpUp() == 1) ? 2 : ((getHpMpUp() == 2) ? 7 : ((getHpMpUp() == 3) ? 15 : ((getHpMpUp() == 4) ? 20 : ((getHpMpUp() == 5) ? 30 : 0))));
/* 863 */     setSkillHPUP(getMaxHp() / 100 * hpmpup);
/* 864 */     setSkillMPUP(getMaxMp() / 100 * hpmpup);
/*     */   }
/*     */   
/*     */   public int getTeam() {
/* 868 */     return this.team;
/*     */   }
/*     */   
/*     */   public void setTeam(int team) {
/* 872 */     this.team = team;
/*     */   }
/*     */   
/*     */   public boolean isAlive() {
/* 876 */     return this.alive;
/*     */   }
/*     */   
/*     */   public void setAlive(boolean alive) {
/* 880 */     this.alive = alive;
/*     */   }
/*     */   
/*     */   public List<Triple<Integer, Integer, Integer>> getSkillList() {
/* 884 */     return this.skilllist;
/*     */   }
/*     */   
/*     */   public MapleCharacter getChr() {
/* 888 */     return this.chr;
/*     */   }
/*     */   
/*     */   public void Heal() {
/* 892 */     this.hp = getMaxHp();
/* 893 */     this.mp = getMaxMp();
/* 894 */     getChr().dispel();
/* 895 */     Map<MapleStat, Long> statup = new EnumMap<>(MapleStat.class);
/* 896 */     statup.put(MapleStat.MAXHP, Long.valueOf(getMaxHp()));
/* 897 */     statup.put(MapleStat.MAXMP, Long.valueOf(getMaxMp()));
/* 898 */     statup.put(MapleStat.HP, Long.valueOf(this.hp));
/* 899 */     statup.put(MapleStat.MP, Long.valueOf(this.mp));
/* 900 */     this.chr.getClient().getSession().writeAndFlush(CWvsContext.updatePlayerStats(statup, false, this.chr));
/* 901 */     this.chr.getMap().broadcastMessage(CField.updatePartyMemberHP(this.chr.getId(), getHp(), getMaxHp()));
/*     */   }
/*     */   
/*     */   public int getDeathcount() {
/* 905 */     return this.deathcount;
/*     */   }
/*     */   
/*     */   public void setDeathcount(int deathcount) {
/* 909 */     this.deathcount = deathcount;
/*     */   }
/*     */ 
/*     */   /*     */ 
/*     */   
/*     */   public int compareTo(MapleBattleGroundCharacter other) {
/* 914 */     return (this.kill <= other.kill) ? 1 : -1;
/*     */   }
/*     */ }


/* Location:              C:\Users\Phellos\Desktop\크루엘라\Ozoh디컴.jar!\server\events\MapleBattleGroundCharacter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */