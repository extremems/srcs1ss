/*      */ package server.events;
/*      */ 
/*      */ import client.MapleCharacter;
/*      */ import client.MapleClient;
/*      */ import client.MapleStat;
/*      */ import client.MapleBuffStat;
/*      */ import client.skills.SkillFactory;
/*      */ import client.inventory.Item;
/*      */ import client.status.MonsterStatus;
/*      */ import client.status.MonsterStatusEffect;
/*      */ import constants.GameConstants;
/*      */ import java.awt.Point;
/*      */ import java.util.ArrayList;
/*      */ import java.util.EnumMap;
/*      */ import java.util.List;
/*      */ import java.util.Map;
/*      */ import server.Randomizer;
/*      */ import client.skills.MapleStatEffect;
/*      */ import server.Timer;
/*      */ import server.games.BattleGroundGameHandler;
/*      */ import server.life.MapleLifeFactory;
/*      */ import server.life.MapleMonster;
/*      */ import server.maps.MapleMap;
/*      */ import server.maps.MapleMapObject;
/*      */ import tools.Pair;
/*      */ import tools.Triple;
/*      */ import tools.data.LittleEndianAccessor;
/*      */ import tools.packet.BattleGroundPacket;
/*      */ import tools.packet.CField;
/*      */ import tools.packet.CWvsContext;
/*      */ import tools.packet.MobPacket;
/*      */ import tools.packet.SLFCGPacket;
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ public class MapleBattleGround
/*      */ {
/*      */   public static void GameStart(MapleMap map) {
/*   47 */     map.resetFully();
/*   48 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303217), new Point(1217, -195));
/*   49 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303220), new Point(-1200, 1410));
/*   50 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303221), new Point(-400, 1352));
/*   51 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303200), new Point(520, 1410));
/*   52 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303201), new Point(220, 1410));
/*   53 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303200), new Point(-80, 1291));
/*   54 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303201), new Point(-220, 1352));
/*   55 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303226), new Point(-340, 1291));
/*   56 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303226), new Point(-340, 1352));
/*   57 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303226), new Point(-518, 1410));
/*   58 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303226), new Point(-164, 1410));
/*   59 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303202), new Point(-2500, 634));
/*   60 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303222), new Point(-1900, 634));
/*   61 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303203), new Point(-1700, 634));
/*   62 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303223), new Point(-1100, 634));
/*   63 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303227), new Point(-700, 634));
/*   64 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303227), new Point(-1900, 634));
/*   65 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303202), new Point(-1900, 259));
/*   66 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303222), new Point(-1300, 259));
/*   67 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303203), new Point(-1600, 259));
/*   68 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303223), new Point(-1400, 259));
/*   69 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303227), new Point(-1600, 259));
/*   70 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303202), new Point(-2400, -99));
/*   71 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303222), new Point(-2300, -99));
/*   72 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303227), new Point(-2150, -99));
/*   73 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303202), new Point(-1300, -100));
/*   74 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303222), new Point(-1200, -100));
/*   75 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303203), new Point(-1100, -100));
/*   76 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303223), new Point(-1000, -100));
/*   77 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303227), new Point(-1000, -100));
/*   78 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303204), new Point(174, 629));
/*   79 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303204), new Point(896, 629));
/*   80 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303205), new Point(1801, 629));
/*   81 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303224), new Point(356, 629));
/*   82 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303224), new Point(1606, 629));
/*   83 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303225), new Point(1073, 629));
/*   84 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303204), new Point(1432, 260));
/*   85 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303205), new Point(500, 260));
/*   86 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303224), new Point(800, 260));
/*   87 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303225), new Point(700, 260));
/*   88 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303228), new Point(500, 629));
/*   89 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303228), new Point(700, 629));
/*   90 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303228), new Point(1500, 629));
/*   91 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303228), new Point(1700, 260));
/*   92 */     map.spawnMonsterOnGroundBelow(MapleLifeFactory.getMonster(9303228), new Point(566, 260)); } public static void Attack(LittleEndianAccessor slea, MapleClient c) {
/*      */     List<Triple<Integer, Integer, Integer>> info;
/*      */     int cooltime;
/*      */     List<Pair<Integer, Integer>> coollist;
/*   96 */     List<Integer> mob = new ArrayList<>();
/*   97 */     int range = 0, attackimg = 0, imgafter = 0, speed = 0, delay = 400, delay2 = 0, cool = 0, stack = 0;
/*   98 */     int skillid = slea.readInt();
/*   99 */     if (skillid == 80001739 && 
/*  100 */       c.getPlayer().getSkillCustomValue(skillid) != null) {
/*  101 */       c.getSession().writeAndFlush(CWvsContext.enableActions(c.getPlayer()));
/*      */       
/*      */       return;
/*      */     } 
/*  105 */     slea.readByte();
/*  106 */     int left = slea.readByte();
/*  107 */     int skilllv = slea.readInt();
/*  108 */     Point oldpos = slea.readIntPos();
/*  109 */     Point newpos = slea.readIntPos();
/*  110 */     int psize = slea.readInt();
/*  111 */     for (int i = 0; i < psize; i++) {
/*  112 */       mob.add(Integer.valueOf(slea.readInt()));
/*      */     }
/*  114 */     int size = slea.readInt();
/*  115 */     for (int j = 0; j < size; j++) {
/*  116 */       mob.add(Integer.valueOf(slea.readInt()));
/*      */     }
/*  118 */     slea.readInt();
/*  119 */     int moveing = slea.readInt();
/*  120 */     switch (skillid) {
/*      */       case 80001647:
/*  122 */         attackimg = Randomizer.rand(1111, 1112);
/*  123 */         imgafter = 300;
/*  124 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80001648:
/*  128 */         attackimg = 1113;
/*  129 */         imgafter = 300;
/*  130 */         delay = 0;
/*  131 */         speed = 1;
/*  132 */         cool = 5;
/*      */         break;
/*      */       
/*      */       case 80001649:
/*  136 */         attackimg = 1114;
/*  137 */         imgafter = 360;
/*  138 */         delay = 0;
/*  139 */         speed = 1;
/*  140 */         cool = 7;
/*      */         break;
/*      */       
/*      */       case 80001650:
/*  144 */         attackimg = 1115;
/*  145 */         imgafter = 150;
/*  146 */         range = 240;
/*  147 */         delay = 0;
/*  148 */         speed = 0;
/*  149 */         cool = 8;
/*      */         break;
/*      */       
/*      */       case 80001651:
/*  153 */         attackimg = 1116;
/*  154 */         imgafter = 15000;
/*  155 */         delay = 0;
/*  156 */         cool = 70;
/*  157 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 11) {
/*  158 */           skilllv = 2;
/*      */         } else {
/*  160 */           skilllv = 1;
/*      */         } 
/*  162 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer());
/*      */         break;
/*      */       
/*      */       case 80001652:
/*  166 */         attackimg = Randomizer.rand(1125, 1126);
/*  167 */         imgafter = 330;
/*  168 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80001653:
/*  172 */         attackimg = 1128;
/*  173 */         imgafter = 1780;
/*  174 */         delay = 0;
/*  175 */         cool = 6;
/*  176 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80001654:
/*  180 */         attackimg = 1127;
/*  181 */         imgafter = 6270;
/*  182 */         delay = 0;
/*  183 */         cool = 9;
/*  184 */         speed = 0;
/*  185 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer());
/*      */         break;
/*      */       
/*      */       case 80001655:
/*  189 */         attackimg = 1125;
/*  190 */         imgafter = 6000;
/*  191 */         delay = 0;
/*  192 */         cool = 30;
/*  193 */         speed = 0;
/*  194 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer(), 6000);
/*  195 */         c.getPlayer().setSkillCustomInfo(80001655, 1500, 6000);
/*      */         break;
/*      */       
/*      */       case 80001676:
/*  199 */         attackimg = 4;
/*  200 */         imgafter = 1920;
/*  201 */         delay = 0;
/*  202 */         delay2 = 1256;
/*  203 */         speed = 0;
/*  204 */         if (c.getPlayer().getBuffedEffect(80001655) != null) {
/*  205 */           c.getPlayer().cancelEffect(c.getPlayer().getBuffedEffect(80001655), false, -1);
/*      */         }
/*  207 */         c.getPlayer().removeSkillCustomInfo(80001655);
/*      */         break;
/*      */       
/*      */       case 80001656:
/*  211 */         attackimg = 1126;
/*  212 */         imgafter = 12000;
/*  213 */         delay = 0;
/*  214 */         cool = 90;
/*  215 */         speed = 0;
/*  216 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer());
/*      */         break;
/*      */       
/*      */       case 80001677:
/*  220 */         attackimg = 4;
/*  221 */         imgafter = 1920;
/*  222 */         delay = 0;
/*  223 */         delay2 = 1256;
/*  224 */         speed = 0;
/*      */         break;
/*      */       
/*      */       case 80001657:
/*  228 */         attackimg = Randomizer.rand(1137, 1138);
/*  229 */         imgafter = 366;
/*  230 */         delay = 330;
/*  231 */         speed = 2;
/*  232 */         range = 367;
/*  233 */         if (left != 1) {
/*  234 */           oldpos.x += 25;
/*  235 */           newpos.x += 25;
/*      */         } else {
/*  237 */           oldpos.x -= 25;
/*  238 */           newpos.x -= 25;
/*      */         } 
/*  240 */         oldpos.y -= 27;
/*  241 */         newpos.y -= 27;
/*      */         break;
/*      */       
/*      */       case 80001675:
/*  245 */         attackimg = Randomizer.rand(1137, 1138);
/*  246 */         imgafter = 366;
/*  247 */         delay = 330;
/*  248 */         speed = 2;
/*  249 */         range = 367;
/*  250 */         if (left != 1) {
/*  251 */           oldpos.x += 25;
/*  252 */           newpos.x += 25;
/*      */         } else {
/*  254 */           oldpos.x -= 25;
/*  255 */           newpos.x -= 25;
/*      */         } 
/*  257 */         oldpos.y -= 27;
/*  258 */         newpos.y -= 27;
/*      */         break;
/*      */       
/*      */       case 80001658:
/*  262 */         attackimg = 1139;
/*  263 */         imgafter = 3690;
/*  264 */         delay = 0;
/*  265 */         speed = 1;
/*  266 */         cool = 10;
/*      */         break;
/*      */       
/*      */       case 80001659:
/*  270 */         attackimg = 1140;
/*  271 */         imgafter = 1717;
/*  272 */         delay = 0;
/*  273 */         speed = 2;
/*  274 */         range = 1202;
/*  275 */         if (left != 1) {
/*  276 */           oldpos.x += 25;
/*  277 */           newpos.x += 25;
/*      */         } else {
/*  279 */           oldpos.x -= 25;
/*  280 */           newpos.x -= 25;
/*      */         } 
/*  282 */         oldpos.y -= 27;
/*  283 */         newpos.y -= 27;
/*  284 */         cool = 12;
/*      */         break;
/*      */       
/*      */       case 80001660:
/*  288 */         attackimg = 1141;
/*  289 */         imgafter = 10450;
/*  290 */         delay = 0;
/*  291 */         speed = 1;
/*  292 */         cool = 60;
/*      */         break;
/*      */       
/*      */       case 80001661:
/*  296 */         attackimg = 1150;
/*  297 */         imgafter = 900;
/*  298 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80001662:
/*  302 */         attackimg = 1151;
/*  303 */         imgafter = 990;
/*  304 */         delay = 0;
/*  305 */         speed = 1;
/*  306 */         cool = 6;
/*      */         break;
/*      */       
/*      */       case 80001663:
/*  310 */         attackimg = 1151;
/*  311 */         imgafter = 6000;
/*  312 */         delay = 0;
/*  313 */         speed = 1;
/*  314 */         cool = 12;
/*      */         break;
/*      */       
/*      */       case 80001664:
/*  318 */         attackimg = 1151;
/*  319 */         imgafter = 3806;
/*  320 */         delay = 0;
/*  321 */         range = 190;
/*  322 */         speed = 2;
/*  323 */         cool = 10;
/*  324 */         if (left != 1) {
/*  325 */           oldpos.x += 80;
/*  326 */           newpos.x += 80;
/*      */         } else {
/*  328 */           oldpos.x -= 80;
/*  329 */           newpos.x -= 80;
/*      */         } 
/*  331 */         oldpos.y -= 27;
/*  332 */         newpos.y -= 27;
/*      */         break;
/*      */       
/*      */       case 80001665:
/*  336 */         attackimg = 1152;
/*  337 */         imgafter = 1980;
/*  338 */         delay = 0;
/*  339 */         range = 190;
/*  340 */         speed = 1;
/*  341 */         cool = 60;
/*      */         break;
/*      */       
/*      */       case 80001666:
/*  345 */         attackimg = Randomizer.rand(1161, 1162);
/*  346 */         imgafter = 900;
/*  347 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80001667:
/*  351 */         attackimg = 1163;
/*  352 */         delay = 0;
/*  353 */         imgafter = 690;
/*  354 */         speed = 1;
/*  355 */         cool = 5;
/*      */         break;
/*      */       
/*      */       case 80001668:
/*  359 */         attackimg = 1164;
/*  360 */         delay = 0;
/*  361 */         imgafter = 3500;
/*  362 */         speed = 0;
/*  363 */         cool = 10;
/*  364 */         c.getPlayer().cancelAllBuffs();
/*  365 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer(), 4000);
/*      */         break;
/*      */       
/*      */       case 80001669:
/*  369 */         attackimg = 1165;
/*  370 */         delay = 0;
/*  371 */         imgafter = 1000;
/*  372 */         speed = 1;
/*  373 */         cool = 7;
/*      */         break;
/*      */       
/*      */       case 80001679:
/*  377 */         attackimg = 1165;
/*  378 */         delay = 0;
/*  379 */         delay2 = 1835;
/*  380 */         imgafter = 540;
/*  381 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80001670:
/*  385 */         attackimg = 1161;
/*  386 */         delay = 0;
/*  387 */         imgafter = 10000;
/*  388 */         speed = 0;
/*  389 */         cool = 50;
/*      */         break;
/*      */       
/*      */       case 80001732:
/*  393 */         attackimg = 1174;
/*  394 */         range = 366;
/*  395 */         speed = 2;
/*  396 */         oldpos.y -= 27;
/*  397 */         newpos.y -= 27;
/*  398 */         c.getPlayer().setSkillCustomInfo(skillid, 2, 0);
/*      */         break;
/*      */       
/*      */       case 80001733:
/*  402 */         attackimg = 4;
/*  403 */         imgafter = 4000;
/*  404 */         delay = 0;
/*  405 */         speed = 0;
/*  406 */         cool = 10;
/*  407 */         oldpos.y -= 27;
/*  408 */         newpos.y -= 27;
/*  409 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer(), 4000);
/*  410 */         c.getPlayer().getBattleGroundChr().setAttackSpeed(c.getPlayer().getBattleGroundChr().getAttackSpeed() - 60);
/*  411 */         c.getPlayer().getBattleGroundChr().setTeam(2);
/*  412 */         c.getPlayer().getMap().broadcastMessage(c.getPlayer(), BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), GameConstants.BattleGroundJobType(c.getPlayer().getBattleGroundChr())), false);
/*  413 */         c.getPlayer().getBattleGroundChr().setTeam(1);
/*  414 */         c.getPlayer().getClient().getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), GameConstants.BattleGroundJobType(c.getPlayer().getBattleGroundChr())));
/*      */         break;
/*      */       
/*      */       case 80001734:
/*  418 */         attackimg = 1175;
/*  419 */         imgafter = 5880;
/*  420 */         delay = 0;
/*  421 */         speed = 1;
/*  422 */         cool = 15;
/*  423 */         c.getPlayer().setSkillCustomInfo(skillid, 2, 0);
/*      */         break;
/*      */       
/*      */       case 80001735:
/*  427 */         attackimg = 1176;
/*  428 */         imgafter = 736;
/*  429 */         range = 480;
/*  430 */         delay = 0;
/*  431 */         speed = 2;
/*  432 */         cool = 15;
/*  433 */         stack = 5;
/*  434 */         oldpos.y -= 27;
/*  435 */         newpos.y -= 27;
/*      */         break;
/*      */       
/*      */       case 80001736:
/*  439 */         attackimg = 1177;
/*  440 */         imgafter = 5340;
/*  441 */         range = 1;
/*  442 */         delay = 0;
/*  443 */         speed = 4;
/*  444 */         cool = 45;
/*  445 */         c.getPlayer().setSkillCustomInfo(80001736, 0, imgafter);
/*      */         break;
/*      */       
/*      */       case 80001737:
/*  449 */         attackimg = Randomizer.rand(1186, 1187);
/*  450 */         imgafter = 300;
/*  451 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80001738:
/*  455 */         attackimg = 1188;
/*  456 */         imgafter = 540;
/*  457 */         delay = 0;
/*  458 */         speed = 1;
/*  459 */         cool = 3;
/*      */         break;
/*      */       
/*      */       case 80001739:
/*  463 */         attackimg = 1190;
/*  464 */         imgafter = 390;
/*  465 */         range = 300;
/*  466 */         delay = 0;
/*  467 */         cool = 10;
/*  468 */         speed = 1;
/*  469 */         c.getPlayer().setSkillCustomInfo(skillid, 0, 5);
/*      */         break;
/*      */       
/*      */       case 80001740:
/*  473 */         attackimg = 1190;
/*  474 */         imgafter = 10000;
/*  475 */         range = 300;
/*  476 */         delay = 0;
/*  477 */         cool = 17;
/*  478 */         speed = 0;
/*  479 */         c.getPlayer().setSkillCustomInfo(skillid, 3, 0);
/*  480 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer(), 10000);
/*      */         break;
/*      */       
/*      */       case 80001741:
/*  484 */         attackimg = 1191;
/*  485 */         stack = 3;
/*  486 */         imgafter = 8700;
/*  487 */         range = 300;
/*  488 */         delay = 0;
/*  489 */         speed = 3;
/*  490 */         c.getPlayer().removeSkillCustomInfo(skillid);
/*  491 */         c.getPlayer().getBattleGroundChr().setTeam(2);
/*  492 */         c.getPlayer().getMap().broadcastMessage(c.getPlayer(), BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), GameConstants.BattleGroundJobType(c.getPlayer().getBattleGroundChr())), false);
/*  493 */         c.getPlayer().getBattleGroundChr().setTeam(1);
/*  494 */         c.getPlayer().getClient().getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), GameConstants.BattleGroundJobType(c.getPlayer().getBattleGroundChr())));
/*      */         break;
/*      */       
/*      */       case 80002338:
/*  498 */         attackimg = Randomizer.rand(1213, 1214);
/*  499 */         imgafter = 300;
/*  500 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80002339:
/*  504 */         attackimg = 1216;
/*  505 */         cool = 7;
/*  506 */         delay = 0;
/*  507 */         range = 617;
/*  508 */         imgafter = 1916;
/*  509 */         speed = 2;
/*      */         break;
/*      */       
/*      */       case 80002340:
/*  513 */         attackimg = 4;
/*  514 */         delay = 0;
/*  515 */         delay2 = 370;
/*  516 */         range = 617;
/*  517 */         imgafter = 4090;
/*  518 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80002341:
/*  522 */         attackimg = 1217;
/*  523 */         delay = 0;
/*  524 */         imgafter = 15000;
/*  525 */         speed = 0;
/*  526 */         cool = 20;
/*  527 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer(), 7000);
/*      */         break;
/*      */       
/*      */       case 80002342:
/*  531 */         attackimg = 1218;
/*  532 */         delay = 0;
/*  533 */         imgafter = 1560;
/*  534 */         cool = 20;
/*  535 */         speed = 0;
/*  536 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer(), 2000);
/*  537 */         c.getPlayer().setSkillCustomInfo(skillid, 2, 0);
/*      */         break;
/*      */       
/*      */       case 80002344:
/*  541 */         attackimg = 1219;
/*  542 */         delay = 0;
/*  543 */         imgafter = 2560;
/*  544 */         cool = 70;
/*  545 */         speed = 0;
/*      */         break;
/*      */       
/*      */       case 80002670:
/*  549 */         attackimg = 4;
/*  550 */         imgafter = 3000000;
/*  551 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer());
/*      */         break;
/*      */       
/*      */       case 80002675:
/*  555 */         attackimg = 1228;
/*  556 */         imgafter = 583;
/*  557 */         range = 700;
/*  558 */         speed = 2;
/*  559 */         oldpos.y -= 27;
/*  560 */         newpos.y -= 27;
/*  561 */         while (c.getPlayer().getBuffedValue(80002670)) {
/*  562 */           c.getPlayer().cancelEffect(c.getPlayer().getBuffedEffect(80002670), false, -1);
/*      */         }
/*      */         break;
/*      */       
/*      */       case 80002680:
/*  567 */         attackimg = 1228;
/*  568 */         imgafter = 736;
/*  569 */         range = 367;
/*  570 */         speed = 2;
/*  571 */         stack = 3;
/*  572 */         oldpos.y -= 27;
/*  573 */         newpos.y -= 27;
/*      */         break;
/*      */       
/*      */       case 80002681:
/*  577 */         attackimg = 1228;
/*  578 */         imgafter = 389;
/*  579 */         range = 390;
/*  580 */         speed = 2;
/*  581 */         oldpos.y -= 27;
/*  582 */         newpos.y -= 27;
/*  583 */         while (c.getPlayer().getBuffedValue(80002670)) {
/*  584 */           c.getPlayer().cancelEffect(c.getPlayer().getBuffedEffect(80002670), false, -1);
/*      */         }
/*      */         break;
/*      */       
/*      */       case 80002671:
/*  589 */         attackimg = 1229;
/*  590 */         imgafter = 4000;
/*  591 */         delay = 0;
/*  592 */         speed = 0;
/*  593 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer());
/*  594 */         cool = 15;
/*      */         break;
/*      */       
/*      */       case 80002673:
/*  598 */         attackimg = 1231;
/*  599 */         imgafter = 1530;
/*  600 */         delay = 0;
/*  601 */         speed = 1;
/*  602 */         cool = 12;
/*  603 */         c.getPlayer().setSkillCustomInfo(skillid, 2, 0);
/*      */         break;
/*      */       
/*      */       case 80002672:
/*  607 */         attackimg = 1230;
/*  608 */         imgafter = 10000;
/*  609 */         delay = 0;
/*  610 */         speed = 0;
/*  611 */         cool = 20;
/*  612 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer());
/*      */         break;
/*      */       
/*      */       case 80002674:
/*  616 */         attackimg = 1234;
/*  617 */         imgafter = 600000;
/*  618 */         delay = 0;
/*  619 */         speed = 0;
/*  620 */         cool = 50;
/*  621 */         SkillFactory.getSkill(skillid).getEffect(skilllv).applyTo(c.getPlayer());
/*      */         break;
/*      */       
/*      */       case 80002676:
/*      */       case 80002677:
/*      */       case 80002678:
/*  627 */         attackimg = 1232;
/*  628 */         imgafter = 600000;
/*  629 */         delay = 0;
/*  630 */         range = 850;
/*  631 */         delay2 = 978;
/*  632 */         speed = 2;
/*  633 */         oldpos.y -= 27;
/*  634 */         newpos.y -= 27;
/*  635 */         if (left != 1) {
/*  636 */           oldpos.x += 80;
/*  637 */           newpos.x += 80;
/*      */         } else {
/*  639 */           oldpos.x -= 80;
/*  640 */           newpos.x -= 80;
/*      */         } 
/*  642 */         while (c.getPlayer().getBuffedValue(80002674)) {
/*  643 */           c.getPlayer().cancelEffect(c.getPlayer().getBuffedEffect(80002674), false, -1);
/*      */         }
/*      */         break;
/*      */       
/*      */       case 80003001:
/*  648 */         attackimg = 1242;
/*  649 */         imgafter = 400;
/*  650 */         range = 400;
/*  651 */         speed = 2;
/*  652 */         oldpos.y -= 27;
/*  653 */         newpos.y -= 27;
/*      */         break;
/*      */       
/*      */       case 80003002:
/*  657 */         info = new ArrayList<>();
/*  658 */         attackimg = (moveing == 0) ? 4 : 1243;
/*  659 */         c.getPlayer().getBattleGroundChr().setJobType(12);
/*  660 */         c.getPlayer().getBattleGroundChr().setTeam(2);
/*  661 */         c.getPlayer().getMap().broadcastMessage(c.getPlayer(), BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), 24), false);
/*  662 */         c.getPlayer().getBattleGroundChr().setTeam(1);
/*  663 */         c.getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), 24));
/*      */         
/*  665 */         c.getPlayer().getBattleGroundChr().getSkillList().clear();
/*  666 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/*  667 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/*  668 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003001), Integer.valueOf(1)));
/*  669 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003002), Integer.valueOf(0)));
/*  670 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003003), Integer.valueOf(0)));
/*  671 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003004), Integer.valueOf(0)));
/*  672 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003005), Integer.valueOf(0)));
/*  673 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003006), Integer.valueOf(0)));
/*  674 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003007), Integer.valueOf(0)));
/*  675 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003008), Integer.valueOf(0)));
/*  676 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003009), Integer.valueOf(0)));
/*  677 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003010), Integer.valueOf(0)));
/*  678 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003011), Integer.valueOf(0)));
/*  679 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003012), Integer.valueOf(0)));
/*  680 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003013), Integer.valueOf(1)));
/*  681 */         info.add(new Triple(Integer.valueOf(80003006), Integer.valueOf(1), Integer.valueOf(0)));
/*  682 */         info.add(new Triple(Integer.valueOf(80003007), Integer.valueOf(1), Integer.valueOf(4000)));
/*  683 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 5) {
/*  684 */           info.add(new Triple(Integer.valueOf(80003008), Integer.valueOf(1), Integer.valueOf(15000)));
/*  685 */           info.add(new Triple(Integer.valueOf(80003003), Integer.valueOf(1), Integer.valueOf(15000)));
/*      */         } 
/*  687 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 7) {
/*  688 */           info.add(new Triple(Integer.valueOf(80003004), Integer.valueOf(1), Integer.valueOf(15000)));
/*  689 */           info.add(new Triple(Integer.valueOf(80003009), Integer.valueOf(1), Integer.valueOf(20000)));
/*  690 */           info.add(new Triple(Integer.valueOf(80003010), Integer.valueOf(1), Integer.valueOf(20000)));
/*      */         } 
/*  692 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 8) {
/*  693 */           info.add(new Triple(Integer.valueOf(80003005), Integer.valueOf((c.getPlayer().getBattleGroundChr().getLevel() >= 11) ? 2 : 1), Integer.valueOf(50000)));
/*  694 */           info.add(new Triple(Integer.valueOf(80003012), Integer.valueOf((c.getPlayer().getBattleGroundChr().getLevel() >= 11) ? 2 : 1), Integer.valueOf(50000)));
/*      */         } 
/*  696 */         c.getSession().writeAndFlush(BattleGroundPacket.AvaterSkill(c.getPlayer().getBattleGroundChr(), 24));
/*  697 */         c.getSession().writeAndFlush(BattleGroundPacket.SkillOnList(c.getPlayer().getBattleGroundChr(), 24, info));
/*  698 */         c.getSession().writeAndFlush(BattleGroundPacket.UpgradeMainSkill(c.getPlayer().getBattleGroundChr()));
/*  699 */         imgafter = 550;
/*  700 */         delay = 0;
/*  701 */         speed = 0;
/*  702 */         cool = 4;
/*  703 */         c.getSession().writeAndFlush(BattleGroundPacket.CoolDown(80003007, cool * 1000, 0));
/*  704 */         coollist = new ArrayList<>();
/*  705 */         coollist.add(new Pair(Integer.valueOf(80003003), Integer.valueOf(5)));
/*  706 */         coollist.add(new Pair(Integer.valueOf(80003004), Integer.valueOf(15)));
/*  707 */         coollist.add(new Pair(Integer.valueOf(80003008), Integer.valueOf(15)));
/*  708 */         coollist.add(new Pair(Integer.valueOf(80003009), Integer.valueOf(20)));
/*  709 */         coollist.add(new Pair(Integer.valueOf(80003012), Integer.valueOf(50)));
/*  710 */         coollist.add(new Pair(Integer.valueOf(80001678), Integer.valueOf(30)));
/*  711 */         for (Pair<Integer, Integer> cooll : coollist) {
/*  712 */           if (c.getPlayer().skillisCooling(((Integer)cooll.getLeft()).intValue())) {
/*  713 */             c.getSession().writeAndFlush(BattleGroundPacket.CoolDown(((Integer)cooll.getLeft()).intValue(), ((Integer)cooll.getRight()).intValue() * 1000, (int)((((Integer)cooll.getRight()).intValue() * 1000) - c.getPlayer().getCooldownLimit(((Integer)cooll.getLeft()).intValue()))));
/*  714 */             if (((Integer)cooll.getLeft()).intValue() == 80003012) {
/*  715 */               c.getSession().writeAndFlush(BattleGroundPacket.CoolDown(80003005, ((Integer)cooll.getRight()).intValue() * 1000, (int)((((Integer)cooll.getRight()).intValue() * 1000) - c.getPlayer().getCooldownLimit(((Integer)cooll.getLeft()).intValue()))));
/*      */             }
/*      */           } 
/*      */         } 
/*      */         break;
/*      */       
/*      */       case 80003003:
/*  722 */         attackimg = 1244;
/*  723 */         imgafter = 981;
/*  724 */         range = 380;
/*  725 */         delay = 0;
/*  726 */         speed = 2;
/*  727 */         cool = 5;
/*  728 */         c.getPlayer().addCooldown(skillid, System.currentTimeMillis(), (cool * 1000));
/*      */         break;
/*      */ 
/*      */       
/*      */       case 80003004:
/*  733 */         attackimg = 1245;
/*  734 */         imgafter = 1740;
/*  735 */         delay = 0;
/*  736 */         speed = 0;
/*  737 */         cool = 15;
/*  738 */         c.getPlayer().addCooldown(skillid, System.currentTimeMillis(), (cool * 1000));
/*      */         break;
/*      */       
/*      */       case 80003006:
/*  742 */         attackimg = 1254;
/*  743 */         imgafter = 488;
/*  744 */         range = 367;
/*  745 */         stack = 2;
/*  746 */         speed = 2;
/*  747 */         oldpos.y -= 27;
/*  748 */         newpos.y -= 27;
/*  749 */         if (left != 1) {
/*  750 */           oldpos.x += 80;
/*  751 */           newpos.x += 80; break;
/*      */         } 
/*  753 */         oldpos.x -= 80;
/*  754 */         newpos.x -= 80;
/*      */         break;
/*      */ 
/*      */       
/*      */       case 80003007:
/*  759 */         info = new ArrayList<>();
/*  760 */         attackimg = (moveing == 0) ? 4 : 1255;
/*  761 */         imgafter = 550;
/*  762 */         delay = 0;
/*  763 */         speed = 0;
/*  764 */         cool = 4;
/*  765 */         c.getPlayer().getBattleGroundChr().setJobType(11);
/*  766 */         c.getPlayer().getBattleGroundChr().setTeam(2);
/*  767 */         c.getPlayer().getMap().broadcastMessage(c.getPlayer(), BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), 23), false);
/*  768 */         c.getPlayer().getBattleGroundChr().setTeam(1);
/*  769 */         c.getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), 23));
/*  770 */         c.getPlayer().getBattleGroundChr().getSkillList().clear();
/*  771 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(2), Integer.valueOf(1), Integer.valueOf(1)));
/*  772 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80001678), Integer.valueOf(1)));
/*  773 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003001), Integer.valueOf(1)));
/*  774 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003002), Integer.valueOf(1)));
/*  775 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003003), Integer.valueOf(0)));
/*  776 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003004), Integer.valueOf(0)));
/*  777 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003005), Integer.valueOf(0)));
/*  778 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003006), Integer.valueOf(0)));
/*  779 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003007), Integer.valueOf(0)));
/*  780 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003008), Integer.valueOf(0)));
/*  781 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003009), Integer.valueOf(0)));
/*  782 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003010), Integer.valueOf(0)));
/*  783 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003011), Integer.valueOf(0)));
/*  784 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003012), Integer.valueOf(0)));
/*  785 */         c.getPlayer().getBattleGroundChr().getSkillList().add(new Triple(Integer.valueOf(0), Integer.valueOf(80003013), Integer.valueOf(1)));
/*  786 */         c.getSession().writeAndFlush(BattleGroundPacket.AvaterSkill(c.getPlayer().getBattleGroundChr(), 23));
/*  787 */         info.add(new Triple(Integer.valueOf(80003001), Integer.valueOf(1), Integer.valueOf(0)));
/*  788 */         info.add(new Triple(Integer.valueOf(80003002), Integer.valueOf(1), Integer.valueOf(4000)));
/*  789 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 5) {
/*  790 */           info.add(new Triple(Integer.valueOf(80003008), Integer.valueOf(1), Integer.valueOf(15000)));
/*  791 */           info.add(new Triple(Integer.valueOf(80003003), Integer.valueOf(1), Integer.valueOf(15000)));
/*      */         } 
/*  793 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 7) {
/*  794 */           info.add(new Triple(Integer.valueOf(80003004), Integer.valueOf(1), Integer.valueOf(15000)));
/*  795 */           info.add(new Triple(Integer.valueOf(80003009), Integer.valueOf(1), Integer.valueOf(20000)));
/*  796 */           info.add(new Triple(Integer.valueOf(80003010), Integer.valueOf(1), Integer.valueOf(20000)));
/*      */         } 
/*  798 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 8) {
/*  799 */           info.add(new Triple(Integer.valueOf(80003005), Integer.valueOf((c.getPlayer().getBattleGroundChr().getLevel() >= 11) ? 2 : 1), Integer.valueOf(50000)));
/*  800 */           info.add(new Triple(Integer.valueOf(80003012), Integer.valueOf((c.getPlayer().getBattleGroundChr().getLevel() >= 11) ? 2 : 1), Integer.valueOf(50000)));
/*      */         } 
/*  802 */         c.getSession().writeAndFlush(BattleGroundPacket.SkillOnList(c.getPlayer().getBattleGroundChr(), 23, info));
/*  803 */         c.getSession().writeAndFlush(BattleGroundPacket.UpgradeMainSkill(c.getPlayer().getBattleGroundChr()));
/*  804 */         c.getSession().writeAndFlush(BattleGroundPacket.CoolDown(80003002, cool * 1000, 0));
/*  805 */         coollist = new ArrayList<>();
/*  806 */         coollist.add(new Pair(Integer.valueOf(80003003), Integer.valueOf(5)));
/*  807 */         coollist.add(new Pair(Integer.valueOf(80003004), Integer.valueOf(15)));
/*  808 */         coollist.add(new Pair(Integer.valueOf(80003008), Integer.valueOf(15)));
/*  809 */         coollist.add(new Pair(Integer.valueOf(80003009), Integer.valueOf(20)));
/*  810 */         coollist.add(new Pair(Integer.valueOf(80003012), Integer.valueOf(50)));
/*  811 */         coollist.add(new Pair(Integer.valueOf(80001678), Integer.valueOf(30)));
/*  812 */         for (Pair<Integer, Integer> cooll : coollist) {
/*  813 */           if (c.getPlayer().skillisCooling(((Integer)cooll.getLeft()).intValue())) {
/*  814 */             c.getSession().writeAndFlush(BattleGroundPacket.CoolDown(((Integer)cooll.getLeft()).intValue(), ((Integer)cooll.getRight()).intValue() * 1000, (int)((((Integer)cooll.getRight()).intValue() * 1000) - c.getPlayer().getCooldownLimit(((Integer)cooll.getLeft()).intValue()))));
/*  815 */             if (((Integer)cooll.getLeft()).intValue() == 80003012) {
/*  816 */               c.getSession().writeAndFlush(BattleGroundPacket.CoolDown(80003005, ((Integer)cooll.getRight()).intValue() * 1000, (int)((((Integer)cooll.getRight()).intValue() * 1000) - c.getPlayer().getCooldownLimit(((Integer)cooll.getLeft()).intValue()))));
/*      */             }
/*      */           } 
/*      */         } 
/*      */         break;
/*      */       
/*      */       case 80003008:
/*  823 */         attackimg = 1256;
/*  824 */         imgafter = 1200;
/*  825 */         delay = 0;
/*  826 */         speed = 1;
/*  827 */         cool = 15;
/*  828 */         c.getPlayer().addCooldown(skillid, System.currentTimeMillis(), (cool * 1000));
/*      */         break;
/*      */       
/*      */       case 80003009:
/*  832 */         attackimg = 1257;
/*  833 */         imgafter = 2800;
/*  834 */         delay = 0;
/*  835 */         speed = 0;
/*  836 */         cool = 20;
/*  837 */         c.getPlayer().addCooldown(skillid, System.currentTimeMillis(), (cool * 1000));
/*      */         break;
/*      */       
/*      */       case 80003010:
/*  841 */         attackimg = 1258;
/*  842 */         imgafter = 499;
/*  843 */         delay = 0;
/*  844 */         range = 600;
/*  845 */         delay2 = 433;
/*  846 */         speed = 2;
/*  847 */         oldpos.y -= 27;
/*  848 */         newpos.y -= 27;
/*  849 */         if (left != 1) {
/*  850 */           oldpos.x += 80;
/*  851 */           newpos.x += 80; break;
/*      */         } 
/*  853 */         oldpos.x -= 80;
/*  854 */         newpos.x -= 80;
/*      */         break;
/*      */ 
/*      */       
/*      */       case 80003011:
/*  859 */         attackimg = 1259;
/*  860 */         imgafter = 90;
/*  861 */         delay = 0;
/*  862 */         delay2 = 708;
/*  863 */         speed = 0;
/*      */         break;
/*      */       
/*      */       case 80003005:
/*      */       case 80003012:
/*  868 */         attackimg = 1260;
/*  869 */         imgafter = 4270;
/*  870 */         delay = 0;
/*  871 */         speed = 1;
/*  872 */         cool = 50;
/*  873 */         c.getPlayer().addCooldown(80003012, System.currentTimeMillis(), (cool * 1000));
/*      */         break;
/*      */       
/*      */       case 80003013:
/*  877 */         attackimg = 4;
/*  878 */         delay = 0;
/*  879 */         imgafter = 1740;
/*  880 */         speed = 1;
/*      */         break;
/*      */       
/*      */       case 80001678:
/*  884 */         cooltime = BattleGroundGameHandler.isEndOfGame() ? 60 : 30;
/*  885 */         attackimg = 4;
/*  886 */         delay = 0;
/*      */         
/*  888 */         cool = cooltime;
/*  889 */         imgafter = 1000;
/*  890 */         speed = 0;
/*  891 */         c.getPlayer().addCooldown(skillid, System.currentTimeMillis(), (cooltime * 1000));
/*      */         break;
/*      */     } 
/*      */     
/*  895 */     if (attackimg > 1000) {
/*  896 */       attackimg += 4;
/*      */     }
/*  898 */     Map<MapleStat, Long> hpmpupdate = new EnumMap<>(MapleStat.class);
/*  899 */     c.getPlayer().setSkillCustomInfo(156789, c.getPlayer().getSkillCustomValue0(156789) + 1, 0);
/*  900 */     if (stack > 0) {
/*  901 */       c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.AttackSkillStack(c.getPlayer(), oldpos, newpos, skillid, attackimg, delay, delay2, imgafter, speed, left, range, stack));
/*      */     } else {
/*  903 */       c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.AttackSkill(c.getPlayer(), oldpos, newpos, skillid, attackimg, delay, delay2, imgafter, speed, left, range, mob, moveing));
/*      */     } 
/*  905 */     c.getSession().writeAndFlush(BattleGroundPacket.CoolDown(skillid, cool * 1000, 0));
/*  906 */     long mpchange = SkillFactory.getSkill(skillid).getEffect(1).getMPCon();
/*  907 */     if (mpchange > 0L) {
/*  908 */       c.getPlayer().getBattleGroundChr().setMp((int)(c.getPlayer().getBattleGroundChr().getMp() - mpchange));
/*  909 */       hpmpupdate.put(MapleStat.MP, Long.valueOf(c.getPlayer().getBattleGroundChr().getMp()));
/*  910 */       if (hpmpupdate.size() > 0) {
/*  911 */         c.getSession().writeAndFlush(CWvsContext.updatePlayerStats(hpmpupdate, false, c.getPlayer()));
/*      */       }
/*      */     } 
/*  914 */     c.getSession().writeAndFlush(CWvsContext.enableActions(c.getPlayer()));
/*      */   }
/*      */   
/*      */   public static void AttackRefresh(LittleEndianAccessor slea, MapleClient c) {
/*  918 */     int stype = slea.readInt();
/*  919 */     if (stype == 1) {
/*  920 */       int count = slea.readInt();
/*  921 */       int type = slea.readInt();
/*  922 */       c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.AttackRefresh(c.getPlayer(), stype, count, type, new int[] { 0 }));
/*  923 */     } else if (stype == 2) {
/*  924 */       int unk1 = slea.readInt();
/*  925 */       int unk2 = slea.readInt();
/*  926 */       int unk3 = slea.readInt();
/*  927 */       int unk4 = slea.readInt();
/*  928 */       c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.AttackRefresh(c.getPlayer(), stype, unk1, unk2, new int[] { unk3, unk4 }));
/*      */     } 
/*      */   }
/*      */   
/*      */   public static void MoveAttack(LittleEndianAccessor slea, MapleClient c) {
/*  933 */     int chrid = slea.readInt();
/*  934 */     int attackcount = slea.readInt();
/*  935 */     int skillid = slea.readInt();
/*  936 */     slea.skip(1);
/*  937 */     slea.skip(1);
/*  938 */     slea.skip(1);
/*  939 */     slea.skip(1);
/*  940 */     slea.skip(4);
/*  941 */     Point pos = slea.readIntPos();
/*  942 */     Point pos1 = slea.readIntPos();
/*  943 */     Point pos2 = slea.readIntPos();
/*  944 */     c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.MoveAttack(c.getPlayer(), pos, pos1, attackcount));
/*      */   }
/*      */ 
/*      */   
/*      */   public static void AttackDamage(LittleEndianAccessor slea, MapleClient c) {
/*  949 */     List<MapleBattleGroundMobInfo> minfo = new ArrayList<>();
/*  950 */     MapleMonster monster = null;
/*  951 */     int damage = 0, mobcount = 0, peoplecount = 0;
/*  952 */     MapleStatEffect effect = null;
/*  953 */     int attackcount = slea.readInt();
/*  954 */     int skillid = slea.readInt();
/*  955 */     int unk = slea.readByte();
/*  956 */     int unk1 = slea.readShort();
/*  957 */     int unk2 = slea.readInt();
/*  958 */     int unk3 = slea.readByte();
/*  959 */     Point pos1 = slea.readIntPos();
/*  960 */     Point pos2 = slea.readIntPos();
/*  961 */     Point pos3 = slea.readIntPos();
/*  962 */     Point pos4 = slea.readIntPos();
/*  963 */     Point pos5 = slea.readIntPos();
/*  964 */     int unk4 = slea.readInt();
/*  965 */     int unk5 = slea.readInt();
/*  966 */     int unk6 = slea.readInt();
/*  967 */     int unk7 = slea.readInt();
/*  968 */     int unk8 = slea.readInt();
/*  969 */     int unk9 = slea.readInt();
/*  970 */     int unk10 = slea.readInt();
/*  971 */     int unk11 = slea.readInt();
/*  972 */     int unk12 = slea.readInt();
/*  973 */     peoplecount = slea.readInt();
/*  974 */     int skilllv = 1;
/*  975 */     switch (skillid) {
/*      */       case 80001656:
/*      */       case 80001660:
/*      */       case 80001665:
/*      */       case 80001670:
/*      */       case 80001736:
/*      */       case 80002344:
/*      */       case 80003005:
/*      */       case 80003012:
/*  984 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 11) {
/*  985 */           skilllv = 2;
/*      */         }
/*      */         break;
/*      */       
/*      */       case 80001738:
/*  990 */         c.getPlayer().setSkillCustomInfo(skillid, c.getPlayer().getSkillCustomValue0(skillid) + 1, 0);
/*  991 */         if (c.getPlayer().getSkillCustomValue0(skillid) >= 3L) {
/*  992 */           c.getPlayer().removeSkillCustomInfo(skillid);
/*      */           
/*  994 */           Item toDrop = new Item((c.getPlayer().getBattleGroundChr().getTeam() == 1) ? 4001849 : 4001847, (short)0, (short)1, 0);
/*  995 */           c.getPlayer().getMap().spawnItemDrop((MapleMapObject)c.getPlayer(), c.getPlayer(), toDrop, new Point((c.getPlayer().getTruePosition()).x, (c.getPlayer().getTruePosition()).y), true, false);
/*      */         } 
/*      */         break;
/*      */     } 
/*      */     
/*      */     int i;
/* 1001 */     for (i = 0; i < peoplecount; i++) {
/* 1002 */       effect = SkillFactory.getSkill(skillid).getEffect(skilllv);
/* 1003 */       int basedam = Randomizer.rand(c.getPlayer().getBattleGroundChr().getMindam(), c.getPlayer().getBattleGroundChr().getMaxdam());
/* 1004 */       int damper = effect.getDamage();
/* 1005 */       if (skillid == 80001732) {
/* 1006 */         if (c.getPlayer().getSkillCustomValue0(skillid) == 1L) {
/* 1007 */           damper = 50;
/*      */         }
/* 1009 */         c.getPlayer().setSkillCustomInfo(skillid, c.getPlayer().getSkillCustomValue0(skillid) - 1, 0);
/* 1010 */       } else if (skillid == 80001734) {
/* 1011 */         if (c.getPlayer().getSkillCustomValue0(skillid) == 2L) {
/* 1012 */           damper = 250;
/*      */         }
/* 1014 */         c.getPlayer().setSkillCustomInfo(skillid, c.getPlayer().getSkillCustomValue0(skillid) - 1, 0);
/* 1015 */       } else if (skillid == 80002342) {
/* 1016 */         if (c.getPlayer().getSkillCustomValue0(skillid) == 2L) {
/* 1017 */           damper = 150;
/*      */         } else {
/* 1019 */           while (c.getPlayer().getBuffedValue(skillid)) {
/* 1020 */             c.getPlayer().cancelEffect(c.getPlayer().getBuffedEffect(skillid), false, -1);
/*      */           }
/* 1022 */           damper = 400;
/*      */         } 
/* 1024 */         c.getPlayer().setSkillCustomInfo(skillid, c.getPlayer().getSkillCustomValue0(skillid) - 1, 0);
/*      */       } 
/* 1026 */       damage = basedam * damper / 100;
/* 1027 */       boolean cri = false;
/* 1028 */       if (Randomizer.isSuccess(c.getPlayer().getBattleGroundChr().getCritical())) {
/* 1029 */         damage *= 2;
/* 1030 */         cri = true;
/*      */       } 
/* 1032 */       if (c.getPlayer().getBuffedValue(80001651)) {
/* 1033 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 11) {
/* 1034 */           damage *= 2;
/*      */         } else {
/* 1036 */           damage = (int)(damage * 1.5D);
/*      */         } 
/*      */       }
/* 1039 */       if (skillid == 80001663) {
/* 1040 */         damage = -150;
/* 1041 */       } else if (skillid == 80001656) {
/* 1042 */         if (c.getPlayer().getBattleGroundChr().getLevel() >= 11) {
/* 1043 */           damage = -1000;
/*      */         } else {
/* 1045 */           damage = -700;
/*      */         } 
/* 1047 */       } else if (skillid == 80001678) {
/* 1048 */         damage = -(c.getPlayer().getBattleGroundChr().getMaxHp() / 100 * 50);
/*      */       } 
/* 1050 */       slea.skip(27);
/* 1051 */       int skid = slea.readInt();
/* 1052 */       slea.readByte();
/* 1053 */       int cid = slea.readInt();
/* 1054 */       int oid = slea.readInt();
/* 1055 */       MapleBattleGroundCharacter chred = null;
/* 1056 */       for (MapleCharacter chr : c.getPlayer().getMap().getAllChracater()) {
/* 1057 */         if (oid == chr.getId()) {
/* 1058 */           chred = chr.getBattleGroundChr();
/*      */           break;
/*      */         } 
/*      */       } 
/* 1062 */       if (chred != null) {
/* 1063 */         minfo.clear();
/* 1064 */         if (c.getPlayer().getBattleGroundChr().getJobType() == 6 && 
/* 1065 */           chred.getChr().getBuffedValue(80001732)) {
/* 1066 */           damage += damage / 100 * 10;
/*      */         }
/*      */         
/* 1069 */         if (chred.getChr().getBuffedValue(80001655) && chred.getChr().getSkillCustomValue0(80001655) > 0L && damage > 0) {
/* 1070 */           int shield = (int)chred.getChr().getSkillCustomValue0(80001655);
/* 1071 */           if (shield - damage > 0) {
/* 1072 */             chred.getChr().setSkillCustomInfo(80001655, chred.getChr().getSkillCustomValue0(80001655) - damage, 0);
/* 1073 */             damage = 1;
/*      */           } else {
/* 1075 */             damage -= shield;
/* 1076 */             chred.getChr().removeSkillCustomInfo(80001655);
/* 1077 */             while (chred.getChr().getBuffedValue(80001655)) {
/* 1078 */               chred.getChr().cancelEffect(chred.getChr().getBuffedEffect(80001655), false, -1);
/*      */             }
/*      */           } 
/*      */         } 
/* 1082 */         if (chred.getChr().getBuffedValue(80001654)) {
/* 1083 */           damage -= 100;
/* 1084 */         } else if (chred.getChr().getBuffedValue(80001733)) {
/* 1085 */           damage -= damage / 100 * 30;
/* 1086 */         } else if (chred.getChr().getBuffedValue(80002672)) {
/* 1087 */           if (damage >= chred.getMaxHp() / 100 * 10) {
/* 1088 */             damage = chred.getMaxHp() / 100 * 10;
/*      */           }
/* 1090 */           int bandam = damage / 100 * 40;
/* 1091 */           c.getPlayer().getMap().broadcastMessage(CField.playerDamaged(c.getPlayer().getId(), bandam));
/* 1092 */           c.getPlayer().getBattleGroundChr().setHp(c.getPlayer().getBattleGroundChr().getHp() - bandam);
/* 1093 */           Map<MapleStat, Long> statup = new EnumMap<>(MapleStat.class);
/* 1094 */           statup.put(MapleStat.HP, Long.valueOf(c.getPlayer().getBattleGroundChr().getHp()));
/* 1095 */           c.getPlayer().getMap().broadcastMessage(CField.updatePartyMemberHP(c.getPlayer().getId(), c.getPlayer().getBattleGroundChr().getHp(), c.getPlayer().getBattleGroundChr().getMaxHp()));
/* 1096 */           c.getSession().writeAndFlush(CWvsContext.updatePlayerStats(statup, false, c.getPlayer()));
/* 1097 */           if (c.getPlayer().getBattleGroundChr().getHp() <= 0 && c.getPlayer().getBattleGroundChr().isAlive()) {
/* 1098 */             c.getPlayer().getBattleGroundChr().getChr().cancelAllBuffs();
/* 1099 */             c.getPlayer().getBattleGroundChr().getChr().cancelAllDebuffs();
/* 1100 */             c.getPlayer().getBattleGroundChr().setAlive(false);
/* 1101 */             chred.setKill(chred.getKill() + 1);
/* 1102 */             if (!BattleGroundGameHandler.isEndOfGame()) {
/* 1103 */               chred.setMoney(chred.getMoney() + 500);
/*      */             } else {
/* 1105 */               c.getSession().writeAndFlush(CField.environmentChange("Map/Effect2.img/PvP/Lose", 16));
/* 1106 */               c.getSession().writeAndFlush(SLFCGPacket.playSE("Sound/MiniGame.img/BattlePvp/Lose"));
/* 1107 */               int alivechr = 0;
/* 1108 */               for (MapleCharacter chr : c.getPlayer().getMap().getAllChracater()) {
/* 1109 */                 if (chr.getBattleGroundChr() != null && chr.getId() != chred.getChr().getId() && 
/* 1110 */                   chr.getBattleGroundChr().isAlive()) {
/* 1111 */                   alivechr++;
/*      */                 }
/*      */               } 
/*      */               
/* 1115 */               int rank = alivechr + 2;
/* 1116 */               c.getPlayer().addKV("BattlePVPRank", rank + "");
/* 1117 */               c.getPlayer().addKV("BattlePVPLevel", c.getPlayer().getBattleGroundChr().getLevel() + "");
/* 1118 */               c.getPlayer().addKV("BattlePVPKill", c.getPlayer().getBattleGroundChr().getKill() + "");
/* 1119 */               if (alivechr == 0) {
/*      */                 
/* 1121 */                 chred.getChr().addKV("BattlePVPRank", "1");
/* 1122 */                 chred.getChr().addKV("BattlePVPLevel", chred.getLevel() + "");
/* 1123 */                 chred.getChr().addKV("BattlePVPKill", chred.getKill() + "");
/* 1124 */                 chred.getChr().getClient().getSession().writeAndFlush(CField.environmentChange("Map/Effect2.img/PvP/Win", 16));
/* 1125 */                 chred.getChr().getClient().getSession().writeAndFlush(SLFCGPacket.playSE("Sound/MiniGame.img/BattlePvp/Win"));
/* 1126 */                 Timer.EtcTimer.getInstance().schedule(() -> { 
                           c.getPlayer().getMap().broadcastMessage(CField.UIPacket.detailShowInfo("게임이 종료 되었습니다 잠시 후 퇴장맵으로 이동 됩니다.", 3, 20, 20)); 
                           c.getPlayer().getMap().broadcastMessage(CField.getClock(5)); 
                           }, 2000L);
/*      */ 
/*      */ 
/*      */                 
/* 1130 */                 Timer.EtcTimer.getInstance().schedule(() -> { 
                            c.getPlayer().getMap().resetFully(); 
                            for (MapleCharacter chr : c.getPlayer().getMap().getAllChracater()) { 
                                chr.dispel(); chr.warp(921174002); 
                            }  
                            MapleBattleGroundCharacter.bchr.clear(); }, 5000L);
/*      */               } 
/*      */             } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 1141 */             c.getPlayer().getBattleGroundChr().setDeath(c.getPlayer().getBattleGroundChr().getDeath() + 1);
/* 1142 */             chred.getChr().getClient().getSession().writeAndFlush(SLFCGPacket.PoloFrittoEffect(30, "Map/Effect2/PvP/Kill"));
/* 1143 */             c.getPlayer().getMap().broadcastMessage(CWvsContext.getTopMsg("[" + chred.getName() + "]님이 [" + c.getPlayer().getName() + "]님을 제압 했습니다."));
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 1150 */             MapleBattleGroundCharacter custom = chred;
/* 1151 */             custom.setTeam(2);
/* 1152 */             chred.getChr().getMap().broadcastMessage(chred.getChr(), BattleGroundPacket.UpdateAvater(custom, GameConstants.BattleGroundJobType(custom)), false);
/* 1153 */             chred.getChr().getClient().getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(chred, GameConstants.BattleGroundJobType(chred)));
/* 1154 */             custom = c.getPlayer().getBattleGroundChr();
/* 1155 */             custom.setTeam(2);
/* 1156 */             c.getPlayer().getMap().broadcastMessage(c.getPlayer(), BattleGroundPacket.UpdateAvater(custom, GameConstants.BattleGroundJobType(custom)), false);
/* 1157 */             c.getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), GameConstants.BattleGroundJobType(c.getPlayer().getBattleGroundChr())));
/*      */           } 
/*      */         } 
/*      */         
/* 1161 */         boolean debuff = true;
/* 1162 */         if (chred.getChr().getBuffedValue(80001740)) {
/* 1163 */           debuff = false;
/* 1164 */           int heal = chred.getMaxHp() / 100 * 10;
/* 1165 */           damage = -heal;
/* 1166 */           chred.getChr().setSkillCustomInfo(80001740, chred.getChr().getSkillCustomValue0(80001740) - 1, 0);
/* 1167 */           chred.getChr().getMap().broadcastMessage(BattleGroundPacket.TakeDamageEffect(chred, (int)chred.getChr().getSkillCustomValue0(80001740)));
/* 1168 */           if (chred.getChr().getSkillCustomValue0(80001740) <= 0L) {
/* 1169 */             chred.getChr().removeSkillCustomInfo(80001740);
/* 1170 */             while (chred.getChr().getBuffedValue(80001740)) {
/* 1171 */               chred.getChr().cancelEffect(chred.getChr().getBuffedEffect(80001740), false, -1);
/*      */             }
/*      */           } 
/*      */         } 
/* 1175 */         if (chred.getChr().getSkillCustomValue(80001736) != null)
/*      */         {
/* 1177 */           debuff = false;
/*      */         }
/* 1179 */         MapleBattleGroundMobInfo minfo2 = new MapleBattleGroundMobInfo(oid, cid, skid, damage, slea.readInt(), slea.readInt(), slea.readInt(), slea.readByte(), slea.readIntPos(), slea.readIntPos());
/* 1180 */         if (!BattleGroundGameHandler.isNotDamage()) {
/* 1181 */           if (cri) {
/* 1182 */             minfo2.setCritiCal(cri);
/*      */           }
/* 1184 */           minfo.add(minfo2);
/* 1185 */           c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.AttackDamage(c.getPlayer(), pos1, pos2, pos3, pos4, pos5, minfo, true, new int[] { attackcount, skillid, unk1, unk2, unk, unk4, unk5, unk6, unk7, unk8, unk9, unk10, unk11, unk12, 1 }));
/* 1186 */           chred.setHp(chred.getHp() - damage);
/* 1187 */           chred.getChr().getMap().broadcastMessage(CField.updatePartyMemberHP(chred.getChr().getId(), chred.getHp(), chred.getMaxHp()));
/* 1188 */           Map<MapleStat, Long> statup = new EnumMap<>(MapleStat.class);
/* 1189 */           statup.put(MapleStat.HP, Long.valueOf(chred.getHp()));
/* 1190 */           chred.getChr().getClient().getSession().writeAndFlush(CWvsContext.updatePlayerStats(statup, false, chred.getChr()));
/* 1191 */           if (chred.getHp() <= 0 && chred.isAlive()) {
/* 1192 */             if (skillid == 80002342) {
/* 1193 */               c.getSession().writeAndFlush(BattleGroundPacket.CoolDown(skillid, 0, 0));
/*      */             }
/* 1195 */             chred.getChr().cancelAllBuffs();
/* 1196 */             chred.getChr().cancelAllDebuffs();
/* 1197 */             chred.setAlive(false);
/* 1198 */             c.getPlayer().getBattleGroundChr().setKill(c.getPlayer().getBattleGroundChr().getKill() + 1);
/* 1199 */             chred.setDeath(chred.getDeath() + 1);
/* 1200 */             c.getSession().writeAndFlush(SLFCGPacket.PoloFrittoEffect(29, "Map/Effect2/PvP/Kill"));
/* 1201 */             c.getPlayer().getMap().broadcastMessage(CWvsContext.getTopMsg("[" + c.getPlayer().getName() + "]님이 [" + chred.getName() + "]님을 제압 했습니다."));
/* 1202 */             if (!BattleGroundGameHandler.isEndOfGame()) {
/* 1203 */               c.getPlayer().getBattleGroundChr().setMoney(c.getPlayer().getBattleGroundChr().getMoney() + 500);
/*      */             } else {
/* 1205 */               chred.getChr().getClient().getSession().writeAndFlush(CField.environmentChange("Map/Effect2.img/PvP/Lose", 16));
/* 1206 */               chred.getChr().getClient().getSession().writeAndFlush(SLFCGPacket.playSE("Sound/MiniGame.img/BattlePvp/Lose"));
/* 1207 */               int alivechr = 0;
/* 1208 */               for (MapleCharacter chr : c.getPlayer().getMap().getAllChracater()) {
/* 1209 */                 if (chr.getBattleGroundChr() != null && chr.getId() != c.getPlayer().getId() && 
/* 1210 */                   chr.getBattleGroundChr().isAlive()) {
/* 1211 */                   alivechr++;
/*      */                 }
/*      */               } 
/*      */               
/* 1215 */               int rank = alivechr + 2;
/* 1216 */               chred.getChr().addKV("BattlePVPRank", rank + "");
/* 1217 */               chred.getChr().addKV("BattlePVPLevel", chred.getLevel() + "");
/* 1218 */               chred.getChr().addKV("BattlePVPKill", chred.getKill() + "");
/* 1219 */               if (alivechr == 0) {
/*      */                 
/* 1221 */                 c.getPlayer().addKV("BattlePVPRank", "1");
/* 1222 */                 c.getPlayer().addKV("BattlePVPLevel", c.getPlayer().getBattleGroundChr().getLevel() + "");
/* 1223 */                 c.getPlayer().addKV("BattlePVPKill", c.getPlayer().getBattleGroundChr().getKill() + "");
/* 1224 */                 c.getSession().writeAndFlush(CField.environmentChange("Map/Effect2.img/PvP/Win", 16));
/* 1225 */                 c.getSession().writeAndFlush(SLFCGPacket.playSE("Sound/MiniGame.img/BattlePvp/Win"));
/* 1226 */                 c.getPlayer().getMap().broadcastMessage(SLFCGPacket.playSE("Sound/MiniGame.img/BattlePvp/Win"));
/* 1227 */                 Timer.EtcTimer.getInstance().schedule(() -> { 
     c.getPlayer().getMap().broadcastMessage(CField.UIPacket.detailShowInfo("게임이 종료 되었습니다 잠시 후 퇴장맵으로 이동 됩니다.", 3, 20, 20)); c.getPlayer().getMap().broadcastMessage(CField.getClock(5)); },2000L);
/*      */ 
/*      */ 
/*      */                 
/* 1231 */                 Timer.EtcTimer.getInstance().schedule(() -> { c.getPlayer().getMap().resetFully(); List<MapleBattleGroundCharacter> remove = new ArrayList<>(); for (MapleCharacter chr : c.getPlayer().getMap().getAllChracater()) { chr.dispel(); chr.warp(921174002); if (chr.getBattleGroundChr() != null) remove.add(chr.getBattleGroundChr());  }  MapleBattleGroundCharacter.bchr.clear(); },5000L);
/*      */               } 
/*      */             } 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */ 
/*      */             
/* 1247 */             MapleBattleGroundCharacter custom = chred;
/* 1248 */             custom.setTeam(2);
/* 1249 */             chred.getChr().getMap().broadcastMessage(chred.getChr(), BattleGroundPacket.UpdateAvater(custom, GameConstants.BattleGroundJobType(custom)), false);
/* 1250 */             custom.setTeam(1);
/* 1251 */             chred.getChr().getClient().getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(chred, GameConstants.BattleGroundJobType(chred)));
/* 1252 */             custom = c.getPlayer().getBattleGroundChr();
/* 1253 */             custom.setTeam(2);
/* 1254 */             c.getPlayer().getMap().broadcastMessage(c.getPlayer(), BattleGroundPacket.UpdateAvater(custom, GameConstants.BattleGroundJobType(custom)), false);
/* 1255 */             c.getPlayer().getBattleGroundChr().setTeam(1);
/* 1256 */             c.getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(c.getPlayer().getBattleGroundChr(), GameConstants.BattleGroundJobType(c.getPlayer().getBattleGroundChr())));
/*      */           }
/* 1258 */           else if (chred.getChr().getBuffedValue(MapleBuffStat.NoDebuff) == null && debuff) {
/* 1259 */             switch (skillid) {
/*      */               case 80001675:
/* 1261 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1262 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(c.getPlayer(), chred.getChr());
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80001676:
/* 1267 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1268 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 2000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80001658:
/* 1273 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1274 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 2000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80001649:
/* 1279 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1280 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 1000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80003003:
/* 1285 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1286 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 1500);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80003005:
/*      */               case 80003012:
/* 1292 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1293 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 3000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80003004:
/* 1298 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1299 */                   SkillFactory.getSkill(80003004).getEffect(1).applyTo(chred.getChr(), 10000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80001732:
/* 1304 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1305 */                   SkillFactory.getSkill(80001732).getEffect(1).applyTo(chred.getChr(), 1000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80001735:
/* 1310 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1311 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 2000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80002340:
/* 1316 */                 if (!chred.getChr().getBuffedValue(skillid)) {
/* 1317 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 1000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80002338:
/* 1322 */                 if (chred.getChr().getBuffedEffect(MapleBuffStat.Stun, skillid) == null) {
/* 1323 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 1000);
/*      */                 }
/*      */                 break;
/*      */               
/*      */               case 80002344:
/* 1328 */                 SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 2000);
/*      */                 break;
/*      */               
/*      */               case 80002673:
/* 1332 */                 if (c.getPlayer().getSkillCustomValue0(skillid) == 1L) {
/* 1333 */                   SkillFactory.getSkill(skillid).getEffect(1).applyTo(chred.getChr(), 3000);
/*      */                 }
/* 1335 */                 c.getPlayer().setSkillCustomInfo(skillid, c.getPlayer().getSkillCustomValue0(skillid) - 1, 0);
/*      */                 break;
/*      */             } 
/*      */ 
/*      */           
/*      */           } 
/*      */         } 
/*      */       } 
/*      */     } 
/* 1344 */     mobcount = slea.readInt();
/* 1345 */     for (i = 0; i < mobcount; i++) {
/* 1346 */       effect = SkillFactory.getSkill(skillid).getEffect(skilllv);
/* 1347 */       int basedam = Randomizer.rand(c.getPlayer().getBattleGroundChr().getMindam(), c.getPlayer().getBattleGroundChr().getMaxdam());
/* 1348 */       int damper = effect.getDamage();
/* 1349 */       if (skillid == 80001732) {
/* 1350 */         if (c.getPlayer().getSkillCustomValue0(skillid) == 1L) {
/* 1351 */           damper = 50;
/*      */         }
/* 1353 */         c.getPlayer().setSkillCustomInfo(skillid, c.getPlayer().getSkillCustomValue0(skillid) - 1, 0);
/* 1354 */       } else if (skillid == 80001734) {
/* 1355 */         if (c.getPlayer().getSkillCustomValue0(skillid) == 2L) {
/* 1356 */           damper = 250;
/*      */         }
/* 1358 */         c.getPlayer().setSkillCustomInfo(skillid, c.getPlayer().getSkillCustomValue0(skillid) - 1, 0);
/* 1359 */       } else if (skillid == 80002342) {
/* 1360 */         if (c.getPlayer().getSkillCustomValue0(skillid) == 2L) {
/* 1361 */           damper = 150;
/*      */         } else {
/* 1363 */           damper = 400;
/*      */         } 
/* 1365 */         c.getPlayer().setSkillCustomInfo(skillid, c.getPlayer().getSkillCustomValue0(skillid) - 1, 0);
/*      */       } 
/* 1367 */       damage = basedam * damper / 100;
/* 1368 */       boolean cri = false;
/* 1369 */       if (Randomizer.isSuccess(c.getPlayer().getBattleGroundChr().getCritical())) {
/* 1370 */         damage *= 2;
/* 1371 */         cri = true;
/*      */       } 
/*      */       
/* 1374 */       slea.skip(27);
/* 1375 */       int skid = slea.readInt();
/* 1376 */       slea.readByte();
/* 1377 */       int cid = slea.readInt();
/* 1378 */       int oid = slea.readInt();
/* 1379 */       monster = c.getPlayer().getMap().getMonsterByOid(oid);
/* 1380 */       if (monster != null) {
/* 1381 */         int dam; if (c.getPlayer().getBattleGroundChr().getJobType() == 6 && 
/* 1382 */           monster.isBuffed(80001732)) {
/* 1383 */           damage += damage / 100 * 10;
/*      */         }
/*      */         
/* 1386 */         if (monster.getStats().getLevel() > c.getPlayer().getBattleGroundChr().getLevel()) {
/* 1387 */           int chi = monster.getStats().getLevel() - c.getPlayer().getBattleGroundChr().getLevel();
/* 1388 */           int percent = (chi == 1) ? 85 : ((chi == 2) ? 70 : ((chi == 3) ? 50 : ((chi == 4) ? 40 : ((chi == 5) ? 25 : ((chi == 6) ? 10 : 0)))));
/* 1389 */           if (chi >= 7) {
/* 1390 */             damage = 1;
/*      */           } else {
/* 1392 */             damage = damage * percent / 100;
/*      */           } 
/*      */         } 
/* 1395 */         List<Pair<MonsterStatus, MonsterStatusEffect>> applys = new ArrayList<>();
/* 1396 */         switch (skillid) {
/*      */           case 80001649:
/* 1398 */             applys.add(new Pair(MonsterStatus.MS_Stun, new MonsterStatusEffect(skillid, effect.getSubTime(), 1L)));
/*      */             break;
/*      */           
/*      */           case 80001675:
/* 1402 */             dam = (c.getPlayer().getBattleGroundChr().getLevel() >= 11) ? 60 : ((c.getPlayer().getBattleGroundChr().getLevel() >= 9) ? 50 : ((c.getPlayer().getBattleGroundChr().getLevel() >= 7) ? 40 : ((c.getPlayer().getBattleGroundChr().getLevel() >= 5) ? 30 : 20)));
/* 1403 */             applys.add(new Pair(MonsterStatus.MS_Burned, new MonsterStatusEffect(skillid, 10000, dam)));
/*      */             break;
/*      */           
/*      */           case 80001676:
/* 1407 */             effect = SkillFactory.getSkill(80001655).getEffect(1);
/* 1408 */             applys.add(new Pair(MonsterStatus.MS_Stun, new MonsterStatusEffect(skillid, 2000, 1L)));
/*      */             break;
/*      */           
/*      */           case 80003003:
/* 1412 */             applys.add(new Pair(MonsterStatus.MS_Stun, new MonsterStatusEffect(skillid, 1500, 1L)));
/*      */             break;
/*      */           
/*      */           case 80003005:
/*      */           case 80003012:
/* 1417 */             applys.add(new Pair(MonsterStatus.MS_Stun, new MonsterStatusEffect(skillid, 3000, 1L)));
/*      */             break;
/*      */           
/*      */           case 80001658:
/* 1421 */             applys.add(new Pair(MonsterStatus.MS_Speed, new MonsterStatusEffect(skillid, 2000, -80L)));
/*      */             break;
/*      */           
/*      */           case 80002340:
/* 1425 */             if (monster.getBuff(skillid) == null) {
/* 1426 */               applys.add(new Pair(MonsterStatus.MS_Speed, new MonsterStatusEffect(skillid, 1000, -50L)));
/*      */             }
/*      */             break;
/*      */           
/*      */           case 80003004:
/* 1431 */             applys.add(new Pair(MonsterStatus.MS_Speed, new MonsterStatusEffect(skillid, 10000, -50L)));
/*      */             break;
/*      */           
/*      */           case 80001732:
/* 1435 */             applys.add(new Pair(MonsterStatus.MS_PvPHelenaMark, new MonsterStatusEffect(skillid, 4000, 10L)));
/*      */             break;
/*      */           
/*      */           case 80001735:
/* 1439 */             applys.add(new Pair(MonsterStatus.MS_Speed, new MonsterStatusEffect(skillid, 2000, -30L)));
/*      */             break;
/*      */           
/*      */           case 80002338:
/* 1443 */             if (monster.getBuff(MonsterStatus.MS_Stun) == null) {
/* 1444 */               if (monster.getBuff(MonsterStatus.MS_PVPRude_Stack) == null && monster.getCustomValue0(skillid) > 0L) {
/* 1445 */                 monster.removeCustomInfo(skillid);
/*      */               }
/*      */               
/* 1448 */               monster.setCustomInfo(skillid, (int)monster.getCustomValue0(skillid) + 1, 0);
/* 1449 */               if (monster.getCustomValue0(skillid) >= 3L) {
/* 1450 */                 monster.removeCustomInfo(skillid);
/* 1451 */                 monster.cancelSingleStatus(monster.getBuff(skillid));
/* 1452 */                 applys.add(new Pair(MonsterStatus.MS_Stun, new MonsterStatusEffect(skillid, 2000, 1L))); break;
/*      */               } 
/* 1454 */               applys.add(new Pair(MonsterStatus.MS_PVPRude_Stack, new MonsterStatusEffect(skillid, 6000, monster.getCustomValue0(skillid))));
/*      */             } 
/*      */             break;
/*      */         } 
/*      */ 
/*      */         
/* 1460 */         if (applys != null && effect != null) {
/* 1461 */           monster.applyStatus(c, (Map<MonsterStatus, MonsterStatusEffect>) applys, effect);
/*      */         }
/* 1463 */         minfo.clear();
/* 1464 */         MapleBattleGroundMobInfo minfo2 = new MapleBattleGroundMobInfo(oid, cid, skid, damage, slea.readInt(), slea.readInt(), slea.readInt(), slea.readByte(), slea.readIntPos(), slea.readIntPos());
/* 1465 */         if (cri) {
/* 1466 */           minfo2.setCritiCal(cri);
/*      */         }
/* 1468 */         minfo.add(minfo2);
/* 1469 */         c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.AttackDamage(c.getPlayer(), pos1, pos2, pos3, pos4, pos5, minfo, false, new int[] { attackcount, skillid, unk1, unk2, unk, unk4, unk5, unk6, unk7, unk8, unk9, unk10, unk11, unk12, 0 }));
/* 1470 */         c.getPlayer().checkMonsterAggro(monster);
/* 1471 */         monster.damage(c.getPlayer(), damage, true);
/*      */       } 
/*      */     } 
/* 1474 */     if (skillid == 80003001 || skillid == 80002339) {
/* 1475 */       c.getSession().writeAndFlush(BattleGroundPacket.BonusAttack(skillid));
/*      */     }
/*      */   }
/*      */   
/*      */   public static void TakeDamage(LittleEndianAccessor slea, MapleClient c) {
/* 1480 */     Map<MapleStat, Long> hpmpupdate = new EnumMap<>(MapleStat.class);
/* 1481 */     int randdam = 0;
/* 1482 */     int oid = slea.readInt();
/* 1483 */     slea.readInt();
/* 1484 */     int type = slea.readInt();
/* 1485 */     MapleMonster monster = c.getPlayer().getMap().getMonsterByOid(oid);
/* 1486 */     if (monster != null) {
/* 1487 */       randdam = Randomizer.rand(monster.getStats().getPhysicalAttack() - 30, monster.getStats().getPhysicalAttack());
/* 1488 */       if (c.getPlayer().getBuffedValue(80001655) && c.getPlayer().getSkillCustomValue0(80001655) > 0L) {
/* 1489 */         int shield = (int)c.getPlayer().getSkillCustomValue0(80001655);
/* 1490 */         if (shield - randdam > 0) {
/* 1491 */           c.getPlayer().setSkillCustomInfo(80001655, c.getPlayer().getSkillCustomValue0(80001655) - randdam, 0);
/* 1492 */           randdam = 1;
/*      */         } else {
/* 1494 */           randdam -= shield;
/* 1495 */           c.getPlayer().removeSkillCustomInfo(80001655);
/* 1496 */           while (c.getPlayer().getBuffedValue(80001655)) {
/* 1497 */             c.getPlayer().cancelEffect(c.getPlayer().getBuffedEffect(80001655), false, -1);
/*      */           }
/*      */         } 
/*      */       } 
/* 1501 */       if (c.getPlayer().getBuffedValue(80001654)) {
/* 1502 */         randdam -= 100;
/* 1503 */       } else if (c.getPlayer().getBuffedValue(80001733)) {
/* 1504 */         randdam -= randdam / 100 * 30;
/* 1505 */       } else if (c.getPlayer().getBuffedValue(80002672)) {
/* 1506 */         int bandam = randdam / 100 * 40;
/* 1507 */         c.getPlayer().getMap().broadcastMessage(MobPacket.damageMonster(oid, bandam, false));
/* 1508 */         monster.damage(c.getPlayer(), bandam, true);
/*      */       } 
/* 1510 */       if (randdam <= 0) {
/* 1511 */         randdam = 1;
/*      */       }
/* 1513 */       boolean healed = false;
/* 1514 */       if (c.getPlayer().getBuffedValue(80001740) && c.getPlayer().getSkillCustomValue0(80001740) > 0L) {
/* 1515 */         healed = true;
/* 1516 */         int heal = c.getPlayer().getBattleGroundChr().getMaxHp() / 100 * 10;
/* 1517 */         randdam = 1;
/* 1518 */         randdam = -heal;
/*      */       } 
/* 1520 */       c.getPlayer().getBattleGroundChr().setHp(c.getPlayer().getBattleGroundChr().getHp() - randdam);
/* 1521 */       hpmpupdate.put(MapleStat.HP, Long.valueOf(c.getPlayer().getBattleGroundChr().getHp()));
/* 1522 */       if (hpmpupdate.size() > 0) {
/* 1523 */         c.getSession().writeAndFlush(CWvsContext.updatePlayerStats(hpmpupdate, false, c.getPlayer()));
/*      */       }
/* 1525 */       c.getPlayer().getMap().broadcastMessage(CField.updatePartyMemberHP(c.getPlayer().getId(), c.getPlayer().getBattleGroundChr().getHp(), c.getPlayer().getBattleGroundChr().getMaxHp()));
/* 1526 */       if (c.getPlayer().getBuffedValue(80001740) && healed) {
/* 1527 */         int heal = c.getPlayer().getBattleGroundChr().getMaxHp() / 100 * 10;
/* 1528 */         randdam = 0;
/* 1529 */         c.getPlayer().setSkillCustomInfo(80001740, c.getPlayer().getSkillCustomValue0(80001740) - 1, 0);
/* 1530 */         c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.TakeDamage(c.getPlayer(), oid, randdam, type, heal));
/* 1531 */         c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.TakeDamageEffect(c.getPlayer().getBattleGroundChr(), (int)c.getPlayer().getSkillCustomValue0(80001740)));
/* 1532 */         if (c.getPlayer().getSkillCustomValue0(80001740) <= 0L) {
/* 1533 */           c.getPlayer().removeSkillCustomInfo(80001740);
/* 1534 */           while (c.getPlayer().getBuffedValue(80001740)) {
/* 1535 */             c.getPlayer().cancelEffect(c.getPlayer().getBuffedEffect(80001740), false, -1);
/*      */           }
/*      */         } 
/*      */       } else {
/* 1539 */         c.getPlayer().getMap().broadcastMessage(BattleGroundPacket.TakeDamage(c.getPlayer(), oid, randdam, type, 0));
/*      */       } 
/*      */     } 
/*      */   }
/*      */   
/*      */   public static void MainTime(LittleEndianAccessor slea, MapleClient c) {
/* 1545 */     if (c.getPlayer().getBattleGroundChr() != null && 
/* 1546 */       c.getPlayer().getBattleGroundChr().getHp() > 0) {

/* 1550 */       boolean heal = true;
/*      */ 
/* 1560 */       Map<MapleStat, Long> hpmpupdate = new EnumMap<>(MapleStat.class);
/*      */       
/* 1562 */       if (c.getPlayer().getBuffedValue(80002341)) {
/* 1563 */         c.getPlayer().getBattleGroundChr().setHp(c.getPlayer().getBattleGroundChr().getHp() + c.getPlayer().getBattleGroundChr().getMaxHp() / 100 * 3);
/*      */       }
/*      */       
/* 1566 */       if (c.getPlayer().getBuffedValue(80002671)) {
/* 1567 */         c.getPlayer().getBattleGroundChr().setHp(c.getPlayer().getBattleGroundChr().getHp() + c.getPlayer().getBattleGroundChr().getMaxHp() / 100 * 8);
/*      */       }
/*      */       
/* 1570 */       if (heal) {
/* 1571 */         if (c.getPlayer().getBattleGroundChr().getHp() < c.getPlayer().getBattleGroundChr().getMaxHp()) {
/* 1572 */           c.getPlayer().getBattleGroundChr().setHp(c.getPlayer().getBattleGroundChr().getHp() + c.getPlayer().getBattleGroundChr().getHpRegen());
/*      */         }
/* 1574 */         if (c.getPlayer().getBattleGroundChr().getMp() < c.getPlayer().getBattleGroundChr().getMaxMp()) {
/* 1575 */           c.getPlayer().getBattleGroundChr().setMp(c.getPlayer().getBattleGroundChr().getMp() + c.getPlayer().getBattleGroundChr().getMpRegen());
/*      */         }
/*      */         
/* 1578 */         if (c.getPlayer().getBattleGroundChr().getMp() > c.getPlayer().getBattleGroundChr().getMaxMp()) {
/* 1579 */           c.getPlayer().getBattleGroundChr().setMp(c.getPlayer().getBattleGroundChr().getMaxMp());
/*      */         }
/* 1581 */         if (c.getPlayer().getBattleGroundChr().getHp() > c.getPlayer().getBattleGroundChr().getMaxHp()) {
/* 1582 */           c.getPlayer().getBattleGroundChr().setHp(c.getPlayer().getBattleGroundChr().getMaxHp());
/*      */         }
/*      */       } 
/*      */       
/* 1586 */       hpmpupdate.put(MapleStat.HP, Long.valueOf(c.getPlayer().getBattleGroundChr().getHp()));
/* 1587 */       hpmpupdate.put(MapleStat.MP, Long.valueOf(c.getPlayer().getBattleGroundChr().getMp()));
/* 1588 */       if (hpmpupdate.size() > 0) {
/* 1589 */         c.getSession().writeAndFlush(CWvsContext.updatePlayerStats(hpmpupdate, false, c.getPlayer()));
/*      */       }
/* 1591 */       c.getPlayer().getMap().broadcastMessage(CField.updatePartyMemberHP(c.getPlayer().getId(), c.getPlayer().getBattleGroundChr().getHp(), c.getPlayer().getBattleGroundChr().getMaxHp()));
/*      */     } 
/*      */   }
/*      */ 
/*      */   
/*      */   public static void UpgradeSkill(LittleEndianAccessor slea, MapleClient c) {
/* 1597 */     MapleBattleGroundCharacter chr = c.getPlayer().getBattleGroundChr();
/* 1598 */     if (chr == null) {
/*      */       return;
/*      */     }
/* 1601 */     int skilllevel = 0, need = 0;
/* 1602 */     int slot = slea.readInt();
/* 1603 */     slea.skip(4);
/* 1604 */     slea.skip(4);
/* 1605 */     skilllevel = (slot == 0) ? chr.getAttackUp() : ((slot == 1) ? chr.getHpMpUp() : ((slot == 2) ? chr.getCriUp() : ((slot == 3) ? chr.getSpeedUp() : ((slot == 4) ? chr.getRegenUp() : 0))));
/* 1606 */     switch (skilllevel) {
/*      */       case 1:
/* 1608 */         need = 460;
/*      */         break;
/*      */       
/*      */       case 2:
/* 1612 */         need = 840;
/*      */         break;
/*      */       
/*      */       case 3:
/* 1616 */         need = 1360;
/*      */         break;
/*      */       
/*      */       case 4:
/* 1620 */         need = 1980;
/*      */         break;
/*      */       
/*      */       case 5:
/* 1624 */         need = 2800;
/*      */         break;
/*      */     } 
/*      */     
/* 1628 */     if (chr != null && chr.getHp() > 0 && need <= chr.getMoney()) {
/* 1629 */       if (slot == 0) {
/* 1630 */         int attackup = (chr.getAttackUp() == 1) ? 2 : ((chr.getAttackUp() == 2) ? 5 : ((chr.getAttackUp() == 3) ? 10 : ((chr.getAttackUp() == 4) ? 15 : ((chr.getAttackUp() == 5) ? 20 : 0))));
/* 1631 */         chr.setSkillMinDamUp(chr.getMindam() / 100 * attackup);
/* 1632 */         chr.setSkillMaxDamUp(chr.getMaxdam() / 100 * attackup);
/* 1633 */         chr.setAttackUp(chr.getAttackUp() + 1);
/* 1634 */       } else if (slot == 1) {
/* 1635 */         int hpmpup = (chr.getHpMpUp() == 1) ? 2 : ((chr.getHpMpUp() == 2) ? 7 : ((chr.getHpMpUp() == 3) ? 15 : ((chr.getHpMpUp() == 4) ? 20 : ((chr.getHpMpUp() == 5) ? 30 : 0))));
/* 1636 */         chr.setSkillHPUP(chr.getMaxHp() / 100 * hpmpup);
/* 1637 */         chr.setSkillMPUP(chr.getMaxMp() / 100 * hpmpup);
/* 1638 */         chr.setHpMpUp(chr.getHpMpUp() + 1);
/* 1639 */       } else if (slot == 2) {
/* 1640 */         int criup = (chr.getCriUp() == 1) ? 3 : ((chr.getCriUp() == 2) ? 6 : ((chr.getCriUp() == 3) ? 12 : ((chr.getCriUp() == 4) ? 18 : ((chr.getCriUp() == 5) ? 25 : 0))));
/* 1641 */         chr.setCritical(criup);
/* 1642 */         chr.setCriUp(chr.getCriUp() + 1);
/* 1643 */       } else if (slot == 3) {
/* 1644 */         int attackspeed = (chr.getSpeedUp() == 5) ? -20 : -10;
/* 1645 */         int speed = (chr.getSpeedUp() == 1) ? 6 : ((chr.getSpeedUp() == 2) ? 6 : ((chr.getSpeedUp() == 3) ? 12 : ((chr.getSpeedUp() == 4) ? 12 : ((chr.getSpeedUp() == 5) ? 12 : 0))));
/* 1646 */         int jump = (chr.getSpeedUp() == 1) ? 2 : ((chr.getSpeedUp() == 2) ? 4 : ((chr.getSpeedUp() == 3) ? 2 : ((chr.getSpeedUp() == 4) ? 4 : ((chr.getSpeedUp() == 5) ? 3 : 0))));
/* 1647 */         chr.setSpeedUp(chr.getSpeedUp() + 1);
/* 1648 */         chr.setAttackSpeed(chr.getAttackSpeed() + attackspeed);
/* 1649 */         chr.setSpeed(chr.getSpeed() + speed);
/* 1650 */         chr.setJump(chr.getJump() + jump);
/* 1651 */       } else if (slot == 4) {
/* 1652 */         int hpup = (chr.getHpMpUp() == 1) ? 14 : ((chr.getHpMpUp() == 2) ? 17 : ((chr.getHpMpUp() == 3) ? 20 : ((chr.getHpMpUp() == 4) ? 23 : ((chr.getHpMpUp() == 5) ? 25 : 0))));
/* 1653 */         int mpup = (chr.getHpMpUp() == 1) ? 6 : ((chr.getHpMpUp() == 2) ? 7 : ((chr.getHpMpUp() == 3) ? 8 : ((chr.getHpMpUp() == 4) ? 10 : ((chr.getHpMpUp() == 5) ? 11 : 0))));
/* 1654 */         chr.setRegenUp(chr.getRegenUp() + 1);
/* 1655 */         chr.setHpRegen(chr.getHpRegen() + hpup);
/* 1656 */         chr.setMpRegen(chr.getMpRegen() + mpup);
/*      */       } 
/* 1658 */       chr.setMoney(chr.getMoney() - need);
/* 1659 */       c.getSession().writeAndFlush(BattleGroundPacket.UpgradeMainSkill(chr));
/* 1660 */       chr.getChr().getMap().broadcastMessage(BattleGroundPacket.UpgradeSkillEffect(chr, slot * 100 + skilllevel + 1));
/* 1661 */       c.getSession().writeAndFlush(BattleGroundPacket.UpdateAvater(chr, GameConstants.BattleGroundJobType(chr)));
/*      */     } 
/*      */   }
/*      */   
/*      */   public static void Respawn(LittleEndianAccessor slea, MapleClient c) {
/* 1666 */     if (c.getPlayer().getBattleGroundChr() != null) {
/* 1667 */       Timer.MapTimer.getInstance().schedule(() -> {  },1000L);
/*      */     }
/*      */   }
/*      */ 
/*      */   
/*      */   public static void SelectAvater(LittleEndianAccessor slea, MapleClient c) {
/* 1680 */     MapleCharacter chr = c.getPlayer();
/* 1681 */     if (chr.getBattleGroundChr() == null) {
/* 1682 */       int type = slea.readInt();
/* 1683 */       String job = (type == 1) ? "만지" : ((type == 2) ? "마이크" : ((type == 3) ? "다크로드" : ((type == 4) ? "하인즈" : ((type == 5) ? "무공" : ((type == 6) ? "헬레나" : ((type == 7) ? "랑이" : ((type == 9) ? "류드" : ((type == 10) ? "웡키" : ((type == 11) ? "폴로&프리토" : "없음")))))))));
/* 1684 */       chr.setBattleGrondJobName(job);
/* 1685 */       MapleBattleGroundCharacter gchr = new MapleBattleGroundCharacter(chr, chr.getBattleGrondJobName());
/* 1686 */       gchr.setDeathcount(3);
/* 1687 */       chr.getMap().broadcastMessage(BattleGroundPacket.ChangeAvater(gchr, GameConstants.BattleGroundJobType(gchr)));
/* 1688 */       chr.getClient().getSession().writeAndFlush(BattleGroundPacket.UpgradeMainSkill(gchr));
/* 1689 */       chr.getClient().getSession().writeAndFlush(BattleGroundPacket.AvaterSkill(gchr, GameConstants.BattleGroundJobType(gchr)));
/* 1690 */       chr.getClient().getSession().writeAndFlush(BattleGroundPacket.SelectAvaterOther(chr, 1, 1));
/* 1691 */       MapleBattleGroundCharacter t = gchr;
/* 1692 */       t.setTeam(2);
/* 1693 */       chr.getMap().broadcastMessage(chr, BattleGroundPacket.ChangeAvater(t, GameConstants.BattleGroundJobType(t)), false);
/*      */       
/* 1695 */       chr.getClient().getSession().writeAndFlush(CField.ImageTalkNpc(9001153, 4000, "싱크로 완료. 몬스터와 보스 몬스터를 처치해 더욱 강해질 수 있습니다.\r\n게임시작 후 1분간은 #r무적#k 상태 입니다."));
/*      */     } 
/*      */   }
/*      */ }


/* Location:              C:\Users\Phellos\Desktop\크루엘라\Ozoh디컴.jar!\server\events\MapleBattleGround.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */