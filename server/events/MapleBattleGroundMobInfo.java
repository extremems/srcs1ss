/*    */ package server.events;
/*    */ 
/*    */ import java.awt.Point;
/*    */ 
/*    */ 
/*    */ public class MapleBattleGroundMobInfo
/*    */ {
/*    */   private int oid;
/*    */   private int cid;
/*    */   private int skillid;
/*    */   private int unk1;
/*    */   private int unk2;
/*    */   private int unk3;
/*    */   private int unk4;
/*    */   private int damage;
/*    */   private Point pos1;
/*    */   private Point pos2;
/*    */   private boolean cri = false;
/*    */   /*    */   
/*    */   public MapleBattleGroundMobInfo(int oid, int cid, int skillid, int damage, int unk1, int unk2, int unk3, int unk4, Point pos1, Point pos2) {
/* 21 */     this.oid = oid;
/* 22 */     this.cid = cid;
/* 23 */     this.skillid = skillid;
/* 24 */     this.damage = damage;
/* 25 */     this.unk1 = unk1;
/* 26 */     this.unk2 = unk2;
/* 27 */     this.unk3 = unk3;
/* 28 */     this.unk4 = unk4;
/* 29 */     this.pos1 = pos1;
/* 30 */     this.pos2 = pos2;
/*    */   }
/*    */   
/* 33 */   public int getOid() { return this.oid; }
/* 34 */   public int getCid() { return this.cid; }
/* 35 */   public int getSkillid() { return this.skillid; }
/* 36 */   public int getDamage() { return this.damage; }
/* 37 */   public int getUnk1() { return this.unk1; }
/* 38 */   public int getUnk2() { return this.unk2; }
/* 39 */   public int getUnk3() { return this.unk3; }
/* 40 */   public int getUnk4() { return this.unk4; }
/* 41 */   public Point getPos1() { return this.pos1; }
/* 42 */   public Point getPos2() { return this.pos1; }
/* 43 */   public boolean getCritiCal() { return this.cri; } public void setCritiCal(boolean cri) {
/* 44 */     this.cri = cri;
/*    */   }
/*    */ }


/* Location:              C:\Users\Phellos\Desktop\크루엘라\Ozoh디컴.jar!\server\events\MapleBattleGroundMobInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */