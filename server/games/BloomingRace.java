/*     */ package server.games;
/*     */ 
import client.MapleBuffStat;
/*     */ import client.MapleCharacter;
/*     */ import client.MapleBuffStatValueHolder;
/*     */ import client.skills.SkillFactory;
/*     */ import handling.channel.ChannelServer;
/*     */ import java.awt.Point;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.List;
/*     */ import server.Randomizer;
/*     */ import server.Timer;
/*     */ import server.life.MapleLifeFactory;
/*     */ import server.life.MapleMonster;
/*     */ import server.maps.MapleMap;
/*     */ import tools.Pair;
/*     */ import tools.packet.CField;
/*     */ import tools.packet.CWvsContext;
/*     */ import tools.packet.SLFCGPacket;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BloomingRace
/*     */ {
/*  33 */   private static List<MapleCharacter> Matchinglist = new ArrayList<>();
/*  34 */   private static List<MapleCharacter> Playerlist = new ArrayList<>();
/*  35 */   private static List<MapleCharacter> RankList = new ArrayList<>(); private static int Rank; private static int randPortal1;
/*     */   private static int randPortal2;
/*     */   private static int randPortal3;
/*     */   private static boolean start = false, already = false;
/*     */   
/*     */   public static void RaceIinviTation() {
/*  41 */     Matchinglist = new ArrayList<>();
/*  42 */     Playerlist = new ArrayList<>();
/*  43 */     RankList = new ArrayList<>();
/*  44 */     randPortal1 = 0;
/*  45 */     randPortal2 = 0;
/*  46 */     randPortal3 = 0;
/*  47 */     Rank = 0;
/*  48 */     start = false;
/*  49 */     already = false;
/*  50 */     for (ChannelServer cserv : ChannelServer.getAllInstances()) {
/*  51 */       for (MapleCharacter player : cserv.getPlayerStorage().getAllCharacters().values()) {
/*  52 */         if (player != null && player.getName() != null) {
/*  53 */           player.getClient().getSession().writeAndFlush(SLFCGPacket.EventMsgSend(100796, 993192500, 30, "BloomingRace"));
/*     */         }
/*     */       } 
/*     */     } 
/*  57 */     Timer.EventTimer.getInstance().schedule(() -> { already = true; RaceWarp(); }, 30000L);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void RaceAddChr(MapleCharacter chr) {
/*  65 */     Matchinglist.add(chr);
/*  66 */     chr.getClient().getSession().writeAndFlush(SLFCGPacket.ContentsWaiting(chr, 993192500, new int[] { 11, 2, 1, 25 }));
/*     */   }
/*     */ 
/*     */   
/*     */   public static void RaceAddChangeChannelChr(int charid) {
/*  71 */     for (MapleCharacter chrs : ChannelServer.getInstance(1).getPlayerStorage().getAllCharacters().values()) {
/*  72 */       if (charid == chrs.getId()) {
/*  73 */         Matchinglist.add(chrs);
/*  74 */         chrs.getClient().getSession().writeAndFlush(SLFCGPacket.ContentsWaiting(chrs, 993192500, new int[] { 11, 2, 1, 25 }));
/*     */         break;
/*     */       } 
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public static void RaceWarp() {
/*  82 */     ChannelServer.getInstance(1).getMapFactory().getMap(993192500).setCustomInfo(993192500, 0, 30000);
/*     */     
/*  84 */     for (MapleCharacter chr : Matchinglist) {
/*  85 */       for (Pair<MapleBuffStat, MapleBuffStatValueHolder> data : (Iterable<Pair<MapleBuffStat, MapleBuffStatValueHolder>>)chr.getEffects()) {
/*  86 */         MapleBuffStatValueHolder mbsvh = (MapleBuffStatValueHolder)data.right;
/*  87 */         if (SkillFactory.getSkill(mbsvh.effect.getSourceId()) != null && 
/*  88 */           mbsvh.effect.getSourceId() != 80002282 && mbsvh.effect.getSourceId() != 2321055) {
/*  89 */       //    chr.cancelEffect(mbsvh.effect, Arrays.asList(new MapleBuffStat[] { (MapleBuffStat)data.left }));
/*     */         }
/*     */       } 
/*     */       
/*  93 */       chr.warp(993192500);
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public static void ReadyRace() {
/*  99 */     ChannelServer ch = ChannelServer.getInstance(1);
/* 100 */     MapleMap map = ch.getMapFactory().getMap(993192500);
/* 101 */     MapleMap playmap = ch.getMapFactory().getMap(993192600);
/* 102 */     playmap.resetFully();
/* 103 */     playmap.setCustomInfo(993192600, 0, 30000);
/* 104 */     for (MapleCharacter chr : map.getAllChracater()) {
/* 105 */       Playerlist.add(chr);
/* 106 */       chr.warp(993192600);
/* 107 */       SpawnMonster(chr);
/*     */     } 
/* 109 */     playmap.broadcastMessage(SLFCGPacket.BloomingRaceHandler(2, new int[] { 30000 }));
/* 110 */     Timer.EventTimer.getInstance().schedule(() -> { 
              playmap.broadcastMessage(CWvsContext.serverNotice(5, "", "현재 맵에서는 잠재 능력과 에디셔널 잠재능력이 적용되지 않습니다."));
              playmap.broadcastMessage(CField.enforceMSG("<블루밍 레이스> 조금만 기다려줘. 곧 시작할 거야", 287, 3500)); 
              },2000L);
/*     */ 
/*     */ 
/*     */     
/* 114 */     Timer.EventTimer.getInstance().schedule(() -> playmap.broadcastMessage(CField.enforceMSG("블루밍 레이스에 온걸 환영해! 신나게 달릴 준비는 됐어?", 287, 3500)), 6000L);
/*     */ 
/*     */     
/* 117 */     Timer.EventTimer.getInstance().schedule(() -> playmap.broadcastMessage(CField.enforceMSG("친구들이 심심하지 않게 여러가지 코스로 준비했으니 재미있게 즐겨줘.", 287, 3500)), 10000L);
/*     */ 
/*     */     
/* 120 */     Timer.EventTimer.getInstance().schedule(() -> playmap.broadcastMessage(CField.enforceMSG("생각만큼 가는 길이 쉽지는 않을 거니까, 긴장하는 게 좋을 거야.", 287, 3500)), 14000L);
/*     */ 
/*     */     
/* 123 */     Timer.EventTimer.getInstance().schedule(() -> playmap.broadcastMessage(CField.enforceMSG("히힛, 하지만 친구들이라면 이 정도는 충분히 가능할 거라고 믿어.", 287, 3500)), 18000L);
/*     */ 
/*     */     
/* 126 */     Timer.EventTimer.getInstance().schedule(() -> playmap.broadcastMessage(CField.enforceMSG("다른 친구들보다 먼저 도착하면 더 좋은 선물을 줄게. 준비됐어?", 287, 3500)), 22000L);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void StartRace() {
/* 133 */     ChannelServer ch = ChannelServer.getInstance(1);
/* 134 */     MapleMap playmap = ch.getMapFactory().getMap(993192600);
/* 135 */     playmap.setCustomInfo(993192601, 0, 300000);
/* 136 */     playmap.setCustomInfo(993192602, 0, 4000);
/* 137 */     playmap.setCustomInfo(993192603, 0, 3500);
/* 138 */     playmap.setCustomInfo(993192604, 0, 3000);
/* 139 */     start = true;
/* 140 */     randPortal1 = Randomizer.rand(1, 3);
/* 141 */     randPortal2 = Randomizer.rand(1, 2);
/* 142 */     randPortal3 = Randomizer.rand(1, 3);
/* 143 */     playmap.broadcastMessage(CField.enforceMSG("시작! 포탈을 타고 레이스를 즐겨줘.", 287, 3500));
/* 144 */     playmap.broadcastMessage(SLFCGPacket.BloomingRaceHandler(4, new int[] { 300000 }));
/*     */   }
/*     */   
/*     */   public static void SpawnMonster(MapleCharacter chr) {
/* 148 */     ChannelServer ch = ChannelServer.getInstance(1);
/* 149 */     MapleMap playmap = ch.getMapFactory().getMap(993192600);
/*     */     
/* 151 */     MapleMonster monster = MapleLifeFactory.getMonster(9833959);
/* 152 */     monster.setOwner(chr.getId());
/* 153 */     playmap.spawnMonsterOnGroundBelow(monster, new Point(2452, 225));
/* 154 */     monster = MapleLifeFactory.getMonster(9833959);
/* 155 */     monster.setOwner(chr.getId());
/* 156 */     playmap.spawnMonsterOnGroundBelow(monster, new Point(2452, -41));
/* 157 */     monster = MapleLifeFactory.getMonster(9833959);
/* 158 */     monster.setOwner(chr.getId());
/* 159 */     playmap.spawnMonsterOnGroundBelow(monster, new Point(1980, -435));
/* 160 */     monster = MapleLifeFactory.getMonster(9833959);
/* 161 */     monster.setOwner(chr.getId());
/* 162 */     playmap.spawnMonsterOnGroundBelow(monster, new Point(2130, -435));
/* 163 */     monster = MapleLifeFactory.getMonster(9833959);
/* 164 */     monster.setOwner(chr.getId());
/* 165 */     playmap.spawnMonsterOnGroundBelow(monster, new Point(2280, -435));
/* 166 */     monster = MapleLifeFactory.getMonster(9833959);
/* 167 */     monster.setOwner(chr.getId());
/* 168 */     playmap.spawnMonsterOnGroundBelow(monster, new Point(2430, -435));
/* 169 */     monster = MapleLifeFactory.getMonster(9833959);
/* 170 */     monster.setOwner(chr.getId());
/* 171 */     playmap.spawnMonsterOnGroundBelow(monster, new Point(2580, -435));
/*     */   }
/*     */   
/*     */   public static int getRank() {
/* 175 */     return Rank;
/*     */   }
/*     */   
/*     */   public static void setRank(int Rank) {
/* 179 */     BloomingRace.Rank = Rank;
/*     */   }
/*     */   
/*     */   public static boolean isStart() {
/* 183 */     return start;
/*     */   }
/*     */   
/*     */   public static void setStart(boolean start) {
/* 187 */     BloomingRace.start = start;
/*     */   }
/*     */   
/*     */   public static int getRandPortal1() {
/* 191 */     return randPortal1;
/*     */   }
/*     */   
/*     */   public static void setRandPortal1(int randPortal1) {
/* 195 */     BloomingRace.randPortal1 = randPortal1;
/*     */   }
/*     */   
/*     */   public static int getRandPortal2() {
/* 199 */     return randPortal2;
/*     */   }
/*     */   
/*     */   public static void setRandPortal2(int randPortal2) {
/* 203 */     BloomingRace.randPortal2 = randPortal2;
/*     */   }
/*     */   
/*     */   public static int getRandPortal3() {
/* 207 */     return randPortal3;
/*     */   }
/*     */   
/*     */   public static void setRandPortal3(int randPortal3) {
/* 211 */     BloomingRace.randPortal3 = randPortal3;
/*     */   }
/*     */   
/*     */   public static List<MapleCharacter> getRankList() {
/* 215 */     return RankList;
/*     */   }
/*     */   
/*     */   public static void setRankList(List<MapleCharacter> RankList) {
/* 219 */     BloomingRace.RankList = RankList;
/*     */   }
/*     */   
/*     */   public static boolean isAlready() {
/* 223 */     return already;
/*     */   }
/*     */   
/*     */   public static void setAlready(boolean already) {
/* 227 */     BloomingRace.already = already;
/*     */   }
/*     */   
/*     */   public static List<MapleCharacter> getMatchinglist() {
/* 231 */     return Matchinglist;
/*     */   }
/*     */   
/*     */   public static List<MapleCharacter> getPlayerlist() {
/* 235 */     return Playerlist;
/*     */   }
/*     */ }