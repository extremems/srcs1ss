package server;

import client.DreamBreakerRank;
import client.skills.SkillFactory;
import client.inventory.MapleInventoryIdentifier;
import constants.Connector.KuronekoServer;
import constants.GameConstants;
import constants.ServerConstants;
import database.DatabaseConnection;
import handling.auction.AuctionServer;
import handling.cashshop.CashShopServer;
import handling.channel.ChannelServer;
import handling.channel.MapleGuildRanking;
import handling.channel.handler.MatrixHandler;
import handling.channel.handler.UnionHandler;
import handling.discord.DiscordSetting;
import handling.farm.FarmServer;
import handling.login.LoginInformationProvider;
import handling.login.LoginServer;
import handling.world.World;
import server.Timer.*;
import server.control.*;
import server.events.MapleOxQuizFactory;
import server.life.MapleLifeFactory;
import server.life.MobAttackInfoFactory;
import server.life.MobSkillFactory;
import server.life.PlayerNPC;
import server.marriage.MarriageManager;
import server.quest.MapleQuest;
import server.quest.QuestCompleteStatus;
import tools.CMDCommand;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import server.admin.AdminToolServer;
import server.field.boss.FieldSkillFactory;
import tools.ConnectorPanel;
import tools.ExpRatePanel;

public class Start {

    public static long startTime = System.currentTimeMillis();
    public static final Start instance = new Start();
    public static AtomicInteger CompletedLoadingThreads = new AtomicInteger(0);

    public void run() {
        //DiscordSetting discord = new DiscordSetting();
        DatabaseConnection.init();
        if (Boolean.parseBoolean(ServerProperties.getProperty("world.admin"))) {
            ServerConstants.Use_Fixed_IV = false;
            System.out.println("[!!! Admin Only Mode Active !!!]");
        }
        System.setProperty("wz", "wz");
        try {
            Connection con = DatabaseConnection.getConnection();
            final PreparedStatement ps = con.prepareStatement("UPDATE accounts SET loggedin = 0, allowed = 0, connecterClient = null");
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            throw new RuntimeException("[EXCEPTION] Please check if the SQL server is active.");
        }
        World.init();
        WorldTimer.getInstance().start();
        EtcTimer.getInstance().start();
        MapTimer.getInstance().start();
        MobTimer.getInstance().start();
        CloneTimer.getInstance().start();
        EventTimer.getInstance().start();
        BuffTimer.getInstance().start();
        PingTimer.getInstance().start();
        ShowTimer.getInstance().start();

        ServerConstants.WORLD_UI = ServerProperties.getProperty("login.serverUI");
        ServerConstants.ChangeMapUI = Boolean.parseBoolean(ServerProperties.getProperty("login.ChangeMapUI"));

        ServerConstants.quicks.add(new QuickMoveEntry(1, 1012000, 6, 10, "이동"));
        ServerConstants.quicks.add(new QuickMoveEntry(2, 9010022, 2, 10, "차원의 거울"));
        ServerConstants.quicks.add(new QuickMoveEntry(3, 1540215, 3, 10, "상점"));
        ServerConstants.quicks.add(new QuickMoveEntry(4, 9000232, 15, 10, "컨텐츠"));
        ServerConstants.quicks.add(new QuickMoveEntry(5, 1012124, 13, 10, "코디"));
        ServerConstants.quicks.add(new QuickMoveEntry(6, 2007, 20, 10, "경매장"));
        ServerConstants.quicks.add(new QuickMoveEntry(7, 1540059, 19, 10, "후원"));
        ServerConstants.quicks.add(new QuickMoveEntry(8, 1540100, 17, 10, "홍보"));
        ServerConstants.quicks.add(new QuickMoveEntry(9, 1540104, 8, 10, "호텔메이플"));
        ServerConstants.quicks.add(new QuickMoveEntry(10, 9000385, 7, 10, "퀘스트"));
        ServerConstants.quicks.add(new QuickMoveEntry(11, 9010108, 21, 10, "유니온"));

        ServerConstants.mirrors.add(new DimentionMirrorEntry("이름입니다", "분노한 자쿰.", 105, 1, 1, "3003105", new ArrayList<>()));
        ServerConstants.mirrors.add(new DimentionMirrorEntry("105는", "텐구.", 105, 2, 2, "3003105", new ArrayList<>()));
        ServerConstants.mirrors.add(new DimentionMirrorEntry("나머지는", "골럭스", 106, 3, 3, "3003105", new ArrayList<>()));
        ServerConstants.mirrors.add(new DimentionMirrorEntry("1씩채워서", "세계여행.", 107, 4, 4, "3003105", new ArrayList<>()));
        ServerConstants.mirrors.add(new DimentionMirrorEntry("1씩채워서", "무릉도장.", 108, 5, 5, "3003105", new ArrayList<>()));
        ServerConstants.mirrors.add(new DimentionMirrorEntry("1씩채워서", "몬스터 파크.", 109, 10, 10, "3003105", new ArrayList<>()));
        DreamBreakerRank.LoadRank();
        AllLoding allLoding = new AllLoding();
        allLoding.start();

        System.out.println("[Loading LOGIN]");
        LoginServer.run_startup_configurations();

        System.out.println("[Loading CHANNEL]");
        ChannelServer.startChannel_Main();

        System.out.println("[Loading CASH SHOP]");
        CashShopServer.run_startup_configurations();

        System.out.println("[Loading Farm]");
        FarmServer.run_startup_configurations();

        Runtime.getRuntime().addShutdownHook(new Thread(new Shutdown()));
        PlayerNPC.loadAll();// touch - so we see database problems early...
        LoginServer.setOn(); //now or later
        InnerAbillity.getInstance().load();
        AdminToolServer.run_startup_configurations();

        WorldTimer.getInstance().register(new MapleEtcControl(), 1000);
        WorldTimer.getInstance().register(new MapleMistControl(), 1000);
        WorldTimer.getInstance().register(new MapleSkillControl(), 1000);
        WorldTimer.getInstance().register(new MapleSummonControl(), 1000);
        WorldTimer.getInstance().register(new MapleMapControl(), 1000);
        WorldTimer.getInstance().register(new MapleRunOnceControl(), 10000);
        ConnectorPanel cp = new ConnectorPanel();
        cp.setVisible(true);
        //ExpRatePanel exp = new ExpRatePanel();
        // exp.setVisible(true);
        //DiscordServer.main();
        try {
            KuronekoServer.run_startup_configurations();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void abc() {
        try {
            Connection con = DatabaseConnection.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM keyvalue LEFT OUTER JOIN characters ON keyvalue.id = characters.id WHERE characters.id IS NULL");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PreparedStatement ps2 = con.prepareStatement("DELETE FROM `keyvalue` WHERE id = ?");
                ps2.setInt(1, rs.getInt("id"));
                ps2.executeUpdate();
                ps2.close();
            }
            ps.close();
            con.close();
        } catch (SQLException ex) {
            throw new RuntimeException("[EXCEPTION] Please check if the SQL server is active.");
        }
    }

    private class AllLoding extends Thread {

        @Override
        public void run() {
            LoadingThread SkillLoader = new LoadingThread(new Runnable() {
                public void run() {
                    // Skill
                    SkillFactory.load();
                }
            }, "SkillLoader", this);

            LoadingThread QuestLoader = new LoadingThread(new Runnable() {
                public void run() {
                    MapleQuest.initQuests();
                    MapleLifeFactory.loadQuestCounts();
                }
            }, "QuestLoader", this);

            LoadingThread QuestCustomLoader = new LoadingThread(new Runnable() {
                public void run() {
                    MapleLifeFactory.loadNpcScripts();
                    QuestCompleteStatus.run();
                }
            }, "QuestCustomLoader", this);

            // Item
            LoadingThread ItemLoader = new LoadingThread(new Runnable() {
                public void run() {
                    MapleInventoryIdentifier.getInstance();
                    CashItemFactory.getInstance().initialize();
                    MapleItemInformationProvider.getInstance().runEtc();
                    MapleItemInformationProvider.getInstance().runItems();
                    AuctionServer.run_startup_configurations();
                }
            }, "ItemLoader", this);

            LoadingThread GuildRankingLoader = new LoadingThread(new Runnable() {
                public void run() {
                    MapleGuildRanking.getInstance().load();
                }
            }, "GuildRankingLoader", this);

            // Etc
            LoadingThread EtcLoader = new LoadingThread(new Runnable() {
                public void run() {
                    LoginInformationProvider.getInstance();
                    RandomRewards.load();
                    MapleOxQuizFactory.getInstance();
                    MapleCarnivalFactory.getInstance();
                    SpeedRunner.loadSpeedRuns();
                    UnionHandler.loadUnion();
                }
            }, "EtcLoader", this);

            LoadingThread MonsterLoader = new LoadingThread(new Runnable() {
                public void run() {
                    MobSkillFactory.getInstance();
                   // FieldSkillFactory.getInstance();
                    MobAttackInfoFactory.getInstance();
                }
            }, "MonsterLoader", this);

            LoadingThread MatrixLoader = new LoadingThread(new Runnable() {
                public void run() {
                    MatrixHandler.loadCore();
                }
            }, "MatrixLoader", this);

            LoadingThread MarriageLoader = new Start.LoadingThread(new Runnable() {
                public void run() {
                    MarriageManager.getInstance();
                }
            }, "MarriageLoader", this);

            LoadingThread[] LoadingThreads = {SkillLoader, QuestLoader, QuestCustomLoader, ItemLoader, GuildRankingLoader, EtcLoader, MonsterLoader, MatrixLoader, MarriageLoader};

            for (Thread t : LoadingThreads) {
                t.start();
            }
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            while (CompletedLoadingThreads.get() != LoadingThreads.length) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

            World.Guild.load();
            GameConstants.isOpen = true;
            System.out.println("[Fully Initialized in " + ((System.currentTimeMillis() - startTime) / 1000) + " seconds]");
            CMDCommand.main();
        }
    }

    private static class LoadingThread extends Thread {

        protected String LoadingThreadName;

        private LoadingThread(Runnable r, String t, Object o) {
            super(new NotifyingRunnable(r, o, t));
            LoadingThreadName = t;
        }

        @Override
        public synchronized void start() {
            System.out.println("[Loading...] Started " + LoadingThreadName + " Thread");
            super.start();
        }
    }

    private static class NotifyingRunnable implements Runnable {

        private String LoadingThreadName;
        private long StartTime;
        private Runnable WrappedRunnable;
        private final Object ToNotify;

        private NotifyingRunnable(Runnable r, Object o, String name) {
            WrappedRunnable = r;
            ToNotify = o;
            LoadingThreadName = name;
        }

        public void run() {
            StartTime = System.currentTimeMillis();
            WrappedRunnable.run();
            System.out.println("[Loading Completed] " + LoadingThreadName + " | Completed in " + (System.currentTimeMillis() - StartTime) + " Milliseconds. (" + (CompletedLoadingThreads.get() + 1) + "/9)");
            synchronized (ToNotify) {
                CompletedLoadingThreads.incrementAndGet();
                ToNotify.notify();
            }
        }
    }

    public static class Shutdown implements Runnable {

        @Override
        public void run() {
            ShutdownServer.getInstance().run();
        }
    }

    public static void main(final String args[]) throws InterruptedException {
        instance.run();
    }
}
